alter table RECIPE_REVIEW add constraint fk_RECIPE_REVIEW_recipeReviewComment_47 foreign key (COMMENT_ID) references RECIPE_REVIEW_COMMENT (COMMENT_ID) on delete cascade on update restrict;

alter table RECIPE add constraint fk_RECIPE_cookForMe_32 foreign key (COOK_FOR_ME) references COOK_FOR_ME (COOK_FOR_ME_ID) on delete cascade on update restrict;

