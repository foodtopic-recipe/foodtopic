-- Taken from https://dba.stackexchange.com/questions/7147/find-highest-level-of-a-hierarchical-field-with-vs-without-ctes/7161#7161?newreg=0a22b260b34f4164b238173270e923bd 

CREATE FUNCTION `GetAncestryTree`(GivenID BIGINT) 
RETURNS varchar(1024) CHARSET utf8
    DETERMINISTIC
BEGIN
    DECLARE rv VARCHAR(1024);
    DECLARE cm CHAR(1);
    DECLARE ch INT;

    SET rv = '';
    SET cm = '';
    SET ch = GivenID;
    WHILE ch > 0 DO
        SELECT IFNULL(PREVIOUS_RECIPE_ID,-1) INTO ch FROM
        (SELECT PREVIOUS_RECIPE_ID FROM RECIPE WHERE RECIPE_ID = ch) A;
        IF ch > 0 THEN
            SET rv = CONCAT(rv,cm,ch);
            SET cm = ',';
        END IF;
    END WHILE;
    RETURN rv;
END