import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.PlayAuthenticate.Resolver;
import com.feth.play.module.pa.exceptions.AccessDeniedException;
import com.feth.play.module.pa.exceptions.AuthException;
import models.User;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Play;
import play.api.mvc.EssentialFilter;
import play.api.mvc.Handler;
import play.i18n.Messages;
import play.libs.F.Promise;
import play.mvc.Call;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import views.html.error.error;

import static play.mvc.Results.badRequest;
import static play.mvc.Results.notFound;

public class Global extends GlobalSettings {
    
    // 404 - page not found error
    public Promise<Result> onHandlerNotFound(RequestHeader request) {
        User user = controllers.Application.getSessionUser();
        return Promise.<Result>pure(notFound(error.render(user, Messages.get("error.404"), Messages.get("error.404.desc"))));
    }
    
    public Promise<Result> onBadRequest(RequestHeader request, String errorLine) {
        User user = controllers.Application.getSessionUser();
        return Promise.<Result>pure(badRequest(error.render(user, Messages.get("error.404"), Messages.get("error.404.desc"))));
    }

	@Override
	public Handler onRouteRequest(RequestHeader request) {
		if (Play.isProd() && request.host().startsWith("www"))
			return controllers.Default.redirect("http://" + request.host().replace("www.", "") + request.uri());
		return super.onRouteRequest(request);
	}
    
    public void onStart(final Application app) {
        PlayAuthenticate.setResolver(new Resolver() {
            
			@Override
			public Call login() {
				// Your login page
				return controllers.routes.Application.index();
			}
            
			@Override
			public Call afterAuth() {
				// The user will be redirected to this page after authentication
				// if no original URL was saved
				return controllers.routes.Application.index();
			}
            
			@Override
			public Call afterLogout() {
				return controllers.routes.Application.index();
			}
            
			@Override
			public Call auth(final String provider) {
				// You can provide your own authentication implementation,
				// however the default should be sufficient for most cases
				return com.feth.play.module.pa.controllers.routes.Authenticate.authenticate(provider);
			}
            
			@Override
			public Call onException(final AuthException e) {
				if (e instanceof AccessDeniedException) {
					return controllers.routes.Application.oAuthDenied(((AccessDeniedException) e).getProviderKey());
				}
                
				// more custom problem handling here...
                
				return super.onException(e);
			}
            
			@Override
			public Call askLink() {
				// We don't support moderated account linking in this sample.
				// See the play-authenticate-usage project for an example
				return null;
			}
            
			@Override
			public Call askMerge() {
				// We don't support moderated account merging in this sample.
				// See the play-authenticate-usage project for an example
				return null;
			}
		});
	}
}