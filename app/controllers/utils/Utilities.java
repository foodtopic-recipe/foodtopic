package controllers.utils;

import models.SubsetCode;
import models.Token;
import models.Token.Type;
import org.apache.commons.lang3.StringUtils;
import play.api.i18n.Lang;
import play.mvc.Controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class Utilities {
	
	public static Token tokenIsValid(final String token, final Type type) {
		Token ret = null;
		if (token != null && !token.trim().isEmpty()) {
			final Token ta = Token.findByToken(token, type);
			if (ta != null && ta.isValid()) {
				ret = ta;
			}
		}
		return ret;
	}
	
	public static String[] spiltStringByComma(String input) {
		if(StringUtils.isEmpty(input))
			return new String[] {};
		
		String[] output = input.contains(",") ? input.split(",") : new String[] {input};
		return output;
	}

	public static ArrayList<SubsetCode> spiltStringByCommaToSubsetCode(String input) {
		ArrayList<SubsetCode> subsetList = new ArrayList<SubsetCode>();

		if(StringUtils.isEmpty(input))
			return subsetList;

		String[] output = input.contains(",") ? input.split(",") : new String[] {input};

		for (String n : output) {
			SubsetCode s = SubsetCode.find.byId(Long.parseLong(n));
			if(s != null)
				subsetList.add(s);
		}

		return subsetList;
	}

	public static String spiltStringByCommaToSubsetCodeString(String input) {
		if(StringUtils.isEmpty(input))
			return "";

		StringBuilder sb = new StringBuilder();
		for (SubsetCode n : spiltStringByCommaToSubsetCode(input)) {
			if (sb.length() > 0) sb.append(',');
			if(n != null)
				sb.append(n.subsetCodeValue);
		}

		return sb.toString();
	}
	
	public static String convertStringArrayToCommaString(String[] input) {
        StringBuilder sb = new StringBuilder();
        for (String n : input) { 
            if (sb.length() > 0) sb.append(',');
            //sb.append("'").append(n).append("'"); //append '' between text
            sb.append(n);
        }
        return sb.toString();
	}

	public static String convertStringListToCommaString(Collection<String> input) {
		StringBuilder sb = new StringBuilder();
		for (String n : input) {
			if (sb.length() > 0) sb.append(',');
			if (n != null && !n.equalsIgnoreCase("null")) sb.append(n);
		}
		return sb.toString();
	}
	
	public static String checkEnglishPlural(int count, String input) {
		StringBuilder sb = new StringBuilder(input);
		Lang currentLang = Controller.lang();
		if (currentLang.language().equals("en") && count > 1) {
			sb.append("s");
		}
		return sb.toString();
	}

	public static String withSuffix(long count) {
		if (count < 1000) return "" + count;
		int exp = (int) (Math.log(count) / Math.log(1000));
		String result = String.format("%.1f %c", count / Math.pow(1000, exp), "kMGTPE".charAt(exp-1));
		if(result.substring(result.length() - 1).equals(".0"))
			return result.substring(result.length()-1);
		return result;
	}

	public static String getTimeInterval(Date date1, Date date2) {
		SimpleDateFormat month_sdf = new SimpleDateFormat("MMMM dd");
		SimpleDateFormat year_sdf = new SimpleDateFormat("MMMM dd, yyyy");
		try {
			long diff = date1.getTime() - date2.getTime();
 
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);

			if(diffDays >= 1) {
				if(diffDays == 1)
					return diffDays + " day";
				else if(diffDays > 365)
					return year_sdf.format(date2);
				else
					return month_sdf.format(date2);
			} else if(diffHours > 0 && diffHours < 24 && diffDays < 1) {
				if(diffHours == 1)
					return diffHours + " hr";
				else
					return diffHours + " hrs";
			} else if(diffMinutes > 0 && diffMinutes < 60 && diffHours < 1) {
				if(diffMinutes == 1)
					return diffMinutes + " min";
				else
					return diffMinutes + " mins";
			} else if(diffSeconds < 60 && diffMinutes < 1 && diffHours < 24) {
				if(diffSeconds == 1)
					return diffSeconds + " sec";
				else 
					return diffSeconds + " secs";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

    public static Date getPrevDateByMins(int minsBefore) {
        long MINS_IN_MS = 1000 * 60;
        return new Date(new Date().getTime() - (minsBefore * MINS_IN_MS));
    }
	
    public static Date getPrevDateByHours(int hoursBefore) {
        long HOURS_IN_MS = 1000 * 60 * 60;
        return new Date(new Date().getTime() - (hoursBefore * HOURS_IN_MS));
    }
    
    public static Date getPrevDateByDays(int daysBefore) {
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        return new Date(new Date().getTime() - (daysBefore * DAY_IN_MS));
    }
    
	public static int returnSmallerNumber(int num1, int num2) {
		if(num1 > num2)
			return num2;
		
		return num1;
	}

	public static String returnLimitedKeywords(String keywords, int limit) {
		String keywordList = "";
		String[] keywordsArray = spiltStringByComma(keywords);
		for(int i=0; i<keywordsArray.length; i++) {
			if(i+1 >= limit || i+1 >= keywordsArray.length) {
				keywordList = keywordList + keywordsArray[i];
				break;
			}
			keywordList = keywordList + keywordsArray[i] + ",";
		}

		return keywordList;
	}

	public static String convertPreparationTimeToSchema(int preparationTime) {
		String schemaPreparationTime = "P";
		int preparationHours = preparationTime/60;
		int remainingTime = preparationTime - (preparationHours * 60);
		if(preparationHours > 0) {
			schemaPreparationTime = schemaPreparationTime + preparationHours + "H";
		}
		if(remainingTime > 0) {
			schemaPreparationTime = schemaPreparationTime + remainingTime + "M";
		}

		return schemaPreparationTime;
	}
	/*
	Levenshtein distance is a string metric for measuring the difference between two sequences.
	Informally, the Levenshtein distance between two words is the minimum number of single-character
	edits (i.e. insertions, deletions or substitutions) required to change one word into the other.
	It is named after Vladimir Levenshtein, who considered this distance in 1965.
	
	Example: 
    	Levenshtein distance between "kitten" and "sitting" is 3,
    	since the following three edits change one into the other,
    	and there is no way to do it with fewer than three edits:
            
            kitten → sitten (substitution of "s" for "k")
            sitten → sittin (substitution of "i" for "e")
            sittin → sitting (insertion of "g" at the end).
	*/
	public static int LevenshteinDistance(String s0, String s1) {
        int len0 = s0.length() + 1;
        int len1 = s1.length() + 1;
     
        // the array of distances                                                       
        int[] cost = new int[len0];                                                     
        int[] newcost = new int[len0];                                                  
     
        // initial cost of skipping prefix in String s0                                 
        for (int i = 0; i < len0; i++) cost[i] = i;                                     
     
        // dynamically computing the array of distances                                  
     
        // transformation cost for each letter in s1                                    
        for (int j = 1; j < len1; j++) {                                                
            // initial cost of skipping prefix in String s1                             
            newcost[0] = j;                                                             
     
            // transformation cost for each letter in s0                                
            for(int i = 1; i < len0; i++) {                                             
                // matching current letters in both strings                             
                int match = (s0.charAt(i - 1) == s1.charAt(j - 1)) ? 0 : 1;             
     
                // computing cost for each transformation                               
                int cost_replace = cost[i - 1] + match;                                 
                int cost_insert  = cost[i] + 1;                                         
                int cost_delete  = newcost[i - 1] + 1;                                  
     
                // keep minimum cost                                                    
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }                                                                           
     
            // swap cost/newcost arrays                                                 
            int[] swap = cost; cost = newcost; newcost = swap;                          
        }                                                                               
     
        // the distance is the cost for transforming all letters in both strings        
        return cost[len0 - 1];                                                          
    }
    
    //Convert the LevenshteinDistance into ratio to better determine
	public static int LevenshteinDistanceRatio(String s0, String s1) {
        int maxLength = 0;
        if(s0.length() > s1.length())
            maxLength = s0.length();
        else 
            maxLength = s0.length();
        
        return Math.round((1 - LevenshteinDistance(s0, s1) / maxLength) * 100);
	}
	
	public static boolean isExternalImage(String path) {
		if (path == null) {
			return false;
		}
		return path.contains("facebook") || path.contains("google") || path.contains(".com");
	}
}
