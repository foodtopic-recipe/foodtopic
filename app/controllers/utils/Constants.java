package controllers.utils;

import models.SubsetCode;

import java.text.SimpleDateFormat;

public class Constants {

	// Constants used in models & controllers - START
	//Integer.getInteger(Play.application().configuration().getString("profileImageUploadPath"));
	public static final int MAX_PHOTO_FILE_SIZE = 500000;
	public static final int MAX_RECIPE_DIRECTIONS_LIMIT = 20;

	public static final int FETCH_RECIPE_PAGE_ONE = 0;
	public static final int MAX_RECIPE_FETCH_LIMIT = 16;
	public static final int MAX_SIMILAR_RECIPE_FETCH_LIMIT = 4;
	public static final int MAX_FEATURED_RECIPE_LIMIT = 1;

	public static final int MAX_DASHBOARD_LIMIT = 5;
	public static final int MAX_NOTIFICATION_FETCH_LIMIT = 10;
	public static final int MAX_NOTIFICATION_DROPDOWN_FETCH_LIMIT = 5;

	public static final int FETCH_USER_PAGE_ONE = 0;
	public static final int MAX_USER_FETCH_LIMIT = 8;

	public static final int FETCH_RECIPE_BOOK_PAGE_ONE = 0;
	public static final int MAX_RECIPE_BOOK_FETCH_LIMIT = 16;

	public static final int FETCH_REVIEW_PAGE_ONE = 0;
	public static final int MAX_REVIEW_FETCH_LIMIT = 5;

	public static final int FETCH_COOK_FOR_ME_ORDER_PAGE_ONE = 0;
	public static final int MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT = 5;

	public static final int MIN_ORDER_BOOKING_DATE_IN_ADVANCE = 3;
	public static final int MIN_ORDER_AMEND_DATE_IN_ADVANCE = 0;
	public static final int MIN_ORDER_BOOKING_HOUR_LIMIT = 1;

	public static final int cookForMeOrderByAnyone = 0;
	public static final int cookForMeOrderByMe = 1;
	public static final int cookForMeOrderByOthers = 2;


	public static final boolean ACTIVE = true;
	public static final boolean INACTIVE = false;

	public static final boolean NOTIFICATION_STATUS_READ = true;
	public static final boolean NOTIFICATION_STATUS_UNREAD = false;

	public static final boolean LIKE = true;
	public static final boolean UNLIKE = false;

	public static final boolean FAVOURITE = true;
	public static final boolean UNFAVOURITE = false;

	public static final boolean REVIEWED = true;
	public static final boolean UNREVIEWED = false;

	public static final boolean FOLLOW = true;
	public static final boolean UNFOLLOW = false;

	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	public static final SimpleDateFormat notificationDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm a");
	public static final SimpleDateFormat sameHourDateFormat = new SimpleDateFormat("dd/MM/yyyy HH");

	// Constants used in models & controllers - END


	// Code ID - START
	public static final int ACCOUNT_STATUS = 1;
	public static final int COVER_PHOTO_LIMIT = 2;
	public static final int MONETIZATION_STATUS = 3;
	public static final int PUBLISH_STATUS = 4;
	public static final int PRIVACY_STATUS = 5;
	public static final int GENDER = 6;
	public static final int RECIPE_DIRECTIONS_LIMIT = 7;
	public static final int NOTIFICATION_TYPE = 8;
	public static final int COMMENT_TYPE = 9;
	public static final int DIFFICULTY_TYPE = 10;
	public static final int REPUTATION_POINTS = 11;
	public static final int ORDER_STATUS = 12;
	public static final int COLLECTION_MODE = 13;
	public static final int PAYMENT_TYPE = 14;
	// Code ID - END


	// Subset Code ID - START
	public static final int ACCOUNT_STATUS_ACTIVE = 1;
	public static final int ACCOUNT_STATUS_INACTIVE = 2;
	public static final int ACCOUNT_STATUS_PENDING = 3;
	public static final SubsetCode SUBSETCODE_ACCOUNT_STATUS_ACTIVE = SubsetCode.getSubsetCodeById(ACCOUNT_STATUS_ACTIVE);
	public static final SubsetCode SUBSETCODE_ACCOUNT_STATUS_INACTIVE = SubsetCode.getSubsetCodeById(ACCOUNT_STATUS_INACTIVE);
	public static final SubsetCode SUBSETCODE_ACCOUNT_STATUS_PENDING = SubsetCode.getSubsetCodeById(ACCOUNT_STATUS_PENDING);

	public static final int MAXIMUM_NUMBER_OF_RECIPE_COVER_PHOTOS = 4;
	public static final SubsetCode SUBSETCODE_MAXIMUM_NUMBER_OF_RECIPE_COVER_PHOTOS = SubsetCode.getSubsetCodeById(MAXIMUM_NUMBER_OF_RECIPE_COVER_PHOTOS);

	public static final int MONETIZATION_STATUS_FREE = 5;
	public static final int MONETIZATION_STATUS_PAID = 6;
	public static final int MONETIZATION_STATUS_BUNDLED = 7;
	public static final SubsetCode SUBSETCODE_MONETIZATION_STATUS_BUNDLED = SubsetCode.getSubsetCodeById(MONETIZATION_STATUS_BUNDLED);
	public static final SubsetCode SUBSETCODE_MONETIZATION_STATUS_PAID = SubsetCode.getSubsetCodeById(MONETIZATION_STATUS_PAID);
	public static final SubsetCode SUBSETCODE_MONETIZATION_STATUS_FREE = SubsetCode.getSubsetCodeById(MONETIZATION_STATUS_FREE);

	public static final int PUBLISH_STATUS_PUBLISHED = 8;
	public static final int PUBLISH_STATUS_DRAFT = 9;
	public static final int PUBLISH_STATUS_DELETED = 10;
	public static final int PUBLISH_STATUS_HIDDEN = 11;
	public static final SubsetCode SUBSETCODE_PUBLISH_STATUS_PUBLISHED = SubsetCode.getSubsetCodeById(PUBLISH_STATUS_PUBLISHED);
	public static final SubsetCode SUBSETCODE_PUBLISH_STATUS_DELETED = SubsetCode.getSubsetCodeById(PUBLISH_STATUS_DELETED);
	public static final SubsetCode SUBSETCODE_PUBLISH_STATUS_HIDDEN = SubsetCode.getSubsetCodeById(PUBLISH_STATUS_HIDDEN);
	public static final SubsetCode SUBSETCODE_PUBLISH_STATUS_DRAFT = SubsetCode.getSubsetCodeById(PUBLISH_STATUS_DRAFT);

	public static final int PRIVACY_STATUS_PRIVATE = 12;
	public static final int PRIVACY_STATUS_PUBLIC = 13;
	public static final SubsetCode SUBSETCODE_PRIVACY_STATUS_PRIVATE = SubsetCode.getSubsetCodeById(PRIVACY_STATUS_PRIVATE);
	public static final SubsetCode SUBSETCODE_PRIVACY_STATUS_PUBLIC = SubsetCode.getSubsetCodeById(PRIVACY_STATUS_PUBLIC);

	public static final int GENDER_MALE = 14;
	public static final int GENDER_FEMALE = 15;
	public static final SubsetCode SUBSETCODE_GENDER_MALE = SubsetCode.getSubsetCodeById(GENDER_MALE);
	public static final SubsetCode SUBSETCODE_GENDER_FEMALE = SubsetCode.getSubsetCodeById(GENDER_FEMALE);

	public static final int MAXIMUM_NUMBER_OF_RECIPE_DIRECTIONS = 16;
	public static final SubsetCode SUBSETCODE_MAXIMUM_NUMBER_OF_RECIPE_DIRECTIONS = SubsetCode.getSubsetCodeById(MAXIMUM_NUMBER_OF_RECIPE_DIRECTIONS);

	public static final int NOTIFICATION_TYPE_FAVOURITES = 17;
	public static final int NOTIFICATION_TYPE_COMMENT = 18;
	public static final int NOTIFICATION_TYPE_LIKED = 19;
	public static final int NOTIFICATION_TYPE_FOLLOWED = 20;
	public static final int NOTIFICATION_TYPE_NEWITEM = 21;
	public static final int NOTIFICATION_TYPE_REVIEW = 22;
	public static final int NOTIFICATION_TYPE_ORDER = 23;
	public static final SubsetCode SUBSETCODE_NOTIFICATION_TYPE_FAVOURITES = SubsetCode.getSubsetCodeById(NOTIFICATION_TYPE_FAVOURITES);
	public static final SubsetCode SUBSETCODE_NOTIFICATION_TYPE_FOLLOWED = SubsetCode.getSubsetCodeById(NOTIFICATION_TYPE_FOLLOWED);
	public static final SubsetCode SUBSETCODE_NOTIFICATION_TYPE_COMMENT = SubsetCode.getSubsetCodeById(NOTIFICATION_TYPE_COMMENT);
	public static final SubsetCode SUBSETCODE_NOTIFICATION_TYPE_NEWITEM = SubsetCode.getSubsetCodeById(NOTIFICATION_TYPE_NEWITEM);
	public static final SubsetCode SUBSETCODE_NOTIFICATION_TYPE_REVIEW = SubsetCode.getSubsetCodeById(NOTIFICATION_TYPE_REVIEW);
	public static final SubsetCode SUBSETCODE_NOTIFICATION_TYPE_LIKED = SubsetCode.getSubsetCodeById(NOTIFICATION_TYPE_LIKED);
	public static final SubsetCode SUBSETCODE_NOTIFICATION_TYPE_ORDER = SubsetCode.getSubsetCodeById(NOTIFICATION_TYPE_ORDER);

	public static final int COMMENT_TYPE_REVIEW = 24;
	public static final int COMMENT_TYPE_COMMENT = 25;
	public static final SubsetCode SUBSETCODE_COMMENT_TYPE_REVIEW = SubsetCode.getSubsetCodeById(COMMENT_TYPE_REVIEW);
	public static final SubsetCode SUBSETCODE_COMMENT_TYPE_QUESTION = SubsetCode.getSubsetCodeById(COMMENT_TYPE_COMMENT);

	public static final int DIFFICULTY_TYPE_EASY = 26;
	public static final int DIFFICULTY_TYPE_MODERATE = 27;
	public static final int DIFFICULTY_TYPE_HARD = 28;
	public static final SubsetCode SUBSETCODE_DIFFICULTY_TYPE_EASY = SubsetCode.getSubsetCodeById(DIFFICULTY_TYPE_EASY);
	public static final SubsetCode SUBSETCODE_DIFFICULTY_TYPE_MODERATE = SubsetCode.getSubsetCodeById(DIFFICULTY_TYPE_MODERATE);
	public static final SubsetCode SUBSETCODE_DIFFICULTY_TYPE_HARD = SubsetCode.getSubsetCodeById(DIFFICULTY_TYPE_HARD);

	public static final int REPUTATION_POINTS_NEW_RECIPE = 29;
	public static final int REPUTATION_POINTS_REINVENT_RECIPE = 30;
	public static final int REPUTATION_POINTS_NEW_REVIEW_1TO3_STARS = 31;
	public static final int REPUTATION_POINTS_NEW_REVIEW_4TO5_STARS = 32;
	public static final int REPUTATION_POINTS_NEW_LIKE= 33;
	public static final int REPUTATION_POINTS_NEW_LIKE_CAP = 34;
	public static final int REPUTATION_POINTS_NEW_FAVOURITE = 35;
	public static final int REPUTATION_POINTS_NEW_FAVOURITE_CAP = 36;
	public static final int REPUTATION_POINTS_MULTIPLICATION_VALUE = 37;
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_NEW_RECIPE = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_NEW_RECIPE);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_REINVENT_RECIPE = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_REINVENT_RECIPE);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_NEW_REVIEW_1TO3_STARS = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_NEW_REVIEW_1TO3_STARS);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_NEW_REVIEW_4TO5_STARS = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_NEW_REVIEW_4TO5_STARS);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_NEW_LIKE = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_NEW_LIKE);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_NEW_LIKE_CAP = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_NEW_LIKE_CAP);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_NEW_FAVOURITE = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_NEW_FAVOURITE);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_NEW_FAVOURITE_CAP = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_NEW_FAVOURITE_CAP);
	public static final SubsetCode SUBSETCODE_REPUTATION_POINTS_MULTIPLICATION_VALUE = SubsetCode.getSubsetCodeById(REPUTATION_POINTS_MULTIPLICATION_VALUE);

	public static final int ORDER_STATUS_NEW = 38;
	public static final int ORDER_STATUS_ACCEPTED = 39;
	public static final int ORDER_STATUS_REJECTED = 40;
	public static final int ORDER_STATUS_CANCELLED = 41;
	public static final int ORDER_STATUS_COMPLETED = 42;
	public static final SubsetCode SUBSETCODE_ORDER_STATUS_NEW = SubsetCode.getSubsetCodeById(ORDER_STATUS_NEW);
	public static final SubsetCode SUBSETCODE_ORDER_STATUS_ACCEPTED = SubsetCode.getSubsetCodeById(ORDER_STATUS_ACCEPTED);
	public static final SubsetCode SUBSETCODE_ORDER_STATUS_REJECTED = SubsetCode.getSubsetCodeById(ORDER_STATUS_REJECTED);
	public static final SubsetCode SUBSETCODE_ORDER_STATUS_CANCELLED = SubsetCode.getSubsetCodeById(ORDER_STATUS_CANCELLED);
	public static final SubsetCode SUBSETCODE_ORDER_STATUS_COMPLETED = SubsetCode.getSubsetCodeById(ORDER_STATUS_COMPLETED);

	public static final int COLLECTION_MODE_DELIVERY = 43;
	public static final int COLLECTION_MODE_SELF_COLLECTION = 44;
	public static final int COLLECTION_MODE_DINE_IN = 45;
	public static final SubsetCode SUBSETCODE_COLLECTION_MODE_DELIVERY = SubsetCode.getSubsetCodeById(COLLECTION_MODE_DELIVERY);
	public static final SubsetCode SUBSETCODE_COLLECTION_MODE_SELF_COLLECTION = SubsetCode.getSubsetCodeById(COLLECTION_MODE_SELF_COLLECTION);
	public static final SubsetCode SUBSETCODE_COLLECTION_MODE_DINE_IN = SubsetCode.getSubsetCodeById(COLLECTION_MODE_DINE_IN);

	public static final int PAYMENT_TYPE_CASH = 46;
	public static final SubsetCode SUBSETCODE_PAYMENT_TYPE_CASH = SubsetCode.getSubsetCodeById(PAYMENT_TYPE_CASH);

	// Subset Code ID - END
}