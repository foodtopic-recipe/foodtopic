package controllers.recipe;

import com.avaje.ebean.Ebean;
import controllers.Application;
import controllers.Secured;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import play.mvc.Security;
import providers.FTPasswordAuthProvider;
import scala.reflect.io.FileOperationException;
import views.html.account.registration.unverified;
import views.html.error.error;
import views.html.recipe.reinventRecipe;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class ReinventRecipe extends Controller {
	
	public int monetizationCode;
    //public double price;
	public List<Long> recipeBookList;
	public Integer privacyCode;
	public Integer statusCode;
	public String recipeName;
	public String recipeDescription;
    public Integer servings;
	public Integer preparationTime;
    public Integer difficulty;
	public List<RecipeIngredient> recipeIngredientList;
	public List<RecipeDirection> recipeDirectionList;
	public List<Integer> categoryList;
	public String tags;
	
    /**
     * Defines a form wrapping the ReinventRecipe class.
     */ 
    final static Form<ReinventRecipe> reinventRecipeForm = form(ReinventRecipe.class);
    
    
    /**
     * Display the reinvent recipe page if login
     *
     * @return reinvent recipe page
     */
    public static Result initReinventRecipeForm(long recipeId) {
        final User user = User.findByEmail(request().username());
        Recipe recipe = Recipe.find.byId(recipeId);
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(recipe == null) {
            return badRequest(Messages.get("recipe.not.found"));
        }
        
        if (!user.emailValidated) {
            if(user.validationEmailSent == null || Utilities.getPrevDateByMins(15).after(user.validationEmailSent)) {
                FTPasswordAuthProvider.getProvider().sendVerifyEmailMailingAfterSignup(user, ctx());
                return ok(unverified.render(user, false, false));
            } else {
                return ok(unverified.render(user, false, true));
            }
        } else if(recipe != null && !recipe.user.equals(user)) {
            // Allow reinvention of recipe only when the original recipe is public & published
            if (recipe.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PUBLIC && recipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_PUBLISHED)
                return ok(reinventRecipe.render(user, recipe, reinventRecipeForm.fill(initReinventForm(recipe))));
            else
                return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipe.reinvent.invalid.rights")));
        } else 
            return notFound(error.render(user, Messages.get("invalid.reinvent"), Messages.get("recipe.reinvent.duplicate.user")));
    }
	
	
    public static ReinventRecipe initReinventForm(Recipe recipe) {
        ReinventRecipe value = new ReinventRecipe();
        
        value.monetizationCode = recipe.monetizationCode.subsetCodeId;
        //value.price = recipe.price;
        value.privacyCode = recipe.privacyCode.subsetCodeId;
        value.statusCode = recipe.statusCode.subsetCodeId;
        value.recipeName = recipe.recipeName;
        value.recipeDescription = recipe.recipeDescription;
        value.difficulty = recipe.difficultyCode != null ? recipe.difficultyCode.subsetCodeId : Constants.DIFFICULTY_TYPE_MODERATE;
        value.servings = recipe.servings;
        value.preparationTime = recipe.preparationTime;
        
        value.recipeIngredientList = recipe.recipeIngredientList;
        value.recipeDirectionList = recipe.recipeDirectionList;
        // This is to clear all images associate with the direction,
        // hoping to prompt user to upload their reinvented direction's image
        for(RecipeDirection direction : value.recipeDirectionList) {
            direction.imagePath = null;
        }
        
        value.recipeBookList = new ArrayList<Long>();
        for(BookRecipe recipesInRecipeBook : recipe.bookRecipeList) {
            value.recipeBookList.add(recipesInRecipeBook.recipeBook.recipeBookId);
        }

		value.categoryList = new ArrayList<Integer>();
        recipe.recipeCategoryList = RecipeCategory.findByRecipeId(recipe.recipeId);
        for(int i=0; i<recipe.recipeCategoryList.size(); i++) {
            if(!StringUtils.isBlank(recipe.recipeCategoryList.get(i).category.categoryName)) {
            	Integer categoryId = recipe.recipeCategoryList.get(i).category.categoryId;
                value.categoryList.add(categoryId);
            }
        }

        recipe.recipeTagList = RecipeTag.findByRecipeId(recipe.recipeId);
        String[] tagSpiltList = new String[recipe.recipeTagList.size()];
        for(int i=0; i<recipe.recipeTagList.size(); i++) {
            if(recipe.recipeTagList.get(i).tag != null && StringUtils.isNotBlank(recipe.recipeTagList.get(i).tag.tagName)) {
                tagSpiltList[i] = recipe.recipeTagList.get(i).tag.tagName;
            }
        }
        if(tagSpiltList.length > 0)
            value.tags = Utilities.convertStringArrayToCommaString(tagSpiltList);
        
        return value;
    }
    
	/**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise
	 */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
        MultipartFormData formData = request().body().asMultipartFormData();
		String[] postAction = formData.asFormUrlEncoded().get("action");
		
		if (postAction == null || postAction.length == 0) {
            Logger.debug("No post action");
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
		} else {
			String action = postAction[0];
			
			if (Messages.get("publish").equals(action) || Messages.get("update").equals(action)) {
                
				List<FilePart> imagesList = formData.getFiles();
                int totalDirectionsPhotos = 0;
                
                int directionsLimit = SubsetCode.getNumberOfDirectionsLimit();
				if(recipeDirectionList.size() > directionsLimit) {
                    errors.add(new ValidationError("recipeDirectionList", Messages.get("recipe.directions.exceed").replace("*LIMIT*", "" + directionsLimit)));
                }
                for (int i=0; i<recipeDirectionList.size(); i++) {
                    FilePart directionPhoto = formData.getFile("recipeDirectionList[" + i + "].imagePath");        		    
                    if (directionPhoto != null) {
                        totalDirectionsPhotos++;
                    }
                }
                int totalCoverPhotos = imagesList.size() - totalDirectionsPhotos;
                
				// Limit number of cover photos to be uploaded to 'x' value controlled by Code Table
                int coverPhotosLimit = SubsetCode.getNumberOfCoverPhotosLimit();
				if (totalCoverPhotos > coverPhotosLimit) {
                    errors.add(new ValidationError("coverPhoto", Messages.get("recipe.coverPhoto.exceed") + " " + coverPhotosLimit));
				}				
                
				/*
				if(monetizationCode != Constants.MONETIZATION_STATUS_PAID) {
					price = 0;
				}
				*/
                if (privacyCode <= 0 || privacyCode == null) {
                    errors.add(new ValidationError("privacyCode", Messages.get("validate.required", Messages.get("recipe.privacyCode"))));
                } else if (SubsetCode.validateSubsetCode(Constants.PRIVACY_STATUS, privacyCode) == null) {
                    errors.add(new ValidationError("privacyCode", Messages.get("invalid.code", Messages.get("recipe.privacyCode"))));
                }

                if (StringUtils.isBlank(recipeName)) {
                    errors.add(new ValidationError("recipeName", Messages.get("validate.required", Messages.get("recipe.name"))));
                }

                if (difficulty == null || difficulty <= 0) {
                    errors.add(new ValidationError("difficulty", Messages.get("recipe.difficulty.required")));
                } else if (SubsetCode.validateSubsetCode(Constants.DIFFICULTY_TYPE, difficulty) == null) {
                    errors.add(new ValidationError("difficulty", Messages.get("invalid.code", Messages.get("recipe.difficulty"))));
                }
                
                try { 
                    if (servings <= 0 || servings == null) {
                        errors.add(new ValidationError("servings", Messages.get("recipe.servings.required")));
                    }
                } catch (NumberFormatException e) {
                    errors.add(new ValidationError("servings", Messages.get("validate.not.numeric")));
                }
                
                try {
                    if (preparationTime <= 0 || preparationTime == null) {
                        errors.add(new ValidationError("preparationTime", Messages.get("recipe.preparationTime.required")));
                    }   
                } catch (NumberFormatException e) {
                    errors.add(new ValidationError("preparationTime", Messages.get("validate.not.numeric")));
                }
                
                for(int i=0; i<recipeIngredientList.size(); i++) {
                    RecipeIngredient ri = recipeIngredientList.get(i);
                    if (StringUtils.isBlank(ri.ingredientName)) {
                        errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientName", Messages.get("recipe.ingredient.required")));
                    }
                    if (StringUtils.isBlank(ri.ingredientQuantity)) {
                        errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientQuantity", Messages.get("recipe.quantity.required")));
                    }
                    /*
                    if (StringUtils.isBlank(ri.ingredientUnit)) {
                        errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientUnit", Messages.get("recipe.unit.required")));
                    }
                    */
                }
                
                for(int i=0; i<recipeDirectionList.size(); i++) {
					RecipeDirection rd = recipeDirectionList.get(i);
					if (StringUtils.isBlank(rd.directionDescription)) {
                        errors.add(new ValidationError("recipeDirectionList["+i+"].directionDescription", Messages.get("recipe.direction.required")));
                    }
                }
				
                if(!StringUtils.isBlank(tags) && !tags.matches("^[\\w,?\\s?]*$")) {
                    errors.add(new ValidationError("tags", Messages.get("tags.not.alphabets.with.comma")));
                }
            }
            
            //TODO: Should validate all image upload to be of ratio near 1.25? 
            List<FilePart> coverPhotoList = formData.getFiles();
            for (int i=0; i<coverPhotoList.size(); i++) {
                if (coverPhotoList.get(i) != null) {
                    File file = coverPhotoList.get(i).getFile();
                    if(file.length() > 500000) {
                        errors.add(new ValidationError("coverPhoto", Messages.get("photo.maxsize")));
                    }
                }
            }
            Logger.debug("Reinvent Recipe error list : " + errors.toString());
        }
        return errors.isEmpty() ? null : errors;
	}
	
	
	public static Result reinventRecipe(long recipeId) {
        Form<ReinventRecipe> filledForm = reinventRecipeForm.bindFromRequest();
        Recipe currentRecipe = Recipe.find.byId(recipeId);
        Recipe newRecipe = new Recipe();
        
        // Additional check to determine whether login user has the rights to edit this recipe
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(currentRecipe == null) {
            return badRequest(Messages.get("recipe.not.found"));
        } else if(currentRecipe.user.equals(user)) {
            return notFound(error.render(user, Messages.get("invalid.reinvent"), Messages.get("recipe.reinvent.duplicate.user")));
        } else if(currentRecipe.privacyCode.subsetCodeId != Constants.PRIVACY_STATUS_PUBLIC && currentRecipe.statusCode.subsetCodeId != Constants.PUBLISH_STATUS_PUBLISHED) {
            return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipe.reinvent.invalid.rights")));
        }
        
        MultipartFormData formData = request().body().asMultipartFormData();
        String[] postAction = formData.asFormUrlEncoded().get("action");
        
        if (postAction == null || postAction.length == 0) {
            Logger.error("User : " + user.userId + " encountered no button action while reinventing recipe");
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
            return badRequest(reinventRecipe.render(user, currentRecipe, filledForm));
        } else {
            String action = postAction[0];
            
            if (filledForm.hasErrors()) {
                return badRequest(reinventRecipe.render(user, currentRecipe, filledForm));
            }
            
            ReinventRecipe reinventRecipeFilledForm = filledForm.get();
            try {
                Ebean.beginTransaction();

                // Check form action to be either reinvent or draft.
                if (Messages.get("draft").equals(action)) {
                    newRecipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_DRAFT;
                    fillUpRecipe(user, newRecipe, currentRecipe, reinventRecipeFilledForm, formData);

                } else if (Messages.get("publish").equals(action)) {
                    int reinventionMinimumPercentage = Integer.valueOf(Play.application().configuration().getString("reinventionMinimumPercentage"));
                    
					boolean defunctIngredient = false;
					if(currentRecipe.recipeIngredientList.size() != reinventRecipeFilledForm.recipeIngredientList.size()) {
                        defunctIngredient = true;
                    } else {
                        for(int i=0; i<currentRecipe.recipeIngredientList.size(); i++) {
                            int result = Utilities.LevenshteinDistanceRatio(currentRecipe.recipeIngredientList.get(i).ingredientName, reinventRecipeFilledForm.recipeIngredientList.get(i).ingredientName);
                            
                            if(reinventionMinimumPercentage > result
                            || !currentRecipe.recipeIngredientList.get(i).ingredientQuantity.equals(reinventRecipeFilledForm.recipeIngredientList.get(i).ingredientQuantity)) {
                                defunctIngredient = true;
                            }
                        }
					}
					
					boolean defunctDirection = false;
					if(currentRecipe.recipeDirectionList.size() != reinventRecipeFilledForm.recipeDirectionList.size()) {
						defunctDirection = true;
					} else {
						for(int i=0; i<currentRecipe.recipeDirectionList.size(); i++) {
                            int result = Utilities.LevenshteinDistanceRatio(currentRecipe.recipeDirectionList.get(i).directionDescription, reinventRecipeFilledForm.recipeDirectionList.get(i).directionDescription);
                            if(reinventionMinimumPercentage > result)
                                defunctDirection = true;
                        }
					}
					
					if (defunctIngredient || defunctDirection) {
						// Check if directions or ingredients have been edited. If either have been edited,
						// create a new recipe and save along with the now defunct recipe
                        newRecipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_PUBLISHED;
                        fillUpRecipe(user, newRecipe, currentRecipe, reinventRecipeFilledForm, formData);

					} else {
                        flash(Application.FLASH_ERROR_KEY, Messages.get("recipe.reinvention.lack.changes"));
                        return badRequest(reinventRecipe.render(user, currentRecipe, filledForm));
					}
				} else {
                    Logger.error("Recipe reinvent encountered invalid button action");
                    flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
                    return badRequest(reinventRecipe.render(user, currentRecipe, filledForm));
				}

                // No exception
                if (Messages.get("publish").equals(action)) {
                    flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.reinvent.success"));
                } else if (Messages.get("draft").equals(action)) {
                    flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.create.success.draft"));
                }
                Ebean.commitTransaction();

            } catch (FileOperationException foe) {
                Logger.error("Recipe reinvention encountered error : ", foe);
                flash(Application.FLASH_ERROR_KEY, foe.getMessage());
                return badRequest(reinventRecipe.render(user, currentRecipe, filledForm));
            } catch (Exception e) {
                Logger.error("Recipe reinvention encountered error : ", e);
                flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
                return badRequest(reinventRecipe.render(user, currentRecipe, filledForm));
            } finally {
                Ebean.endTransaction();
            }
            return redirect(controllers.recipe.routes.ViewRecipe.initViewRecipeForm(newRecipe.recipeId).url());
        }
	}
    
    
    /**
     * This method is use to input form data into recipe model class.
     * 
     * @param recipe
     * @param reinventRecipeFilledForm
     * @param formData
     * @return
     * @throws FileOperationException
     */
	private static Recipe fillUpRecipe(User user, Recipe recipe, Recipe currentRecipe, ReinventRecipe reinventRecipeFilledForm, MultipartFormData formData) throws FileOperationException {
		//recipe.monetizationCode = reinventRecipeFilledForm.monetizationCode;
		recipe.monetizationCode = Constants.SUBSETCODE_MONETIZATION_STATUS_FREE;
		//recipe.price = reinventRecipeFilledForm.price;
		recipe.privacyCode = SubsetCode.getSubsetCodeById(reinventRecipeFilledForm.privacyCode);
		recipe.bookRecipeList = new ArrayList<BookRecipe>(currentRecipe.bookRecipeList);
		recipe.user = user;
		recipe.recipeName = StringUtils.isBlank(reinventRecipeFilledForm.recipeName) ? "Draft" : reinventRecipeFilledForm.recipeName;
		recipe.recipeDescription = reinventRecipeFilledForm.recipeDescription;
		recipe.preparationTime = reinventRecipeFilledForm.preparationTime;
		recipe.servings = reinventRecipeFilledForm.servings;
        recipe.difficultyCode = SubsetCode.getSubsetCodeById(reinventRecipeFilledForm.difficulty);

        if(reinventRecipeFilledForm.categoryList != null) {
            for (int i = 0; i < reinventRecipeFilledForm.categoryList.size(); i++) {
                recipe.recipeCategoryList.add(new RecipeCategory(recipe, Category.getCategoryById(reinventRecipeFilledForm.categoryList.get(i))));
            }
        }
		
        String[] tagList = Utilities.spiltStringByComma(reinventRecipeFilledForm.tags);
        for(int i=0; i<tagList.length; i++) {
            if(!StringUtils.isBlank(tagList[i])) {
                String tagName = tagList[i].trim();
                Tag tag = Tag.getTagByName(tagName);
                
                if(tag == null) {
                    tag = new Tag();
                    tag.tagName = tagName;
                    tag.save();
                }

	            recipe.recipeTagList.add(new RecipeTag(recipe, tag));
            }
        }
        
        // Save recipe first to get recipe id for uploading of images if this is a new recipe
        if(recipe.recipeId == null) {
			//Update the ingredients & directions field with the 
			recipe.recipeIngredientList = reinventRecipeFilledForm.recipeIngredientList;
            recipe.recipeDirectionList = reinventRecipeFilledForm.recipeDirectionList;
            
            recipe.previousRecipeId = currentRecipe.recipeId;
            recipe.save();

            //Update user's reputation based on reinvent recipe points
            user.reputationPoints += Reputation.getReinventRecipeReputationPoints();
            user.update();
        }
        
        //-- Upload photos after saving recipe --
        try {
            List<FilePart> coverPhotoList = formData.getFiles();
            int totalDirectionsPhotos = 0;
            
            // Upload each recipe direction's photos
			for (int i=0; i<reinventRecipeFilledForm.recipeDirectionList.size(); i++) {
				FilePart directionPhoto = formData.getFile("recipeDirectionList[" + i + "].imagePath");
                
                if (directionPhoto != null) {
                    String fileType = directionPhoto.getFilename().substring(directionPhoto.getFilename().lastIndexOf("."));
                    String fileName = UUID.randomUUID().toString() + fileType;
                    
                    reinventRecipeFilledForm.recipeDirectionList.get(i).imagePath = Play.application().configuration().getString("recipeDirectionImageUploadPath").replace("*RECIPEID*", "" + recipe.recipeId) + fileName;
                    recipe.recipeDirectionList.get(i).imagePath = reinventRecipeFilledForm.recipeDirectionList.get(i).imagePath;
                    
                    String filePath = play.Play.application().path().toString() + "/" + reinventRecipeFilledForm.recipeDirectionList.get(i).imagePath;
                    byte[] byteFile1 = IOUtils.toByteArray(new FileInputStream(directionPhoto.getFile()));
                    
                    File directionImageFile = new File(filePath);
                    FileUtils.writeByteArrayToFile(directionImageFile, byteFile1);
                    
                    totalDirectionsPhotos++;
                    Logger.debug("Recipe Direction " + i+1 + " photo : " + filePath);
                } else {
                    // Commented the codes below because reinvented recipe will not inherit it's parent's images
                    //if(currentRecipe.recipeDirectionList.size() > i)
                    //    reinventRecipeFilledForm.recipeDirectionList.get(i).imagePath = currentRecipe.recipeDirectionList.get(i).imagePath;
                }
			}
            
            // Upload each recipe's cover photos after deducting the number of photos that
			// will be uploaded under the recipe's direction table because the following
			// code is getting all images to be uploaded from the create recipe form.
			for (int i=0; i<coverPhotoList.size() - totalDirectionsPhotos; i++) {
				FilePart coverPhoto = coverPhotoList.get(i);
				
				// Limit number of cover photos to be uploaded to 'x' value controlled by Code Table
				if (coverPhoto != null && i < SubsetCode.getNumberOfCoverPhotosLimit()) {
                    RecipeImage ri = new RecipeImage();
                    String fileType = coverPhoto.getFilename().substring(coverPhoto.getFilename().lastIndexOf("."));
                    //ri.imageName = coverPhoto.getFilename() + fileType;
                    ri.imageName = UUID.randomUUID().toString() + fileType;
                    ri.imagePath = Play.application().configuration().getString("recipeCoverPhotoUploadPath").replace("*RECIPEID*", "" + recipe.recipeId) + ri.imageName;
                    
                    String filePath = play.Play.application().path().toString() + "/" + ri.imagePath;
                    byte[] byteFile1 = IOUtils.toByteArray(new FileInputStream(coverPhoto.getFile()));
                    
                    File coverImageFile = new File(filePath);
                    FileUtils.writeByteArrayToFile(coverImageFile, byteFile1);
                    
                    recipe.recipeImageList.add(i, ri);
                    
                    Logger.debug("Recipe Cover Photo " + i+1 + " : " + filePath);
				}
			}
			recipe.update();
        } catch (Exception e) {
            throw new FileOperationException(Messages.get("error.upload.recipe.images"));
        }
        
        return recipe;
	}
}
