package controllers.recipe;

import com.avaje.ebean.Ebean;
import controllers.Secured;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import models.Recipe;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class DeleteRecipe extends Controller {
	
	public static Result deleteRecipe() {
		DynamicForm data = form().bindFromRequest();
		long recipeId = 0;
		try {
		    recipeId = Long.parseLong(data.get("recipeId"));
        } catch (Exception e) {
			Logger.error("Recipe deletion encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
        Recipe currentRecipe = Recipe.find.byId(recipeId);
        String message = "";
        
        // Additional check to determine whether login user has the rights to delete this recipe
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(currentRecipe == null) {
            return badRequest(Messages.get("recipe.not.found"));
        } else if(!currentRecipe.user.equals(user)) {
            return badRequest(Messages.get("recipe.delete.invalid.rights"));
        }

        try {
            Ebean.beginTransaction();
            if(currentRecipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_PUBLISHED) {
                currentRecipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_DELETED;
                if (currentRecipe.cookForMe != null)
                    currentRecipe.cookForMe.cookForMeStatus = Constants.INACTIVE;
                currentRecipe.save();

                // Remove the user's reputation points based on new recipe points
                user.reputationPoints -= Reputation.getNewRecipeReputationPoints();
                user.update();

                message = Messages.get("recipe.delete.success");
            } else if(currentRecipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_DELETED) {
                //currentRecipe.statusCode = "HIDDEN";
                message = Messages.get("recipe.delete.success");
            } else if(currentRecipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_HIDDEN) {

            } else if(currentRecipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_DRAFT) {
                currentRecipe.delete();
                message = Messages.get("recipe.delete.success.draft");
            }

            Ebean.commitTransaction();
        } catch (Exception e) {
            Logger.error("Recipe deletion encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        } finally {
            Ebean.endTransaction();
        }

        return ok(message);
    }
}
