package controllers.recipe;

import controllers.Application;
import controllers.Secured;

import controllers.utils.Constants;
import controllers.utils.Utilities;

import models.BookRecipe;
import models.Category;
import models.FeaturedRecipe;
import models.Recipe;
import models.RecipeCategory;
import models.RecipeTag;
import models.User;

import play.Logger;

import play.data.DynamicForm;

import play.i18n.Messages;

import play.mvc.Controller;

import play.mvc.Http.Cookie;

import play.mvc.Result;
import play.mvc.Security;

import views.html.error.error;

import views.html.recipe.listRecipe;
import views.html.recipe.listRecipeRow;
import views.html.recipe.viewRecipe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import static play.data.Form.form;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class ViewRecipe extends Controller {
    /**
     * Creates a new ViewRecipe object.
     */
    public ViewRecipe() {
    }

    /**
     * Used at dashboard index.scala.html & ViewRecipe.java.
     *
     * @param   user
     * @param   limit
     * @param   page
     *
     * @return  active following latest recipe list value.
     */
    public static List<Recipe> getActiveFollowingLatestRecipeList(final User user, final int limit, final int page) {
        return Recipe.findActiveFollowingLatestPublishedPublicRecipe(user, limit, page);
    }

    /**
     * Returns the featured recipe value.
     *
     * @param   page
     *
     * @return  featured recipe value.
     */
    public static Recipe getFeaturedRecipe(final int page) {
        final List<Recipe> recipeList = getFeaturedRecipes(page);

        return (recipeList.size() > 0) ? recipeList.get(0) : null;
    }

    /**
     * Returns the featured recipes value.
     *
     * @param   page
     *
     * @return  featured recipes value.
     */
    public static List<Recipe> getFeaturedRecipes(final int page) {
        List<Recipe> recipeList = new ArrayList<Recipe>();
        final Set<Recipe> mergeSet = new HashSet<Recipe>(recipeList);

        final List<FeaturedRecipe> featuredRecipeList = FeaturedRecipe.findAllFeaturedRecipe(page);

        for (final FeaturedRecipe fr : featuredRecipeList) {
            mergeSet.add(fr.recipe);
        }

        mergeSet.addAll(Recipe.findPopularPublishedPublicRecipe(Constants.FETCH_RECIPE_PAGE_ONE));
        recipeList = new ArrayList<Recipe>(mergeSet);

        return recipeList;
    }

    /**
     * Used at ViewRecipe.java.
     *
     * @param   page
     *
     * @return  latest recipe list value.
     */
    public static List<Recipe> getLatestRecipeList(final int page) {
        return Recipe.findLatestPublishedPublicRecipe(page);
    }

    /**
     * Ajax call to retrieve another set of recipe list.
     *
     * @return  next recipe list value.
     */
    public static Result getNextRecipeList() {
        final DynamicForm data = form().bindFromRequest();
        int page = 0;
        int recipeId = 0;
        long searchedUserId = 0;
        String search = "";
        String categoryName = "";

        try {
            page = Integer.parseInt(data.get("page"));

            if (StringUtils.isNotBlank(data.get("recipeId"))) {
                recipeId = Integer.parseInt(data.get("recipeId"));
            }

            categoryName = data.get("categoryName");
            search = data.get("search");

            if (StringUtils.isNotBlank(data.get("forWhom"))) {
                searchedUserId = Long.parseLong(data.get("forWhom"));
            }
        } catch (Exception e) {
            Logger.error("Get next recipe list encountered error : ", e);
        }

        List<Recipe> recipeList = new ArrayList<Recipe>();

        if (StringUtils.containsIgnoreCase(search, "similar")) {
            recipeList = getSimilarRecipeList(recipeId, page);
        } else if (StringUtils.containsIgnoreCase(search, "latest")) {
            recipeList = Recipe.findLatestPublishedPublicRecipe(page);
        } else if (StringUtils.containsIgnoreCase(search, "popular")) {
            recipeList = Recipe.findPopularPublishedPublicRecipe(page);
        } else if (StringUtils.containsIgnoreCase(search, "my")) {
            final User loginedUser = Application.getSessionUser();
            final User searchedUser = User.find.byId(searchedUserId);

            if (searchedUser == null) {
                return notFound(error.render(loginedUser, Messages.get("user.not.found"),
                            Messages.get("user.not.found.desc")));
            }

            recipeList = Recipe.findAllRecipeByUser(searchedUser, page);
        } else if (StringUtils.containsIgnoreCase(search, "category")) {
            final User user = Application.getSessionUser();
            final Category category = Category.searchCategoryByName(categoryName);

            if (category == null) {
                return notFound(error.render(user, Messages.get("category.not.found"),
                            Messages.get("category.not.found.desc", categoryName)));
            }

            recipeList = Recipe.findPublishedPublicRecipeByCategory(category.categoryId,
                    Constants.FETCH_RECIPE_PAGE_ONE);
        } else {
            return badRequest();
        }

        return ok(listRecipeRow.render(recipeList, Constants.MAX_RECIPE_FETCH_LIMIT));
    }

    /**
     * Used at ViewRecipe.java.
     *
     * @param   page
     *
     * @return  popular recipe list value.
     */
    public static List<Recipe> getPopularRecipeList(final int page) {
        return Recipe.findPopularPublishedPublicRecipe(page);
    }

    /**
     * Returns the recipe categories for menubar value.
     *
     * @return  recipe categories for menubar value.
     */
    public static List<Category> getRecipeCategoriesForMenubar() {
        return Category.listCategoryByMenubarStatus();
    }

    /**
     * Used at viewRecipe.scala.html & ViewRecipe.java.
     *
     * @param   recipeId
     * @param   page
     *
     * @return  similar recipe list value.
     */
    public static List<Recipe> getSimilarRecipeList(final long recipeId, final int page) {
        return Recipe.findSimilarPublishedPublicRecipe(recipeId, page);
    }

    /**
     * Returns the user non deleted non hidden recipe count value.
     *
     * @param   user
     *
     * @return  user non deleted non hidden recipe count value.
     */
    public static int getUserNonDeletedNonHiddenRecipeCount(final User user) {
        return Recipe.findAllNonDeletedNonHiddenRecipeCountByUser(user);
    }

    /**
     * DOCUMENT ME!
     *
     * @return  Result
     */
    public static Result initListLatestRecipeForm() {
        final User user = Application.getSessionUser();

        return ok(listRecipe.render(user, Messages.get("recipe.latest"),
                    getLatestRecipeList(Constants.FETCH_RECIPE_PAGE_ONE), Constants.MAX_RECIPE_FETCH_LIMIT, true));
    }

    /**
     * Combine to get all user's recipes excluding deleted & hidden status.
     *
     * @param   searchedUserId
     *
     * @return  Result
     */
    public static Result initListOthersRecipeForm(final long searchedUserId) {
        final User searchedUser = User.find.byId(searchedUserId);
        final User user = Application.getSessionUser();

        if (searchedUser == null) {
            return notFound(error.render(user, Messages.get("user.not.found"), Messages.get("user.not.found.desc")));
        }

        return ok(listRecipe.render(user,
                    searchedUser.firstName + " " + searchedUser.lastName + "'s " + Messages.get("recipes"),
                    Recipe.findAllNonDeletedNonHiddenRecipeByUser(searchedUser, Constants.FETCH_RECIPE_PAGE_ONE),
                    Constants.MAX_RECIPE_FETCH_LIMIT, true));
    }

    /**
     * DOCUMENT ME!
     *
     * @return  Result
     */
    @Security.Authenticated(Secured.class)
    // Get all user's recipes that exclude deleted & hidden status
    public static Result initListOwnRecipeForm() {
        final User user = User.findByEmail(request().username());

        return ok(listRecipe.render(user, Messages.get("recipe.my"),
                    Recipe.findAllNonDeletedNonHiddenRecipeByUser(user, Constants.FETCH_RECIPE_PAGE_ONE),
                    Constants.MAX_RECIPE_FETCH_LIMIT, true));
    }

    /**
     * DOCUMENT ME!
     *
     * @return  Result
     */
    public static Result initListPopularRecipeForm() {
        final User user = Application.getSessionUser();

        return ok(listRecipe.render(user, Messages.get("recipe.popular"),
                    getPopularRecipeList(Constants.FETCH_RECIPE_PAGE_ONE), Constants.MAX_RECIPE_FETCH_LIMIT, true));
    }

    /**
     * DOCUMENT ME!
     *
     * @param   categoryName
     *
     * @return  Result
     */
    public static Result initListRecipeCategoryForm(final String categoryName) {
        final User user = Application.getSessionUser();
        final Category category = Category.searchCategoryByName(categoryName);

        if (category == null) {
            return notFound(error.render(user, Messages.get("category.not.found"),
                        Messages.get("category.not.found.desc", categoryName)));
        }

        return ok(listRecipe.render(user, category.categoryDescription,
                    Recipe.findPublishedPublicRecipeByCategory(category.categoryId, Constants.FETCH_RECIPE_PAGE_ONE),
                    Constants.MAX_RECIPE_FETCH_LIMIT, true));
    }

    /**
     * DOCUMENT ME!
     *
     * @param   recipeId
     *
     * @return  Result
     */
    public static Result initListSimilarRecipeForm(final long recipeId) {
        final User user = Application.getSessionUser();

        return ok(listRecipe.render(user, Messages.get("recipe.similar"),
                    getSimilarRecipeList(recipeId, Constants.FETCH_RECIPE_PAGE_ONE),
                    Constants.MAX_SIMILAR_RECIPE_FETCH_LIMIT, false));
    }

    /**
     * Display the view recipe page if login.
     *
     * @param   recipeId
     *
     * @return  view recipe page
     */
    public static Result initViewRecipeForm(final Long recipeId) {
        final User user = Application.getSessionUser();
        final Recipe recipe = Recipe.find.byId(recipeId);
        final List<Recipe> ancestryRecipe = Recipe.findAncestryRecipe(recipeId);

        if (recipe == null) {
            return notFound(error.render(user, Messages.get("recipe.not.found"),
                        Messages.get("recipe.not.found.desc")));
        } else if (user == null) {
            if ((recipe.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PUBLIC) &&
                    (recipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_PUBLISHED)) {
                recipe.recipeCategoryList = RecipeCategory.findByRecipeId(recipe.recipeId);
                recipe.recipeTagList = RecipeTag.findByRecipeId(recipe.recipeId);

                return ok(viewRecipe.render(user, recipe, ancestryRecipe));
            } else {
                return notFound(error.render(user, Messages.get("user.not.found"),
                            Messages.get("user.not.found.desc")));
            }
        }

        final List<BookRecipe> bookRecipeList = BookRecipe.findActiveRecipesByUserAndRecipe(user.userId, recipeId);

        if ((recipe != null) &&
                ((recipe.user.equals(user)) ||
                    (!recipe.user.equals(user) &&
                        (recipe.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PUBLIC) &&
                        (recipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_PUBLISHED)) ||
                    !bookRecipeList.isEmpty())) {
            // Check per session whether has this user view this recipe
            // before. (Not recommended due to user logout will reset)
            final String sessionViews = session().get("ViewedRecipes");
            String[] recipes = Utilities.spiltStringByComma(sessionViews);
            final boolean sessionViewed = Arrays.asList(recipes).contains(String.valueOf(recipe.recipeId));
            Logger.debug("Recipe " + recipe.recipeId + " Session Viewed : " + sessionViewed);

            // Check using cookies whether has this user view this recipe
            // before. (Client can delete the cookie on their own)
            final Cookie cookie = request().cookies().get("ViewedRecipes");
            final String cookieValue = (cookie != null) ? cookie.value() : "";
            recipes = Utilities.spiltStringByComma(cookieValue);

            final boolean cookieViewed = Arrays.asList(recipes).contains(String.valueOf(recipe.recipeId));
            Logger.debug("Recipe " + recipe.recipeId + " Cookie Viewed : " + cookieViewed);

            // Do a check to determine whether both cookie & user session has expired for this recipe view
            if (!sessionViewed && !cookieViewed) {
                session().put("ViewedRecipes", recipe.recipeId + "," + cookieValue);
                response().setCookie("ViewedRecipes", recipe.recipeId + "," + cookieValue);

                // Update recipe views
                recipe.views += 1;
                recipe.save();
            }

            recipe.recipeCategoryList = RecipeCategory.findByRecipeId(recipe.recipeId);
            recipe.recipeTagList = RecipeTag.findByRecipeId(recipe.recipeId);

            return ok(viewRecipe.render(user, recipe, ancestryRecipe));
        } else {
            return notFound(error.render(user, Messages.get("recipe.not.found"),
                        Messages.get("recipe.not.found.desc")));
        }
    }
}
