package controllers.recipe;

import com.avaje.ebean.Ebean;
import controllers.Application;
import controllers.Secured;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import play.mvc.Security;
import providers.FTPasswordAuthProvider;
import views.html.account.registration.unverified;
import views.html.error.error;
import views.html.recipe.createRecipe;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class CreateRecipe extends Controller {
	
	public int monetizationCode;
    //public double price;
	public List<Long> recipeBookList;
	public Integer privacyCode;
	public String recipeName;
	public String recipeDescription;
    public Integer servings;
	public Integer preparationTime;
	public Integer difficulty;
	public List<RecipeIngredient> recipeIngredientList;
	public List<RecipeDirection> recipeDirectionList;
	public List<Integer> categoryList;
	public String tags;

	public boolean cookForMe;
	public List<String> collectionMode;
	public Integer paymentType;
	public String currency;
	public Double cookForMeCost;
	public Double deliveryFee;
	public String cookForMeRemarks;


    /**
     * Defines a constructor.
     */ 
	public CreateRecipe() {
		monetizationCode = Constants.MONETIZATION_STATUS_FREE;
		recipeBookList = new ArrayList<Long>();
		//price = 0.00;
		privacyCode = Constants.PRIVACY_STATUS_PUBLIC;
		difficulty = Constants.DIFFICULTY_TYPE_MODERATE;
        servings = 0;
		preparationTime = 0;
		recipeIngredientList = new ArrayList<RecipeIngredient>();
		recipeDirectionList = new ArrayList<RecipeDirection>();
		categoryList = new ArrayList<Integer>();
		tags = "";

		cookForMeCost = 0.0;
		deliveryFee = 0.0;
		paymentType = Constants.PAYMENT_TYPE_CASH;
		cookForMeRemarks = "";
		collectionMode = new ArrayList<String>();
	}
	
    /**
     * Defines a form wrapping the CreateRecipe class.
     */ 
    final static Form<CreateRecipe> createRecipeForm = form(CreateRecipe.class);

	
	/**
	 * Display the create recipe page if login
	 *
	 * @return create recipe page
	 */
    public static Result initCreateRecipeForm() {
        final User user = User.findByEmail(request().username());
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if (!user.emailValidated) {
            if(user.validationEmailSent == null || Utilities.getPrevDateByMins(15).after(user.validationEmailSent)) {
                FTPasswordAuthProvider.getProvider().sendVerifyEmailMailingAfterSignup(user, ctx());
                return ok(unverified.render(user, false, false));
            } else {
                return ok(unverified.render(user, false, true));
            }
        } else {
            return ok(createRecipe.render(user, createRecipeForm.fill(new CreateRecipe())));
        }
    }
    
	/**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise
	 */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
        MultipartFormData formData = request().body().asMultipartFormData();
		String[] postAction = formData.asFormUrlEncoded().get("action");
		
		if (postAction == null || postAction.length == 0) {
			Logger.debug("No post action");
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
		} else {
			String action = postAction[0];
			
			if (Messages.get("publish").equals(action)) {
				
				List<FilePart> imagesList = formData.getFiles();
                int totalDirectionsPhotos = 0;
                
                int directionsLimit = SubsetCode.getNumberOfDirectionsLimit();
				if(recipeDirectionList.size() > directionsLimit) {
                    errors.add(new ValidationError("recipeDirectionList", Messages.get("recipe.directions.exceed").replace("*LIMIT*", "" + directionsLimit)));
                }
                for (int i=0; i<recipeDirectionList.size(); i++) {
    				FilePart directionPhoto = formData.getFile("recipeDirectionList[" + i + "].imagePath");        		    
    	            if (directionPhoto != null) {
    	            	totalDirectionsPhotos++;
    	            }
                }
                int totalCoverPhotos = imagesList.size() - totalDirectionsPhotos;

				// Limit number of cover photos to be uploaded to 'x' value controlled by Code Table
                int coverPhotosLimit = SubsetCode.getNumberOfCoverPhotosLimit();
				if (totalCoverPhotos > coverPhotosLimit) {
		            errors.add(new ValidationError("coverPhoto", Messages.get("recipe.coverPhoto.exceed") + " " + coverPhotosLimit));
				}

				/*
				if(monetizationCode != Constants.MONETIZATION_STATUS_PAID) {
					price = 0;
				}
				*/
				if (privacyCode <= 0 || privacyCode == null) {
					errors.add(new ValidationError("privacyCode", Messages.get("validate.required", Messages.get("recipe.privacyCode"))));
				} else if (SubsetCode.validateSubsetCode(Constants.PRIVACY_STATUS, privacyCode) == null) {
					errors.add(new ValidationError("privacyCode", Messages.get("invalid.code", Messages.get("recipe.privacyCode"))));
				}
				
		        if (StringUtils.isBlank(recipeName)) {
		            errors.add(new ValidationError("recipeName", Messages.get("validate.required", Messages.get("recipe.name"))));
		        }

				if (difficulty == null || difficulty <= 0) {
					errors.add(new ValidationError("difficulty", Messages.get("recipe.difficulty.required")));
				} else if (SubsetCode.validateSubsetCode(Constants.DIFFICULTY_TYPE, difficulty) == null) {
					errors.add(new ValidationError("difficulty", Messages.get("invalid.code", Messages.get("recipe.difficulty"))));
				}

				if(cookForMe) {
					if (paymentType == null || paymentType <= 0) {
						errors.add(new ValidationError("paymentType", Messages.get("validate.required", Messages.get("cookForMe.paymentType"))));
					} else if (SubsetCode.validateSubsetCode(Constants.PAYMENT_TYPE, paymentType) == null) {
						errors.add(new ValidationError("paymentType", Messages.get("invalid.code", Messages.get("cookForMe.paymentType"))));
					}

					User user = User.findByEmail(request().username());
					if (collectionMode.size() == 0) {
						errors.add(new ValidationError("collectionMode", Messages.get("validate.required", Messages.get("cookForMe.collectionMode"))));
					} else {
						for (int i = 0; i < collectionMode.size(); i++) {
							SubsetCode parsedCollectionMode = null;
							try {
								if (collectionMode.get(i) != null) {
									parsedCollectionMode = SubsetCode.validateSubsetCode(Constants.COLLECTION_MODE, Integer.parseInt(collectionMode.get(i)));
									if (parsedCollectionMode == null) {
										errors.add(new ValidationError("collectionMode", Messages.get("invalid.code", Messages.get("cookForMe.collectionMode"))));
									}
								}
							} catch (NumberFormatException e) {
								errors.add(new ValidationError("collectionMode", Messages.get("validate.not.numeric")));
							}

							if(parsedCollectionMode != null && parsedCollectionMode.subsetCodeId != Constants.COLLECTION_MODE_DELIVERY && (StringUtils.isBlank(user.contactNumber) || user.address == null || (user.address != null
									&& StringUtils.isBlank(user.address.addressLine1) || StringUtils.isBlank(user.address.postalCode)))) {
								errors.add(new ValidationError("collectionMode", Messages.get("profile.update.required")));
							}
						}
					}

					if (StringUtils.isBlank(currency)) {
						errors.add(new ValidationError("currency", Messages.get("validate.required", Messages.get("cookForMe.currency"))));
					}

					try {
						if (cookForMeCost == null || cookForMeCost < 0) {
							errors.add(new ValidationError("cookForMeCost", Messages.get("validate.required", Messages.get("cookForMe.cookForMeCost"))));
						}
					} catch (NumberFormatException e) {
						errors.add(new ValidationError("cookForMeCost", Messages.get("validate.not.numeric")));
					}

					try {
						if (deliveryFee == null || deliveryFee < 0) {
							errors.add(new ValidationError("deliveryCost", Messages.get("validate.required", Messages.get("cookForMe.deliveryCost"))));
						}
					} catch (NumberFormatException e) {
						errors.add(new ValidationError("deliveryCost", Messages.get("validate.not.numeric")));
					}
				}

		        try { 
    		        if (servings == null || servings <= 0) {
    		            errors.add(new ValidationError("servings", Messages.get("validate.required", Messages.get("recipe.servings"))));
    		        }
                } catch (NumberFormatException e) {
    		        errors.add(new ValidationError("servings", Messages.get("validate.not.numeric")));
                }
    		
		        try {
    		        if (preparationTime == null || preparationTime <= 0) {
    		            errors.add(new ValidationError("preparationTime", Messages.get("validate.required", Messages.get("recipe.preparationTime"))));
    		        }
                } catch (NumberFormatException e) {
    		        errors.add(new ValidationError("preparationTime", Messages.get("validate.not.numeric")));
                }
		        
				for(int i=0; i<recipeIngredientList.size(); i++) {
					RecipeIngredient ri = recipeIngredientList.get(i);
					if (StringUtils.isBlank(ri.ingredientName)) {
		                errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientName", Messages.get("validate.required", Messages.get("recipe.ingredient"))));
					}
					if (StringUtils.isBlank(ri.ingredientQuantity)) {
		                errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientQuantity", Messages.get("validate.required", Messages.get("recipe.ingredientQuantity"))));
					}
					/*
					if (StringUtils.isBlank(ri.ingredientUnit)) {
		                errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientUnit", Messages.get("validate.required", Messages.get("recipe.ingredientUnit"))));
					}
					*/
				}
				
				for(int i=0; i<recipeDirectionList.size(); i++) {
					RecipeDirection rd = recipeDirectionList.get(i);
					if (StringUtils.isBlank(rd.directionDescription)) {
		                errors.add(new ValidationError("recipeDirectionList["+i+"].directionDescription", Messages.get("validate.required", Messages.get("recipe.direction"))));
					}
				}
				
		        if(StringUtils.isNotBlank(tags) && !tags.matches("^[\\w,?\\s?]*$")) {
		            errors.add(new ValidationError("tags", Messages.get("tags.not.alphabets.with.comma")));
		        }
	        
			} else if (Messages.get("saveDraft").equals(action)) {
				
			}
			
			//TODO: Should validate all image upload to be of ratio near 1.25? 
			List<FilePart> coverPhotoList = formData.getFiles();
			for (int i=0; i<coverPhotoList.size(); i++) {
                if (coverPhotoList.get(i) != null) {
                    File file = coverPhotoList.get(i).getFile();
                    if(file.length() > 500000) {
                        errors.add(new ValidationError("coverPhoto", Messages.get("photo.maxsize")));
                    }
                }
			}
			Logger.debug("Create Recipe error list : " + errors.toString());
		}
        return errors.isEmpty() ? null : errors;
	}
	
	public static Result postRecipe() {
        Form<CreateRecipe> filledForm = createRecipeForm.bindFromRequest();
        User user = User.findByEmail(request().username());
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        
        MultipartFormData formData = request().body().asMultipartFormData();
		String[] postAction = formData.asFormUrlEncoded().get("action");
		
		if (postAction == null || postAction.length == 0) {
			Logger.error("User : " + user.userId + " encountered no button action while creating recipe");
			flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
            return badRequest(createRecipe.render(user, createRecipeForm.fill(new CreateRecipe())));
		} else {
			String action = postAction[0];
			
			if (filledForm.hasErrors()) {
                return badRequest(createRecipe.render(user, filledForm));
            }
            
	        CreateRecipe createRecipeFilledForm = filledForm.get();
	        Recipe recipe = new Recipe();

	        try {
				Ebean.beginTransaction();

	            recipe.monetizationCode = SubsetCode.getSubsetCodeById(createRecipeFilledForm.monetizationCode);
	    		//recipe.price = createRecipeFilledForm.price;
            	recipe.privacyCode = SubsetCode.getSubsetCodeById(createRecipeFilledForm.privacyCode);
	            
	            if(createRecipeFilledForm.monetizationCode == Constants.MONETIZATION_STATUS_BUNDLED) {
	            	boolean privateRecipeBook = false;
	            	
		            for(int i=0; i<createRecipeFilledForm.recipeBookList.size(); i++) {
		            	RecipeBook recipeBook = RecipeBook.find.byId(createRecipeFilledForm.recipeBookList.get(i));

		            	if(recipeBook != null) {
			            	// Manually create a join between recipe book and the recipe.
			            	BookRecipe recipeInBook = new BookRecipe();
		            		recipeInBook.recipeBook = recipeBook;
		            		recipeInBook.recipe = recipe;
		            		recipe.bookRecipeList.add(recipeInBook);
	
				            // Check if all selected recipe book to be included and its privacy code is public
				            // if public, recipe privacy will be public. Should there be a private in any of
				            // the recipe book to be included, recipe will auto be private
		            		if(recipeBook.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PRIVATE) {
		            			privateRecipeBook = true;
		            		}
		            	}
	            	}
		            
		            if(privateRecipeBook)
		            	recipe.privacyCode = Constants.SUBSETCODE_PRIVACY_STATUS_PRIVATE;
		            else
		            	recipe.privacyCode = Constants.SUBSETCODE_PRIVACY_STATUS_PUBLIC;
	            }
				recipe.user = user;	            
				recipe.recipeName = StringUtils.isBlank(createRecipeFilledForm.recipeName) ? "Draft" : createRecipeFilledForm.recipeName;
		        recipe.recipeDescription = createRecipeFilledForm.recipeDescription;
		        recipe.preparationTime = createRecipeFilledForm.preparationTime;
		        recipe.servings = createRecipeFilledForm.servings;
	            recipe.recipeIngredientList = createRecipeFilledForm.recipeIngredientList;
	            recipe.recipeDirectionList = createRecipeFilledForm.recipeDirectionList;
				recipe.difficultyCode = SubsetCode.getSubsetCodeById(createRecipeFilledForm.difficulty);

                for(int i=0; i<createRecipeFilledForm.categoryList.size(); i++) {
                	recipe.recipeCategoryList.add(new RecipeCategory(recipe, Category.getCategoryById(createRecipeFilledForm.categoryList.get(i))));
                }
	            
	            String[] tagList = Utilities.spiltStringByComma(createRecipeFilledForm.tags);            
	            for(int i=0; i<tagList.length; i++) {
	            	if(StringUtils.isNotBlank(tagList[i])) {
	            		String tagName = tagList[i].trim();
	            		Tag tag = Tag.getTagByName(tagName);
	            		
	            		if(tag == null) {
	            			tag = new Tag();
	            			tag.tagName = tagName;
	            			tag.save();
	            		}
	            		
			            recipe.recipeTagList.add(new RecipeTag(recipe, tag));
	            	}
	            }

				if(createRecipeFilledForm.cookForMe) {
					CookForMe cookForMe = new CookForMe();
					cookForMe.paymentType = SubsetCode.getSubsetCodeById(createRecipeFilledForm.paymentType);
					cookForMe.collectionMode = Utilities.convertStringListToCommaString(createRecipeFilledForm.collectionMode);
					cookForMe.currency = createRecipeFilledForm.currency;
					cookForMe.cookingCost = createRecipeFilledForm.cookForMeCost;
					cookForMe.deliveryFee = createRecipeFilledForm.deliveryFee;
					cookForMe.cookForMeStatus = Constants.ACTIVE;
					cookForMe.recipe = recipe;
					recipe.cookForMe = cookForMe;
				}

	        	// Check whether button action belongs to publish or save draft as i
	        	// need to differentiate them in whether to check for validations
                if (Messages.get("publish").equals(action)) {
                    recipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_PUBLISHED;
                    recipe.publishedDate = new Timestamp(new Date().getTime());

                } else if (Messages.get("draft").equals(action)) {
					recipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_DRAFT;

                } else {
                    Logger.error("Recipe creation encountered invalid button action");
                    flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
                    return badRequest(createRecipe.render(user, filledForm));
				}
                
                // Save recipe first to get recipe id for uploading of images
                if(recipe.recipeId == null) {
                    recipe.save();

                    // Update user's reputation based on new recipe points
                    user.reputationPoints += Reputation.getNewRecipeReputationPoints();
                    user.update();
                }
	            
	            //-- Upload photos after saving recipe --
	            try {
	                List<FilePart> coverPhotoList = formData.getFiles();
	                int totalDirectionsPhotos = 0;
	                
	                // Upload each recipe direction's photos
	    			for (int i=0; i<createRecipeFilledForm.recipeDirectionList.size(); i++) {
	    				FilePart directionPhoto = formData.getFile("recipeDirectionList[" + i + "].imagePath");

	    	            if (directionPhoto != null) {
	    	    			String fileType = directionPhoto.getFilename().substring(directionPhoto.getFilename().lastIndexOf("."));
	    	    			String fileName = UUID.randomUUID().toString() + fileType;
	    	    			
	    	    			createRecipeFilledForm.recipeDirectionList.get(i).imagePath = Play.application().configuration().getString("recipeDirectionImageUploadPath").replace("*RECIPEID*", "" + recipe.recipeId) + fileName;
	    	    		    
	    	    		    String filePath = play.Play.application().path().toString() + "/" + createRecipeFilledForm.recipeDirectionList.get(i).imagePath;
	    	                byte[] byteFile1 = IOUtils.toByteArray(new FileInputStream(directionPhoto.getFile()));

	    	    		    File directionImageFile = new File(filePath);
	    	    		    FileUtils.writeByteArrayToFile(directionImageFile, byteFile1);

	    	    		    totalDirectionsPhotos++;
	    	    		    Logger.debug("Recipe Direction " + i + " photo : " + createRecipeFilledForm.recipeDirectionList.get(i).imagePath);
	    	            }
	    			}

	                // Upload each recipe's cover photos after deducting the number of photos that
	    			// will be uploaded under the recipe's direction table because the following
	    			// code is getting all images to be uploaded from the create recipe form.
	    			for (int i=0; i<coverPhotoList.size() - totalDirectionsPhotos; i++) {
	    				FilePart coverPhoto = coverPhotoList.get(i);
	    				
	    				// Limit number of cover photos to be uploaded to 'x' value controlled by Code Table
	    				//TODO: if (coverPhoto != null && i < Integer.parseInt(Code.getNumberOfCoverPhotos())) {
	    				if (coverPhoto != null && i < SubsetCode.getNumberOfCoverPhotosLimit()) {
	    	    			RecipeImage ri = new RecipeImage();
	    	    			String fileType = coverPhoto.getFilename().substring(coverPhoto.getFilename().lastIndexOf("."));
	    	    			//ri.imageName = coverPhoto.getFilename() + fileType;
	    	    			ri.imageName = UUID.randomUUID().toString() + fileType;
	    	    		    ri.imagePath = Play.application().configuration().getString("recipeCoverPhotoUploadPath").replace("*RECIPEID*", "" + recipe.recipeId) + ri.imageName;
	    	    		    
	    	    		    String filePath = play.Play.application().path().toString() + ri.imagePath;
	    	    		    byte[] byteFile1 = IOUtils.toByteArray(new FileInputStream(coverPhoto.getFile()));
	    	    		    
	    	    		    File coverImageFile = new File(filePath);
	    	    		    FileUtils.writeByteArrayToFile(coverImageFile, byteFile1);

	    	    			recipe.recipeImageList.add(ri);
	    	    		    Logger.debug("Recipe Cover Photo " + i+1 + " : " + filePath);
	    	    		}
	    			}
	                recipe.update();
	                
	                if (Messages.get("publish").equals(action)) {		
		                flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.create.success"));
					} else if (Messages.get("draft").equals(action)) {	
		                flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.create.success.draft"));
					}
	    	        
	            } catch (Exception e) {
	                Logger.error("Recipe creation encountered error : ", e);
	                flash(Application.FLASH_ERROR_KEY, Messages.get("error.upload.recipe.images"));
			        return badRequest(createRecipe.render(user, filledForm));
	            }

				Ebean.commitTransaction();
	        } catch (Exception e) {
	            Logger.error("Recipe creation encountered error : ", e);
	            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
		        return badRequest(createRecipe.render(user, filledForm));
	        } finally {
				Ebean.endTransaction();
			}

	        return ok(createRecipe.render(user, createRecipeForm.fill(new CreateRecipe())));
		}
	}
}
