package controllers.recipe;

import com.avaje.ebean.Ebean;
import controllers.Application;
import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import play.mvc.Security;
import scala.reflect.io.FileOperationException;
import views.html.error.error;
import views.html.recipe.editRecipe;
import views.html.recipe.editRecipeAcknowledgeModal;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.*;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditRecipe extends Controller {

	public int monetizationCode;
    //public double price;
	public List<Long> recipeBookList;
	public Integer privacyCode;
	public Integer statusCode;
	public String recipeName;
	public String recipeDescription;
    public Integer servings;
	public Integer preparationTime;
    public Integer difficulty;
	public List<RecipeIngredient> recipeIngredientList;
	public List<RecipeDirection> recipeDirectionList;
	public List<Integer> categoryList;
	public String tags;

    public boolean cookForMe;
    public List<String> collectionMode;
    public Integer paymentType;
    public String currency;
    public Double cookForMeCost;
    public Double deliveryFee;
    public String cookForMeRemarks;

    static List<Recipe> ancestryRecipe = new ArrayList<Recipe>();

    private static int reinventionMinimumPercentage = Integer.valueOf(Play.application().configuration().getString("reinventionMinimumPercentage"));

    /**
     * Defines a form wrapping the EditRecipe class.
     */
    final static Form<EditRecipe> editRecipeForm = form(EditRecipe.class);


    public static Result initAcknowledgementModal() {
        return ok(editRecipeAcknowledgeModal.render());
    }

    /**
     * Display the edit recipe page if login
     *
     * @return edit recipe page
     */
    public static Result initEditRecipeForm(long recipeId) {
        final User user = User.findByEmail(request().username());
        Recipe recipe = Recipe.find.byId(recipeId);

        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(recipe == null) {
            return badRequest(Messages.get("recipe.not.found"));
        }
        List<Recipe> ancestryRecipe = Recipe.findAncestryRecipe(recipeId);

        if(recipe != null && recipe.user.equals(user)) {
            return ok(editRecipe.render(user, recipe, ancestryRecipe, editRecipeForm.fill(initEditForm(recipe))));
        } else
            return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipe.edit.invalid.rights")));
    }


    public static EditRecipe initEditForm(Recipe recipe) {
        EditRecipe value = new EditRecipe();

        value.monetizationCode = recipe.monetizationCode != null ? recipe.monetizationCode.subsetCodeId : Constants.MONETIZATION_STATUS_FREE;
        //value.price = recipe.price;
        value.privacyCode = recipe.privacyCode != null ? recipe.privacyCode.subsetCodeId : Constants.PRIVACY_STATUS_PUBLIC;
        value.statusCode = recipe.statusCode != null ? recipe.statusCode.subsetCodeId : Constants.PUBLISH_STATUS_PUBLISHED;
        value.recipeName = recipe.recipeName;
        value.recipeDescription = recipe.recipeDescription;
        value.difficulty = recipe.difficultyCode != null ? recipe.difficultyCode.subsetCodeId : Constants.DIFFICULTY_TYPE_MODERATE;
        value.servings = recipe.servings;
        value.preparationTime = recipe.preparationTime;

        value.recipeIngredientList = recipe.recipeIngredientList;
        value.recipeDirectionList = recipe.recipeDirectionList;

        value.recipeBookList = new ArrayList<Long>();
        for(BookRecipe recipesInRecipeBook : recipe.bookRecipeList) {
            value.recipeBookList.add(recipesInRecipeBook.recipeBook.recipeBookId);
        }

        recipe.recipeCategoryList = RecipeCategory.findByRecipeId(recipe.recipeId);
		value.categoryList = new ArrayList<Integer>();
        for(int i=0; i<recipe.recipeCategoryList.size(); i++) {
            if(recipe.recipeCategoryList.get(i).category != null && StringUtils.isNotBlank(recipe.recipeCategoryList.get(i).category.categoryName)) {
                value.categoryList.add(recipe.recipeCategoryList.get(i).category.categoryId);
            }
        }

        recipe.recipeTagList = RecipeTag.findByRecipeId(recipe.recipeId);
        String[] tagSpiltList = new String[recipe.recipeTagList.size()];
        for(int i=0; i<recipe.recipeTagList.size(); i++) {
            if(recipe.recipeTagList.get(i).tag != null && StringUtils.isNotBlank(recipe.recipeTagList.get(i).tag.tagName)) {
                tagSpiltList[i] = recipe.recipeTagList.get(i).tag.tagName;
            }
        }
        if(tagSpiltList.length > 0) {
            value.tags = Utilities.convertStringArrayToCommaString(tagSpiltList);
        }

        if(recipe.cookForMe != null) {
            value.cookForMe = true;
            value.cookForMeCost = recipe.cookForMe.cookingCost;
            value.collectionMode = Arrays.asList(Utilities.spiltStringByComma(recipe.cookForMe.collectionMode));
            value.deliveryFee = recipe.cookForMe.deliveryFee;
            value.paymentType = recipe.cookForMe.paymentType.subsetCodeId;
            value.cookForMeRemarks = recipe.cookForMe.remarks;
            value.currency = recipe.cookForMe.currency;
        }
        return value;
    }

	/**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise
	 */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();

        MultipartFormData formData = request().body().asMultipartFormData();
		String[] postAction = formData.asFormUrlEncoded().get("action");

		if (postAction == null || postAction.length == 0) {
            Logger.debug("No post action");
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
		} else {
			String action = postAction[0];

			if (Messages.get("publish").equals(action) || Messages.get("update").equals(action)) {

				List<FilePart> imagesList = formData.getFiles();
                int totalDirectionsPhotos = 0;

                int directionsLimit = SubsetCode.getNumberOfDirectionsLimit();
				if(recipeDirectionList.size() > directionsLimit) {
                    errors.add(new ValidationError("recipeDirectionList", Messages.get("recipe.directions.exceed").replace("*LIMIT*", "" + directionsLimit)));
                }
                for (int i=0; i<recipeDirectionList.size(); i++) {
                    FilePart directionPhoto = formData.getFile("recipeDirectionList[" + i + "].imagePath");
                    if (directionPhoto != null) {
                        totalDirectionsPhotos++;
                    }
                }
                int totalCoverPhotos = imagesList.size() - totalDirectionsPhotos;

				// Limit number of cover photos to be uploaded to 'x' value controlled by Code Table
                int coverPhotosLimit = SubsetCode.getNumberOfCoverPhotosLimit();
				if (totalCoverPhotos > coverPhotosLimit) {
                    errors.add(new ValidationError("coverPhoto", Messages.get("recipe.coverPhoto.exceed") + " " + coverPhotosLimit));
				}

				//if(!monetizationCode.equalsIgnoreCase(Messages.get(Constants.MONETIZATION_STATUS_PAID))) {
				//	price = 0;
				//}

                if (privacyCode <= 0 || privacyCode == null) {
                    errors.add(new ValidationError("privacyCode", Messages.get("validate.required", Messages.get("recipe.privacyCode"))));
                } else if (SubsetCode.validateSubsetCode(Constants.PRIVACY_STATUS, privacyCode) == null) {
                    errors.add(new ValidationError("privacyCode", Messages.get("invalid.code", Messages.get("recipe.privacyCode"))));
                }

                if (StringUtils.isBlank(recipeName)) {
                    errors.add(new ValidationError("recipeName", Messages.get("validate.required", Messages.get("recipe.name"))));
                }

                if (statusCode == null || statusCode <= 0) {
                    errors.add(new ValidationError("statusCode", Messages.get("validate.required", Messages.get("recipe.statusCode"))));
                } else if (SubsetCode.validateSubsetCode(Constants.PUBLISH_STATUS, statusCode) == null) {
                    errors.add(new ValidationError("statusCode", Messages.get("invalid.code", Messages.get("recipe.statusCode"))));
                } else {
                }

                if (difficulty == null || difficulty <= 0) {
                    errors.add(new ValidationError("difficulty", Messages.get("recipe.difficulty.required")));
                } else if (SubsetCode.validateSubsetCode(Constants.DIFFICULTY_TYPE, difficulty) == null) {
                    errors.add(new ValidationError("difficulty", Messages.get("invalid.code", Messages.get("recipe.difficulty"))));
                }

                if(cookForMe) {
                    if (paymentType == null || paymentType <= 0) {
                        errors.add(new ValidationError("paymentType", Messages.get("validate.required", Messages.get("cookForMe.paymentType"))));
                    } else if (SubsetCode.validateSubsetCode(Constants.PAYMENT_TYPE, paymentType) == null) {
                        errors.add(new ValidationError("paymentType", Messages.get("invalid.code", Messages.get("cookForMe.paymentType"))));
                    }

                    if (collectionMode.size() == 0) {
                        errors.add(new ValidationError("collectionMode", Messages.get("validate.required", Messages.get("cookForMe.collectionMode"))));
                    } else {
                        for (int i = 0; i < collectionMode.size(); i++) {
                            try {
                                if (collectionMode.get(i) != null && SubsetCode.validateSubsetCode(Constants.COLLECTION_MODE, Integer.parseInt(collectionMode.get(i))) == null) {
                                    errors.add(new ValidationError("collectionMode", Messages.get("invalid.code", Messages.get("cookForMe.collectionMode"))));
                                }
                            } catch (NumberFormatException e) {
                                errors.add(new ValidationError("collectionMode", Messages.get("validate.not.numeric")));
                            }
                        }
                    }

                    if (StringUtils.isBlank(currency)) {
                        errors.add(new ValidationError("currency", Messages.get("validate.required", Messages.get("cookForMe.currency"))));
                    }

                    try {
                        if (cookForMeCost == null || cookForMeCost < 0) {
                            errors.add(new ValidationError("cookForMeCost", Messages.get("validate.required", Messages.get("cookForMe.cookForMeCost"))));
                        }
                    } catch (NumberFormatException e) {
                        errors.add(new ValidationError("cookForMeCost", Messages.get("validate.not.numeric")));
                    }

                    try {
                        if (deliveryFee == null || deliveryFee < 0) {
                            errors.add(new ValidationError("deliveryFee", Messages.get("validate.required", Messages.get("cookForMe.deliveryFee"))));
                        }
                    } catch (NumberFormatException e) {
                        errors.add(new ValidationError("deliveryFee", Messages.get("validate.not.numeric")));
                    }
                }

                try {
                    if (servings == null || servings <= 0) {
                        errors.add(new ValidationError("servings", Messages.get("validate.required", Messages.get("recipe.servings"))));
                    }
                } catch (NumberFormatException e) {
                    errors.add(new ValidationError("servings", Messages.get("validate.not.numeric")));
                }

                try {
                    if (preparationTime == null || preparationTime <= 0) {
                        errors.add(new ValidationError("preparationTime", Messages.get("validate.required", Messages.get("recipe.preparationTime"))));
                    }
                } catch (NumberFormatException e) {
                    errors.add(new ValidationError("preparationTime", Messages.get("validate.not.numeric")));
                }

                for(int i=0; i<recipeIngredientList.size(); i++) {
                    RecipeIngredient ri = recipeIngredientList.get(i);
                    if (StringUtils.isBlank(ri.ingredientName)) {
                        errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientName", Messages.get("validate.required", Messages.get("recipe.ingredient"))));
                    }
                    if (StringUtils.isBlank(ri.ingredientQuantity)) {
                        errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientQuantity", Messages.get("validate.required", Messages.get("recipe.ingredientQuantity"))));
                    }
                    /*
                    if (StringUtils.isBlank(ri.ingredientUnit)) {
                        errors.add(new ValidationError("recipeIngredientList["+i+"].ingredientUnit", Messages.get("validate.required", Messages.get("recipe.ingredientUnit"))));
                    }
                    */
                }

                for(int i=0; i<recipeDirectionList.size(); i++) {
                    RecipeDirection rd = recipeDirectionList.get(i);
                    if (StringUtils.isBlank(rd.directionDescription)) {
                        errors.add(new ValidationError("recipeDirectionList["+i+"].directionDescription", Messages.get("validate.required", Messages.get("recipe.direction"))));
                    }
                }

                if(StringUtils.isNotBlank(tags) && !tags.matches("^[\\w,?\\s?]*$")) {
                    errors.add(new ValidationError("tags", Messages.get("tags.not.alphabets.with.comma")));
                }
			}

			//TODO: Should validate all image upload to be of ratio near 1.25?
			List<FilePart> coverPhotoList = formData.getFiles();
			for (int i=0; i<coverPhotoList.size(); i++) {
                if (coverPhotoList.get(i) != null) {
                    File file = coverPhotoList.get(i).getFile();
                    if(file.length() > 500000) {
                        errors.add(new ValidationError("coverPhoto", Messages.get("photo.maxsize")));
                    }
                }
			}

            Logger.debug("Edit Recipe error list : " + errors.toString());
		}
        return errors.isEmpty() ? null : errors;
	}

	public static Result updateRecipe(long recipeId) {
		boolean defunctIngredient = false;
		boolean defunctDirection = false;

        Form<EditRecipe> filledForm = editRecipeForm.bindFromRequest();
        Recipe currentRecipe = Recipe.find.byId(recipeId);

        // Additional check to determine whether login user has the rights to edit this recipe
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(currentRecipe == null) {
            return badRequest(Messages.get("recipe.not.found"));
        } else if(!currentRecipe.user.equals(user)) {
            return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipe.edit.invalid.rights")));
        } else {
            if (currentRecipe.statusCode.equals(Constants.PUBLISH_STATUS_DELETED) && CookForMeOrders.findNewOrAcceptedOrders(currentRecipe.cookForMe.cookForMeId, Constants.FETCH_COOK_FOR_ME_ORDER_PAGE_ONE).size() > 0)
                return badRequest(Messages.get("recipe.cookForMeOrders.pending"));
        }

        MultipartFormData formData = request().body().asMultipartFormData();
		String[] postAction = formData.asFormUrlEncoded().get("action");

		if (postAction == null || postAction.length == 0) {
            Logger.error("User : " + user.userId + " encountered no button action while editing recipe");
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
            return badRequest(editRecipe.render(user, currentRecipe, ancestryRecipe, filledForm));
		} else {
			String action = postAction[0];

			if (filledForm.hasErrors()) {
                return badRequest(editRecipe.render(user, currentRecipe, ancestryRecipe, filledForm));
            }

            EditRecipe editRecipeFilledForm = filledForm.get();
            try {
                Ebean.beginTransaction();

                // Check form action to be either publish, saveDraft or update.
                // Only recipes in Draft status during editing will be able to publish or save as draft.
                // Recipes which have already being published will ONLY be able to update recipe.
				if (Messages.get("publish").equals(action)) {
					currentRecipe = fillUpRecipe(user, currentRecipe, currentRecipe, editRecipeFilledForm, formData);
					currentRecipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_PUBLISHED;
                    currentRecipe.publishedDate = new Timestamp(new Date().getTime());
                    currentRecipe.update();

                    // Update user's reputation based on new recipe points as the reputation points is not added when creating as draft
                    user.reputationPoints += Reputation.getNewRecipeReputationPoints();
                    user.update();
				} else if (Messages.get("draft").equals(action)) {
					currentRecipe = fillUpRecipe(user, currentRecipe, currentRecipe, editRecipeFilledForm, formData);
					currentRecipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_DRAFT;
                    currentRecipe.update();

				} else if (Messages.get("update").equals(action)) {
                    if(currentRecipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_PUBLISHED || currentRecipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_DELETED) {
                        currentRecipe.statusCode = SubsetCode.getSubsetCodeById(editRecipeFilledForm.statusCode);
                    }

					if(currentRecipe.recipeIngredientList.size() != editRecipeFilledForm.recipeIngredientList.size()) {
                        defunctIngredient = true;
					} else {
                        for(int i=0; i<currentRecipe.recipeIngredientList.size(); i++) {
                            int result = Utilities.LevenshteinDistanceRatio(currentRecipe.recipeIngredientList.get(i).ingredientName, editRecipeFilledForm.recipeIngredientList.get(i).ingredientName);

                            if(reinventionMinimumPercentage > result
                            || !currentRecipe.recipeIngredientList.get(i).ingredientQuantity.equals(editRecipeFilledForm.recipeIngredientList.get(i).ingredientQuantity)) {
                                defunctIngredient = true;
                            }
                        }
					}

					if(currentRecipe.recipeDirectionList.size() != editRecipeFilledForm.recipeDirectionList.size()) {
						defunctDirection = true;
					} else {
						for(int i=0; i<currentRecipe.recipeDirectionList.size(); i++) {
                            int result = Utilities.LevenshteinDistanceRatio(currentRecipe.recipeDirectionList.get(i).directionDescription, editRecipeFilledForm.recipeDirectionList.get(i).directionDescription);
                            if(reinventionMinimumPercentage > result)
                                defunctDirection = true;
                        }
					}

					// Check if directions or ingredients have been edited. If either have been edited,
					// create a new recipe and save along with the now defunct recipe
					if (defunctIngredient || defunctDirection) {
					// or if user selected this recipe to save as a new version
					// if(editRecipeFilledForm.saveAsNewRecipe) {
                        Recipe newRecipe = fillUpRecipe(user, new Recipe(), currentRecipe, editRecipeFilledForm, formData);

                        // Loop thru the current recipe and copy over cover photos into the new recipe
						for (int i=newRecipe.recipeImageList.size(); i<currentRecipe.recipeImageList.size(); i++) {
                            RecipeImage ri = new RecipeImage();
                            ri.imageName = currentRecipe.recipeImageList.get(i).imageName;
                            ri.imagePath = currentRecipe.recipeImageList.get(i).imagePath;
                            newRecipe.recipeImageList.add(ri);
						}
                        newRecipe.update();

                        // Update the now defunct recipe to "HIDDEN" status
                        currentRecipe.statusCode = Constants.SUBSETCODE_PUBLISH_STATUS_HIDDEN;
                        currentRecipe.update();

                        // Create Notification to all users that fav or liked the now defunct recipe
                        // that the creator had edited the recipe and now in a new version.
                        for(User u : BookRecipe.findUniqueUserPerRecipe(currentRecipe.recipeId)) {
                        	CreateNotification.recipeNotification(u, user, Messages.get("notification.editRecipe"), "/viewRecipe/"+newRecipe.recipeId);
                        }

                        // Replace the new recipe as the current & return back to edit recipe form
                        currentRecipe = newRecipe;
					} else {
						currentRecipe = fillUpRecipe(user, currentRecipe, currentRecipe, editRecipeFilledForm, formData);
                        currentRecipe.update();
					}

				//} else if (Messages.get("delete").equals(action)) {
                    //currentRecipe.delete();
                } else {
                    Logger.error("Recipe editing encountered invalid button action");
                    flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
                    return badRequest(editRecipe.render(user, currentRecipe, ancestryRecipe, filledForm));
				}

                // No exception
                if (Messages.get("publish").equals(action)) {
                    flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.create.success"));
                } else if (Messages.get("update").equals(action)) {
                    if (defunctIngredient || defunctDirection)
                        flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.update.success.new.version"));
                    else
                        flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.update.success"));
                } else if (Messages.get("draft").equals(action)) {
                    flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipe.update.success.draft"));
                }

                Ebean.commitTransaction();
            } catch (FileOperationException foe) {
                Logger.error("Recipe editing encountered error : ", foe);
                flash(Application.FLASH_ERROR_KEY, foe.getMessage());
                return badRequest(editRecipe.render(user, currentRecipe, ancestryRecipe, filledForm));
            } catch (Exception e) {
                Logger.error("Recipe editing encountered error : ", e);
                flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
                return badRequest(editRecipe.render(user, currentRecipe, ancestryRecipe, filledForm));
            } finally {
                Ebean.endTransaction();
            }

            return redirect(controllers.recipe.routes.ViewRecipe.initViewRecipeForm(currentRecipe.recipeId).url());
		}
	}


    /**
     * This method is use to input form data into recipe model class.
     *
     * @param recipe
     * @param editRecipeFilledForm
     * @param formData
     * @return
     * @throws FileOperationException
     */
	private static Recipe fillUpRecipe(User user, Recipe recipe, Recipe currentRecipe, EditRecipe editRecipeFilledForm, MultipartFormData formData) throws FileOperationException {
		//recipe.monetizationCode = editRecipeFilledForm.monetizationCode;
		recipe.monetizationCode = Constants.SUBSETCODE_MONETIZATION_STATUS_FREE;
		//recipe.price = editRecipeFilledForm.price;
		recipe.privacyCode = SubsetCode.getSubsetCodeById(editRecipeFilledForm.privacyCode);
		recipe.bookRecipeList = new ArrayList<BookRecipe>(currentRecipe.bookRecipeList);

        if(editRecipeFilledForm.monetizationCode == Constants.MONETIZATION_STATUS_BUNDLED) {
            boolean privateRecipeBook = false;
            if(editRecipeFilledForm.recipeBookList == null)
                editRecipeFilledForm.recipeBookList = new ArrayList<Long>();

            List<Long> formRecipeBookSubList = new ArrayList<Long>(editRecipeFilledForm.recipeBookList);
            List<RecipeBook> recipeBookSubList = new ArrayList<RecipeBook>(currentRecipe.user.recipeBookList);

            // Run thru 2 arrays to determine which recipe book is removed &
            // which recipe book is to be added only if this is not a new recipe
            if(recipe.recipeId != null) {
                for(int i=0; i<currentRecipe.user.recipeBookList.size(); i++) {
                    for(int j=0; j<editRecipeFilledForm.recipeBookList.size(); j++) {
                        RecipeBook book = RecipeBook.find.byId(editRecipeFilledForm.recipeBookList.get(j));

                        // Check if all selected recipe book to be included and its privacy code is public
                        // if public, recipe privacy will be public. Should there be a private in any of
                        // the recipe book to be included, recipe will auto be private
                        if(book != null && book.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PRIVATE) {
                            privateRecipeBook = true;
                        }

                        if(book != null && currentRecipe.user.recipeBookList.get(i).equals(book) && book.bookRecipeList.contains(recipe)) {
                            formRecipeBookSubList.remove(book.recipeBookId);
                            recipeBookSubList.remove(book);
                        }
                    }
                }
            }

            // Manually create a join between recipe book and the recipe.
            for(int i=0; i<formRecipeBookSubList.size(); i++) {
                RecipeBook book = RecipeBook.find.byId(formRecipeBookSubList.get(i));

                if(book != null && book.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PRIVATE) {
                    privateRecipeBook = true;
                }

                BookRecipe recipeInBook = new BookRecipe();
                recipeInBook.recipeBook = book;
                recipeInBook.recipe = recipe;
                recipe.bookRecipeList.add(recipeInBook);
            }

            // Manually delete the join between recipe book and the recipe.
            for(int i=0; i<recipeBookSubList.size(); i++) {
                for(BookRecipe br : recipe.bookRecipeList) {
                    if(br.recipeBook.equals(recipeBookSubList.get(i)))
                    br.delete();
                }
            }

            if(privateRecipeBook)
                recipe.privacyCode = Constants.SUBSETCODE_PRIVACY_STATUS_PRIVATE;
            else
                recipe.privacyCode = Constants.SUBSETCODE_PRIVACY_STATUS_PUBLIC;
        }

		recipe.user = user;
		recipe.recipeName = StringUtils.isBlank(editRecipeFilledForm.recipeName) ? "Draft" : editRecipeFilledForm.recipeName;
		recipe.recipeDescription = editRecipeFilledForm.recipeDescription;
		recipe.preparationTime = editRecipeFilledForm.preparationTime;
		recipe.servings = editRecipeFilledForm.servings;
        recipe.difficultyCode = SubsetCode.getSubsetCodeById(editRecipeFilledForm.difficulty);

        recipe.recipeCategoryList = RecipeCategory.findByRecipeId(currentRecipe.recipeId);
        if(recipe.recipeCategoryList != null) {
            for (int i = 0; i < recipe.recipeCategoryList.size(); i++) {
                RecipeCategory.deleteRecipeCategory(recipe.recipeCategoryList.get(i).recipe.recipeId, recipe.recipeCategoryList.get(i).category.categoryId);
            }
            recipe.recipeCategoryList.clear();
        }

        if(editRecipeFilledForm.categoryList != null) {
    		for (int i = 0; i < editRecipeFilledForm.categoryList.size(); i++) {
    			recipe.recipeCategoryList.add(new RecipeCategory(recipe, Category.getCategoryById(editRecipeFilledForm.categoryList.get(i))));
    		}
        }

        recipe.recipeTagList = RecipeTag.findByRecipeId(currentRecipe.recipeId);
		for (int i = 0; i < recipe.recipeTagList.size(); i++) {
			recipe.recipeTagList.get(i).delete();
		}
		recipe.recipeTagList.clear();

        String[] tagList = Utilities.spiltStringByComma(editRecipeFilledForm.tags);
        for(int i=0; i<tagList.length; i++) {
            if(StringUtils.isNotBlank(tagList[i])) {
                String tagName = tagList[i].trim();
                Tag tag = Tag.getTagByName(tagName);

                if(tag == null) {
                    tag = new Tag();
                    tag.tagName = tagName;
                    tag.save();
                }

	            recipe.recipeTagList.add(new RecipeTag(recipe, tag));
            }
        }

        if(editRecipeFilledForm.cookForMe) {
            CookForMe cookForMe = new CookForMe();
            if(recipe.cookForMe != null)
                cookForMe = recipe.cookForMe;

            cookForMe.paymentType = SubsetCode.getSubsetCodeById(editRecipeFilledForm.paymentType);
            cookForMe.collectionMode = Utilities.convertStringListToCommaString(editRecipeFilledForm.collectionMode);
            cookForMe.currency = editRecipeFilledForm.currency;
            cookForMe.cookingCost = editRecipeFilledForm.cookForMeCost;
            cookForMe.deliveryFee = editRecipeFilledForm.deliveryFee;
            cookForMe.cookForMeStatus = Constants.ACTIVE;
            cookForMe.recipe = recipe;
            recipe.cookForMe = cookForMe;
        } else {
            if(recipe.cookForMe != null) {
                recipe.cookForMe.cookForMeStatus = Constants.INACTIVE;
                //recipe.cookForMe.delete();
            }
        }

        // Save recipe first to get recipe id for uploading of images if this is a new recipe
        if(recipe.recipeId == null) {
			//Update the ingredients & directions field with the
			recipe.recipeIngredientList = editRecipeFilledForm.recipeIngredientList;
            recipe.recipeDirectionList = editRecipeFilledForm.recipeDirectionList;

            recipe.statusCode = currentRecipe.statusCode;
            recipe.previousRecipeId = currentRecipe.recipeId;
            recipe.save();
        }

        //-- Upload photos after saving recipe --
        try {
            List<FilePart> coverPhotoList = formData.getFiles();
            Logger.debug("coverPhotoList.size() : " + coverPhotoList.size());
            int totalDirectionsPhotos = 0;

            // Upload each recipe direction's photos
			for (int i=0; i<editRecipeFilledForm.recipeDirectionList.size(); i++) {
				FilePart directionPhoto = formData.getFile("recipeDirectionList[" + i + "].imagePath");

                if (directionPhoto != null) {

                    // Delete the old direction photo
					if(currentRecipe.recipeDirectionList.size() > i) {
						String currrentImagePath = currentRecipe.recipeDirectionList.get(i).imagePath;
						if(StringUtils.isNotBlank(currrentImagePath)) {
	                        File oldDirectionImage = new File(currrentImagePath);
	                        if (oldDirectionImage.exists()) {
	                            oldDirectionImage.delete();
	                            recipe.recipeDirectionList.get(i).imagePath = null;
	                            Logger.debug("Attempt to remove old direction image of : " + currrentImagePath + " Successful.");
	                        } else {
	                            Logger.debug("Attempt to remove old direction image of : " + currrentImagePath + " Failed.");
	                        }
	                    }
					}

                    String fileType = directionPhoto.getFilename().substring(directionPhoto.getFilename().lastIndexOf("."));
                    String fileName = UUID.randomUUID().toString() + fileType;

                    editRecipeFilledForm.recipeDirectionList.get(i).imagePath = Play.application().configuration().getString("recipeDirectionImageUploadPath").replace("*RECIPEID*", "" + recipe.recipeId) + fileName;
                    recipe.recipeDirectionList.get(i).imagePath = editRecipeFilledForm.recipeDirectionList.get(i).imagePath;

                    String filePath = play.Play.application().path().toString() + editRecipeFilledForm.recipeDirectionList.get(i).imagePath;
                    byte[] byteFile1 = IOUtils.toByteArray(new FileInputStream(directionPhoto.getFile()));

                    File directionImageFile = new File(filePath);
                    FileUtils.writeByteArrayToFile(directionImageFile, byteFile1);

                    totalDirectionsPhotos++;
                    Logger.debug("Recipe Direction " + i+1 + " photo : " + filePath);
                } else {
                    if(currentRecipe.recipeDirectionList.size() > i)
                        editRecipeFilledForm.recipeDirectionList.get(i).imagePath = currentRecipe.recipeDirectionList.get(i).imagePath;
                }
			}

            // Upload each recipe's cover photos after deducting the number of photos that
			// will be uploaded under the recipe's direction table because the following
			// code is getting all images to be uploaded from the create recipe form.
			for (int i=0; i<coverPhotoList.size() - totalDirectionsPhotos; i++) {
				FilePart coverPhoto = coverPhotoList.get(i);

				// Get the cover photo limit from Code Table
				int coverPhotoLimit = SubsetCode.getNumberOfCoverPhotosLimit();

				// Limit number of cover photos to be uploaded to 'x' value controlled by Code Table
				if (coverPhoto != null) {

                    // Delete the old cover photo
					if(currentRecipe.recipeImageList.size() > i) {
						String currrentImagePath = currentRecipe.recipeImageList.get(i).imagePath;
						if(StringUtils.isNotBlank(currrentImagePath)) {
	                        File oldCoverPhoto = new File(currrentImagePath);
	                        if (oldCoverPhoto.exists()) {
	                        	oldCoverPhoto.delete();
	                            recipe.recipeImageList.get(i).delete();
	                            recipe.recipeImageList.remove(i);
	                            Logger.debug("Attempt to remove old cover photo of : " + currrentImagePath + " Successful.");
	                        } else {
	                        	recipe.recipeImageList.get(i).delete();
	                            recipe.recipeImageList.remove(i);
	                            Logger.debug("Attempt to remove old cover photo of : " + currrentImagePath + " Failed. Image does not exist.");
	                        }
	                    }
					}

					if(i < coverPhotoLimit) {
                        RecipeImage ri = new RecipeImage();
                        String fileType = coverPhoto.getFilename().substring(coverPhoto.getFilename().lastIndexOf("."));
                        //ri.imageName = coverPhoto.getFilename() + fileType;
                        ri.imageName = UUID.randomUUID().toString() + fileType;
                        ri.imagePath = Play.application().configuration().getString("recipeCoverPhotoUploadPath").replace("*RECIPEID*", "" + recipe.recipeId) + ri.imageName;

                        String filePath = play.Play.application().path().toString() + ri.imagePath;
                        byte[] byteFile1 = IOUtils.toByteArray(new FileInputStream(coverPhoto.getFile()));

                        File coverImageFile = new File(filePath);
                        FileUtils.writeByteArrayToFile(coverImageFile, byteFile1);

                        recipe.recipeImageList.add(i, ri);

                        Logger.debug("Recipe Cover Photo " + i+1 + " : " + filePath);
					}
                }
			}
        } catch (Exception e) {
        	e.printStackTrace();
            throw new FileOperationException(Messages.get("error.upload.recipe.images"));
        }

        return recipe;
	}
}
