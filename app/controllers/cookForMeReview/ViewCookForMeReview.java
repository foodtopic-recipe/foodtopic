package controllers.cookForMeReview;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.*;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.cookForMeReview.listReview;
import views.html.cookForMeReview.listReviewRow;

import java.util.*;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class ViewCookForMeReview extends Controller {

    /**
     * Listing of cook for me review
     *
     * @return review list
     */
    public static List<CookForMeReview> getActiveReviewList(long orderId) {
        final User user = Application.getSessionUser();
        List<CookForMeReview> cookForMeReviewList = new ArrayList<CookForMeReview>();

        if (user != null) {
            Set<CookForMeReview> mergeSet = new HashSet<CookForMeReview>(cookForMeReviewList);
            CookForMeReview cReview = CookForMeReview.findActiveReviewByUserAndCookForMeOrder(user.userId, orderId);
            if (cReview != null) mergeSet.add(cReview);
            mergeSet.addAll(CookForMeReview.findActiveReviewByCookForMeOrder(orderId, Constants.FETCH_REVIEW_PAGE_ONE, true));
            cookForMeReviewList = new ArrayList<CookForMeReview>(mergeSet);
        } else {
            cookForMeReviewList = CookForMeReview.findActiveReviewByCookForMeOrder(orderId, Constants.FETCH_REVIEW_PAGE_ONE, true);
        }

        return cookForMeReviewList;
    }

    public static boolean reviewCreatedForOrder(long orderId) {
        final User user = User.findByEmail(request().username());

        if (user != null) {
            CookForMeReview review = CookForMeReview.findActiveReviewByUserAndCookForMeOrder(user.userId, orderId);

            if (review == null) {
                return false;
            }
        }
        return true;
    }
    /**
     * Display the list of cookForMe active review
     *
     * @return list cookForMe active review page
     */
    public static Result initListCookForMeReviewForm(long orderId) {
        final User user = User.findByEmail(request().username());
        CookForMeOrders order = CookForMeOrders.find.byId(orderId);

        if (user == null)
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        else if (order == null)
            return badRequest(Messages.get("cookForMeOrder.not.found"));
        else {
            return ok(listReview.render(user, getActiveReviewList(orderId), Constants.MAX_REVIEW_FETCH_LIMIT, orderId));
        }
    }

    public static int findTotalNumberOfCookForMeReviews(long orderId) {
        final User user = User.findByEmail(request().username());
        if (user == null)
            return 0;
        else
            return CookForMeReview.findActiveReviewByCookForMeOrder(orderId, Constants.FETCH_REVIEW_PAGE_ONE, false).size();
    }

    public static Result getNextCookForMeReviewList() {
        final User user = User.findByEmail(request().username());

        if (user == null)
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));

        DynamicForm data = form().bindFromRequest();
        int page = 0, limit = 0, orderId = 0;
        String search = "";

        try {
            page = Integer.parseInt(data.get("page"));
            limit = Integer.parseInt(data.get("limit"));
            orderId = Integer.parseInt(data.get("orderId"));
        } catch (Exception e) {
            Logger.error("Get next review list encountered error : ", e);
        }

        List<CookForMeReview> reviewList = getActiveReviewList(orderId);

        int skipCount = limit * page;
        if (reviewList.size() <= skipCount) {
            return ok();
        } else {
            int lastRecord = limit + skipCount >= reviewList.size() ? reviewList.size() : limit + skipCount;
            reviewList = reviewList.subList(skipCount, lastRecord);

            return ok(listReviewRow.render(user, reviewList, lastRecord));
        }
    }

    public static boolean getUserUpvoteFlag(final long commentId) {
        CookForMeReviewCommentFlag cookForMeCommentFlag = getVoteFlag(commentId);

        return ((cookForMeCommentFlag != null) && (cookForMeCommentFlag.flag == true)) ? true : false;
    }

    public static boolean getUserDownvoteFlag(final long commentId) {
        CookForMeReviewCommentFlag cookForMeCommentFlag = getVoteFlag(commentId);

        return ((cookForMeCommentFlag != null) && (cookForMeCommentFlag.flag == false)) ? true : false;
    }

    public static CookForMeReviewCommentFlag getVoteFlag(final long commentId) {
        try {
            final User user = Application.getSessionUser();

            if (user != null) {
                CookForMeReviewCommentFlag cookForMeCommentFlag = CookForMeReviewCommentFlag.findByUserAndComment(user.userId, commentId);
                return cookForMeCommentFlag;
            }
        } catch (Exception e) {
            Logger.error("Get CookForMe User Flag Comment encountered error : ", e);

        }
        return null;
    }
}
