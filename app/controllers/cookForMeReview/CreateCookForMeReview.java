package controllers.cookForMeReview;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Application;
import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import providers.FTPasswordAuthProvider;
import views.html.error.errorModal;
import views.html.cookForMeReview.createReviewModal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class CreateCookForMeReview extends Controller {

	public long orderId;
	public int reviewScore;
	public String comment;
	public String imagePath;

    /**
     * Defines a constructor.
     */ 
	public CreateCookForMeReview() {
		//reviewScore = 1;
		comment = "";
	}
	
    /**
     * Defines a form wrapping the CreateCookForMeReview class.
     */ 
    final static Form<CreateCookForMeReview> createCookForMeReviewForm = form(CreateCookForMeReview.class);

    public static boolean showCreateCookForMeReview(long orderId) {
        final User user = User.findByEmail(request().username());
        final CookForMeOrders order = CookForMeOrders.find.byId(orderId);
        
        if(user == null || order == null) {
            return false;
        }

        CookForMeReview existingReview = CookForMeReview.findAllReviewByUserAndCookForMeOrder(user.userId, order.orderId);
        return existingReview == null ? true : !existingReview.reviewStatus; 
    }

    public static Result initCookForMeReviewModal(long orderId) {
        final User user = User.findByEmail(request().username());
        final CookForMeOrders order = CookForMeOrders.find.byId(orderId);

        // Additional check to determine whether login user has the rights to create this order review
        if(user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        return ok(createReviewModal.render(user, order, createCookForMeReviewForm.fill(new CreateCookForMeReview())));
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();

        try {
            if (reviewScore <= 0) {
                errors.add(new ValidationError("reviewScore", Messages.get("validate.required", Messages.get("review.rating"))));
            }
        } catch (NumberFormatException e) {
            errors.add(new ValidationError("reviewScore", Messages.get("validate.not.numeric")));
        }

        return errors.isEmpty() ? null : errors;
    }

    public static Result postCookForMeReview() {
        ObjectNode jsonResult = Json.newObject();

        try {
            Ebean.beginTransaction();

            Form<CreateCookForMeReview> filledForm = createCookForMeReviewForm.bindFromRequest();

            final User user = User.findByEmail(request().username());

            if (user == null) {
                jsonResult.put("result", Messages.get("signin.required.desc"));
                return badRequest(jsonResult.toString());
            } else if (filledForm.hasErrors()) {
                Logger.info(" filledForm.errorsAsJson().toString() : " + filledForm.errorsAsJson().toString());
                return badRequest(filledForm.errorsAsJson().toString());
            }

            CreateCookForMeReview createCookForMeReviewFilledForm = filledForm.get();
            CookForMeOrders order = CookForMeOrders.find.byId(createCookForMeReviewFilledForm.orderId);

            if (order == null) {
                jsonResult.put("result", Messages.get("cookForMeOrder.not.found"));
                return badRequest(jsonResult.toString());
            } else if(!order.user.equals(user) && !order.cookForMe.recipe.user.equals(user)) {
                // Additional check to determine whether login user has the rights to complete this order
                jsonResult.put("result", errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("cookForMeOrder.update.invalid.rights")).toString());
                return badRequest(jsonResult.toString());
            } else if (order.orderStatus.subsetCodeId != Constants.ORDER_STATUS_ACCEPTED
                    && order.orderStatus.subsetCodeId != Constants.ORDER_STATUS_COMPLETED) {
                jsonResult.put("result", Messages.get("cookForMeOrder.not.accepted"));
                return badRequest(jsonResult.toString());
            } else {
                if (order.bookingDate.before(new Date())) {

                    CookForMeReview review = CookForMeReview.findAllReviewByUserAndCookForMeOrder(user.userId, order.orderId);
                    boolean newReview = false;

                    if (review == null) {
                        review = new CookForMeReview();
                        newReview = true;
                    }
                    review.user = user;
                    review.cookForMeOrders = order;
                    review.reviewScore = createCookForMeReviewFilledForm.reviewScore;
                    review.reviewStatus = Constants.REVIEWED;

                    if (!StringUtils.isBlank(createCookForMeReviewFilledForm.comment)) {
                        CookForMeReviewComment reviewComment = new CookForMeReviewComment();
                        reviewComment.user = user;
                        reviewComment.cookForMeOrders = order;
                        reviewComment.cookForMeReview = review;
                        reviewComment.commentType = Constants.SUBSETCODE_COMMENT_TYPE_REVIEW;
                        reviewComment.commentDescription = createCookForMeReviewFilledForm.comment;
                        reviewComment.save();
                        review.cookForMeReviewComment = reviewComment;
                    }

                    User notify;
                    if (order.cookForMe.recipe.user.userId == user.userId) {
                        notify = order.user;
                    } else {
                        notify = order.cookForMe.recipe.user;
                    }

                    if(order.orderStatus != Constants.SUBSETCODE_ORDER_STATUS_COMPLETED) {
                        order.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_COMPLETED;
                        order.update();

                        CreateNotification.cookForMeNotification(notify, user, Messages.get("notification.completeOrder", order.collectionMode.subsetCodeValue, Constants.dateFormat.format(order.bookingDate)), controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url());

                        if (newReview) {
                            FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(notify.email, notify.firstName + " " + notify.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.completed.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.dateFormat.format(order.bookingDate), order.collectionMode.subsetCodeValue, order.servings, "."), Messages.get("cookForMeOrder.cancelled.order.email.header"));
                        } else {
                            FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(notify.email, notify.firstName + " " + notify.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.completed.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.dateFormat.format(order.bookingDate), order.collectionMode.subsetCodeValue, order.servings, " " + Messages.get("cookForMeOrder.completed.order.review.email.content")), Messages.get("cookForMeOrder.cancelled.order.email.header"));
                        }
                    }

                    if (newReview) {
                        review.save();
                        CreateNotification.reviewNotification(notify, user, Messages.get("notification.newCookForMeReview", review.reviewScore, order.collectionMode.subsetCodeValue, Constants.notificationDateFormat.format(order.bookingDate)), null);

                        // Update user's reputation based on new review points (Cannot self review order to gain reputation points)
                        if (order.user.userId != user.userId) {
                            order.user.reputationPoints += Reputation.getNewReviewReputationPoints(review.reviewScore);
                            order.user.update();
                        }

                        jsonResult.put("result", errorModal.render(user, "success",  Messages.get("cookForMeOrder.completed"), Messages.get("cookForMeReview.completed.success.desc") + " " + Messages.get("cookForMeReview.review.success.desc")).toString());
                    } else {
                        review.update();
                        CreateNotification.reviewNotification(notify, user, Messages.get("notification.editCookForMeReview", order.collectionMode.subsetCodeValue, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.notificationDateFormat.format(order.bookingDate)), null);

                        jsonResult.put("result", errorModal.render(user, "success",  Messages.get("cookForMeReview.submitted"), Messages.get("cookForMeReview.review.success.desc")).toString());
                    }
                    Ebean.commitTransaction();
                }
            }

            return ok(jsonResult.toString());
        } catch (Exception e) {
            Logger.error("Cook For Me Review creation encountered error : ", e);
            jsonResult.put("result", Messages.get("error.technical"));
            return badRequest(jsonResult.toString());
        } finally {
            Ebean.endTransaction();
        }
	}
}
