package controllers.cookForMeReview;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Application;
import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.utils.Constants;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.error.errorModal;
import views.html.cookForMeReview.editReviewModal;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditCookForMeReview extends Controller {

	public long orderId;
	public int reviewScore;
	public String comment;
	public String imagePath;


	public static Result initCookForMeReviewModal(long orderId) {
        final User user = User.findByEmail(request().username());
        CookForMeReview review = CookForMeReview.find.byId(orderId);

        // Additional check to determine whether login user has the rights to edit this order
        if(user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(review == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("review.not.found")));
        }
		return ok(editReviewModal.render(review));
	}

    public static Result updateCookForMeReview() {
		DynamicForm data = form().bindFromRequest();
        long reviewId = 0;
        int reviewScore = 0;

        String rawReviewId = data.get("reviewId");
        String rawReviewScore = data.get("rating");
        String rawComment = data.get("comment");
        try {
            reviewId = Long.parseLong(rawReviewId);
            reviewScore = Integer.parseInt(rawReviewScore);
        } catch (Exception e) {
			Logger.error("Update of cookForMeReview : " + rawReviewId + "and score of : " + rawReviewScore + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        // Additional check to determine whether login user has the rights to edit this review
        final User user = User.findByEmail(request().username());
        CookForMeReview currentReview = CookForMeReview.find.byId(reviewId);

        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(currentReview == null) {
            return badRequest(Messages.get("review.not.found"));
        } else if(currentReview.cookForMeOrders.orderStatus.subsetCodeId != Constants.ORDER_STATUS_COMPLETED) {
            return badRequest(Messages.get("cookForMeOrders.require.completed.status"));
        } else if(!currentReview.user.equals(user)) {
            return badRequest(Messages.get("cookForMeOrders.edit.invalid.rights"));
        }

        /*if (StringUtils.isBlank(rawComment)) {
            return badRequest(Messages.get("validate.required", Messages.get("comment")));
        }*/

        try {
            currentReview.reviewScore = reviewScore;

            if (StringUtils.isNotBlank(rawComment)) {
                if (currentReview.cookForMeReviewComment != null) {
                    currentReview.cookForMeReviewComment.commentDescription = rawComment;
                    currentReview.cookForMeReviewComment.update();
                } else {
                    CookForMeReviewComment reviewComment = new CookForMeReviewComment();
                    reviewComment.user = user;
                    reviewComment.cookForMeOrders = currentReview.cookForMeOrders;
                    reviewComment.cookForMeReview = currentReview;
                    reviewComment.commentType = Constants.SUBSETCODE_COMMENT_TYPE_REVIEW;
                    reviewComment.commentDescription = rawComment;
                    reviewComment.save();

                    currentReview.cookForMeReviewComment = reviewComment;
                }
            } else {
                if (currentReview.cookForMeReviewComment != null && StringUtils.isNotBlank(currentReview.cookForMeReviewComment.commentDescription)) {
                    currentReview.cookForMeReviewComment.delete();
                }
            }
            currentReview.update();

            if (currentReview.cookForMeOrders.cookForMe.recipe.user.userId == user.userId) {
                CreateNotification.reviewNotification(currentReview.cookForMeOrders.user, user, Messages.get("notification.editCookForMeReview", currentReview.cookForMeOrders.collectionMode.subsetCodeValue, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.notificationDateFormat.format(currentReview.cookForMeOrders.bookingDate)), null);
            } else {
                CreateNotification.reviewNotification(currentReview.cookForMeOrders.cookForMe.recipe.user, user, Messages.get("notification.editCookForMeReview", currentReview.cookForMeOrders.collectionMode.subsetCodeValue, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.notificationDateFormat.format(currentReview.cookForMeOrders.bookingDate)), null);
            }

            ObjectNode jsonResult = Json.newObject();
            jsonResult.put("result", errorModal.render(user, "success",  Messages.get("cookForMeReview.edit"), Messages.get("cookForMeReview.update.success")).toString());

            return ok(jsonResult.toString());
        } catch (Exception e) {
            Logger.error("Review update encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
	}
}
