package controllers.cookForMeReview;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.*;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.error.errorModal;
import views.html.cookForMeReview.deleteReviewModal;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class DeleteCookForMeReview extends Controller {

    public static Result initCookForMeReviewModal(final Long reviewId) {
        final User user = User.findByEmail(request().username());
        final CookForMeReview review = CookForMeReview.find.byId(reviewId);

        // Additional check to determine whether login user has the rights to edit this order review
        if (user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if (review == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("review.not.found")));
        }

        return ok(deleteReviewModal.render(review));
    }

    public static Result deleteCookForMeReview() {
        final DynamicForm data = form().bindFromRequest();
        long reviewId = 0;

        final String rawReviewId = data.get("reviewId");

        try {
            reviewId = Long.parseLong(rawReviewId);
            Logger.debug("Delete CookForMeReview - reviewId : " + reviewId);
        } catch (Exception e) {
            Logger.error("Deletion of CookForMeReview : " + rawReviewId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        final CookForMeReview currentCookForMeReview = CookForMeReview.find.byId(reviewId);
        Logger.debug("Delete Review found review : " + currentCookForMeReview);

        // Additional check to determine whether login user has the rights to delete this review
        final User user = User.findByEmail(request().username());

        if (user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if (currentCookForMeReview == null) {
            return badRequest(Messages.get("cookForMeReview.not.found"));
        } else if (!currentCookForMeReview.user.equals(user)) {
            return badRequest(Messages.get("cookForMeReview.delete.invalid.rights"));
        }

        try {
            currentCookForMeReview.reviewStatus = Constants.UNREVIEWED;
            CookForMeReviewComment reviewComment = null;

            // Break the bonds between review & comment
            if (currentCookForMeReview.cookForMeReviewComment != null) {
                reviewComment = currentCookForMeReview.cookForMeReviewComment;

                // Break the bonds between comment & comment flag if any
                for(CookForMeReviewCommentFlag commentFlag : reviewComment.cookForMeReviewCommentFlagList) {
                    commentFlag.delete();
                }
                currentCookForMeReview.cookForMeReviewComment = null;
            }
            currentCookForMeReview.update();

            if(reviewComment != null)
                reviewComment.delete();

            ObjectNode jsonResult = Json.newObject();
            jsonResult.put("result", errorModal.render(user, "success",  Messages.get("cookForMeReview.delete"), Messages.get("cookForMeReview.delete.success")).toString());

            return ok(jsonResult.toString());
        } catch (Exception e) {
            Logger.error("CookForMeReview deletion encountered error : ", e);

            return badRequest(Messages.get("error.technical"));
        }
    }
}
