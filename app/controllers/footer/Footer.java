package controllers.footer;

import controllers.Application;
import models.User;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.footer.aboutUs;
import views.html.footer.advertising;
import views.html.footer.contactUs;
import views.html.footer.privacyPolicy;
import views.html.footer.termsOfService;

import static play.data.Form.form;

public class Footer extends Controller {
	
    public static Result aboutUs() {
        final User user = Application.getSessionUser();
        return ok(aboutUs.render(user, Messages.get("aboutUs"), user == null));
    }
	
    public static Result advertising() {
        final User user = Application.getSessionUser();
        return ok(advertising.render(user, Messages.get("advertising"), user == null, form(CreateSubmission.class).fill(new CreateSubmission())));
    }
    
    public static Result contactUs() {
        final User user = Application.getSessionUser();
        return ok(contactUs.render(user, Messages.get("contactUs"), user == null, form(CreateSubmission.class).fill(new CreateSubmission())));
    }
    
    public static Result privacyPolicy() {
        final User user = Application.getSessionUser();
        return ok(privacyPolicy.render(user, Messages.get("privacyPolicy"), user == null));
    }
    
    public static Result termsOfService() {
        final User user = Application.getSessionUser();
        return ok(termsOfService.render(user, Messages.get("termsOfService"), user == null));
    }
}