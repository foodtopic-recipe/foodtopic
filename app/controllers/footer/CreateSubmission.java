package controllers.footer;

import controllers.Application;
import models.SubmissionForm;
import models.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import providers.FTPasswordAuthProvider;
import views.html.footer.advertising;
import views.html.footer.contactUs;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

public class CreateSubmission extends Controller {

	public String firstName;
	public String lastName;
	public String email;
	public String comments;

    /**
     * Defines a form wrapping the CreateSubmission class.
     */ 
    final static Form<CreateSubmission> submissionForm = form(CreateSubmission.class);
    
    /**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise	
    */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<>();
		
		if (StringUtils.isBlank(firstName)) {
            errors.add(new ValidationError("firstName", Messages.get("profile.firstName.required")));
        }
		if (StringUtils.isBlank(lastName)) {
            errors.add(new ValidationError("lastName", Messages.get("profile.lastName.required")));
        }
		if (StringUtils.isBlank(email)) {
            errors.add(new ValidationError("email", Messages.get("profile.email.required")));
        }
        if (StringUtils.isBlank(comments)) {
        	errors.add(new ValidationError("comments", Messages.get("comment.required")));
        }
        
        return errors.isEmpty() ? null : errors;
	}
	
    public static Result contactUsSubmission() {
        Form<CreateSubmission> filledForm = submissionForm.bindFromRequest();
        final DynamicForm data = form().bindFromRequest();
        String gRecaptchaResponse = data.get("g-recaptcha-response");
        
		final User user = Application.getSessionUser();
        
		if (filledForm.hasErrors()) {
            return badRequest(contactUs.render(user, Messages.get("contactUs"), user == null, filledForm));
        }
        
        CreateSubmission submissionFilledForm = filledForm.get();
        
        try {
            sendPost(gRecaptchaResponse);

            SubmissionForm submission = new SubmissionForm();
            submission.email = submissionFilledForm.email;
            submission.firstName = submissionFilledForm.firstName;
            submission.lastName = submissionFilledForm.lastName;
            submission.comments = submissionFilledForm.comments;
            
            submission.save();
            
            FTPasswordAuthProvider.getProvider().sendInternalEmailAlert(Messages.get("support.email"), "Internal Auto Email", submissionFilledForm.firstName + " " + submissionFilledForm.lastName, submissionFilledForm.comments, "Contact Us");
            
            flash(Application.FLASH_SUCCESS_KEY, Messages.get("contactUs.submission.success"));
            return ok(contactUs.render(user, Messages.get("contactUs"), user == null, filledForm));
        } catch (Exception e) {
            Logger.error("ContactUs Submission encountered error : ", e);
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
            return badRequest(advertising.render(user, Messages.get("contactUs"), user == null, filledForm));
        }
	}
	
    public static Result advertisingSubmission() {
        Form<CreateSubmission> filledForm = submissionForm.bindFromRequest();
        final DynamicForm data = form().bindFromRequest();
        String gRecaptchaResponse = data.get("g-recaptcha-response");

        final User user = Application.getSessionUser();
		
		if (filledForm.hasErrors()) {
            return badRequest(advertising.render(user, Messages.get("advertising"), user == null, filledForm));
        }
        
        CreateSubmission submissionFilledForm = filledForm.get();
        
        try {
            sendPost(gRecaptchaResponse);

            SubmissionForm submission = new SubmissionForm();
            submission.email = submissionFilledForm.email;
            submission.firstName = submissionFilledForm.firstName;
            submission.lastName = submissionFilledForm.lastName;
            submission.comments = "Advertising Opportunity ! " + submissionFilledForm.comments;
            
            submission.save();
            
            FTPasswordAuthProvider.getProvider().sendInternalEmailAlert(Messages.get("support.email"), "Internal Auto Email", submissionFilledForm.firstName + " " + submissionFilledForm.lastName, submissionFilledForm.comments, "New Advertising Opportunity!");
            
            flash(Application.FLASH_SUCCESS_KEY, Messages.get("advertising.submission.success"));
            return ok(advertising.render(user, Messages.get("advertising"), user == null, filledForm));
        } catch (Exception e) {
            Logger.error("Advertising Submission encountered error : ", e);
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
            return badRequest(advertising.render(user, Messages.get("advertising"), user == null, filledForm));
        }
	}

    private static void sendPost(String gRecaptchaResponse) throws Exception {
        String url = "https://www.google.com/recaptcha/api/siteverify";
        /*
        HttpClient httpClient = HttpClientBuilder.create().build(); //Use this instead
        try {
            HttpPost request = new HttpPost(url);
            StringEntity params = new StringEntity("details={\"secret\":\"6LdTWBwTAAAAAPMP4jfr5I9JgWB7921tkABiHFtV&response\",\"response\":"+gRecaptchaResponse+"} ");
            request.addHeader("content-type", "application/x-www-form-urlencoded");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);

            // handle response here...
        }catch (Exception ex) {
            // handle exception here
        }*/

        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        String urlParameters = "secret=6LdTWBwTAAAAAPMP4jfr5I9JgWB7921tkABiHFtV&response="+gRecaptchaResponse;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        Logger.debug("\nSending 'POST' request to URL : " + url);
        Logger.debug("Post parameters : " + urlParameters);
        Logger.debug("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        Logger.info(response.toString());

        if(response.toString().contains("\"success\": false")) {
            throw new Exception("Failed to verify reCAPTCHA");
        }
    }
}
