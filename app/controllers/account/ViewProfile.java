package controllers.account;

import controllers.Application;
import controllers.Secured;

import controllers.exceptions.CustomException;
import controllers.utils.Constants;

import models.FeaturedUser;
import models.User;

import play.i18n.Messages;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import views.html.account.profile.viewProfile;

import views.html.error.error;

import java.io.File;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class ViewProfile extends Controller {
    /**
     * Creates a new ViewProfile object.
     */
    public ViewProfile() {
    }

    /**
     * Returns the featured users value.
     *
     * @param   page
     *
     * @return  featured users value.
     */
    public static List<User> getFeaturedUsers(final int page) {
        List<User> userList = new ArrayList<User>();
        final Set<User> mergeSet = new HashSet<User>(userList);

        final List<FeaturedUser> featuredUserList = FeaturedUser.findAllFeaturedUser(page);

        for (final FeaturedUser fu : featuredUserList) {
            mergeSet.add(fu.user);
        }

        mergeSet.addAll(User.findPopularUsers(Constants.FETCH_USER_PAGE_ONE));
        userList = new ArrayList<User>(mergeSet);

        return userList;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  Result
     */
    @Security.Authenticated(Secured.class)
    public static Result viewOwnProfile() {
        final User user = User.findByEmail(request().username());

        if (user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }

        return ok(viewProfile.render(user, user));
    }

    /**
     * DOCUMENT ME!
     *
     * @param   userId
     *
     * @return  Result
     */
    public static Result viewProfileById(final long userId) {
        final User loginedUser = Application.getSessionUser();
        final User searchedUser = User.find.byId(userId);

        if (loginedUser == null) {
            return notFound(error.render(loginedUser, Messages.get("signin.required"),
                        Messages.get("signin.required.desc")));
        } else if (searchedUser == null) {
            return notFound(error.render(loginedUser, Messages.get("user.not.found"),
                        Messages.get("user.not.found.desc")));
        }

        return ok(viewProfile.render(loginedUser, searchedUser));
    }
}
