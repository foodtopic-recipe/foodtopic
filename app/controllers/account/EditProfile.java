package controllers.account;

import com.avaje.ebean.Ebean;
import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.Address;
import models.CookForMe;
import models.User;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import play.mvc.Security;
import views.html.account.profile.editProfile;
import views.html.error.error;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditProfile extends Controller {

    public Long userId;
	public String profilePhoto;
	public String firstName;
	public String lastName;
	public String aboutMe;
    public String gender;
	public String dateOfBirth;
    public String contactNumber;
    public String countryCode;
    public String addressLine1;
    public String addressLine2;
    public String addressLine3;
    public String postalCode;

    final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Defines a form wrapping the EditProfile class.
     */ 
    final static Form<EditProfile> editProfileForm = form(EditProfile.class);

    
	/**
     * Display the edit profile page if login
     *
     * @return edit profile page
     */
    public static Result initEditProfileForm() {
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        return ok(editProfile.render(user, editProfileForm.fill(initForm(user))));
    }
    
    
    public static EditProfile initForm(User user) {
        EditProfile value = new EditProfile();
        
        value.userId = user.userId;
        value.profilePhoto = user.profileImagePath;
        value.firstName = user.firstName;
        value.lastName = user.lastName;
        value.aboutMe = user.aboutMe;
        value.gender = user.gender;
        value.dateOfBirth = user.dateOfBirth != null ? dateFormat.format(user.dateOfBirth) : "";
        value.contactNumber = user.contactNumber;

        if(user.address != null) {
            value.countryCode = user.address.countryCode;
            value.addressLine1 = user.address.addressLine1;
            value.addressLine2 = user.address.addressLine2;
            value.addressLine3 = user.address.addressLine3;
            value.postalCode = user.address.postalCode;
        }

        return value;
    }
    
    
	/**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise
	 */
	public List<ValidationError> validate() {
        final User user = User.findByEmail(request().username());
		List<ValidationError> errors = new ArrayList<ValidationError>();

        if(StringUtils.isBlank(firstName)) {
            errors.add(new ValidationError("firstName", Messages.get("profile.firstName.required")));
        }
        if(StringUtils.isBlank(lastName)) {
            errors.add(new ValidationError("lastName", Messages.get("profile.lastName.required")));
        }

        if (CookForMe.findByUserId(user.userId).size() > 0) {
            if(StringUtils.isBlank(contactNumber)) {
                errors.add(new ValidationError("contactNumber", Messages.get("validate.required", Messages.get("profile.contactNumber"))));
            } else {
                try {
                    Integer.parseInt(contactNumber);
                } catch (NumberFormatException e) {
                    errors.add(new ValidationError("contactNumber", Messages.get("validate.not.numeric")));
                }
            }
            if(StringUtils.isBlank(countryCode)) {
                errors.add(new ValidationError("countryCode", Messages.get("validate.required", Messages.get("profile.countryCode"))));
            }
            if(StringUtils.isBlank(addressLine1)) {
                errors.add(new ValidationError("addressLine1", Messages.get("validate.required", Messages.get("profile.addressLine1"))));
            }
            if(StringUtils.isBlank(postalCode)) {
                errors.add(new ValidationError("postalCode", Messages.get("validate.required", Messages.get("profile.postalCode"))));
            }
        }

        if(StringUtils.isNotBlank(dateOfBirth)) {
            try {
                user.dateOfBirth = dateFormat.parse(dateOfBirth);
            } catch (ParseException e) {
                errors.add(new ValidationError("dateOfBirth", Messages.get("validate.not.date")));
            }
        }

        MultipartFormData formData = request().body().asMultipartFormData();
        FilePart profilePhoto = formData.getFile("profilePhoto");
        if (profilePhoto != null) {
            File file = profilePhoto.getFile();
            if(file.length() > Constants.MAX_PHOTO_FILE_SIZE) {
                errors.add(new ValidationError("profilePhoto", Messages.get("photo.maxsize")));
            }
        }
        return errors.isEmpty() ? null : errors;
	}
	
	public static Result updateProfile() {
        Form<EditProfile> filledForm = editProfileForm.bindFromRequest();
        final User user = User.findByEmail(request().username());
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        
        MultipartFormData formData = request().body().asMultipartFormData();

		if (filledForm.hasErrors()) {
            return badRequest(editProfile.render(user, filledForm));
        }
        EditProfile editProfileFilledForm = filledForm.get();

        // Additional check to determine whether login user is editing their own profile
        if(!editProfileFilledForm.userId.equals(user.userId)) {
            return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("profile.edit.invalid.rights")));
        }

        try {
            Ebean.beginTransaction();

            if(!editProfileFilledForm.aboutMe.isEmpty())
                user.aboutMe = editProfileFilledForm.aboutMe;
            if(!editProfileFilledForm.firstName.isEmpty())
                user.firstName = editProfileFilledForm.firstName;
            if(!editProfileFilledForm.lastName.isEmpty())
                user.lastName = editProfileFilledForm.lastName;
            user.gender = editProfileFilledForm.gender;
            if(!editProfileFilledForm.dateOfBirth.isEmpty())
                user.dateOfBirth = dateFormat.parse(editProfileFilledForm.dateOfBirth);
            if(!editProfileFilledForm.contactNumber.isEmpty())
                user.contactNumber = editProfileFilledForm.contactNumber;

            Address userAddress = new Address();
            if(user.address != null) {
                userAddress = user.address;
            }
            if(!editProfileFilledForm.addressLine1.isEmpty())
                userAddress.addressLine1 = editProfileFilledForm.addressLine1;
            if(!editProfileFilledForm.addressLine2.isEmpty())
                userAddress.addressLine2 = editProfileFilledForm.addressLine2;
            if(!editProfileFilledForm.addressLine3.isEmpty())
                userAddress.addressLine3 = editProfileFilledForm.addressLine3;
            if(!editProfileFilledForm.postalCode.isEmpty())
                userAddress.postalCode = editProfileFilledForm.postalCode;
            if(!editProfileFilledForm.countryCode.isEmpty())
                userAddress.countryCode = editProfileFilledForm.countryCode;

            userAddress.user = user;
            user.address = userAddress;

            FilePart profilePhoto = formData.getFile("profilePhoto");

            if (profilePhoto != null) {
                /* TODO : Should delete the old profile photo?
                if(StringUtils.isNotBlank(user.profileImagePath)) {
                    File oldProfilePhoto = new File(user.profileImagePath);
                    if (oldProfilePhoto.exists()) {
                        oldProfilePhoto.delete();
                    } else {
                        Logger.info("Attempt to remove old profile photo of : " + user.profileImagePath + " failed.");
                    }
                }*/

                String fileType = profilePhoto.getFilename().substring(profilePhoto.getFilename().lastIndexOf("."));
                String fileName = UUID.randomUUID().toString() + fileType;

                editProfileFilledForm.profilePhoto = Play.application().configuration().getString("profileImageUploadPath").replace("*USERID*", user.userId.toString()) + fileName;

                String filePath = play.Play.application().path().toString() + editProfileFilledForm.profilePhoto;
                byte[] byteFile1 = IOUtils.toByteArray(new FileInputStream(profilePhoto.getFile()));

                File directionImageFile = new File(filePath);
                FileUtils.writeByteArrayToFile(directionImageFile, byteFile1);

                user.profileImagePath = editProfileFilledForm.profilePhoto;
                Logger.debug("Profile image path : " + filePath);
            }

            user.update();
            Ebean.commitTransaction();

        } catch (Exception e) {
            Logger.error("User " + user.userId + " Edit Profile encountered exception : ", e);
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
            return badRequest(editProfile.render(user, filledForm));
        } finally {
            Ebean.endTransaction();
        }

        flash(Application.FLASH_SUCCESS_KEY, Messages.get("profile.update.success"));
        return redirect(controllers.account.routes.ViewProfile.viewOwnProfile().url());
    }
}
