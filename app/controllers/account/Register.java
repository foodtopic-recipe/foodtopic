package controllers.account;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider;
import controllers.Application;
import models.Token;
import models.Token.Type;
import models.User;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import providers.FTPasswordAuthProvider;
import providers.FTPasswordAuthProvider.FTSignup;
import views.html.account.registration.signup;
import views.html.account.registration.unverified;

/**
 * Signup to FoodTopic : save and send confirmation email.
 */
public class Register extends Controller {

    /**
     * Display the unverified screen.
     * @param passwordChange of message to display
     * (passwordChange is true if show unverified when changing of password)
     * @return unverified email screen
     */
	public static Result unverified(boolean passwordChange) {
        final User user = Application.getSessionUser();
		return ok(unverified.render(user, passwordChange, false));
	}

    /**
     * Display the signup form.
     *
     * @return signup form
     */
	public static Result signup() {
		return ok(signup.render(FTPasswordAuthProvider.SIGNUP_FORM));
	}
    
    /**
     * Save the new user.
     *
     * @return Successful page or created form if unsuccessful
     */
	public static Result save() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<FTSignup> filledForm = FTPasswordAuthProvider.SIGNUP_FORM.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(signup.render(filledForm));
		} else {
			// Everything was filled, do something with your part of the form before handling the user signup
			Logger.debug(ctx().toString());
			return UsernamePasswordAuthProvider.handleSignup(ctx());
		}
	}
    
    public static Result verify(final String token) {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Token ta = controllers.utils.Utilities.tokenIsValid(token, Type.EMAIL_VERIFICATION);
		if (ta == null) {
			return badRequest();
		}
		final String email = ta.targetUser.email;
		User.verify(ta.targetUser);
		flash(Application.FLASH_SUCCESS_KEY, Messages.get("signup.verify_email.success", email));
		if (Application.getSessionUser() != null) {
			return redirect(controllers.routes.Dashboard.index());
		} else {
			return redirect(controllers.account.routes.Login.authenticate());
		}
	}
    
    public static Result verifyEmail() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
        final User user = Application.getSessionUser();
		if (user.emailValidated) {
			// E-Mail has been validated already
			flash(Application.FLASH_SUCCESS_KEY, Messages.get("signup.verify_email.error.already_validated"));
		} else if (!StringUtils.isBlank(user.email)) {
			flash(Application.FLASH_SUCCESS_KEY, Messages.get("signup.verify_email.message.instructions_sent", user.email));
			FTPasswordAuthProvider.getProvider().sendVerifyEmailMailingAfterSignup(user, ctx());
		} else {
			flash(Application.FLASH_SUCCESS_KEY, Messages.get("signup.verify_email.error.set_email_first", user.email));
		}
		
		// Check if user is logged in
		if (PlayAuthenticate.isLoggedIn(session())) {
			return redirect(controllers.account.routes.ViewProfile.viewOwnProfile());
		} else 
			return redirect(controllers.routes.Application.index());
		
	}
	
	public static Result exists() {
		flash(Application.FLASH_ERROR_KEY, Messages.get("signup.user.exists"));
		return redirect(controllers.account.routes.Register.signup());
	}
	
}
