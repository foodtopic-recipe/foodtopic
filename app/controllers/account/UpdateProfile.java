package controllers.account;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Application;
import controllers.Secured;
import models.Address;
import models.User;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.account.profile.updateProfileModal;
import views.html.error.errorModal;

import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class UpdateProfile extends Controller {

    public Long userId;
    public String contactNumber;
    public String countryCode;
	public String addressLine1;
	public String addressLine2;
	public String addressLine3;
	public String postalCode;

    /**
     * Defines a form wrapping the UpdateProfile class.
     */
    final static Form<UpdateProfile> updateProfileForm = form(UpdateProfile.class);

    public UpdateProfile() {
        contactNumber = "";
        countryCode = "";
        addressLine1 = "";
        addressLine2 = "";
        addressLine3 = "";
        postalCode = "";
    }

    public UpdateProfile(User user) {
        if(user != null) {
            if(user.address != null) {
                addressLine1 = user.address.addressLine1;
                addressLine2 = user.address.addressLine2;
                addressLine3 = user.address.addressLine3;
                countryCode = user.address.countryCode;
                postalCode = user.address.postalCode;
            }
            contactNumber = user.contactNumber;
        }
    }

    public static Result loadProfileForCFMModal() {
        final User user = User.findByEmail(request().username());

        // Additional check to determine whether login user has the rights to update address
        if(user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(StringUtils.isBlank(user.contactNumber) || user.address == null || (user.address != null
                    && StringUtils.isBlank(user.address.addressLine1) || StringUtils.isBlank(user.address.postalCode))) {
            return ok(updateProfileModal.render(user, updateProfileForm.fill(new UpdateProfile(user))));
        }

        return ok();
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        if (StringUtils.isBlank(contactNumber)) {
            errors.add(new ValidationError("contactNumber", Messages.get("validate.required", Messages.get("profile.contactNumber"))));
        } else {
            try {
                Integer.parseInt(contactNumber);
            } catch (NumberFormatException e) {
                errors.add(new ValidationError("contactNumber", Messages.get("validate.not.numeric")));
            }
        }
        if (StringUtils.isBlank(countryCode)) {
            errors.add(new ValidationError("countryCode", Messages.get("validate.required", Messages.get("profile.countryCode"))));
        }
        if(StringUtils.isBlank(addressLine1)) {
            errors.add(new ValidationError("addressLine1", Messages.get("validate.required", Messages.get("profile.addressLine1"))));
        }
        if (StringUtils.isBlank(postalCode)) {
            errors.add(new ValidationError("postalCode", Messages.get("validate.required", Messages.get("profile.postalCode"))));
        }
        return errors.isEmpty() ? null : errors;
    }

    public static Result updateProfileForCFM() {
        Form <UpdateProfile> updateForm = updateProfileForm.bindFromRequest();

        try {
            final User user = User.findByEmail(request().username());
            if(user == null) {
                return badRequest(Messages.get("user.not.found"));
            } else if (updateForm.hasErrors()) {
                return badRequest(updateForm.errorsAsJson().toString());
            } else {
                UpdateProfile updateProfileFilledForm = updateForm.get();
                Address userAddress = new Address();
                if(user.address != null) {
                    userAddress = user.address;
                }
                userAddress.addressLine1 = updateProfileFilledForm.addressLine1;
                userAddress.addressLine2 = updateProfileFilledForm.addressLine2;
                userAddress.addressLine3 = updateProfileFilledForm.addressLine3;
                userAddress.postalCode = updateProfileFilledForm.postalCode;
                userAddress.countryCode = updateProfileFilledForm.countryCode;
                user.contactNumber = updateProfileFilledForm.contactNumber;
                userAddress.user = user;
                user.address = userAddress;
                user.save();

                ObjectNode jsonResult = Json.newObject();
                jsonResult.put("status", "success");
                return ok(jsonResult.toString());
            }
        } catch (Exception e) {
            Logger.error("Update profile for Cook For Me encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
	}
}
