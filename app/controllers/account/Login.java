package controllers.account;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider;
import controllers.Application;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import providers.FTPasswordAuthProvider;
import providers.FTPasswordAuthProvider.FTLogin;
import views.html.account.login.login;
import views.html.account.login.loginModal;

/**
 * Login to FoodTopic : Class used by Login Form.
 */
public class Login extends Controller {
	
	/**
	 * Handle login form submission.
	 *
	 * @return Dashboard if auth OK or login form if auth failed
	 */
	public static Result authenticate() {
		boolean isAjax = request().body().asFormUrlEncoded().get("ajax") != null 
				&& request().body().asFormUrlEncoded().get("ajax")[0].toString().equals("1") ? true:false;
		Form <FTLogin> loginForm = FTPasswordAuthProvider.LOGIN_FORM.bindFromRequest();

		if (isAjax) {
        	if (loginForm.hasErrors()) 
            	return badRequest(loginForm.errorsAsJson().toString());
            else {
            	UsernamePasswordAuthProvider.handleLogin(ctx());
            	if (flash().get(Application.FLASH_ERROR_KEY) == null) {
            		ObjectNode jsonResult = Json.newObject();
            		jsonResult.put("status", "success");
            		return ok(jsonResult.toString());
            	} else {
            		ObjectNode jsonResult = Json.newObject();
            		jsonResult.put("flash", flash().get(Application.FLASH_ERROR_KEY));
            		return badRequest(jsonResult.toString());
            	}
            }
        }
        else {
        	if (loginForm.hasErrors()) 
            	return badRequest(login.render(loginForm));
            else 
            	return UsernamePasswordAuthProvider.handleLogin(ctx());
        }
    }
	
	
	/**
     * Render Login Page
     *
     * @return Login Page
     */
	public static Result login() {
		return ok(login.render(FTPasswordAuthProvider.LOGIN_FORM));
	}
	
	/**
     * Render Login Page
     *
     * @return Login Page
     */
	public static Result loginModal() {
		return ok(loginModal.render(FTPasswordAuthProvider.LOGIN_FORM));
	}
	

	/**
     * Logout and clean the session.
     *
     * @return Index page
     */
	public static Result logout() {
		session().clear();
		PlayAuthenticate.logout(session());
		flash(controllers.Application.FLASH_SUCCESS_KEY, Messages.get("logout.success"));
		return Application.GO_HOME;
	}
}
