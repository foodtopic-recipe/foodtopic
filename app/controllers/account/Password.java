package controllers.account;

import static play.data.Form.form;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Utilities;
import models.LinkedAccount;
import models.Token;
import models.User;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import providers.FTLoginAuthUser;
import providers.FTPasswordAuthProvider;
import providers.FTPasswordAuthProvider.FTpasswordChange;
import providers.FTPasswordAuthProvider.FTpasswordForgot;
import providers.FTPasswordAuthProvider.FTpasswordReset;
import providers.FTPasswordAuthUser;
import views.html.account.password.change;
import views.html.account.password.changePasswordModal;
import views.html.account.password.forgotPassword;
import views.html.account.password.forgotPasswordSent;
import views.html.account.password.no_token_or_invalid;
import views.html.account.password.resetPassword;
import views.html.account.registration.unverified;


/**
 * Controller for password handling.
 */
public class Password extends Controller {
	
	public static class PasswordReset extends FTpasswordReset {

		public PasswordReset() {
		}

		public PasswordReset(final String token) {
			this.token = token;
		}

		public String token;

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}
	}

	private static final Form<PasswordReset> PASSWORD_RESET_FORM = form(PasswordReset.class);
	
	@Security.Authenticated(Secured.class)
	public static Result changeForm() {
        final User user = User.findByEmail(request().username());
	    
		if (!user.emailValidated) {
		    if(user.validationEmailSent == null || Utilities.getPrevDateByMins(15).after(user.validationEmailSent)) {
			    FTPasswordAuthProvider.getProvider().sendVerifyEmailMailingAfterSignup(user, ctx());
		    	return ok(unverified.render(user, true, false));
		    } else {
			    return ok(unverified.render(user, false, true));
		    }
		} else {
			return ok(change.render(user, FTPasswordAuthProvider.PASSWORD_CHANGE_FORM, isOldPasswordRequired(user)));
		}
	}
	
	@Security.Authenticated(Secured.class)
	public static Result changePasswordModal() {
        final User user = User.findByEmail(request().username());
		return ok(changePasswordModal.render(user, FTPasswordAuthProvider.PASSWORD_CHANGE_FORM, isOldPasswordRequired(user)));
	}
	
	@Security.Authenticated(Secured.class)
	public static Result changePassword() {
        final User user = User.findByEmail(request().username());
		FTLoginAuthUser loginAuthUser = new FTLoginAuthUser(user.email);
		Form<FTpasswordChange> passwordChangeForm = FTPasswordAuthProvider.PASSWORD_CHANGE_FORM.bindFromRequest();
        
        boolean oldPasswordRequired = false;
    	String oldPassword = null;
    	UsernamePasswordAuthUser oldPasswordAuth = null;
        
        if (passwordChangeForm.hasErrors()) {
            // User did not fill everything properly
            return badRequest(passwordChangeForm.errorsAsJson().toString());
		} else {
            FTpasswordChange passwordChangeFilledForm = passwordChangeForm.get();
            
            if (isOldPasswordRequired(user)) {
                oldPasswordRequired = true;
                oldPassword = passwordChangeFilledForm.currentPassword;
                oldPasswordAuth = new FTPasswordAuthUser(oldPassword);
            }
		
			final String newPassword = passwordChangeForm.get().newPassword;
			UsernamePasswordAuthUser newPasswordAuth = new FTPasswordAuthUser(newPassword);
			
			if(!StringUtils.isBlank(newPasswordAuth.getHashedPassword()) && oldPasswordRequired) {
                if(oldPasswordAuth.getHashedPassword().equals(newPasswordAuth.getHashedPassword())) {
                    passwordChangeForm.reject("currentPassword", Messages.get("password.current.new.same"));
                    return badRequest(passwordChangeForm.errorsAsJson().toString());
                }
                for (final LinkedAccount acc : user.linkedAccounts) {
                    if (FTPasswordAuthProvider.getProvider().getKey().equals(acc.providerKey)) {
                        if (!oldPasswordAuth.checkPassword(acc.providerUserId, oldPasswordAuth.getPassword())) {
                            passwordChangeForm.reject("currentPassword", Messages.get("password.current.wrong"));
                            return badRequest(passwordChangeForm.errorsAsJson().toString());
                        }
                    }
                }
            }
			
			user.changePassword(newPasswordAuth,true);
			ObjectNode jsonResult = Json.newObject();
			jsonResult.put("status", "success");
			flash(Application.FLASH_SUCCESS_KEY, Messages.get("password.change.success"));
			return ok(jsonResult.toString());
		}
	}
	
	public static boolean isOldPasswordRequired(User user) {
		final FTLoginAuthUser loginAuthUser = new FTLoginAuthUser(user.email);
		boolean oldPasswordRequired = false;
		
		for (final LinkedAccount acc : user.linkedAccounts) {
			if (FTPasswordAuthProvider.getProvider().getKey().equals(acc.providerKey)) {
				oldPasswordRequired = true;
			}
		}
		return oldPasswordRequired;
	}
	
	public static Result forgot() {
        final User user = User.findByEmail(request().username());
		return ok(forgotPassword.render(user, FTPasswordAuthProvider.PASSWORD_FORGOT_FORM));
	}
	
	public static Result doForgot() {
        User loginedUser = User.findByEmail(request().username());
		final Form<FTpasswordForgot> filledForm = FTPasswordAuthProvider.PASSWORD_FORGOT_FORM.bindFromRequest();
		if (filledForm.hasErrors()) {
			// User did not fill everything properly
			return badRequest(forgotPassword.render(loginedUser, filledForm));
		} else {
			if (loginedUser != null) {
				FTPasswordAuthProvider.getProvider().sendPasswordResetMailing(loginedUser, ctx());
			} else {
				User user = User.findByAuthUserIdentity(new FTLoginAuthUser(filledForm.get().email));
				if (user != null)
					FTPasswordAuthProvider.getProvider().sendPasswordResetMailing(user, ctx());
				else {
					filledForm.reject("email", Messages.get("account.notfound"));
					return badRequest(forgotPassword.render(user, filledForm));
				}
			}
		}
		return ok(forgotPasswordSent.render(loginedUser));
	}
	
	public static Result resetPassword(final String token) {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Token ta = controllers.utils.Utilities.tokenIsValid(token, Token.Type.PASSWORD_RESET);
		if (ta == null) {
			return badRequest(no_token_or_invalid.render());
		}

		return ok(resetPassword.render(PASSWORD_RESET_FORM.fill(new PasswordReset(token))));
	}
	
	public static Result doResetPassword() {
		com.feth.play.module.pa.controllers.Authenticate.noCache(response());
		final Form<PasswordReset> filledForm = PASSWORD_RESET_FORM.bindFromRequest();
		
		if (filledForm.hasErrors()) {
			return badRequest(resetPassword.render(filledForm));
		} else {
			final String token = filledForm.get().token;
			final String newPassword = filledForm.get().newPassword;

			final Token ta = controllers.utils.Utilities.tokenIsValid(token, Token.Type.PASSWORD_RESET);
			if (ta == null) {
				return badRequest(no_token_or_invalid.render());
			}
			
			final User u = ta.targetUser;
			try {
				// Pass true for the second parameter if you want to
				// automatically create a password and the exception never to
				// happen
				u.changePassword(new FTPasswordAuthUser(newPassword),false);
			} catch (final RuntimeException re) {
				flash(Application.FLASH_SUCCESS_KEY, Messages.get("password.reset.no_password_account"));
			}
			
			final boolean login = FTPasswordAuthProvider.getProvider().isLoginAfterPasswordReset();
			if (login) {
				// automatically log in
				flash(Application.FLASH_SUCCESS_KEY, Messages.get("password.reset.success.auto_login"));
                
				return PlayAuthenticate.loginAndRedirect(ctx(), new FTPasswordAuthUser(filledForm.get().confirmPassword));
			} else {
				// send the user to the login page
				flash(Application.FLASH_SUCCESS_KEY, Messages.get("password.reset.success.manual_login"));
			}
			
			return redirect(controllers.routes.Application.index());
		}
	}
}
