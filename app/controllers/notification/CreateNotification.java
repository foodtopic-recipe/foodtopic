package controllers.notification;

import controllers.Secured;
import controllers.utils.Constants;
import models.Notification;
import models.SubsetCode;
import models.User;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class CreateNotification extends Controller {

    public static void newNotification(User user, User notifier, String description, String url, int notificationType) {
        Notification notification = new Notification();
        notification.user = user;
        notification.notificationType = SubsetCode.getSubsetCodeById(notificationType);
        notification.notifier = notifier;
        notification.description = description;
        notification.url = url;
        notification.notificationStatus = Constants.NOTIFICATION_STATUS_UNREAD;

        notification.save();
    }

    public static void recipeNotification(User user, User notifier, String description, String url) {
        try {
            newNotification(user, notifier, description, url, Constants.NOTIFICATION_TYPE_NEWITEM);
        } catch (Exception e) {
            Logger.error("Creating Recipe Notification for user: " + user.userId + " encountered error : ", e);
        } 
    }

    public static void reviewNotification(User user, User notifier, String description, String url) {
        try {
            newNotification(user, notifier, description, url, Constants.NOTIFICATION_TYPE_REVIEW);
        } catch (Exception e) {
            Logger.error("Creating Review Notification for user: " + user.userId + " encountered error : ", e);
        }
    }

    public static void cookForMeNotification(User user, User notifier, String description, String url) throws Exception {
        try {
            newNotification(user, notifier, description, url, Constants.NOTIFICATION_TYPE_ORDER);
        } catch (Exception e) {
            Logger.error("Creating CookForMe Notification for user: " + user.userId + " encountered error : ", e);
            throw e;
        }
    }

    public static void commentNotification(User user, User notifier, String description, String url) {
        try {
            newNotification(user, notifier, description, url, Constants.NOTIFICATION_TYPE_COMMENT);
        } catch (Exception e) {
            Logger.error("Creating Comment Notification for user: " + user.userId + " encountered error : ", e);
        }
    }
    
    public static void followerNotification(User user, User notifier, String description, String url) {
        try {
            newNotification(user, notifier, description, url, Constants.NOTIFICATION_TYPE_FOLLOWED);
        } catch (Exception e) {
            Logger.error("Creating Follower Notification for user: " + user.userId + " encountered error : ", e);
        } 
    }
    
    public static void favouritesNotification(User user, User notifier, String description, String url) {
        try {
            newNotification(user, notifier, description, url, Constants.NOTIFICATION_TYPE_FAVOURITES);
        } catch (Exception e) {
            Logger.error("Creating Favourites Notification for user: " + user.userId + " encountered error : ", e);
        } 
    }
    
    public static void likedNotification(User user, User notifier, String description, String url) {
        try {
            newNotification(user, notifier, description, url, Constants.NOTIFICATION_TYPE_LIKED);
        } catch (Exception e) {
            Logger.error("Creating Liked Notification for user: " + user.userId + " encountered error : ", e);
        } 
    }
}
