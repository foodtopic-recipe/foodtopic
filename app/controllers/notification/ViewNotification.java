package controllers.notification;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.Notification;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.notification.listNotification;
import views.html.notification.listNotificationRow;

import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class ViewNotification extends Controller {
    
	public static List<Notification> findNotificationByUserForNotificationDropdown() {
		List<Notification> notificationList = new ArrayList<Notification>();
        final User user = Application.getSessionUser();

        if(user != null) {
            notificationList = Notification.findAllNotificationByUser(user.userId, Constants.MAX_NOTIFICATION_DROPDOWN_FETCH_LIMIT, 0, true);
        }

		return notificationList;
	}
	
	/**
	 * Display the view notification page if login
	 *
	 * @return view notification page
	 */
    public static Result initViewNotificationForm() {
        final User user = Application.getSessionUser();
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        
        readAllNotification();
        
        return ok(listNotification.render(user, "Notifications", Notification.findAllNotificationByUser(user.userId, Constants.MAX_NOTIFICATION_FETCH_LIMIT, 0, true)));
    }
    
    public static Result readAllNotification() {
        final User user = Application.getSessionUser();

        if(user != null)
        	Notification.userReadAllUnreadNotification(user.userId);

        ObjectNode jsonResult = Json.newObject();
        jsonResult.put("status", "success");
        return ok(jsonResult.toString());
    }
    
    public static int findNumberOfNotifications() {
        final User user = Application.getSessionUser();

        if(user == null)
            return 0;
        else
            return Notification.findAllNotificationByUser(user.userId, Constants.MAX_NOTIFICATION_FETCH_LIMIT, 0, false).size();
    }
    
    public static int findNumberOfUnreadNotifications() {
        final User user = Application.getSessionUser();

        if(user == null)
            return 0;
        else
            return Notification.findAllUnreadNotificationByUser(user.userId, 0, false).size();
    }
    
    public static int findNumberOfReadNotifications() {
        final User user = Application.getSessionUser();

        if(user == null)
            return 0;
        else
            return Notification.findAllReadNotificationByUser(user.userId, 0, false).size();
    }

    public static List<Notification> getNotificationByTypeDescription(User user, int notificationType, String description) {
        List<Notification> notificationList = new ArrayList<Notification>();

        if(user != null) {
            notificationList = Notification.findAllNotificationByTypeDescription(user.userId, notificationType, description);
        }

        return notificationList;
    }
    
    public static Result getNextNotificationList() {
        DynamicForm data = form().bindFromRequest();
        List<Notification> notificationList;
        final User user = Application.getSessionUser();

		int page = 0;
		
		try {
		    page = Integer.parseInt(data.get("page"));
        } catch (Exception e) {
			Logger.error("Get next list encountered error : ", e);
			return notFound(error.render(user, Messages.get("error.404"), Messages.get("error.404.desc")));
        }

        if(user != null) {
        	notificationList = Notification.findAllNotificationByUser(user.userId, Constants.MAX_NOTIFICATION_FETCH_LIMIT, page, true);
        } else 
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));

        return ok(listNotificationRow.render(notificationList, false));
    }
}
