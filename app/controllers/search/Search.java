package controllers.search;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import controllers.Application;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.Recipe;
import models.RecipeCategory;
import models.RecipeIngredient;
import models.RecipeTag;
import models.Tag;
import models.User;
import play.data.Form;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.recipe.listRecipe;

public class Search extends Controller {
    
	private static int MAX_AUTOCOMPLETE_RESULT = 10;
	
	@Constraints.Required
    public String searchInput;
    
    /**
     * Defines a form wrapping the Search class.
     */ 
    final static Form<Search> searchForm = form(Search.class);

    // Use as a method to get search form at main.scala.html
    public static Form<Search> getSearchForm() {
        return searchForm;
    }

    // Get latest recipes based on date published
    public static List<Recipe> listLatestRecipes() {
        return Recipe.findLatestPublishedPublicRecipe(Constants.FETCH_RECIPE_PAGE_ONE);
    }
    
    public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
		if (StringUtils.isBlank(searchInput)) {
			errors.add(new ValidationError("searchInput", Messages.get("search.input.required")));
	    }
		
        return errors.isEmpty() ? null : errors;
	}
    
    public static Result getTagsAutoComplete(String input) {
    	List<Tag> tagList = Tag.listAllTags(input, MAX_AUTOCOMPLETE_RESULT);
		HashMap<String, String> data = new HashMap<String, String>();
		for(Tag tag : tagList) {
			data.put(tag.tagId.toString(), tag.tagName);
		}
		return ok(Json.toJson(data));
    }
    
    public static Result getIngredientNameAutoComplete(String input) {
    	List<RecipeIngredient> ingredientList = RecipeIngredient.searchIngredientContainingName(input, MAX_AUTOCOMPLETE_RESULT);
		HashMap<String, String> data = new HashMap<String, String>();
		for(RecipeIngredient ri : ingredientList) {
			data.put(ri.ingredientName, ri.ingredientName);
		}
		return ok(Json.toJson(data));
    }
    
    public static Result getIngredientQuantityAutoComplete(String input) {
    	List<RecipeIngredient> ingredientList = RecipeIngredient.searchIngredientContainingQuantity(input, MAX_AUTOCOMPLETE_RESULT);
		HashMap<String, String> data = new HashMap<String, String>();
		for(RecipeIngredient ri : ingredientList) {
			data.put(ri.ingredientQuantity, ri.ingredientQuantity);
		}
		return ok(Json.toJson(data));
    }
    
    public static Result getIngredientUnitAutoComplete(String input) {
    	List<RecipeIngredient> ingredientList = RecipeIngredient.searchIngredientContainingUnit(input, MAX_AUTOCOMPLETE_RESULT);
		HashMap<String, String> data = new HashMap<String, String>();
		for(RecipeIngredient ri : ingredientList) {
			data.put(ri.ingredientUnit, ri.ingredientUnit);
		}
		return ok(Json.toJson(data));
    }
    
    public static Result getSearchAutoComplete(String input) {
    	List<Tag> tagList = Tag.listAllTags(input, MAX_AUTOCOMPLETE_RESULT);
		HashMap<String, String> data = new HashMap<String, String>();
		for(Tag tag : tagList) {
			data.put(tag.tagId.toString(), tag.tagName);
		}
		return ok(Json.toJson(data));
    }
	
    public static Result search() {
        Form<Search> filledForm = searchForm.bindFromRequest();
        
        final User user = Application.getSessionUser();
        
        if (filledForm.hasErrors()) {
            List<Recipe> recipeList = new ArrayList<Recipe>();
            Set<Recipe> mergeSet = new HashSet<Recipe>(recipeList);
            
            //TODO: Enhance on this portion to add perhaps 12 popular mix with random new recipes
            mergeSet.addAll(listLatestRecipes());
            mergeSet.addAll(Recipe.findAllPublishedPublicRecipe(Constants.FETCH_RECIPE_PAGE_ONE));
            recipeList = new ArrayList<Recipe>(mergeSet);
            return ok(listRecipe.render(user, Utilities.checkEnglishPlural(recipeList.size(), Messages.get("search.result")), recipeList, 12, true));
        }
        
        Search searchFilledForm = filledForm.get();
        List<Recipe> recipeList = new ArrayList<Recipe>();
        Set<Recipe> mergeSet = new HashSet<Recipe>(recipeList);
        mergeSet.addAll(Recipe.searchPublishedPublicRecipeByName(searchFilledForm.searchInput, Constants.FETCH_RECIPE_PAGE_ONE));
        mergeSet.addAll(Recipe.searchPublishedPublicRecipeByIngredient(searchFilledForm.searchInput, Constants.FETCH_RECIPE_PAGE_ONE));
        mergeSet.addAll(Recipe.searchPublishedPublicRecipeByDirection(searchFilledForm.searchInput, Constants.FETCH_RECIPE_PAGE_ONE));
        mergeSet.addAll(RecipeCategory.searchPublishedPublicRecipeByCategoryName(searchFilledForm.searchInput, Constants.FETCH_RECIPE_PAGE_ONE));
        mergeSet.addAll(RecipeTag.searchPublishedPublicRecipeByTagName(searchFilledForm.searchInput, Constants.FETCH_RECIPE_PAGE_ONE));
        mergeSet.addAll(Recipe.searchPublishedPublicRecipeByUserName(searchFilledForm.searchInput, Constants.FETCH_RECIPE_PAGE_ONE));
        recipeList = new ArrayList<Recipe>(mergeSet);
        
        return ok(listRecipe.render(user, Utilities.checkEnglishPlural(recipeList.size(), Messages.get("search.result")), recipeList, 12, true));
    }
    
    public static Result searchTag(long tagId) {
        final User user = Application.getSessionUser();
        
        List<Recipe> recipeList = new ArrayList<Recipe>();
        Set<Recipe> mergeSet = new HashSet<Recipe>(recipeList);
        mergeSet.addAll(RecipeTag.findAllPublishedPublicRecipeByTag(tagId, Constants.FETCH_RECIPE_PAGE_ONE));
        recipeList = new ArrayList<Recipe>(mergeSet);
        
        return ok(listRecipe.render(user, Utilities.checkEnglishPlural(recipeList.size(), Messages.get("search.result")), recipeList, 12, true));
    }
}