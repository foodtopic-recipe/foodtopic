package controllers.search;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import providers.FTPasswordAuthProvider;
import controllers.Secured;
import controllers.utils.Utilities;

import models.Recipe;
import models.User;

import org.apache.commons.lang.StringUtils;

import play.data.validation.ValidationError;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.i18n.Messages;
import play.mvc.Security;
import play.mvc.Result;
import play.data.Form;

import views.html.account.registration.unverified;
import views.html.search.advanceSearch;

@Security.Authenticated(Secured.class)
public class AdvanceSearch extends Controller {
    
	@Constraints.Required
    public String searchInput;
    
    /**
     * Defines a form wrapping the AdvanceSearch class.
     */ 
    final static Form<AdvanceSearch> advanceSearchForm = form(AdvanceSearch.class);
    
    public static Result initAdvanceSearchForm() {
        final User user = User.findByEmail(request().username());
        
        if (!user.emailValidated) {
            if(user.validationEmailSent == null || Utilities.getPrevDateByMins(15).after(user.validationEmailSent)) {
                FTPasswordAuthProvider.getProvider().sendVerifyEmailMailingAfterSignup(user, ctx());
                return ok(unverified.render(user, false, false));
            } else {
                return ok(unverified.render(user, false, true));
            }
        } else {
            return ok(advanceSearch.render(user, advanceSearchForm.fill(new AdvanceSearch())));
        }
    }
    
    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        
        if (StringUtils.isBlank(searchInput)) {
            errors.add(new ValidationError("searchInput", Messages.get("search.input.required")));
        }
		
        return errors.isEmpty() ? null : errors;
    }
    
    public static Result advanceSearch() {
        Form<AdvanceSearch> filledForm = advanceSearchForm.bindFromRequest();
        final User user = User.findByEmail(request().username());
        
        if (filledForm.hasErrors()) {
            return ok();
        }
        
        AdvanceSearch advanceSearchFilledForm = filledForm.get();
        List<Recipe> recipeList = new ArrayList<Recipe>();
        Set<Recipe> mergeSet = new HashSet<Recipe>(recipeList);
        
        return ok();
    }
}