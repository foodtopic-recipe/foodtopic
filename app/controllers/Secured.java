package controllers;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;

import models.User;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

public class Secured extends Security.Authenticator {

	@Override
	public String getUsername(Http.Context ctx) {
		AuthUser ident = PlayAuthenticate.getUser(ctx);
		User user = User.findByAuthUserIdentity(ident);
		return user == null ? null : user.email;
	}

	@Override
	public Result onUnauthorized(Http.Context ctx) {
		return redirect(controllers.routes.Application.index());
	}
}
