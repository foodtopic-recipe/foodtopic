package controllers.follower;

import controllers.Secured;
import models.Follower;
import models.User;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.follower.viewFollowers;

@Security.Authenticated(Secured.class)
public class ViewFollower extends Controller {
	
	/**
	 * Display the view follower page if login
	 *
	 * @return view follower page
	 */
    public static Result initViewFollowerForm() {
        final User user = User.findByEmail(request().username());
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        return ok(viewFollowers.render(user, "Followers", Follower.findByActiveFollowing(user.userId), true));
    }
    
    public static Result initViewFollowingForm() {
        final User user = User.findByEmail(request().username());
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        return ok(viewFollowers.render(user, "Following", Follower.findByActiveFollower(user.userId), false));
    }
    
    public static int findNumberOfFollowers(long followerId) {
        return Follower.findByFollower(followerId).size();
    }

    public static int findNumberOfActiveFollowers(long followerId) {
        return Follower.findByActiveFollower(followerId).size();
    }

    public static int findNumberOfFollowing(long followingId) {
        return Follower.findByFollowing(followingId).size();
    }
    
    public static int findNumberOfActiveFollowing(long followingId) {
        return Follower.findByActiveFollowing(followingId).size();
    }
}
