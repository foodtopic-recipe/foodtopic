package controllers.follower;

import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.utils.Constants;
import models.Follower;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditFollower extends Controller {

	// Create join between user and user~
	public static Result followUser() {
		try {
			DynamicForm data = form().bindFromRequest();
			long followingId = Long.parseLong(data.get("userId"));

			User user = User.findByEmail(request().username());
			User following = User.find.byId(followingId);

			if (user == null) {
				return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
			} else if (following == null) {
				return badRequest(Messages.get("user.not.found"));
			} else if (following != null) {
				Follower follower = Follower.checkFollowingRelationExists(user.userId, following.userId);

				if (following.equals(user)) {
					return badRequest(Messages.get("cannot.follow.yourself"));
				} else if (follower == null) {
					Follower newFollower = new Follower();
					newFollower.follower = user;
					newFollower.following = following;
					newFollower.followerStatus = Constants.FOLLOW;

					user.followerList.add(newFollower);
					user.update();

					CreateNotification.followerNotification(following, user, Messages.get("notification.followed"), null);
				} else if (follower != null && follower.followerStatus == Constants.UNFOLLOW) {
					follower.followerStatus = Constants.FOLLOW;
					//follower.update();

					user.followerList.add(follower);
					user.update();
					
				} else {
					return badRequest(Messages.get("already.following"));
				}
			}
			return ok(Messages.get("user.follow.success"));

		} catch (Exception e) {
			Logger.error("Following user encountered error : ", e);
			return badRequest(Messages.get("error.technical"));
		}
	}

	public static Result unfollowUser() {
		try {
			DynamicForm data = form().bindFromRequest();
			long unfollowingId = Long.parseLong(data.get("userId"));

			User user = User.findByEmail(request().username());
			User unfollowing = User.find.byId(unfollowingId);

			if (user == null) {
				return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
			} else if (unfollowing == null) {
				return badRequest(Messages.get("not.following.user"));
			} else if (unfollowing != null) {
				Follower follower = Follower.checkFollowingActiveRelationExists(user.userId, unfollowing.userId);
				if (follower != null) {
					follower.followerStatus = Constants.UNFOLLOW;
					follower.update();
					
					user.followerList.remove(follower);
					user.update();
				} else
					return badRequest(Messages.get("not.following.user"));
			}
			return ok(Messages.get("user.unfollow.success"));

		} catch (Exception e) {
			Logger.error("Following user encountered error : ", e);
			return badRequest(Messages.get("error.technical"));
		}
	}
}
