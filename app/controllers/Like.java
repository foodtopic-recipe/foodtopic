package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.notification.CreateNotification;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import models.Recipe;
import models.RecipeLikes;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;

import static play.data.Form.form;

public class Like extends Controller {

	@Security.Authenticated(Secured.class)
	public static Result recipeLike() {
		DynamicForm data = form().bindFromRequest();
		long recipeId = 0;

		String rawRecipeId = data.get("recipeId");
		try {
			recipeId = Long.parseLong(rawRecipeId);
		} catch (Exception e) {
			Logger.error("Creation of likes join : " + rawRecipeId + " encountered error : ", e);
			return badRequest(Messages.get("error.technical"));
		}

		try {
			final User user = User.findByEmail(request().username());
			Recipe recipe = Recipe.find.byId(recipeId);

			// Validate whether user & recipe is valid
			if (user == null) {
				return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
			} else if (recipe == null) {
				return badRequest(Messages.get("recipe.not.found"));
			}

			RecipeLikes likes = RecipeLikes.findAllUserAndRecipeLikes(user.userId, recipe.recipeId);
			if (likes != null) {
				likes.likeStatus = likes.likeStatus == Constants.LIKE ? Constants.UNLIKE : Constants.LIKE;
				likes.update();
			} else {
				// Create new RecipeLikes
				likes = new RecipeLikes();
				likes.user = user;
				likes.recipe = recipe;
				likes.likeStatus = Constants.LIKE;
				
				//Update user's reputation based on new like points (Cannot self like recipe to gain reputation points)
				if(recipe.user.userId != user.userId) {
					recipe.user.reputationPoints += Reputation.getNewLikeReputationPoints(user.reputationPoints);
				    
					CreateNotification.likedNotification(recipe.user, user, Messages.get("notification.liked", controllers.recipe.routes.ViewRecipe.initViewRecipeForm(recipeId).url(), recipe.recipeName), null);
                }
				recipe.recipeLikesList.add(likes);
				recipe.update();
			}

			ObjectNode jsonResult = Json.newObject();
			jsonResult.put("recipeId", recipe.recipeId);
			return ok(jsonResult.toString());
		} catch (Exception e) {
			Logger.error("Recipe likes encountered error : ", e);
			return badRequest(Messages.get("error.technical"));
		}
	}

	// Delete joins between recipe and recipe likes
	@Security.Authenticated(Secured.class)
	public static Result recipeUnlike() {
		DynamicForm data = form().bindFromRequest();
		long recipeLikesId = 0;

		String rawRecipeLikesId = data.get("recipeLikesId");
		try {
			recipeLikesId = Long.parseLong(rawRecipeLikesId);
		} catch (Exception e) {
			Logger.error("Deletion of likes join : " + rawRecipeLikesId + " encountered error : ", e);
			return badRequest(Messages.get("error.technical"));
		}

		try {
			final User user = User.findByEmail(request().username());
			RecipeLikes likes = RecipeLikes.find.byId(recipeLikesId);

			// Validate whether user & recipe likes is valid
			if (user == null) {
				return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
			} else if (likes != null) {
				// Additional check to determine whether login user has the
				// rights to edit this recipe likes
				if (!likes.user.equals(user))
					return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("likes.delete.invalid.rights")));
				else {
					likes.likeStatus = likes.likeStatus == Constants.LIKE ? Constants.UNLIKE : Constants.LIKE;
					likes.update();
				}
			} else {
				return badRequest(Messages.get("likes.not.found"));
			}

			return ok(Messages.get("likes.remove.success"));

		} catch (Exception e) {
			Logger.error("Recipe Likes deletion encountered error : ", e);
			return badRequest(Messages.get("error.technical"));
		}
	}

	public static boolean getUserLikedRecipe(long recipeId) {
		RecipeLikes likes = null;
		try {
			User user = Application.getSessionUser();
			if (user != null) {
				likes = RecipeLikes.findUserAndRecipeActiveLikes(user.userId, recipeId);
			}
		} catch (Exception e) {
			Logger.error("Get User Recipe Likes encountered error : ", e);
			return false;
		}
		return likes != null ? true : false;
	}

	public static int getRecipeLikes(long recipeId) {
		return RecipeLikes.findRecipeLikesCount(recipeId);
	}

}
