package controllers.cookForMe;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Application;
import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.notification.ViewNotification;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import providers.FTPasswordAuthProvider;
import views.html.cookForMe.cookForMeOrderModal;
import views.html.error.error;
import views.html.error.errorModal;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditCookForMeOrder extends Controller {

    private static String CRUD = Messages.get("update");
    public String bookingDate;
    public String collectionMode;
    public String paymentType;
    public Integer servings;
    public Double cookForMeCost;
    public Double deliveryFee;
    public String remarks;

    private static CookForMeOrders cookForMeOrder;
    private static Date minBookingDate;
    private static SubsetCode collectionModeCode, paymentTypeCode;

    /**
     * Defines a form wrapping the Create Cook For Me Order class.
     */
    final static Form<EditCookForMeOrder> cookForMeOrderForm = form(EditCookForMeOrder.class);

    public EditCookForMeOrder() {
        bookingDate = "";
        servings = 0;
        paymentType = Constants.SUBSETCODE_PAYMENT_TYPE_CASH.toString();
        collectionMode = "";
        cookForMeCost = 0.0;
        deliveryFee = 0.0;
        remarks = "";
    }

    public EditCookForMeOrder(CookForMeOrders order) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, Constants.MIN_ORDER_BOOKING_DATE_IN_ADVANCE);
        bookingDate = Constants.dateFormat.format(order.bookingDate);
        minBookingDate = c.getTime();
        servings = order.servings;
        paymentType = Constants.SUBSETCODE_PAYMENT_TYPE_CASH.toString();
        remarks = "";
        if(order != null) {
            cookForMeOrder = order;
            collectionMode = Utilities.spiltStringByComma(order.cookForMe.collectionMode)[0];
            cookForMeCost = order.cookForMe.cookingCost;
            deliveryFee = order.cookForMe.deliveryFee;
        }
    }

    public static Result initUpdateOrderModal(long orderId) {
        final User user = User.findByEmail(request().username());
        CookForMeOrders order = CookForMeOrders.find.byId(orderId);

        // Additional check to determine whether login user has the rights to edit their order
        if(user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(order == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("cookForMeOrder.not.found"), Messages.get("cookForMeOrder.not.found.desc")));
        } else if(order.user.userId != user.userId) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("cookForMeOrder.edit.invalid.rights")));
        } else if(order.cookForMe.recipe.user.equals(user)) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("cookForMe.updateOrder.fail"), Messages.get("cookForMe.same.user.found")));
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, Constants.MIN_ORDER_AMEND_DATE_IN_ADVANCE);
            if(c.getTime().after(order.bookingDate))
                return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, "", Messages.get("cookForMeOrder.edit.duration.past")));
        }

		return ok(cookForMeOrderModal.render(user, CRUD, order.cookForMe.recipe, cookForMeOrderForm.fill(new EditCookForMeOrder(order))));
	}

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        if (StringUtils.isBlank(bookingDate)) {
            errors.add(new ValidationError("bookingDate", Messages.get("validate.required", Messages.get("cookForMe.bookingDate"))));
        } else if(StringUtils.isNotBlank(bookingDate)) {
            try {
                if(Constants.dateFormat.parse(bookingDate).before(minBookingDate)) {
                    errors.add(new ValidationError("bookingDate", Messages.get("cookForMe.bookingDate.future")));
                }
            } catch (ParseException e) {
                errors.add(new ValidationError("bookingDate", Messages.get("validate.not.date")));
            }
        }

        try {
            if (servings == null || servings <= 0) {
                errors.add(new ValidationError("servings", Messages.get("validate.required", Messages.get("recipe.servings"))));
            }
        } catch (NumberFormatException e) {
            errors.add(new ValidationError("servings", Messages.get("validate.not.numeric")));
        }

        if (StringUtils.isBlank(collectionMode)) {
            errors.add(new ValidationError("collectionMode", Messages.get("validate.required", Messages.get("cookForMe.collectionMode"))));
        } else {
            try {
                collectionModeCode = SubsetCode.validateSubsetCode(Constants.COLLECTION_MODE, Integer.parseInt(collectionMode));
                if (collectionModeCode == null) {
                    errors.add(new ValidationError("collectionMode", Messages.get("invalid.code", Messages.get("cookForMe.collectionMode"))));
                }
            } catch (NumberFormatException e) {
                errors.add(new ValidationError("collectionMode", Messages.get("validate.not.numeric")));
            }
        }

        if (StringUtils.isBlank(paymentType)) {
            errors.add(new ValidationError("paymentType", Messages.get("validate.required", Messages.get("cookForMe.paymentType"))));
        } else {
            try {
                paymentTypeCode = SubsetCode.validateSubsetCode(Constants.PAYMENT_TYPE, Integer.parseInt(paymentType));
                if (SubsetCode.validateSubsetCode(Constants.PAYMENT_TYPE, Integer.parseInt(paymentType)) == null) {
                    errors.add(new ValidationError("paymentType", Messages.get("invalid.code", Messages.get("cookForMe.paymentType"))));
                }
            } catch (NumberFormatException e) {
                errors.add(new ValidationError("paymentType", Messages.get("validate.not.numeric")));
            }
        }

        try {
            double cost = cookForMeOrder.cookForMe.cookingCost/cookForMeOrder.cookForMe.recipe.servings * servings;
            if (cookForMeCost == null || cookForMeCost < 0) {
                errors.add(new ValidationError("cookForMeCost", Messages.get("validate.required", Messages.get("cookForMe.cookForMeCost"))));
            } else if (cookForMeCost != cost) {
                cookForMeCost = cost;
                errors.add(new ValidationError("cookForMeCost", Messages.get("cookForMeOrder.cookForMeCost.updated")));
                errors.add(new ValidationError("updatedCookForMeCost", ""+cookForMeCost));
            }
        } catch (NumberFormatException e) {
            errors.add(new ValidationError("cookForMeCost", Messages.get("validate.not.numeric")));
        }

        if(collectionModeCode == Constants.SUBSETCODE_COLLECTION_MODE_DELIVERY) {
            try {
                if (deliveryFee == null || deliveryFee < 0) {
                    errors.add(new ValidationError("deliveryFee", Messages.get("validate.required", Messages.get("cookForMe.deliveryFee"))));
                } else if (deliveryFee != cookForMeOrder.cookForMe.deliveryFee) {
                    deliveryFee = cookForMeOrder.cookForMe.deliveryFee;
                    errors.add(new ValidationError("deliveryFee", Messages.get("cookForMeOrder.deliveryFee.updated")));
                    errors.add(new ValidationError("updatedDeliveryFee", ""+deliveryFee));
                }
            } catch (NumberFormatException e) {
                errors.add(new ValidationError("deliveryFee", Messages.get("validate.not.numeric")));
            }
        }

        return errors.isEmpty() ? null : errors;
    }


    public static Result updateRecipeOrder() {
        ObjectNode jsonResult = Json.newObject();
        Form <EditCookForMeOrder> orderForm = cookForMeOrderForm.bindFromRequest();

        try {
            final User user = User.findByEmail(request().username());
            if(user == null) {
                jsonResult.put("result", Messages.get("signin.required.desc"));
                return badRequest(jsonResult.toString());
            } else if (orderForm.hasErrors()) {
                return badRequest(orderForm.errorsAsJson().toString());
            } else {
                EditCookForMeOrder orderFilledForm = orderForm.get();
                Date originalBookingDate = cookForMeOrder.bookingDate;
                cookForMeOrder.bookingDate = new java.sql.Timestamp(Constants.dateFormat.parse(orderFilledForm.bookingDate).getTime());
                cookForMeOrder.collectionMode = collectionModeCode;
                cookForMeOrder.paymentType = paymentTypeCode;
                cookForMeOrder.currency = cookForMeOrder.cookForMe.currency;
                cookForMeOrder.servings = orderFilledForm.servings;
                cookForMeOrder.cookingCost = orderFilledForm.cookForMeCost;
                cookForMeOrder.deliveryFee = collectionModeCode.subsetCodeId == Constants.SUBSETCODE_COLLECTION_MODE_DELIVERY.subsetCodeId ? orderFilledForm.deliveryFee : 0;
                cookForMeOrder.remarks = orderFilledForm.remarks;
                cookForMeOrder.user = user;
                cookForMeOrder.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_NEW;

                cookForMeOrder.update();

                CreateNotification.cookForMeNotification(cookForMeOrder.cookForMe.recipe.user, user, Messages.get("notification.updateOrder", collectionModeCode.subsetCodeValue, orderFilledForm.bookingDate), controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url());
                FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(cookForMeOrder.cookForMe.recipe.user.email, cookForMeOrder.cookForMe.recipe.user.firstName + " " + cookForMeOrder.cookForMe.recipe.user.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.update.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, cookForMeOrder.collectionMode.subsetCodeValue, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.notificationDateFormat.format(originalBookingDate)), Messages.get("cookForMeOrder.update.order.email.header"));

                jsonResult.put("result", errorModal.render(user, "success",  Messages.get("cookForMeOrder.order.update.success.header"), Messages.get("cookForMeOrder.order.update.success.desc")).toString());

                return ok(jsonResult.toString());
            }
        } catch (Exception e) {
            Logger.error("Placing order encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
    }

    public static Result acceptOrder() {
        DynamicForm data = form().bindFromRequest();
        String rawOrderId = data.get("orderId");
        long orderId;
        try {
            orderId = Long.parseLong(rawOrderId);
        } catch (Exception e) {
            Logger.error("Accepting order id : " + rawOrderId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        try {
            final User user = User.findByEmail(request().username());
            CookForMeOrders order = CookForMeOrders.find.byId(orderId);

            if(user == null) {
                return badRequest(Messages.get("user.not.found"));
            } else if(order != null) {
                // Additional check to determine whether login user has the rights to accept this order
                if(!order.cookForMe.recipe.user.equals(user)) {
                    return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("cookForMeOrder.update.invalid.rights")));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_NEW) {
                    User notify;
                    if(order.cookForMe.recipe.user.equals(user)) {
                        notify = order.user;
                    } else {
                        notify = order.cookForMe.recipe.user;
                    }

                    if(order.bookingDate.after(new Date())) {
                        order.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_ACCEPTED;
                        order.update();

                        CreateNotification.cookForMeNotification(notify, user, Messages.get("notification.acceptOrder", order.collectionMode.subsetCodeValue, Constants.dateFormat.format(order.bookingDate)), controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url());
                        FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(notify.email, notify.firstName + " " + notify.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.accepted.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.dateFormat.format(order.bookingDate), order.collectionMode.subsetCodeValue, order.servings), Messages.get("cookForMeOrder.cancelled.order.email.header"));

                    } else {
                        order.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_CANCELLED;
                        order.update();

                        CreateNotification.cookForMeNotification(notify, user, Messages.get("notification.expired.cancelOrder", order.collectionMode.subsetCodeValue, Constants.dateFormat.format(order.bookingDate)), controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url());
                        FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(notify.email, notify.firstName + " " + notify.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.cancelled.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.dateFormat.format(order.bookingDate), order.collectionMode.subsetCodeValue, order.servings), Messages.get("cookForMeOrder.cancelled.order.email.header"));

                        return badRequest(Messages.get("cookForMeOrder.already.expired"));
                    }
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_ACCEPTED) {
                    return badRequest(Messages.get("cookForMeOrder.already.accepted"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_REJECTED) {
                    return badRequest(Messages.get("cookForMeOrder.already.rejected"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_CANCELLED) {
                    return badRequest(Messages.get("cookForMeOrder.already.cancelled"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_COMPLETED) {
                    return badRequest(Messages.get("cookForMeOrder.already.completed"));
                }
            } else {
                return badRequest(Messages.get("cookForMeOrder.not.found"));
            }

            return ok(Messages.get("cookForMeOrder.accepted"));
        } catch (Exception e) {
            Logger.error("Recipe deletion from cook for me order encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
    }

    public static Result rejectOrder() {
        DynamicForm data = form().bindFromRequest();
        String rawOrderId = data.get("orderId");
        long orderId;
        try {
            orderId = Long.parseLong(rawOrderId);
        } catch (Exception e) {
            Logger.error("Rejecting order id : " + rawOrderId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        try {
            final User user = User.findByEmail(request().username());
            CookForMeOrders order = CookForMeOrders.find.byId(orderId);

            if(user == null) {
                return badRequest(Messages.get("user.not.found"));
            } else if(order != null) {
                // Additional check to determine whether login user has the rights to reject this order
                if(!order.cookForMe.recipe.user.equals(user)) {
                    return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("cookForMeOrder.update.invalid.rights")));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_NEW) {
                    if(order.bookingDate.after(new Date())) {
                        order.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_REJECTED;
                        order.update();
                    } else {
                        order.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_CANCELLED;
                        order.update();
                        return badRequest(Messages.get("cookForMeOrder.already.expired"));
                    }
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_ACCEPTED) {
                    return badRequest(Messages.get("cookForMeOrder.already.accepted"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_REJECTED) {
                    return badRequest(Messages.get("cookForMeOrder.already.rejected"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_CANCELLED) {
                    return badRequest(Messages.get("cookForMeOrder.already.cancelled"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_COMPLETED) {
                    return badRequest(Messages.get("cookForMeOrder.already.completed"));
                }
            } else {
                return badRequest(Messages.get("cookForMeOrder.not.found"));
            }

            return ok(Messages.get("cookForMeOrder.rejected"));
        } catch (Exception e) {
            Logger.error("Recipe deletion from cook for me order encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
    }

    public static Result cancelOrder() {
        DynamicForm data = form().bindFromRequest();
        String rawOrderId = data.get("orderId");
        long orderId;
        try {
            orderId = Long.parseLong(rawOrderId);
        } catch (Exception e) {
            Logger.error("Cancelling order id : " + rawOrderId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        try {
            final User user = User.findByEmail(request().username());
            CookForMeOrders order = CookForMeOrders.find.byId(orderId);

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, Constants.MIN_ORDER_AMEND_DATE_IN_ADVANCE);
            Date minCancellationDate;
            try {
                minCancellationDate = Constants.dateFormat.parse(c.getTime().toString());
            } catch (Exception e) {
                minCancellationDate = c.getTime();
            }

            if(user == null) {
                return badRequest(Messages.get("user.not.found"));
            } else if(order != null) {
                // Additional check to determine whether login user has the rights to cancel this order
                if(!order.user.equals(user) && !order.cookForMe.recipe.user.equals(user)) {
                    return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("cookForMeOrder.update.invalid.rights")));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_NEW || order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_ACCEPTED) {
                    if(minCancellationDate.after(order.bookingDate)) {
                        return badRequest(Messages.get("cookForMeOrder.cancel.duration.past"));
                    }

                    order.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_CANCELLED;
                    order.update();

                    User notify;
                    if(order.cookForMe.recipe.user.equals(user)) {
                        notify = order.user;
                    } else {
                        notify = order.cookForMe.recipe.user;
                    }

                    CreateNotification.cookForMeNotification(notify, user, Messages.get("notification.cancelOrder", order.collectionMode.subsetCodeValue, Constants.dateFormat.format(order.bookingDate)), controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url());
                    FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(notify.email, notify.firstName + " " + notify.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.cancelled.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.dateFormat.format(order.bookingDate), order.collectionMode.subsetCodeValue, order.servings), Messages.get("cookForMeOrder.cancelled.order.email.header"));

                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_CANCELLED) {
                    return badRequest(Messages.get("cookForMeOrder.already.cancelled"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_COMPLETED) {
                    return badRequest(Messages.get("cookForMeOrder.already.completed"));
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_REJECTED) {
                    return badRequest(Messages.get("cookForMeOrder.already.rejected"));
                }
            } else {
                return badRequest(Messages.get("cookForMeOrder.not.found"));
            }

            return ok(Messages.get("cookForMeOrder.cancelled"));
        } catch (Exception e) {
            Logger.error("Recipe deletion from cook for me order encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
    }

    public static Result completeOrder() {
        ObjectNode jsonResult = Json.newObject();
        DynamicForm data = form().bindFromRequest();
        String rawOrderId = data.get("orderId");
        long orderId;
        try {
            orderId = Long.parseLong(rawOrderId);
        } catch (Exception e) {
            Logger.error("Cancelling order id : " + rawOrderId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        try {
            final User user = User.findByEmail(request().username());
            CookForMeOrders order = CookForMeOrders.find.byId(orderId);

            if(user == null) {
                jsonResult.put("result", Messages.get("signin.required.desc"));
                return badRequest(jsonResult.toString());
            } else if(order != null) {
                // Additional check to determine whether login user has the rights to complete this order
                if(!order.user.equals(user) && !order.cookForMe.recipe.user.equals(user)) {
                    jsonResult.put("result", errorModal.render(user, "", Messages.get("invalid.rights"), Messages.get("cookForMeOrder.update.invalid.rights")).toString());
                    return ok(jsonResult.toString());
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_ACCEPTED) {
                    if(order.bookingDate.after(new Date())) {
                        order.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_COMPLETED;
                        order.update();

                        User notify;
                        if (order.cookForMe.recipe.user.equals(user)) {
                            notify = order.user;
                        } else {
                            notify = order.cookForMe.recipe.user;
                        }

                        CreateNotification.cookForMeNotification(notify, user, Messages.get("notification.completeOrder", order.collectionMode.subsetCodeValue, Constants.dateFormat.format(order.bookingDate)), controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url());
                        FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(notify.email, notify.firstName + " " + notify.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.completed.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.dateFormat.format(order.bookingDate), order.collectionMode.subsetCodeValue, order.servings, "."), Messages.get("cookForMeOrder.cancelled.order.email.header"));
                    }
                } else if(order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_COMPLETED) {
                    return ok();
                }
            } else {
                jsonResult.put("result", Messages.get("cookForMeOrder.not.found"));
                return badRequest(jsonResult.toString());
            }

            jsonResult.put("result", errorModal.render(user, "success",  Messages.get("cookForMeOrder.order.success.header"), Messages.get("cookForMeOrder.order.success.desc")).toString());

            return ok(jsonResult.toString());
        } catch (Exception e) {
            Logger.error("Recipe completion from cook for me order encountered error : ", e);
            jsonResult.put("result", Messages.get("error.technical"));
            return badRequest(jsonResult.toString());
        }
    }

    public static Result requestForAddress() {
        DynamicForm data = form().bindFromRequest();
        String rawOrderId = data.get("orderId");
        long orderId;
        try {
            orderId = Long.parseLong(rawOrderId);
        } catch (Exception e) {
            Logger.error("Cancelling order id : " + rawOrderId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        try {
            final User user = User.findByEmail(request().username());
            CookForMeOrders order = CookForMeOrders.find.byId(orderId);

            if(user == null) {
                return badRequest(Messages.get("user.not.found"));
            } else if(order != null) {
                // Additional check to determine whether login user has the rights to request for diner address
                if(!order.user.equals(user) && !order.cookForMe.recipe.user.equals(user)) {
                    return badRequest(Messages.get("invalid.rights"));
                }

                String notificationMessage = Messages.get("notification.requestAddress", controllers.account.routes.UpdateProfile.loadProfileForCFMModal().url(), order.collectionMode.subsetCodeValue, Constants.dateFormat.format(order.bookingDate));

                if(ViewNotification.getNotificationByTypeDescription(order.user, Constants.NOTIFICATION_TYPE_ORDER, notificationMessage).size() > 0) {
                    return badRequest(Messages.get("cookForMeOrder.request.address.before"));
                } else {
                    CreateNotification.cookForMeNotification(order.user, user, notificationMessage, "");
                }
            }
        } catch (Exception e) {
            Logger.error("Request address for Cook For Me encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
        return ok(Messages.get("cookForMeOrder.request.address.wait"));
    }
}