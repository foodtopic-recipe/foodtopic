package controllers.cookForMe;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.cookForMe.listCookForMeOrders;
import views.html.cookForMe.listCookForMeOrdersRow;
import views.html.error.error;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static play.data.Form.form;


@Security.Authenticated(Secured.class)
public class ViewCookForMeOrder extends Controller {

    private static boolean displayMain = true;
    private static boolean viewOwnOrders = true;
    /**
     * Creates a new ViewCookForMeOrder object.
     */
    public ViewCookForMeOrder() { }

    public static int getUserCookForMeOrderCount(final User searchedUser) {
        final User user = Application.getSessionUser();
        if(user.userId == searchedUser.userId)
            return CookForMeOrders.countByUserId(user.userId);
        else
            return CookForMeOrders.completedOrderCountByUserId(searchedUser.userId);
    }

    /**
     * Next order list value.
     *
     * @return  next order list value.
     */
    public static Result getNextOrderList() {
        final DynamicForm data = form().bindFromRequest();
        User user = User.findByEmail(request().username());

        int page = 0;
        int orderStatusId = 0;
        long searchedUserId = 0;
        int search = 0;
        String dateRange = null;
        String createDateRange = null;
        Date startDate = null;
        Date createStartDate = null;
        Date endDate = null;
        Date createEndDate = null;
        DateFormat formater = new SimpleDateFormat("ddMMyyyy hh:mm");


        try {
            page = Integer.parseInt(data.get("page"));
            searchedUserId = Long.parseLong(data.get("forWhom"));
            search = Integer.parseInt(data.get("search"));
            orderStatusId = Integer.parseInt(data.get("orderStatus"));

            dateRange = data.get("date");
            String[] dateTextArr = dateRange.split("-");
            if (!dateTextArr[0].equals("0")) {
                startDate = formater.parse(dateTextArr[0] +  " 00:00");
            }
            if (!dateTextArr[1].equals("0")) {
                endDate = formater.parse(dateTextArr[1] + " 23:59");
            }

            createDateRange = data.get("created");
            dateTextArr = createDateRange.split("-");
            if (!dateTextArr[0].equals("0")) {
                createStartDate = formater.parse(dateTextArr[0] +  " 00:00");
            }
            if (!dateTextArr[1].equals("0")) {
                createEndDate = formater.parse(dateTextArr[1] + " 23:59");
            }

        } catch (Exception e) {
            Logger.error("Get next cook for me order list for login user: " + user.userId + " encountered error : ", e);
        }
        User searchedUser = User.find.byId(searchedUserId);
        if(searchedUser == null) {
            return badRequest();
        }

        List<CookForMeOrders> orderList = new ArrayList<CookForMeOrders>();
        SubsetCode orderStatus = SubsetCode.getSubsetCodeById(orderStatusId);
        int totalCount = 0;

        if (search == Constants.cookForMeOrderByOthers) {
            orderList = CookForMeOrders.findByHostId(searchedUser.userId, orderStatus, page, startDate, endDate, createStartDate, createEndDate);
            totalCount = CookForMeOrders.countByHostId(searchedUser.userId, orderStatus, startDate, endDate, createStartDate, createEndDate);
        } else if (search == Constants.cookForMeOrderByMe) {
            orderList = CookForMeOrders.findByDinerId(searchedUser.userId, orderStatus, page, startDate, endDate, createStartDate, createEndDate);
            totalCount = CookForMeOrders.countByDinerId(searchedUser.userId, orderStatus, startDate, endDate, createStartDate, createEndDate);
        } else if (search == Constants.cookForMeOrderByAnyone) {
            orderList = CookForMeOrders.findByUserId(searchedUser.userId, orderStatus, page, startDate, endDate, createStartDate, createEndDate);
            totalCount = CookForMeOrders.countByUserId(searchedUser.userId, orderStatus, startDate, endDate, createStartDate, createEndDate);
        }
        else {
            return badRequest();
        }

        boolean loadmore = (totalCount > (page+1) * Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT);
        return ok(listCookForMeOrdersRow.render(user, orderList, Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT, loadmore, viewOwnOrders));
    }

    /**
     * Display the view cook for me order page if login.
     *
     * @return  view cook for me order page
     */
    public static Result initViewOrderForm() {
        User user = User.findByEmail(request().username());
        List<CookForMeOrders> cookForMeHostOrdersList = new ArrayList<>();
        List<CookForMeOrders> cookForMeDinerOrdersList = new ArrayList<>();

        if (user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else {
            return ok(listCookForMeOrders.render(user, Messages.get("cookForMeOrders.my") , cookForMeHostOrdersList, cookForMeDinerOrdersList, Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT, displayMain, viewOwnOrders));
        }
    }

    public static boolean cancelOrderRights(long orderId) {
        final User user = User.findByEmail(request().username());
        CookForMeOrders order = CookForMeOrders.find.byId(orderId);

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, Constants.MIN_ORDER_AMEND_DATE_IN_ADVANCE);

        if(user == null || order == null) {
            return false;
        } else if(order.user.equals(user) || order.cookForMe.recipe.user.equals(user) || (c.getTime().after(order.bookingDate) && order.orderStatus.subsetCodeId == Constants.ORDER_STATUS_ACCEPTED)) {
            return true;
        }

        return false;
    }
}
