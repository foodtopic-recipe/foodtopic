package controllers.cookForMe;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Application;
import controllers.Secured;
import controllers.account.routes;
import controllers.notification.CreateNotification;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import providers.FTPasswordAuthProvider;
import views.html.cookForMe.cookForMeOrderModal;
import views.html.error.errorModal;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class CreateCookForMeOrder extends Controller {

    private static String CRUD = Messages.get("create");
    public String bookingDate;
    public String collectionMode;
    public String paymentType;
    public Integer servings;
    public Double cookForMeCost;
    public Double deliveryFee;
    public String remarks;

    private static CookForMe cookForMe;
    private static Date minBookingDate;
    private static SubsetCode collectionModeCode, paymentTypeCode;


    /**
     * Defines a form wrapping the Create Cook For Me Order class.
     */
    final static Form<CreateCookForMeOrder> cookForMeOrderForm = form(CreateCookForMeOrder.class);

    public CreateCookForMeOrder() {
        bookingDate = "";
        servings = 0;
        paymentType = Constants.SUBSETCODE_PAYMENT_TYPE_CASH.toString();
        collectionMode = "";
        cookForMeCost = 0.0;
        deliveryFee = 0.0;
        remarks = "";
    }

    public CreateCookForMeOrder(Recipe recipe) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, Constants.MIN_ORDER_BOOKING_DATE_IN_ADVANCE);
        bookingDate = Constants.dateFormat.format(c.getTime());
        try {
            minBookingDate = Constants.dateFormat.parse(bookingDate);
        } catch (Exception e) {
            minBookingDate = c.getTime();
        }
        servings = recipe.servings;
        paymentType = Constants.SUBSETCODE_PAYMENT_TYPE_CASH.toString();
        remarks = "";
        if(recipe.cookForMe != null) {
            cookForMe = CookForMe.find.byId(recipe.cookForMe.cookForMeId);
            collectionMode = Utilities.spiltStringByComma(recipe.cookForMe.collectionMode)[0];
            cookForMeCost = recipe.cookForMe.cookingCost;
            deliveryFee = recipe.cookForMe.deliveryFee;
        }
    }

    public static Result initCreateOrderModal(long recipeId) {
        final User user = User.findByEmail(request().username());
        Recipe recipe = Recipe.find.byId(recipeId);

        // Additional check to determine whether login user has the rights to place an order
        Logger.info("user : " + user);
        if(user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(recipe == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("recipe.not.found")));
        } else if(recipe.cookForMe == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("cookForMe.not.found")));
        } else if(recipe.user.equals(user)) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("cookForMe.placeOrder.fail"), Messages.get("cookForMe.same.user.found")));
        }

		return ok(cookForMeOrderModal.render(user, CRUD, recipe, cookForMeOrderForm.fill(new CreateCookForMeOrder(recipe))));
	}

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();

        if (StringUtils.isBlank(bookingDate)) {
            errors.add(new ValidationError("bookingDate", Messages.get("validate.required", Messages.get("cookForMe.bookingDate"))));
        } else if(StringUtils.isNotBlank(bookingDate)) {
            try {
                Date formattedBookingDate = Constants.dateFormat.parse(bookingDate);
                if(formattedBookingDate.before(minBookingDate)) {
                    errors.add(new ValidationError("bookingDate", Messages.get("cookForMe.bookingDate.future")));
                }
                final User user = User.findByEmail(request().username());

                // Get formatted booking date with additional time added to check
                // whether is this new order a double booking within a period (Set by Constants)
                Calendar c = Calendar.getInstance();
                c.setTime(formattedBookingDate);
                c.add(Calendar.HOUR, Constants.MIN_ORDER_BOOKING_HOUR_LIMIT);

                Date formattedStartBookingDate, formattedEndBookingDate;
                try {
                    formattedStartBookingDate = Constants.sameHourDateFormat.parse(bookingDate);
                    formattedEndBookingDate = Constants.sameHourDateFormat.parse(Constants.sameHourDateFormat.format(c.getTime()));
                } catch (Exception e) {
                    formattedStartBookingDate= formattedBookingDate;
                    formattedEndBookingDate = c.getTime();
                }

                List<CookForMeOrders> orderList = CookForMeOrders.findRepeatedBookingByTime(user.userId, cookForMe.recipe.recipeId, formattedStartBookingDate, formattedEndBookingDate);
                if(orderList.size() != 0) {
                    errors.add(new ValidationError("bookingDate", Messages.get("cookForMe.same.hour.booking", Constants.MIN_ORDER_BOOKING_HOUR_LIMIT)));
                }
            } catch (ParseException e) {
                errors.add(new ValidationError("bookingDate", Messages.get("validate.not.date")));
            }
        }

        try {
            if (servings == null || servings <= 0) {
                errors.add(new ValidationError("servings", Messages.get("validate.required", Messages.get("recipe.servings"))));
            }
        } catch (NumberFormatException e) {
            errors.add(new ValidationError("servings", Messages.get("validate.not.numeric")));
        }

        if (StringUtils.isBlank(collectionMode)) {
            errors.add(new ValidationError("collectionMode", Messages.get("validate.required", Messages.get("cookForMe.collectionMode"))));
        } else {
            try {
                collectionModeCode = SubsetCode.validateSubsetCode(Constants.COLLECTION_MODE, Integer.parseInt(collectionMode));
                if (collectionModeCode == null) {
                    errors.add(new ValidationError("collectionMode", Messages.get("invalid.code", Messages.get("cookForMe.collectionMode"))));
                }
            } catch (NumberFormatException e) {
                errors.add(new ValidationError("collectionMode", Messages.get("validate.not.numeric")));
            }
        }

        if (StringUtils.isBlank(paymentType)) {
            errors.add(new ValidationError("paymentType", Messages.get("validate.required", Messages.get("cookForMe.paymentType"))));
        } else {
            try {
                paymentTypeCode = SubsetCode.validateSubsetCode(Constants.PAYMENT_TYPE, Integer.parseInt(paymentType));
                if (SubsetCode.validateSubsetCode(Constants.PAYMENT_TYPE, Integer.parseInt(paymentType)) == null) {
                    errors.add(new ValidationError("paymentType", Messages.get("invalid.code", Messages.get("cookForMe.paymentType"))));
                }
            } catch (NumberFormatException e) {
                errors.add(new ValidationError("paymentType", Messages.get("validate.not.numeric")));
            }
        }

        try {
            double cost = cookForMe.cookingCost/cookForMe.recipe.servings * servings;
            if (cookForMeCost == null || cookForMeCost < 0) {
                errors.add(new ValidationError("cookForMeCost", Messages.get("validate.required", Messages.get("cookForMe.cookForMeCost"))));
            } else if (cookForMeCost != cost) {
                cookForMeCost = cost;
                errors.add(new ValidationError("cookForMeCost", Messages.get("cookForMeOrder.cookForMeCost.updated")));
                errors.add(new ValidationError("updatedCookForMeCost", ""+cookForMeCost));
            }
        } catch (NumberFormatException e) {
            errors.add(new ValidationError("cookForMeCost", Messages.get("validate.not.numeric")));
        }

        if(collectionModeCode == Constants.SUBSETCODE_COLLECTION_MODE_DELIVERY) {
            try {
                if (deliveryFee == null || deliveryFee < 0) {
                    errors.add(new ValidationError("deliveryFee", Messages.get("validate.required", Messages.get("cookForMe.deliveryFee"))));
                } else if (deliveryFee != cookForMe.deliveryFee) {
                    deliveryFee = cookForMe.deliveryFee;
                    errors.add(new ValidationError("deliveryFee", Messages.get("cookForMeOrder.deliveryFee.updated")));
                    errors.add(new ValidationError("updatedDeliveryFee", ""+deliveryFee));
                }
            } catch (NumberFormatException e) {
                errors.add(new ValidationError("deliveryFee", Messages.get("validate.not.numeric")));
            }
        }

        return errors.isEmpty() ? null : errors;
    }

    public static Result placeRecipeOrder() {
        ObjectNode jsonResult = Json.newObject();
        Form <CreateCookForMeOrder> orderForm = cookForMeOrderForm.bindFromRequest();

        try {
            final User user = User.findByEmail(request().username());
            if(user == null) {
                jsonResult.put("result", Messages.get("signin.required.desc"));
                return badRequest(jsonResult.toString());
            } else if (orderForm.hasErrors()) {
                return badRequest(orderForm.errorsAsJson().toString());
            } else {
                CreateCookForMeOrder orderFilledForm = orderForm.get();
                Date formattedBookingDate = Constants.dateFormat.parse(orderFilledForm.bookingDate);

                CookForMeOrders cookForMeOrder = new CookForMeOrders();
                cookForMeOrder.bookingDate = new java.sql.Timestamp(formattedBookingDate.getTime());
                cookForMeOrder.collectionMode = collectionModeCode;
                cookForMeOrder.paymentType = paymentTypeCode;
                cookForMeOrder.cookForMe = cookForMe;
                cookForMeOrder.currency = cookForMe.currency;
                cookForMeOrder.servings = orderFilledForm.servings;
                cookForMeOrder.cookingCost = orderFilledForm.cookForMeCost;
                cookForMeOrder.deliveryFee = collectionModeCode.subsetCodeId == Constants.SUBSETCODE_COLLECTION_MODE_DELIVERY.subsetCodeId ? orderFilledForm.deliveryFee : 0;
                cookForMeOrder.remarks = orderFilledForm.remarks;
                cookForMeOrder.user = user;
                cookForMeOrder.orderStatus = Constants.SUBSETCODE_ORDER_STATUS_NEW;

                cookForMe.cookForMeOrders.add(cookForMeOrder);
                if(user.equals(cookForMe.recipe.user))
                    cookForMe.update();

                cookForMeOrder.save();
                CreateNotification.cookForMeNotification(cookForMe.recipe.user, user, Messages.get("notification.newOrder", collectionModeCode.subsetCodeValue, Constants.notificationDateFormat.format(formattedBookingDate)), controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url());
                FTPasswordAuthProvider.getProvider().sendCookForMeEmailAlert(cookForMe.recipe.user.email, cookForMe.recipe.user.firstName + " " + cookForMe.recipe.user.lastName, user.firstName + " " + user.lastName, Messages.get("cookForMeOrder.new.order.email.content", controllers.account.routes.ViewProfile.viewProfileById(user.userId).url(), user.firstName + " " + user.lastName, cookForMeOrder.collectionMode.subsetCodeValue, controllers.cookForMe.routes.ViewCookForMeOrder.initViewOrderForm().url(), Constants.notificationDateFormat.format(formattedBookingDate)), Messages.get("cookForMeOrder.new.order.email.header"));

                jsonResult.put("result", errorModal.render(user, "success",  Messages.get("cookForMeOrder.order.success.header"), Messages.get("cookForMeOrder.order.success.desc")).toString());

                return ok(jsonResult.toString());
            }
        } catch (Exception e) {
            Logger.error("Placing order encountered error : ", e);
            jsonResult.put("result", Messages.get("error.technical"));
            return badRequest(jsonResult.toString());
        }
    }
}
