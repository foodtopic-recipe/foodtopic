package controllers.reputation;

import controllers.utils.Constants;
import play.mvc.Controller;

public class Reputation extends Controller {
	
    public static int getNewRecipeReputationPoints() {
    	double reputationPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_NEW_RECIPE.subsetCodeValue);
    	return (int) Math.round(reputationPoints);
    }
    
    public static int getReinventRecipeReputationPoints() {
    	double reputationPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_REINVENT_RECIPE.subsetCodeValue);
    	return (int) Math.round(reputationPoints);
    }

    public static int getNewReviewReputationPoints(int reviewStars) {
    	double oneToThreeStarsReputationPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_NEW_REVIEW_1TO3_STARS.subsetCodeValue);
    	double fourToFiveStarsReputationPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_NEW_REVIEW_4TO5_STARS.subsetCodeValue);
    	
    	if(reviewStars < 4)
    		return (int) Math.round(oneToThreeStarsReputationPoints);
    	else
    		return (int) Math.round(fourToFiveStarsReputationPoints);
    }
    
    // Get the reputation points of user who gave the like and multiply with the multiplication value
    // If value turns out to be higher than the cap value set, return only the cap value.
    public static int getNewLikeReputationPoints(long userReputationPoints) {
    	double minimumReputationPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_NEW_LIKE.subsetCodeValue);
    	double reputationMultiplicationValue = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_MULTIPLICATION_VALUE.subsetCodeValue);
    	double likeCapPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_NEW_LIKE_CAP.subsetCodeValue);

    	double calculation = userReputationPoints * reputationMultiplicationValue;
    	if(calculation < minimumReputationPoints)
    		return (int) Math.round(minimumReputationPoints);
    	else if(calculation > likeCapPoints)
    		return (int) Math.round(likeCapPoints);
    	else
    		return (int) Math.round(calculation);
    }

    // Get the reputation points of user who favourite the recipe and multiply with the multiplication value
    // If value turns out to be higher than the cap value set, return only the cap value.
    public static int getNewFavouriteReputationPoints(long userReputationPoints) {
    	double minimumReputationPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_NEW_FAVOURITE.subsetCodeValue);
    	double reputationMultiplicationValue = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_MULTIPLICATION_VALUE.subsetCodeValue);
    	double favouriteCapPoints = Double.parseDouble(Constants.SUBSETCODE_REPUTATION_POINTS_NEW_FAVOURITE_CAP.subsetCodeValue);
    	
    	double calculation = userReputationPoints * reputationMultiplicationValue;
    	if(calculation < minimumReputationPoints)
    		return (int) Math.round(minimumReputationPoints);
    	else if(calculation > favouriteCapPoints)
    		return (int) Math.round(favouriteCapPoints);
    	else
    		return (int) Math.round(calculation);
    }
}