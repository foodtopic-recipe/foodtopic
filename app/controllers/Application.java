package controllers;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import controllers.exceptions.CustomException;
import controllers.footer.CreateSubmission;
import controllers.recipe.ViewRecipe;
import controllers.utils.Constants;
import models.User;
import org.apache.commons.io.IOUtils;
import play.Logger;
import play.Routes;
import play.api.Play;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.error.error;
import views.html.home;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static play.data.Form.form;


/**
 * DOCUMENT ME!
 *
 * @version  $Revision$, $Date$
 */
public class Application extends Controller {
    public static final String FLASH_SUCCESS_KEY = "success";
    public static final String FLASH_ERROR_KEY = "danger";
    public static final String FLASH_WARNING_KEY = "warning";

    public static Result GO_HOME = redirect(controllers.routes.Application.home());
    public static Result GO_DASHBOARD = redirect(controllers.routes.Dashboard.index());

    /**
     * Creates a new Application object.
     */
    public Application() {
    }

    /**
     * DOCUMENT ME!
     *
     * @param   t
     *
     * @return  String
     */
    public static String formatTimestamp(final long t) {
        return new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(new Date(t));
    }

    /**
     * This method is used in Global.java to get user to pass thru error.scala.html, some controllers & forms.
     *
     * @return  session user value.
     */
    public static User getSessionUser() {
        final AuthUser currentAuthUser = PlayAuthenticate.getUser(session());
        final User sessionUser = User.findByAuthUserIdentity(currentAuthUser);

        return sessionUser;
    }

    /**
     * DOCUMENT ME!
     *
     * @return  Result
     */
    public static Result home() {
        if (PlayAuthenticate.isLoggedIn(session())) {
            return GO_DASHBOARD;
        } else {
            Logger.debug("Clearing invalid session credentials");
            session().clear();
        }

        return ok(home.render(ViewRecipe.getFeaturedRecipes(Constants.FETCH_RECIPE_PAGE_ONE), form(CreateSubmission.class).fill(new CreateSubmission())));
    }

    /**
     * Display the login page or dashboard if connected.
     *
     * @return  login page or dashboard
     */
    public static Result index() {
        if (PlayAuthenticate.isLoggedIn(session())) {
            return GO_DASHBOARD;
        } else {
            Logger.debug("Clearing invalid session credentials");
            session().clear();
        }

        return ok(home.render(ViewRecipe.getFeaturedRecipes(Constants.FETCH_RECIPE_PAGE_ONE), form(CreateSubmission.class).fill(new CreateSubmission())));
    }

    /**
     * Ajax Routes.
     *
     * @return  Result
     */
    public static Result javascriptRoutes() {
        response().setContentType("text/javascript");

        return ok(Routes.javascriptRouter("jsRoutes", controllers.routes.javascript.Like.recipeLike(),
                    controllers.routes.javascript.Like.recipeUnlike(),
                    controllers.routes.javascript.Dashboard.getNextDashboardList(),
                    controllers.account.routes.javascript.Login.authenticate(),
                    controllers.account.routes.javascript.Password.changePassword(),
                    controllers.account.routes.javascript.UpdateProfile.updateProfileForCFM(),
                    controllers.account.routes.javascript.UpdateProfile.loadProfileForCFMModal(),
                    controllers.cookForMe.routes.javascript.CreateCookForMeOrder.placeRecipeOrder(),
                    controllers.cookForMe.routes.javascript.EditCookForMeOrder.updateRecipeOrder(),
                    controllers.cookForMe.routes.javascript.EditCookForMeOrder.acceptOrder(),
                    controllers.cookForMe.routes.javascript.EditCookForMeOrder.rejectOrder(),
                    controllers.cookForMe.routes.javascript.EditCookForMeOrder.cancelOrder(),
                    controllers.cookForMe.routes.javascript.EditCookForMeOrder.completeOrder(),
                    controllers.cookForMe.routes.javascript.EditCookForMeOrder.requestForAddress(),
                    controllers.cookForMe.routes.javascript.ViewCookForMeOrder.getNextOrderList(),
                    controllers.cookForMeReview.routes.javascript.CreateCookForMeReview.postCookForMeReview(),
                    controllers.cookForMeReview.routes.javascript.EditCookForMeReview.updateCookForMeReview(),
                    controllers.cookForMeReview.routes.javascript.DeleteCookForMeReview.deleteCookForMeReview(),
                    controllers.follower.routes.javascript.EditFollower.followUser(),
                    controllers.follower.routes.javascript.EditFollower.unfollowUser(),
                    controllers.recipeReview.routes.javascript.CreateRecipeReview.postReview(),
                    controllers.recipeReview.routes.javascript.EditRecipeReview.updateReview(),
                    controllers.recipeReview.routes.javascript.DeleteRecipeReview.deleteReview(),
                    controllers.recipeReview.routes.javascript.ViewRecipeReview.getNextReviewList(),
                    controllers.commentFlag.routes.javascript.EditCommentFlag.recipeReviewUpVote(),
                    controllers.commentFlag.routes.javascript.EditCommentFlag.recipeReviewDownVote(),
                    controllers.notification.routes.javascript.ViewNotification.readAllNotification(),
                    controllers.notification.routes.javascript.ViewNotification.getNextNotificationList(),
                    controllers.recipe.routes.javascript.EditRecipe.initAcknowledgementModal(),
                    controllers.recipe.routes.javascript.DeleteRecipe.deleteRecipe(),
                    controllers.recipe.routes.javascript.ViewRecipe.getNextRecipeList(),
                    controllers.recipeBook.routes.javascript.DeleteRecipeBook.deleteRecipeBook(),
                    controllers.bookRecipe.routes.javascript.EditBookRecipe.deleteBookRecipe(),
                    controllers.bookRecipe.routes.javascript.EditBookRecipe.addFavourites()));
    }

    /**
     * DOCUMENT ME!
     *
     * @param   providerKey
     *
     * @return  Result
     */
    public static Result oAuthDenied(final String providerKey) {
        flash("", "You need to accept the OAuth connection in order to use this website!");

        return redirect(controllers.routes.Application.index());
    }

    /**
     * Created to use routes to get images from filepath in DB.
     *
     * @param   path
     *
     * @return  Result
     */
    public static Result serveImage(final String path) {
        ByteArrayInputStream noRecipeImage = null, noProfileImage = null, noDirectionsImage = null, input = null;
        byte[] byteArray;

        try {
            try {
                byteArray = IOUtils.toByteArray(new FileInputStream(Play.getFile("public/images/recipe-noimage.png", Play.current())));
                noRecipeImage = new ByteArrayInputStream(byteArray);

                byteArray = IOUtils.toByteArray(new FileInputStream(Play.getFile("public/images/profile-noimage.jpg", Play.current())));
                noProfileImage = new ByteArrayInputStream(byteArray);

                byteArray = IOUtils.toByteArray(new FileInputStream(Play.getFile("public/images/dir-noimage.jpg", Play.current())));
                noDirectionsImage = new ByteArrayInputStream(byteArray);
            } catch (Exception e) {

            }

            if ((path == null) || path.isEmpty()) {
                throw new CustomException("Empty Path");
            }

            File file;
            if (path.contains("ext/")) {
                file = new File(play.Play.application().path().toString() + "/" + path);
            } else {
                file = new File(path);
            }

            if (file.exists()) {
                byteArray = IOUtils.toByteArray(new FileInputStream(file));
                input = new ByteArrayInputStream(byteArray);

                return ok(input).as("image/jpeg");
            } else {
                throw new CustomException("Invalid File");
            }
        } catch (Exception e) {
            if (path.contains("ext/users/")) {
                return ok(noProfileImage).as("image/jpeg");
            } else if (path.contains("ext/recipes/")) {
                if(path.contains("/directionImages/"))
                    ok(noDirectionsImage).as("image/jpeg");
                else if(path.contains("/review/"))
                    ok(noRecipeImage).as("image/jpeg");
                else
                    ok(noRecipeImage).as("image/jpeg");
            } else {
                return notFound(error.render(getSessionUser(), Messages.get("error.404"), Messages.get("error.404.desc")));
            }
        }
        return ok();
    }
}
