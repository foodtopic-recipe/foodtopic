package controllers.bookRecipe;

import controllers.Application;
import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.BookRecipe;
import models.Recipe;
import models.RecipeBook;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.error.errorModal;
import views.html.recipe.addFavRecipeModal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditBookRecipe extends Controller {

	public static Result addFavouritesModal(long recipeId) {
        final User user = User.findByEmail(request().username());
        Recipe recipe = Recipe.find.byId(recipeId);
        
        // Additional check to determine whether login user has the rights to edit this recipe
        if(user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(recipe == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("recipe.not.found")));
        }
		return ok(addFavRecipeModal.render(user, recipe));
	}
	
	public static Result addFavourites() {
		int addfavouritesCount = 0;
        int deletefavouritesCount = 0;
            
		DynamicForm data = form().bindFromRequest();
		
        String recipeBookIds = data.get("recipeBookIds");
		String rawRecipeId = data.get("recipeId");
        long recipeId = 0;
        try {
            recipeId = Long.parseLong(rawRecipeId);
        } catch (Exception e) {
            Logger.error("Creation of book recipe join : " + rawRecipeId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
            
        try {
            final User user = User.findByEmail(request().username());
            Recipe recipe = Recipe.find.byId(recipeId);
            
            // Validate whether user & recipe is valid
			if(user == null) {
                return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
            } else if(recipe == null) {
                return badRequest(Messages.get("recipe.not.found"));
            }
            
			// Spilt the recipe book id string into string array
			String[] recipeBookList = Utilities.spiltStringByComma(recipeBookIds.replace("recipeBookList=","").replace("&",","));
			
			// Duplicate another set of recipe books from webpage (Creation List)
			List<String> recipeBookCreationSubList = new ArrayList<String>(Arrays.asList(recipeBookList));
			
			// Duplicate another set of recipe books from user model (Deletion List)
            List<RecipeBook> recipeBookDeletionSubList = new ArrayList<RecipeBook>(user.recipeBookList);
            
            // Run thru 2 arrays to determine which recipe book is removed &
            // which recipe book is to be added only if this is not a new recipe
            for(RecipeBook rb : user.recipeBookList) {
                for(String recipeBookID : recipeBookList) {
                    RecipeBook recipeBook = RecipeBook.find.byId(Long.parseLong(recipeBookID.trim()));
                    
                    // Search whether does the recipe already exist & tied to the user
                    List<BookRecipe> recipeInRecipeBookList = BookRecipe.findAllRecipesByRecipeAndRecipeBook(recipeId, recipeBook.recipeBookId);
                    
                    // If the recipe already exist in the same recipe book, remove from the creation list
                    if(!recipeInRecipeBookList.isEmpty()) {
                    	// Ensure the recipe join status is "FAVOURITE"
                    	for(BookRecipe br : recipeInRecipeBookList) {
                    		br.bookRecipeStatus = Constants.FAVOURITE;
                    		br.update();
                        }
                        recipeBookCreationSubList.remove(recipeBookID);
                    }
                    if (rb.equals(recipeBook)) {
                        recipeBookDeletionSubList.remove(recipeBook);
                    }
                }
            }
            
            // Manually create a join between recipe book and the recipe.
            for(String recipeBookID : recipeBookCreationSubList) {
                RecipeBook recipeBook = RecipeBook.find.byId(Long.parseLong(recipeBookID.trim()));
                
                if(recipeBook != null) {
                    // Create new BookRecipe
                    BookRecipe recipeInBook = new BookRecipe();
                    recipeInBook.recipeBook = recipeBook;
                    recipeInBook.recipe = recipe;
                    recipeInBook.bookRecipeStatus = Constants.FAVOURITE;
                    
                    // recipeInBook.save();
                    recipe.bookRecipeList.add(recipeInBook);
                    addfavouritesCount++;
                    
                    // Update user's reputation based on new favourite points (Cannot self favourite recipe to gain reputation points)
                    if(recipe.user.userId != user.userId) {
                    	recipe.user.reputationPoints += Reputation.getNewFavouriteReputationPoints(user.reputationPoints);
                    	recipe.user.update();
                    	
                        CreateNotification.favouritesNotification(recipe.user, user, Messages.get("notification.favourites", controllers.recipe.routes.ViewRecipe.initViewRecipeForm(recipeId).url(), recipe.recipeName), null);
                    }
                }
            }
            
            // Manually delete the join between recipe book and the recipe.
            for(RecipeBook rb : recipeBookDeletionSubList) {
                List<BookRecipe> recipeInRecipeBookList = BookRecipe.findAllRecipesByRecipeAndRecipeBook(recipe.recipeId, rb.recipeBookId);
                for(BookRecipe br : recipeInRecipeBookList) {
                    br.bookRecipeStatus = Constants.UNFAVOURITE;
                    br.update();
                    deletefavouritesCount++;
                }
            }
            
            if(addfavouritesCount > 0) {
                recipe.update();
                return ok(Messages.get("recipeToRecipeBook.add.success"));
            } else if(deletefavouritesCount > 0) {
                return ok(Messages.get("recipeFromRecipeBook.remove.success"));
            } else {
                return ok("No Updates");
            }
		} catch (Exception e) {
			Logger.error("Recipe creation in recipe book encountered error : ", e);
			return badRequest(Messages.get("error.technical"));
		}
	}
    
	// Delete joins between recipe and recipe book
	public static Result deleteBookRecipe() {
		DynamicForm data = form().bindFromRequest();
		String rawBookRecipeId = data.get("bookRecipeId");
        long bookRecipeId;
        try {
            bookRecipeId = Long.parseLong(rawBookRecipeId);
        } catch (Exception e) {
            Logger.error("Deletion of book recipe join : " + rawBookRecipeId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
        
        try {
            final User user = User.findByEmail(request().username());
            BookRecipe recipeInBook = BookRecipe.find.byId(bookRecipeId);
            
            if(user == null) {
                return badRequest(Messages.get("user.not.found"));
            } else if(recipeInBook != null) {
                // Additional check to determine whether login user has the rights to edit this recipe
                if(!recipeInBook.recipeBook.user.equals(user)) 
                    return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipeBook.delete.invalid.rights")));
                else {
                	recipeInBook.bookRecipeStatus = Constants.UNFAVOURITE;
                	recipeInBook.update();
                }
            } else {
                return badRequest(Messages.get("recipeBook.does.not.contain.recipe"));
            }

            return ok(Messages.get("recipeFromRecipeBook.remove.success"));
            
        } catch (Exception e) {
            Logger.error("Recipe deletion from recipe book encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
	}
	
	public static boolean getUserActiveFavouritedRecipe(long recipeId) {
        List<BookRecipe> favs = new ArrayList<BookRecipe>();
        try {
            User user = Application.getSessionUser();
            if(user != null) {
                favs = BookRecipe.findActiveRecipesByUserAndRecipe(user.userId, recipeId);
            }
        } catch (Exception e) {
            Logger.error("Get User RecipeBook Favs encountered error : ", e);
            return false;
        }
        return favs.size()>0?true:false;
	}
	
	public static int getActiveRecipeFavs(long recipeId) {
	    return BookRecipe.findActiveRecipeFavCount(recipeId);
	}
	
	public static int getAllRecipeFavs(long recipeId) {
	    return BookRecipe.findAllRecipeFavCount(recipeId);
	}
	
}
