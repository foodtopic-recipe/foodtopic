package controllers.recipeReview;

import controllers.Application;
import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.utils.Constants;
import models.RecipeReview;
import models.RecipeReviewComment;
import models.User;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.error.errorModal;
import views.html.recipeReview.editReviewModal;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditRecipeReview extends Controller {

	public long reviewId;
	public long recipeId;
	public int reviewScore;
	public String comment;
	public String imagePath;


	public static Result updateReviewModal(long reviewId) {
        final User user = User.findByEmail(request().username());
        RecipeReview review = RecipeReview.find.byId(reviewId);
        
        // Additional check to determine whether login user has the rights to edit this recipe
        if(user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(review == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("review.not.found")));
        }
		return ok(editReviewModal.render(review));
	}

    public static Result updateReview() {
		DynamicForm data = form().bindFromRequest();
        long reviewId = 0;
        int reviewScore = 0;
        
        String rawReviewId = data.get("reviewId");
        String rawReviewScore = data.get("rating");
        String rawComment = data.get("comment");
        try {
            reviewId = Long.parseLong(rawReviewId);
            reviewScore = Integer.parseInt(rawReviewScore);
        } catch (Exception e) {
			Logger.error("Update of review : " + rawReviewId + "and score of : " + rawReviewScore + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
        
        //Additional check to determine whether login user has the rights to edit this review
        final User user = User.findByEmail(request().username());
        RecipeReview currentReview = RecipeReview.find.byId(reviewId);
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(currentReview == null) {
            return badRequest(Messages.get("review.not.found"));
        } else if(currentReview.recipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_DELETED || currentReview.recipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_HIDDEN) {
            return badRequest(Messages.get("recipe.deleted.status"));
        } else if(!currentReview.user.equals(user)) {
            return badRequest(Messages.get("review.edit.invalid.rights"));
        }
		
        /*if (StringUtils.isBlank(rawComment)) {
            return badRequest(Messages.get("validate.required", Messages.get("comment")));
        }*/
        
        try {
            currentReview.reviewScore = reviewScore;

            if(currentReview.recipeReviewComment != null) {
                currentReview.recipeReviewComment.commentDescription = rawComment;
                currentReview.recipeReviewComment.update();
            } else {
                RecipeReviewComment reviewComment = new RecipeReviewComment();
                reviewComment.user = user;
                reviewComment.recipe = currentReview.recipe;
                reviewComment.recipeReview = currentReview;
                reviewComment.commentType = Constants.SUBSETCODE_COMMENT_TYPE_REVIEW;
                reviewComment.commentDescription = rawComment;
                reviewComment.save();

                currentReview.recipeReviewComment = reviewComment;
            }

            currentReview.update();
            
            CreateNotification.reviewNotification(currentReview.recipe.user, user, Messages.get("notification.editReview", controllers.recipe.routes.ViewRecipe.initViewRecipeForm(currentReview.recipe.recipeId).url(), currentReview.recipe.recipeName), null);
            
            /*
            MultipartFormData formData = request().body().asMultipartFormData();
            Logger.info(" PRINT IMAGE PATH : " + filledForm.get().imagePath);
            FilePart reviewPhoto = formData.getFile("imagePath");
            
            if (reviewPhoto != null) {
                String fileType = reviewPhoto.getFilename().substring(reviewPhoto.getFilename().lastIndexOf("."));
                String fileName = UUID.randomUUID().toString() + fileType;
                
                review.imagePath = Play.application().configuration().getString("reviewImageUploadPath").replace("*RECIPEID*", "" + recipe.recipeId).replace("*REVIEWID*", "" + review.reviewId) + fileName;
                
                String filePath = play.Play.application().path().toString() + "/" + review.imagePath;
                byte[] byteFile = IOUtils.toByteArray(new FileInputStream(reviewPhoto.getFile()));
                
                File imageFile = new File(filePath);
                FileUtils.writeByteArrayToFile(imageFile, byteFile);
                
                Logger.info("Review image path : " + imageFile.getAbsolutePath());
            }
            review.update();
            */
            
            return ok(Messages.get("review.update.success"));
        } catch (Exception e) {
            Logger.error("Review update encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
	}
}
