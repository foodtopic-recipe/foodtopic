package controllers.recipeReview;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.RecipeReview;
import models.RecipeReviewComment;
import models.RecipeReviewCommentFlag;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.error.errorModal;
import views.html.recipeReview.deleteReviewModal;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class DeleteRecipeReview extends Controller {

    public static Result deleteReviewModal(final Long reviewId) {
        final User user = User.findByEmail(request().username());
        final RecipeReview review = RecipeReview.find.byId(reviewId);

        // Additional check to determine whether login user has the rights to edit this recipe
        if (user == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if (review == null) {
            return ok(errorModal.render(user, Application.FLASH_ERROR_KEY, Messages.get("invalid.rights"), Messages.get("review.not.found")));
        }

        return ok(deleteReviewModal.render(review));
    }

    public static Result deleteReview() {
        final DynamicForm data = form().bindFromRequest();
        long reviewId = 0;

        final String rawReviewId = data.get("reviewId");

        try {
            reviewId = Long.parseLong(rawReviewId);
            Logger.debug("Delete Review - reviewId : " + reviewId);
        } catch (Exception e) {
            Logger.error("Deletion of review : " + rawReviewId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }

        final RecipeReview currentReview = RecipeReview.find.byId(reviewId);
        Logger.debug("Delete Review found review : " + currentReview);

        // Additional check to determine whether login user has the rights to
        // delete this review
        final User user = User.findByEmail(request().username());

        if (user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if (currentReview == null) {
            return badRequest(Messages.get("review.not.found"));
        } else if (!currentReview.user.equals(user)) {
            return badRequest(Messages.get("review.delete.invalid.rights"));
        }

        try {
            currentReview.reviewStatus = Constants.UNREVIEWED;
            RecipeReviewComment reviewComment = null;

            // Break the bonds between review & comment
            if (currentReview.recipeReviewComment != null) {
                reviewComment = currentReview.recipeReviewComment;

                // Break the bonds between comment & comment flag if any
                for(RecipeReviewCommentFlag commentFlag : reviewComment.recipeReviewCommentFlagList) {
                    commentFlag.delete();
                }
                currentReview.recipeReviewComment = null;
            }
            currentReview.update();

            if(reviewComment != null)
                reviewComment.delete();

            return ok(Messages.get("review.delete.success"));
        } catch (Exception e) {
            Logger.error("Review deletion encountered error : ", e);

            return badRequest(Messages.get("error.technical"));
        }
    }
}
