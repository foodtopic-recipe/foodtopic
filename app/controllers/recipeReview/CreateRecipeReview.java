package controllers.recipeReview;

import controllers.Application;
import controllers.Secured;
import controllers.notification.CreateNotification;
import controllers.reputation.Reputation;
import controllers.utils.Constants;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class CreateRecipeReview extends Controller {

	public long recipeId;
	public int reviewScore;
	public String comment;
	public String imagePath;


    /**
     * Defines a constructor.
     */ 
	public CreateRecipeReview() {
		//reviewScore = 1;
		comment = "";
	}
	
    /**
     * Defines a form wrapping the CreateReview class.
     */ 
    final static Form<CreateRecipeReview> createRecipeReviewForm = form(CreateRecipeReview.class);
    
    public static Form<CreateRecipeReview> getCreateReviewForm() {
		return createRecipeReviewForm.fill(new CreateRecipeReview());
	}
    
    // This method is used in view recipe page where this determines whether to display create review form
    public static boolean showCreateReview(long recipeId) {
        final User user = Application.getSessionUser();
        final Recipe recipe = Recipe.find.byId(recipeId);
        
        if(user == null || recipe == null) {
            return false;
        }
        
        RecipeReview existingReview = RecipeReview.findAllReviewByUserAndRecipe(user.userId, recipe.recipeId);
        return existingReview == null ? true : !existingReview.reviewStatus; 
    }
    /**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise	
    
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
		if (StringUtils.isBlank(rating)) {
            errors.add(new ValidationError("rating", Messages.get("review.rating.required")));
        }
        else if(!StringUtils.isNumeric(rating)) {
            errors.add(new ValidationError("rating", Messages.get("validate.not.numeric")));
        }
		
        if (StringUtils.isBlank(comment)) {
        	errors.add(new ValidationError("comment", Messages.get("validate.required", Messages.get("comment"))));
        }
        
        return errors.isEmpty() ? null : errors;
	}
	*/
	
    public static Result postReview() {
        Form<CreateRecipeReview> filledForm = createRecipeReviewForm.bindFromRequest();
        
        if (filledForm.get().reviewScore < 1) {
            return badRequest(Messages.get("review.rating.required"));
        }
		
        /*
        if (StringUtils.isBlank(filledForm.get().comment)) {
            return badRequest(Messages.get("validate.required", Messages.get("comment")));
        }*/
        
        CreateRecipeReview createReviewFilledForm = filledForm.get();
        
        final User user = User.findByEmail(request().username());
        Recipe recipe = Recipe.find.byId(createReviewFilledForm.recipeId);
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(recipe == null) {
            return badRequest(Messages.get("recipe.not.found"));
        } else if(recipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_DELETED || recipe.statusCode.subsetCodeId == Constants.PUBLISH_STATUS_HIDDEN) {
            return badRequest(Messages.get("recipe.deleted.status"));
        }
        
        RecipeReview review = RecipeReview.findAllReviewByUserAndRecipe(user.userId, recipe.recipeId);

        try {
        	boolean newReview = false;
        	
        	if(review == null) {
        		review = new RecipeReview();
        		newReview = true;
            }
            review.user = user;
            review.recipe = recipe;
            review.reviewScore = createReviewFilledForm.reviewScore;
            review.reviewStatus = Constants.REVIEWED;
            
            if(!StringUtils.isBlank(createReviewFilledForm.comment)) {
            	RecipeReviewComment reviewComment = new RecipeReviewComment();
                reviewComment.user = user;
                reviewComment.recipe = recipe;
                reviewComment.recipeReview = review;
                reviewComment.commentType = Constants.SUBSETCODE_COMMENT_TYPE_REVIEW;
                reviewComment.commentDescription = createReviewFilledForm.comment;
                reviewComment.save();
                review.recipeReviewComment = reviewComment;
            }
            
            if(newReview) {
            	review.save();
            	CreateNotification.reviewNotification(recipe.user, user, Messages.get("notification.newReview", review.reviewScore, controllers.recipe.routes.ViewRecipe.initViewRecipeForm(createReviewFilledForm.recipeId).url(), recipe.recipeName), null);

                // Update user's reputation based on new review points (Cannot self review recipe to gain reputation points)
                if (recipe.user.userId != user.userId) {
                    recipe.user.reputationPoints += Reputation.getNewReviewReputationPoints(review.reviewScore);
                    recipe.user.update();
                }
            } else {
            	review.update();
            	CreateNotification.reviewNotification(recipe.user, user, Messages.get("notification.editReview", controllers.recipe.routes.ViewRecipe.initViewRecipeForm(createReviewFilledForm.recipeId).url(), recipe.recipeName), null);
            }
			
            /*
            MultipartFormData formData = request().body().asMultipartFormData();
            Logger.info(" PRINT IMAGE PATH : " + filledForm.get().imagePath);
            FilePart reviewPhoto = formData.getFile("imagePath");
            
            if (reviewPhoto != null) {
                String fileType = reviewPhoto.getFilename().substring(reviewPhoto.getFilename().lastIndexOf("."));
                String fileName = UUID.randomUUID().toString() + fileType;
                
                review.imagePath = Play.application().configuration().getString("reviewImageUploadPath").replace("*RECIPEID*", "" + recipe.recipeId).replace("*REVIEWID*", "" + review.reviewId) + fileName;
                
                String filePath = play.Play.application().path().toString() + "/" + review.imagePath;
                byte[] byteFile = IOUtils.toByteArray(new FileInputStream(reviewPhoto.getFile()));
                
                File imageFile = new File(filePath);
                FileUtils.writeByteArrayToFile(imageFile, byteFile);
                
                Logger.info("Review image path : " + imageFile.getAbsolutePath());
            }
            review.update();
            */

            return ViewRecipeReview.initListReviewForm(recipe.recipeId);
        } catch (Exception e) {
            Logger.error("Review creation encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
	}
}
