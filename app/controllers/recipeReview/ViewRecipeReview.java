package controllers.recipeReview;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.Recipe;
import models.RecipeReview;
import models.RecipeReviewCommentFlag;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.recipeReview.listReview;
import views.html.recipeReview.listReviewRow;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class ViewRecipeReview extends Controller {
	
    /**
	 * Listing of recipe review
	 *
	 * @return review list
	 */
    public static List<RecipeReview> getActiveReviewList(long recipeId) {
		final User user = Application.getSessionUser();
        List<RecipeReview> reviewList = new ArrayList<RecipeReview>();
			
        if(user != null) {
            Set<RecipeReview> mergeSet = new HashSet<RecipeReview>(reviewList);
            RecipeReview rReview = RecipeReview.findActiveReviewByUserAndRecipe(user.userId, recipeId);
            if(rReview != null) mergeSet.add(rReview);
            mergeSet.addAll(RecipeReview.findActiveReviewByRecipe(recipeId, Constants.FETCH_REVIEW_PAGE_ONE, true));
            reviewList = new ArrayList<RecipeReview>(mergeSet);
        } else {
        	reviewList = RecipeReview.findActiveReviewByRecipe(recipeId, Constants.FETCH_REVIEW_PAGE_ONE, true);
        }
        return reviewList;
    }
    
    /**
	 * Display the list of recipe active review
	 *
	 * @return list recipe active review page
	 */
    public static Result initListReviewForm(long recipeId) {
        final User user = User.findByEmail(request().username());
        Recipe recipe = Recipe.find.byId(recipeId);
        
        if(user == null) 
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        else if(recipe == null)
            return badRequest(Messages.get("recipe.not.found"));
        else 
            return ok(listReview.render(user, getActiveReviewList(recipeId), Constants.MAX_REVIEW_FETCH_LIMIT, recipeId));
    }

    public static int findTotalNumberOfReviews(long recipeId) {
        final User user = User.findByEmail(request().username());
        if(user == null)
            return 0;
        else
            return RecipeReview.findActiveReviewByRecipe(recipeId, Constants.FETCH_REVIEW_PAGE_ONE, false).size();
    }

    public static Result getNextReviewList() {
        final User user = User.findByEmail(request().username());
        
        if(user == null) 
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        
        DynamicForm data = form().bindFromRequest();
		int page = 0, limit = 0, recipeId = 0;
		String search = "";
		
		try {
		    page = Integer.parseInt(data.get("page"));
		    limit = Integer.parseInt(data.get("limit"));
		    recipeId = Integer.parseInt(data.get("recipeId"));
        } catch (Exception e) {
			Logger.error("Get next review list encountered error : ", e);
        }
        
        List<RecipeReview> reviewList = getActiveReviewList(recipeId);
            
        int skipCount = limit * page;
        if(reviewList.size() <= skipCount) {
            return ok();
        } else {
            int lastRecord = limit + skipCount >= reviewList.size() ? reviewList.size() : limit + skipCount;
            reviewList = reviewList.subList(skipCount, lastRecord);
            
            return ok(listReviewRow.render(user, reviewList, lastRecord));
        }
    }
    
    public static boolean getUserUpvoteFlag(final long commentId) {
        RecipeReviewCommentFlag commentFlag = null;
        
        try {
            final User user = Application.getSessionUser();
            
            if (user != null) {
                commentFlag = RecipeReviewCommentFlag.findByUserAndComment(user.userId, commentId);
            }
        } catch (Exception e) {
            Logger.error("Get User Flag Comment encountered error : ", e);
            
            return false;
        }
        
        return ((commentFlag != null) && (commentFlag.flag == true)) ? true : false;
    }
    
    public static boolean getUserDownvoteFlag(final long commentId) {
        RecipeReviewCommentFlag commentFlag = null;
        
        try {
            final User user = Application.getSessionUser();
            
            if (user != null) {
                commentFlag = RecipeReviewCommentFlag.findByUserAndComment(user.userId, commentId);
            }
        } catch (Exception e) {
            Logger.error("Get User Flag Comment encountered error : ", e);
            
            return false;
        }
        
        return ((commentFlag != null) && (commentFlag.flag == false)) ? true : false;
    }}
