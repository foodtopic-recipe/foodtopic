package controllers;

import controllers.account.ViewProfile;
import controllers.recipe.ViewRecipe;
import controllers.utils.Constants;
import models.Recipe;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.dashboard.index;
import views.html.error.error;
import views.html.dashboard.listDashboardRow;

import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class Dashboard extends Controller {
    
    public static Result index() {
        final User user = User.findByEmail(request().username());
        
        return ok(index.render(user, ViewRecipe.getActiveFollowingLatestRecipeList(user, Constants.MAX_DASHBOARD_LIMIT, 0), ViewRecipe.getFeaturedRecipe(Constants.FETCH_RECIPE_PAGE_ONE), ViewProfile.getFeaturedUsers(Constants.FETCH_USER_PAGE_ONE), Constants.MAX_DASHBOARD_LIMIT));
    }

    /**
     * Ajax call to retrieve another set of recipe list
     *
     * @return  next recipe list value.
     *
     */
    public static Result getNextDashboardList() {
        final DynamicForm data = form().bindFromRequest();
        int page = 0;

        try {
            page = Integer.parseInt(data.get("page"));
        } catch (Exception e) {
            Logger.error("Get next dashboard list encountered error : ", e);
        }
        final User user = User.findByEmail(request().username());

        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }

        List<Recipe> recipeList = ViewRecipe.getActiveFollowingLatestRecipeList(user, Constants.MAX_DASHBOARD_LIMIT, page);

        return ok(listDashboardRow.render(recipeList, Constants.MAX_DASHBOARD_LIMIT));
    }
}
