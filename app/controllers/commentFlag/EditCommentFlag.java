package controllers.commentFlag;

import controllers.Secured;
import models.RecipeReviewComment;
import models.RecipeReviewCommentFlag;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditCommentFlag extends Controller {

	public static Result recipeReviewUpVote() {
		DynamicForm data = form().bindFromRequest();
		
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        
        String rawCommentId = data.get("commentId");
        long commentId = 0;
        try {
            commentId = Long.parseLong(rawCommentId);
        } catch (Exception e) {
			Logger.error("User : " + user.userId + " trying to upvote comment : " + rawCommentId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
        
        RecipeReviewComment comment = RecipeReviewComment.findByCommentId(commentId);
        if(comment == null) {
            return notFound(error.render(user, Messages.get("comment.not.found"), Messages.get("comment.not.found.desc")));
        }
        try {
        	RecipeReviewCommentFlag commentFlagResult = RecipeReviewCommentFlag.findByUserAndComment(user.userId, commentId);
            if(commentFlagResult != null) {
                if(!commentFlagResult.flag) {
                    commentFlagResult.flag = true;
                    commentFlagResult.update();
                } else {
                    return ok();
                    //return badRequest(Messages.get("upvote.only.once"));
                }
            } else {
            	RecipeReviewCommentFlag flag = new RecipeReviewCommentFlag();
                flag.user = user;
                flag.recipeReviewComment = comment;
                flag.flag = true;
                flag.save();
            }
            return ok(Messages.get("commentFlag.upvote.success"));
        } catch (Exception e) {
            Logger.error("Review creation encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
	}
    
	public static Result recipeReviewDownVote() {
		DynamicForm data = form().bindFromRequest();
		
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        
        String rawCommentId = data.get("commentId");
        long commentId = 0;
        try {
            commentId = Long.parseLong(rawCommentId);
        } catch (Exception e) {
			Logger.error("User : " + user.userId + " trying to downvote comment : " + rawCommentId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
        
        RecipeReviewComment comment = RecipeReviewComment.findByCommentId(commentId);
        if(comment == null) {
            return notFound(error.render(user, Messages.get("comment.not.found"), Messages.get("comment.not.found.desc")));
        }
        try {
        	RecipeReviewCommentFlag commentFlagResult = RecipeReviewCommentFlag.findByUserAndComment(user.userId, commentId);
            if(commentFlagResult != null) {
                if(commentFlagResult.flag) {
                    commentFlagResult.flag = false;
                    commentFlagResult.update();
                } else {
                    return ok();
                    //return badRequest(Messages.get("downvote.only.once"));
                }
            } else {
            	RecipeReviewCommentFlag flag = new RecipeReviewCommentFlag();
                flag.user = user;
                flag.recipeReviewComment = comment;
                flag.flag = false;
                flag.save();
            }
            
            return ok(Messages.get("commentFlag.downvote.success"));
        } catch (Exception e) {
            Logger.error("Review creation encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
	}
}
