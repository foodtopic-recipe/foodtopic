package controllers.recipeBook;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.recipeBook.editRecipeBook;

import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class EditRecipeBook extends Controller {

	public int monetizationCode;
    public double price;
	public int privacyCode;
	public String recipeBookName;
	public String tags;
	
	/**
     * Defines a form wrapping the EditRecipeBook class.
     */ 
    final static Form<EditRecipeBook> editRecipeBookForm = form(EditRecipeBook.class);
    
    
    public static Result initEditRecipeBookForm(long recipeBookId) {
        final User user = User.findByEmail(request().username());
        RecipeBook recipeBook = RecipeBook.find.byId(recipeBookId);
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(recipeBook != null && recipeBook.user.equals(user)) {
            return ok(editRecipeBook.render(user, recipeBook, editRecipeBookForm.fill(initForm(recipeBook))));
        } else {
            return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipeBook.edit.invalid.rights")));
        }
    }
    
    
    public static EditRecipeBook initForm(RecipeBook recipebook) {
        EditRecipeBook value = new EditRecipeBook();

        value.monetizationCode = recipebook.monetizationCode != null ? recipebook.monetizationCode.subsetCodeId : Constants.MONETIZATION_STATUS_FREE;
        value.price = recipebook.price;
        value.privacyCode = recipebook.privacyCode != null ? recipebook.privacyCode.subsetCodeId : Constants.PRIVACY_STATUS_PUBLIC;
        value.recipeBookName = recipebook.bookName;

        String[] tagSpiltList = new String[]{};
        for(int i=0; i<recipebook.recipeBookTagList.size(); i++) {
            if(recipebook.recipeBookTagList.get(i).tag != null && !StringUtils.isBlank(recipebook.recipeBookTagList.get(i).tag.tagName)) {
                tagSpiltList[i] = recipebook.recipeBookTagList.get(i).tag.tagName;
            }
        }
        value.tags = Utilities.convertStringArrayToCommaString(tagSpiltList);
        
        return value;
    }
    
    /**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise
	 */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
		if (StringUtils.isBlank(recipeBookName)) {
			errors.add(new ValidationError("recipeBookName", Messages.get("validate.required", Messages.get("recipeBook.name"))));
		}
        
		if (!StringUtils.isBlank(tags) && !tags.matches("^[\\w,?\\s?]*$")) {
			errors.add(new ValidationError("tags", Messages.get("tags.not.alphabets.with.comma")));
		}
        
		Logger.debug("Edit Recipe Book error list : " + errors.toString());
			    
        return errors.isEmpty() ? null : errors;
	}
	
	public static Result updateRecipeBook(long recipeBookId) {
        Form<EditRecipeBook> filledForm = editRecipeBookForm.bindFromRequest();
        RecipeBook recipeBook = RecipeBook.find.byId(recipeBookId);
        
        // Additional check to determine whether login user has the rights to edit this recipe book
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(recipeBook == null) {
            return notFound(error.render(user, Messages.get("recipeBook.not.found"), Messages.get("recipeBook.not.found.desc")));
        } else if(!recipeBook.user.equals(user)) {
            return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipeBook.edit.invalid.rights")));
        }

        if (filledForm.hasErrors()) {
            return badRequest(editRecipeBook.render(user, recipeBook, filledForm));
        }

        EditRecipeBook editRecipeBookFilledForm = filledForm.get();

        try {
            //recipeBook.monetizationCode = SubsetCode.getSubsetCodeById(editRecipeBookFilledForm.monetizationCode);
            recipeBook.monetizationCode = Constants.SUBSETCODE_MONETIZATION_STATUS_FREE;
            recipeBook.privacyCode = SubsetCode.getSubsetCodeById(editRecipeBookFilledForm.privacyCode);
            recipeBook.bookName = editRecipeBookFilledForm.recipeBookName;

            for (int i = 0; i < recipeBook.recipeBookTagList.size(); i++) {
                recipeBook.recipeBookTagList.get(i).delete();
            }
            recipeBook.recipeBookTagList.clear();

            String[] tagList = Utilities.spiltStringByComma(editRecipeBookFilledForm.tags);
            for(int i=0; i<tagList.length; i++) {
                if(!StringUtils.isBlank(tagList[i])) {
                    String tagName = tagList[i].trim();
                    Tag tag = Tag.getTagByName(tagName);

                    if(tag == null) {
                        tag = new Tag();
                        tag.tagName = tagName;
                        tag.save();
                    }

                    recipeBook.recipeBookTagList.add(new RecipeBookTag(recipeBook, tag));
                }
            }
            recipeBook.update();

            flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipeBook.update.success"));
            return redirect(controllers.recipeBook.routes.ViewRecipeBook.initListOwnRecipeBookForm().url());
        } catch (Exception e) {
            Logger.error("RecipeBook update encountered error : ", e);
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
            return badRequest(editRecipeBook.render(user, recipeBook, filledForm));
        }
	}
}
