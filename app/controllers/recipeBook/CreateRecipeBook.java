package controllers.recipeBook;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import models.*;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.recipeBook.createRecipeBook;

import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class CreateRecipeBook extends Controller {
	
	public int monetizationCode;
    public double price;
	public int privacyCode;
	public String recipeBookName;
	public String tags;

    /**
     * Defines a constructor.
     */ 
	public CreateRecipeBook() {
		monetizationCode = Constants.MONETIZATION_STATUS_FREE;
		price = 0.00;
		privacyCode = Constants.PRIVACY_STATUS_PUBLIC;
		tags = "";
	}

    /**
     * Defines a form wrapping the CreateRecipe class.
     */ 
    final static Form<CreateRecipeBook> createRecipeBookForm = form(CreateRecipeBook.class);
    
	
	/**
	 * Display the create recipe book page if login
	 *
	 * @return create recipe book page
	 */
    public static Result initCreateRecipeBookForm() {
        final User user = User.findByEmail(request().username());
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        return ok(createRecipeBook.render(user, createRecipeBookForm.fill(new CreateRecipeBook())));
    }
    
	/**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise
	 */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
		if (StringUtils.isBlank(recipeBookName)) {
			errors.add(new ValidationError("recipeBookName", Messages.get("validate.required", Messages.get("recipeBook.name"))));
		}
        
		if (!StringUtils.isBlank(tags) && !tags.matches("^[\\w,?\\s?]*$")) {
			errors.add(new ValidationError("tags", Messages.get("tags.not.alphabets.with.comma")));
		}

		Logger.debug("Create Recipe Book error list : " + errors.toString());
			    
        return errors.isEmpty() ? null : errors;
	}
	
    
	public static Result createBook() {
        Form<CreateRecipeBook> filledForm = createRecipeBookForm.bindFromRequest();
        final User user = User.findByEmail(request().username());
        
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        }
        if (filledForm.hasErrors()) {
			return badRequest(createRecipeBook.render(user, filledForm));
		}
        
		CreateRecipeBook createRecipeBookFilledForm = filledForm.get();
		RecipeBook recipeBook = new RecipeBook();
        
		try {
			recipeBook.user = user;
            
			recipeBook.monetizationCode = SubsetCode.getSubsetCodeById(createRecipeBookFilledForm.monetizationCode);
			recipeBook.privacyCode = SubsetCode.getSubsetCodeById(createRecipeBookFilledForm.privacyCode);
			recipeBook.bookName = createRecipeBookFilledForm.recipeBookName;
			
			String[] tagList = Utilities.spiltStringByComma(createRecipeBookFilledForm.tags);            
                for(int i=0; i<tagList.length; i++) {
                    if(!StringUtils.isBlank(tagList[i])) {
                        String tagName = tagList[i].trim();
                        Tag tag = Tag.getTagByName(tagName);
                        
                        if(tag == null) {
                            tag = new Tag();
                            tag.tagName = tagName;
                            tag.save();
                        }

                        recipeBook.recipeBookTagList.add(new RecipeBookTag(recipeBook, tag));
                        tagList[i] = tag.tagId.toString();
                    }
                }
            recipeBook.save();
            
			flash(Application.FLASH_SUCCESS_KEY, Messages.get("recipeBook.create.success"));
			
		} catch (Exception e) {
			Logger.error("Recipe creation encountered error : ", e);
			flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
			return badRequest(createRecipeBook.render(user, filledForm));
		}
		
		return ok(createRecipeBook.render(user, createRecipeBookForm));
	}
}
