package controllers.recipeBook;

import static play.data.Form.form;

import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.Secured;
import models.RecipeBook;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;

@Security.Authenticated(Secured.class)
public class DeleteRecipeBook extends Controller {
    
	public static Result deleteRecipeBook() {
		DynamicForm data = form().bindFromRequest();
		long recipeBookId = 0;
		
		String rawRecipeBookId = data.get("recipeBookId");
		try {
		    recipeBookId = Long.parseLong(rawRecipeBookId);
        } catch (Exception e) {
			Logger.error("Recipe Book deletion of " + rawRecipeBookId + " encountered error : ", e);
            return badRequest(Messages.get("error.technical"));
        }
        RecipeBook currentRecipeBook = RecipeBook.find.byId(recipeBookId);
        
        //Additional check to determine whether login user has the rights to delete this recipe book
        final User user = User.findByEmail(request().username());
        if(user == null) {
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        } else if(currentRecipeBook == null) {
            return badRequest(Messages.get("recipeBook.not.found"));
        } else if(!currentRecipeBook.user.equals(user)) {
            return notFound(error.render(user, Messages.get("invalid.rights"), Messages.get("recipeBook.delete.invalid.rights")));
        }
        
        currentRecipeBook.delete();
        
		ObjectNode jsonResult = Json.newObject();
		jsonResult.put("redirect", "/listRecipeBook/own");
		return ok(jsonResult.toString());
        //return ok(listRecipeBook.render(user, Messages.get("recipeBook.my"), RecipeBook.findAllRecipeBookByUser(user.userId, Constants.FETCH_RECIPE_BOOK_PAGE_ONE)));
	}
}
