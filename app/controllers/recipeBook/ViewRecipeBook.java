package controllers.recipeBook;

import java.util.ArrayList;
import java.util.List;

import controllers.Secured;
import controllers.utils.Constants;
import models.BookRecipe;
import models.RecipeBook;
import models.User;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.error.error;
import views.html.recipeBook.listRecipeBook;
import views.html.recipeBook.viewRecipeBook;

@Security.Authenticated(Secured.class)
public class ViewRecipeBook extends Controller {


	/**
	 * Display the list recipe book page if login
	 *
	 * @return list recipe book page
	 */
    public static Result initListOwnRecipeBookForm() {
    	User user = User.findByEmail(request().username());
    	List<RecipeBook> recipeBookList = RecipeBook.findAllRecipeBookByUser(user.userId, Constants.FETCH_RECIPE_BOOK_PAGE_ONE);
    	
    	if(user == null)
			return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
		else if(recipeBookList != null)
    		return ok(listRecipeBook.render(user, Messages.get("recipeBook.my"), recipeBookList));
    	else 
    		return notFound(error.render(user, Messages.get("no.recipeBook.association"), Messages.get("no.recipeBook.association.desc")));
    }

	/**
	 * Display the list recipe book page if login
	 *
	 * @return list recipe book page
	 */
    public static Result initListPopularRecipeBookForm() {
    	User user = User.findByEmail(request().username());
    	List<RecipeBook> recipeBookList = RecipeBook.findPopularPublicRecipeBook(Constants.FETCH_RECIPE_BOOK_PAGE_ONE);
    	
    	if(user == null)
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        else if(recipeBookList != null)
    		return ok(listRecipeBook.render(user, Messages.get("recipeBook.popular"), recipeBookList));
    	else 
    		return notFound(error.render(user, Messages.get("recipeBook.not.found"), Messages.get("recipeBook.not.found.desc")));
    }


	/**
	 * Display the view recipe book page if login
	 *
	 * @return view recipe book page
	 */
    public static Result initViewRecipeBookForm(long recipeBookId) {
    	User user = User.findByEmail(request().username());
    	RecipeBook recipeBook = RecipeBook.find.byId(recipeBookId);
    	
    	if(user == null)
            return notFound(error.render(user, Messages.get("signin.required"), Messages.get("signin.required.desc")));
        else if(recipeBook != null && ((recipeBook.user.equals(user) && recipeBook.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PRIVATE) || recipeBook.privacyCode.subsetCodeId == Constants.PRIVACY_STATUS_PUBLIC))
    		return ok(viewRecipeBook.render(user, recipeBook, BookRecipe.findActiveRecipesByRecipeBook(recipeBook.recipeBookId)));
    	else 
    		return notFound(error.render(user, Messages.get("recipeBook.not.found"), Messages.get("recipeBook.not.found.desc")));
    }

	public static List<BookRecipe> getActiveRecipesInBook(RecipeBook recipeBook) {
		List<BookRecipe> bookRecipeList = new ArrayList<>();

		for(BookRecipe bookRecipe : recipeBook.bookRecipeList) {
			if(bookRecipe.bookRecipeStatus) {
				bookRecipeList.add(bookRecipe);
			}
		}
		return bookRecipeList;
	}
}
