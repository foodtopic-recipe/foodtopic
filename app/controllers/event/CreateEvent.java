package controllers.event;

import controllers.Application;
import controllers.Secured;
import controllers.utils.Constants;
import models.Event;
import models.SubsetCode;
import models.User;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Result;
import play.mvc.Security;
import views.html.event.createEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static play.data.Form.form;

@Security.Authenticated(Secured.class)
public class CreateEvent extends Controller {

	@Constraints.Required
	public int monetizationCode;
	public String eventName;
    public Date eventDate;
    public String eventDescription;
    public int eventType;
    public int maxParticipants;
    public double costPerPax;
    public String location;
	
    /**
     * Defines a constructor.
     */ 
	public CreateEvent() {
		eventName = "";
		eventDate = new Date();
		maxParticipants = 0;
		costPerPax = 0;
	}
    
    /**
     * Defines a form wrapping the CreateEvent class.
     */ 
    final static Form<CreateEvent> createEventForm = form(CreateEvent.class);
    
	/**
	 * Display the create event page if login
	 *
	 * @return create event page
	 */
    public static Result initCreateEventForm() {
        final User user = User.findByEmail(request().username());
        return ok(createEvent.render(user, createEventForm.fill(new CreateEvent())));
    }

	/**
	 * Defining an ad-hoc validation - which is called after checking annotation-based constraints and only if they pass.
	 *
	 * @return null if validation ok, string with details otherwise
	 */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
        MultipartFormData formData = request().body().asMultipartFormData();
		String[] postAction = formData.asFormUrlEncoded().get("action");
		
		if (postAction == null || postAction.length == 0) {
			Logger.debug("No post action");
            flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
		} else {
			String action = postAction[0];

			if (Messages.get("publish").equals(action)) {
				if (StringUtils.isBlank(eventName)) {
		            errors.add(new ValidationError("eventName", Messages.get("event.name.required")));
				}

				if (StringUtils.isBlank(eventDescription)) {
		            errors.add(new ValidationError("eventDescription", Messages.get("event.description.required")));
				}

				/*if (StringUtils.isBlank(eventType)) {
		            errors.add(new ValidationError("eventType", Messages.get("event.type.required")));
				}*/
				
				if (maxParticipants <= 0) {
		            errors.add(new ValidationError("maxParticipants", Messages.get("event.participants.required")));
				}
				
				if(monetizationCode != Constants.MONETIZATION_STATUS_PAID) {
					costPerPax = 0;
				}
			} else if (Messages.get("saveDraft").equals(action)) {
				
			}
		}
        return errors.isEmpty() ? null : errors;
	}

    public static Result newEvent() {
    	Form<CreateEvent> filledForm = createEventForm.bindFromRequest();
		final User user = User.findByEmail(request().username());

        MultipartFormData formData = request().body().asMultipartFormData();
		String[] postAction = formData.asFormUrlEncoded().get("action");

		if (postAction == null || postAction.length == 0) {
			Logger.error("User : " + user.userId + " encountered no button action while creating event");
			flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
			return badRequest(createEvent.render(user, filledForm));
		} else {
			String action = postAction[0];

			if (filledForm.hasErrors()) {
				return badRequest(createEvent.render(user, filledForm));
			}

			CreateEvent createEventFilledForm = filledForm.get();
			Event event = new Event();

			try {
				event.monetizationCode = SubsetCode.getSubsetCodeById(createEventFilledForm.monetizationCode);
				event.costPerPax = createEventFilledForm.costPerPax;

				event.user = user;
				event.eventName = StringUtils.isBlank(createEventFilledForm.eventName) ? "Draft" : createEventFilledForm.eventName;
				event.eventDate = createEventFilledForm.eventDate;
				event.eventDescription = createEventFilledForm.eventDescription;
				event.eventType = SubsetCode.getSubsetCodeById(createEventFilledForm.eventType);
				event.maxParticipants = createEventFilledForm.maxParticipants;
				event.location = createEventFilledForm.location;

				//Save event
				if (event.eventId == null)
					event.save();

				if (Messages.get("publish").equals(action)) {
					flash(Application.FLASH_SUCCESS_KEY, Messages.get("event.create.success"));
				} else if (Messages.get("draft").equals(action)) {
					flash(Application.FLASH_SUCCESS_KEY, Messages.get("event.create.success.draft"));
				}

			} catch (Exception e) {
				Logger.error("Event creation encountered error : ", e);
				flash(Application.FLASH_ERROR_KEY, Messages.get("error.technical"));
				return badRequest(createEvent.render(user, filledForm));
			}
		}

        return ok(createEvent.render(user, createEventForm.fill(new CreateEvent())));
    }
}
