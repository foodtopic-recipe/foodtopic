package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name="EVENT_ATTENDEE")
public class EventAttendee extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "EVENT_ATTENDEE_ID", unique = true, nullable = false)
    public Long eventAttendeeId;

	@ManyToOne
	@JoinColumn(name="EVENT_ID", referencedColumnName="EVENT_ID")
    public Event event;
	
	@OneToOne
	@JoinColumn(name="ATTENDEE_ID", referencedColumnName="USER_ID")
    public User user;

    @OneToOne
    @JoinColumn(name = "STATUS_CODE", referencedColumnName = "SUBSET_CODE_ID") 
    public SubsetCode statusCode;

	
	// -- Queries (long id, EventAttendee.class)
    public static Model.Finder<Long, EventAttendee> find = new Model.Finder<Long, EventAttendee>(Long.class, EventAttendee.class);

    /**
     * Retrieve a list of Event Attendees based on an Event.
     *
     * @param eventId to search
     * @return list of event attendees
     */
    public static List<EventAttendee> findByEvent(String eventId) {
        return find.where().eq("EVENT_ID", eventId).findList();
    }
}
