package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import controllers.utils.Constants;
import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_BOOK_TAG")
public class RecipeBookTag extends Base {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="RECIPE_BOOK_ID", referencedColumnName="RECIPE_BOOK_ID")
	public RecipeBook recipeBook;

	@ManyToOne
	@JoinColumn(name="TAG_ID", referencedColumnName="TAG_ID")
	public Tag tag;

	public RecipeBookTag(RecipeBook recipeBook, Tag tag) {
		this.recipeBook = recipeBook;
		this.tag = tag;
	}
	
	// -- Queries (long id, RecipeBookTag.class)
    public static Model.Finder<Long, RecipeBookTag> find = new Model.Finder<Long, RecipeBookTag>(Long.class, RecipeBookTag.class);

	/**
     * Retrieve a list of RecipeBooks based on a tag.
     *
     * @param tagId
     * @return list of recipeBooks
     */
    public static List<RecipeBookTag> findRecipeBookByRecipeBookTag(long tagId, int page) {
        return find.where().eq("TAG_ID", tagId).findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<RecipeBook> findAllPublicRecipeBookByTag(long tagId, int page) {
		List<RecipeBook> recipeBookList = new ArrayList<RecipeBook>();
		List<RecipeBookTag> recipeBookTagList = find.where().eq("recipe.PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).eq("TAG_ID", tagId).findPagingList(Constants.MAX_RECIPE_BOOK_FETCH_LIMIT).getPage(page).getList();
        for(RecipeBookTag recipeBookTag : recipeBookTagList) {
        	recipeBookList.add(recipeBookTag.recipeBook);
        }
        return recipeBookList;
    }

    public static List<RecipeBook> searchPublicRecipeBookByTagName(String tagName, int page) {
		List<RecipeBook> recipeBookList = new ArrayList<RecipeBook>();
		List<RecipeBookTag> recipeBookTagList = find.where().eq("recipe.PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).eq("tag.NAME", tagName).findPagingList(Constants.MAX_RECIPE_BOOK_FETCH_LIMIT).getPage(page).getList();
        for(RecipeBookTag recipeBookTag : recipeBookTagList) {
        	recipeBookList.add(recipeBookTag.recipeBook);
        }
        return recipeBookList;
	}

    public static List<RecipeBookTag> findAllPublicRecipeBookTagByTag(long tagId, int page) {
        return find.where().eq("recipe.STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("recipe.PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).eq("TAG_ID", tagId).isNotNull("recipe.PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_BOOK_FETCH_LIMIT).getPage(page).getList();
    }
    
	/**
     * Retrieve a list of Tags based on a recipe book.
     *
     * @param recipeBookId to search
     * @return list of tags of a recipe book
     */
    public static List<RecipeBookTag> findTagsByRecipeBook(long recipeBookId, int page) {
        return find.where().eq("RECIPE_BOOK_ID", recipeBookId).findPagingList(Constants.MAX_RECIPE_BOOK_FETCH_LIMIT).getPage(page).getList();
    }
}
