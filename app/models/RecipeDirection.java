package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_DIRECTION")
public class RecipeDirection extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "RECIPE_DIRECTION_ID", unique = true, nullable = false)
    public Long recipeDirectionId;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
    public Recipe recipe;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "DIRECTION_DESCRIPTION")
    public String directionDescription;
    
	@Column(name = "DIRECTION_IMAGE_PATH")
    public String imagePath;

	
	// -- Queries (long id, RecipeDirection.class)
    public static Model.Finder<Long, RecipeDirection> find = new Model.Finder<Long, RecipeDirection>(Long.class, RecipeDirection.class);

    /**
     * Retrieve a list of Recipe Directions based on a particular recipe.
     *
     * @param recipeId to search
     * @return list of recipe directions
     */
    public static List<RecipeDirection> findByRecipe(String recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).findList();
    }
}
