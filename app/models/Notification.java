package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import controllers.utils.Constants;
import play.Logger;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="NOTIFICATION")
public class Notification extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "NOTIFICATION_ID", unique = true, nullable = false)
    public Long notificationId;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
    public User user;
	
    @OneToOne
    @JoinColumn(name = "NOTIFICATION_TYPE", referencedColumnName = "SUBSET_CODE_ID") 
    public SubsetCode notificationType;

	@ManyToOne
	@JoinColumn(name="NOTIFIER_ID", referencedColumnName="USER_ID")
    public User notifier;
    
	@Column(name = "DESCRIPTION")
    public String description;
    
	@Column(name = "URL")
    public String url;
    
    @Formats.NonEmpty
	@Column(name = "NOTIFICATION_STATUS") //READ (1/TRUE) or UNREAD (0/FALSE)
    public boolean notificationStatus;
    
	// -- Queries (long id, Notification.class)
    public static Model.Finder<Long, Notification> find = new Model.Finder<Long, Notification>(Long.class, Notification.class);

	public static List<Notification> findAllNotificationByTypeDescription(long userId, int notificationType, String description) {
		return find.where().eq("NOTIFICATION_TYPE", notificationType).eq("DESCRIPTION", description).eq("USER_ID", userId).order().desc("CREATED_DATE").findList();
	}

	public static List<Notification> findAllNotificationByUser(long userId, int limit, int page, boolean paging) {
    	if(!paging)
        	return find.where().eq("USER_ID", userId).order().desc("CREATED_DATE").findList();
        else
        	return find.where().eq("USER_ID", userId).order().desc("CREATED_DATE").findPagingList(limit).getPage(page).getList();
    }
    
    public static List<Notification> findAllUnreadNotificationByUser(long userId, int page, boolean paging) {
    	if(!paging)
        	return find.where().eq("USER_ID", userId).eq("NOTIFICATION_STATUS", Constants.NOTIFICATION_STATUS_UNREAD).order().desc("CREATED_DATE").findList();
        else
        	return find.where().eq("USER_ID", userId).eq("NOTIFICATION_STATUS", Constants.NOTIFICATION_STATUS_UNREAD).order().desc("CREATED_DATE").findPagingList(Constants.MAX_NOTIFICATION_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<Notification> findAllReadNotificationByUser(long userId, int page, boolean paging) {
    	if(!paging)
        	return find.where().eq("USER_ID", userId).eq("NOTIFICATION_STATUS", Constants.NOTIFICATION_STATUS_READ).order().desc("CREATED_DATE").findList();
        else
        	return find.where().eq("USER_ID", userId).eq("NOTIFICATION_STATUS", Constants.NOTIFICATION_STATUS_READ).order().desc("CREATED_DATE").findPagingList(Constants.MAX_NOTIFICATION_FETCH_LIMIT).getPage(page).getList();
    }

    public static void userReadAllUnreadNotification(long userId) {
    	String queryString = "UPDATE NOTIFICATION SET NOTIFICATION_STATUS = :status WHERE USER_ID = :userId AND NOTIFICATION_STATUS = :unreadStatus";
    	SqlUpdate updateQuery = Ebean.createSqlUpdate(queryString);
    	updateQuery.setParameter("status", Constants.NOTIFICATION_STATUS_READ);
    	updateQuery.setParameter("userId", userId);
    	updateQuery.setParameter("unreadStatus", Constants.NOTIFICATION_STATUS_UNREAD);
    	
    	int modifiedCount = Ebean.execute(updateQuery);
    	 
    	Logger.debug("Update notification of user : " + userId + " where " + modifiedCount + " unread notifications is being read");
    }
}
