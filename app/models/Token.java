package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.QueryIterator;
import com.avaje.ebean.annotation.EnumValue;

import play.data.format.Formats;

@Entity
@Table(name="TOKEN")
public class Token extends Base {

	public enum Type {
		@EnumValue("EV")
		EMAIL_VERIFICATION,

		@EnumValue("PR")
		PASSWORD_RESET
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Verification time frame (until the user clicks on the link in the email)
	 * in seconds
	 * Defaults to one week
	 */
	private final static long VERIFICATION_TIME = 7 * 24 * 3600;

	@Id
	@Column(name = "TOKEN_ID", unique = true)
	public Long id;

	@Column(name = "TOKEN", unique = true)
	public String token;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
	public User targetUser;

	@Column(name = "TYPE")
	public Type type;

	@Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "EXPIRE_DATE")
	public Date expiryDate;

	public static final Finder<Long, Token> find = new Finder<Long, Token>(
			Long.class, Token.class);

	public static Token findByToken(final String token, final Type type) {
		return find.where().eq("token", token).eq("type", type).findUnique();
	}

	public static void deleteByUser(final User u, final Type type) {
		QueryIterator<Token> iterator = find.where()
				.eq("targetUser.userId", u.userId).eq("type", type).findIterate();
		Ebean.delete(iterator);
		iterator.close();
	}

	public boolean isValid() {
		return this.expiryDate.after(new Date());
	}

	public static Token create(final Type type, final String token, final User targetUser) {
		final Token ua = new Token();
		ua.targetUser = targetUser;
		ua.token = token;
		ua.type = type;
		ua.expiryDate = new Date(new Date().getTime() + VERIFICATION_TIME * 1000);
		ua.save();
		return ua;
	}
}
