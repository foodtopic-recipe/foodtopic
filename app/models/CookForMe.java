package models;

import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="COOK_FOR_ME")
public class CookForMe extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "COOK_FOR_ME_ID", unique = true, nullable = false)
	public Long cookForMeId;

    @OneToOne
    @JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
    public Recipe recipe;

    @OneToOne
    @JoinColumn(name = "PAYMENT_TYPE", referencedColumnName = "SUBSET_CODE_ID")
    public SubsetCode paymentType;

    @Constraints.Required
    @Column(name = "COLLECTION_MODE")
    public String collectionMode;

    @Constraints.Required
    @Column(name = "CURRENCY")
    public String currency;

	@Column(name = "COOKING_COST")
    public double cookingCost;

    @Column(name = "DELIVERY_FEE")
    public double deliveryFee;

    @Column(name = "REMARKS")
    public String remarks;

    @Formats.NonEmpty
    @Column(name = "COOK_FOR_ME_STATUS") //ACTIVE (1/TRUE) or INACTIVE (0/FALSE)
    public boolean cookForMeStatus;

    @OneToMany(targetEntity= CookForMeOrders.class, mappedBy="cookForMe", cascade=CascadeType.ALL)
    public List<CookForMeOrders> cookForMeOrders;


	// -- Queries (long id, CookForMe.class)
    public static Finder<Long, CookForMe> find = new Finder<Long, CookForMe>(Long.class, CookForMe.class);


    /**
     * Retrieve a list of cook for me based from a particular recipe.
     *
     * @param recipeId to search
     * @return list of cook for me
     */
    public static List<CookForMe> findByRecipe(long recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).findList();
    }

    /**
     * Retrieve a list of cook for me based from a particular recipe.
     *
     * @param userId to search
     * @return list of cook for me
     */
    public static List<CookForMe> findByUserId(long userId) {
        return find.where().eq("recipe.user.userId", userId).findList();
    }
}
