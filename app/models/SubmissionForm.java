package models;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name="SUBMISSIONFORM")
public class SubmissionForm extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "SUBMISSION_ID", unique = true, nullable = false)
    public Long submissionId;

    @Constraints.Required
    @Formats.NonEmpty
    @Constraints.Email
    @Column(name = "EMAIL", nullable = false)
    public String email;
    
    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "FIRST_NAME", nullable = false)
    public String firstName;
    
    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "LAST_NAME", nullable = false)
    public String lastName;
	
    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "COMMENTS", nullable = false)
    public String comments;
	

	// -- Queries (long id, user.class)
    public static Model.Finder<Long, User> find = new Model.Finder<Long, User>(Long.class, User.class);
    
}
