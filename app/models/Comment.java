package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@MappedSuperclass
public abstract class Comment extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "COMMENT_ID", unique = true, nullable = false)
	public Long commentId;

	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
	public User user;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COMMENT_TYPE", referencedColumnName = "SUBSET_CODE_ID") // Review, Question
	public SubsetCode commentType;

	@Column(name = "COMMENT_DESCRIPTION")
	public String commentDescription;

	
	// -- Queries (long id, Comment.class)
	public static Model.Finder<Long, Comment> find = new Model.Finder<Long, Comment>(Long.class, Comment.class);

    public static Comment findByCommentId(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).findUnique();
    }
    
    public static List<Comment> findByUser(long userId) {
        return find.where().eq("USER_ID", userId).findList();
    }
}
