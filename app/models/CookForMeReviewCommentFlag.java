package models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="COOK_FOR_ME_REVIEW_COMMENT_FLAG")
public class CookForMeReviewCommentFlag extends Flag {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="COMMENT_ID", referencedColumnName="COMMENT_ID")
    public CookForMeReviewComment cookForMeReviewComment;


    public static Finder<Long, CookForMeReviewCommentFlag> find = new Finder<Long, CookForMeReviewCommentFlag>(Long.class, CookForMeReviewCommentFlag.class);

    public static List<CookForMeReviewCommentFlag> findByComment(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).findList();
    }
    
    public static CookForMeReviewCommentFlag findByUserAndComment(long userId, long commentId) {
        return find.where().eq("USER_ID", userId).eq("COMMENT_ID", commentId).findUnique();
    }
    
    public static int countDownVoteByComment(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).eq("FLAG", false).findRowCount();
    }
    
    public static int countUpVoteByComment(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).eq("FLAG", true).findRowCount();
    }
}
