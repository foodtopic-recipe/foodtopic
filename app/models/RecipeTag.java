package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import controllers.utils.Constants;
import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_TAG")
public class RecipeTag extends Base {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
	public Recipe recipe;


	@ManyToOne
	@JoinColumn(name="TAG_ID", referencedColumnName="TAG_ID")
	public Tag tag;

	public RecipeTag(Recipe recipe, Tag tag) {
		this.recipe = recipe;
		this.tag = tag;
	}
	
	// -- Queries (long id, RecipeTag.class)
    public static Model.Finder<Long, RecipeTag> find = new Model.Finder<Long, RecipeTag>(Long.class, RecipeTag.class);

	/**
	 * Retrieve a list of RecipeTag based on tag Id.
	 *
	 * @param tagId
	 * @return list of RecipeTag
	 */
	public static List<RecipeTag> findByTagId(long tagId) {
		return find.where().eq("TAG_ID", tagId).findList();
	}

	/**
	 * Retrieve a list of RecipeTag based on recipe Id.
	 *
	 * @param recipeId to search
	 * @return list of RecipeTag
	 */
	public static List<RecipeTag> findByRecipeId(long recipeId) {
		return find.where().eq("RECIPE_ID", recipeId).findList();
	}

    public static List<Recipe> findAllPublishedPublicRecipeByTag(long tagId, int page) {
		List<Recipe> recipeList = new ArrayList<Recipe>();
		List<RecipeTag> recipetagList = find.where().eq("recipe.statusCode.subsetCodeId", Constants.PUBLISH_STATUS_PUBLISHED).eq("recipe.privacyCode.subsetCodeId", Constants.PRIVACY_STATUS_PUBLIC).eq("TAG_ID", tagId).isNotNull("recipe.publishedDate").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
        for(RecipeTag recipetag : recipetagList) {
			recipeList.add(recipetag.recipe);
        }
        return recipeList;
    }

    public static List<Recipe> searchPublishedPublicRecipeByTagName(String tagName, int page) {
		List<Recipe> recipeList = new ArrayList<Recipe>();
		List<RecipeTag> recipetagList = find.where().eq("recipe.statusCode.subsetCodeId", Constants.PUBLISH_STATUS_PUBLISHED).eq("recipe.privacyCode.subsetCodeId", Constants.PRIVACY_STATUS_PUBLIC).eq("tag.tagName", tagName).isNotNull("recipe.publishedDate").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
        for(RecipeTag recipetag : recipetagList) {
			recipeList.add(recipetag.recipe);
        }
        return recipeList;
	}

    public static List<RecipeTag> findAllPublishedPublicRecipeTagByTag(long tagId, int page) {
        return find.where().eq("recipe.statusCode.subsetCodeId", Constants.PUBLISH_STATUS_PUBLISHED).eq("recipe.privacyCode.subsetCodeId", Constants.PRIVACY_STATUS_PUBLIC).eq("TAG_ID", tagId).isNotNull("recipe.publishedDate").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }

	public void delete() {
		SqlUpdate deleteQuery = Ebean.createSqlUpdate("Delete from RECIPE_TAG WHERE RECIPE_ID = :recipe_id and TAG_ID = :tag_id");
		deleteQuery.setParameter("recipe_id", this.recipe.recipeId);
		deleteQuery.setParameter("tag_id", this.tag.tagId);
		deleteQuery.execute();
	}

}
