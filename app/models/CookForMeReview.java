package models;

import controllers.utils.Constants;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "COOK_FOR_ME_REVIEW")
public class CookForMeReview extends Review {
    private static final long serialVersionUID = 1L;

    public static Finder<Long, CookForMeReview> find = new Finder<Long, CookForMeReview>(Long.class, CookForMeReview.class);

    @ManyToOne
    @JoinColumn(name="ORDER_ID", referencedColumnName="ORDER_ID")
    public CookForMeOrders cookForMeOrders;

    @OneToOne(cascade = CascadeType.REMOVE, optional = true)
    @JoinColumn(name = "COMMENT_ID", referencedColumnName = "COMMENT_ID")
    public CookForMeReviewComment cookForMeReviewComment;

    /**
     * Creates a new CookForMeReview object.
     */
    public CookForMeReview() { }

    /**
     * Retrieve a list of Active CookForMeOrders Reviews based on a particular orderId.
     *
     * @param   orderId
     * @param   page
     *
     * @return  list of cook for me order reviews
     */
    public static List<CookForMeReview> findActiveReviewByCookForMeOrder(final long orderId, final int page, boolean paging) {
        if(!paging)
            return find.where().eq("ORDER_ID", orderId).eq("REVIEW_STATUS", Constants.REVIEWED).order().desc("CREATED_DATE").findList();
        else
            return find.where().eq("ORDER_ID", orderId).eq("REVIEW_STATUS", Constants.REVIEWED).order().desc("CREATED_DATE").findPagingList(Constants.MAX_REVIEW_FETCH_LIMIT).getPage(page).getList();
    }

    /**
     * Retrieve a list of Active CookForMeOrder Reviews from a particular user & orderId.
     *
     * @param   userId
     * @param   orderId
     *
     * @return  list of CookForMe reviews
     */
    public static CookForMeReview findActiveReviewByUserAndCookForMeOrder(final long userId, final long orderId) {
        return find.where().eq("USER_ID", userId).eq("ORDER_ID", orderId).eq("REVIEW_STATUS", Constants.REVIEWED).order().desc("CREATED_DATE").findUnique();
    }


    /**
     * Retrieve a list of CookForMeOrders Reviews based on a particular orderId.
     *
     * @param   orderId
     * @param   page
     *
     * @return  list of cook for me order reviews
     */
    public static List<CookForMeReview> findAllReviewByCookForMeOrder(final long orderId, final int page, boolean paging) {
        if(!paging)
            return find.where().eq("ORDER_ID", orderId).order().desc("CREATED_DATE").findList();
        else
            return find.where().eq("ORDER_ID", orderId).order().desc("CREATED_DATE").findPagingList(Constants.MAX_REVIEW_FETCH_LIMIT).getPage(page).getList();
    }

    /**
     * Retrieve a list of CookForMeOrder Reviews from a particular user & orderId.
     *
     * @param   userId
     * @param   orderId
     *
     * @return  list of CookForMe reviews
     */
    public static CookForMeReview findAllReviewByUserAndCookForMeOrder(final long userId, final long orderId) {
        return find.where().eq("USER_ID", userId).eq("ORDER_ID", orderId).order().desc("CREATED_DATE").findUnique();
    }

}
