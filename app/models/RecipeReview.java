package models;

import controllers.utils.Constants;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "RECIPE_REVIEW")
public class RecipeReview extends Review {
    private static final long serialVersionUID = 1L;

    // -- Queries (long id, RecipeReview.class)
    public static Model.Finder<Long, RecipeReview> find = new Model.Finder<Long, RecipeReview>(Long.class, RecipeReview.class);

    @Column(name = "REVIEW_IMAGE_PATH")
    public String imagePath;

    @ManyToOne
    @JoinColumn(name = "RECIPE_ID", referencedColumnName = "RECIPE_ID")
    public Recipe recipe;

    @OneToOne(cascade = CascadeType.REMOVE, optional = true)
    @JoinColumn(name = "COMMENT_ID", referencedColumnName = "COMMENT_ID")
    public RecipeReviewComment recipeReviewComment;

    /**
     * Creates a new RecipeReview object.
     */
    public RecipeReview() { }

    /**
     * Retrieve a list of Active Recipe Reviews based on a particular recipe.
     *
     * @param   recipeId
     * @param   page
     *
     * @return  list of recipe reviews
     */
    public static List<RecipeReview> findActiveReviewByRecipe(final long recipeId, final int page, boolean paging) {
        if(!paging)
            return find.where().eq("RECIPE_ID", recipeId).eq("REVIEW_STATUS", Constants.REVIEWED).order().desc("CREATED_DATE").findList();
        else
            return find.where().eq("RECIPE_ID", recipeId).eq("REVIEW_STATUS", Constants.REVIEWED).order().desc("CREATED_DATE").findPagingList(Constants.MAX_REVIEW_FETCH_LIMIT).getPage(page).getList();
    }

    /**
     * Retrieve a list of Active Recipe Reviews from a particular user & recipe.
     *
     * @param   userId
     * @param   recipeId
     *
     * @return  list of recipe reviews
     */
    public static RecipeReview findActiveReviewByUserAndRecipe(final long userId, final long recipeId) {
        return find.where().eq("USER_ID", userId).eq("RECIPE_ID", recipeId).eq("REVIEW_STATUS", Constants.REVIEWED).order().desc("CREATED_DATE").findUnique();
    }

    /**
     * Retrieve a list of Recipe Reviews based on a particular recipe.
     *
     * @param   recipeId
     * @param   page
     *
     * @return  list of recipe reviews
     */
    public static List<RecipeReview> findAllReviewByRecipe(final long recipeId, final int page, boolean paging) {
        if(!paging)
            return find.where().eq("RECIPE_ID", recipeId).order().desc("CREATED_DATE").findList();
        else
            return find.where().eq("RECIPE_ID", recipeId).order().desc("CREATED_DATE").findPagingList(Constants.MAX_REVIEW_FETCH_LIMIT).getPage(page).getList();
    }

    /**
     * Retrieve a list of Recipe Reviews from a particular user & recipe.
     *
     * @param   userId
     * @param   recipeId
     *
     * @return  list of recipe reviews
     */
    public static RecipeReview findAllReviewByUserAndRecipe(final long userId, final long recipeId) {
        return find.where().eq("USER_ID", userId).eq("RECIPE_ID", recipeId).order().desc("CREATED_DATE").findUnique();
    }
}
