package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name="TAG")
public class Tag extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "TAG_ID", unique = true, nullable = false)
    public Long tagId;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "NAME")
    public String tagName;
    
    @OneToMany(targetEntity=RecipeTag.class, mappedBy="tag", cascade=CascadeType.ALL)
    public List<RecipeTag> recipeTagList;

    @OneToMany(targetEntity=RecipeBookTag.class, mappedBy="tag", cascade=CascadeType.ALL)
    public List<RecipeBookTag> recipeBookTagList;

	// -- Queries (long id, Tag.class)
    public static Model.Finder<Long, Tag> find = new Model.Finder<Long, Tag>(Long.class, Tag.class);

    
    public static List<Tag> listAllTags() {
        return find.findList();
    }

    public static List<Tag> listAllTags(String input, int limit) {
        return find.where().startsWith("NAME", input).order().asc("NAME").findList();
    }
    
    public static Tag getTagByName(String tagName) {
        return find.where().eq("NAME", tagName).findUnique();
    }
    
    public static List<Tag> searchTagContainingName(String tagName) {
        return find.where().contains("NAME", tagName).findList();
    }
}
