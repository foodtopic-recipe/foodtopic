package models;

import controllers.utils.Constants;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@MappedSuperclass
public abstract class Likes extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "LIKES_ID", unique = true, nullable = false)
    public Long likesId;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
    public User user;
	
    @Formats.NonEmpty
	@Column(name = "LIKES_STATUS") //LIKE (1/TRUE) or UNLIKE (0/FALSE)
	public boolean likeStatus;
	
	// -- Queries (long id, Likes.class)
    public static Model.Finder<Long, Likes> find = new Model.Finder<Long, Likes>(Long.class, Likes.class);

    public static List<Likes> findUserLikes(long userId) {
        return find.where().eq("USER_ID", userId).eq("LIKES_STATUS", Constants.LIKE).findList();
    }
}
