package models;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@MappedSuperclass
public abstract class Base extends Model {

	private static final long serialVersionUID = 1L;

    @Constraints.Required
    @CreatedTimestamp
	@Column(name = "CREATED_DATE", nullable = false)
	public Timestamp createdDate;

    @Constraints.Required
    @UpdatedTimestamp
	@Column(name = "UPDATED_DATE", nullable = false)
    public Timestamp updatedDate;
    
    @Version
	@Column(name = "VERSION", nullable = false)
    public long version;

}
