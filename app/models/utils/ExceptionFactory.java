package models.utils;

import play.Logger;

/**
 * @author 
 * @since 09/09/14
 */
public class ExceptionFactory {

    public static AppException getNewAppException(Exception exception) {
        Logger.error(exception.getMessage());
        AppException app = new AppException();
        app.initCause(exception);
        return app;
    }
}
