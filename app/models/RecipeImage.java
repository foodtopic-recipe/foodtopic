package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_IMAGE")
public class RecipeImage extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "RECIPE_IMAGE_ID", unique = true, nullable = false)
    public Long recipeImageId;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
    public Recipe recipe;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "RECIPE_IMAGE_NAME")
    public String imageName;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "RECIPE_IMAGE_PATH")
    public String imagePath;
    
	
	// -- Queries (long id, RecipeImage.class)
    public static Model.Finder<Long, RecipeImage> find = new Model.Finder<Long, RecipeImage>(Long.class, RecipeImage.class);

    /**
     * Retrieve a list of Recipe Images based on a particular recipe.
     *
     * @param recipeId to search
     * @return list of recipe images
     */
    public static List<RecipeImage> findByRecipe(String recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).findList();
    }
}
