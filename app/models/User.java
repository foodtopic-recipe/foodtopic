package models;

import com.avaje.ebean.ExpressionList;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;
import com.feth.play.module.pa.user.*;
import controllers.utils.Constants;
import models.Token.Type;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="USER")
public class User extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "USER_ID", unique = true, nullable = false)
    public Long userId;

    @Constraints.Required
    @Formats.NonEmpty
    @Constraints.Email
    @Column(name = "EMAIL", unique = true, nullable = false)
    public String email;

    //@Constraints.Required
    //@Formats.NonEmpty
	//@Column(name = "CURRENT_PASSWORD", nullable = true)
    //public String currentPassword;
	
	@Column(name = "LAST_LOGIN")
	public Date lastLogin;

    //@Constraints.Required
    @Formats.NonEmpty
	@Column(name = "SECRET_ANSWER", nullable = true)
    public String secretAnswer;
    
    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "FIRST_NAME", nullable = true)
    public String firstName;
    
    @Formats.NonEmpty
	@Column(name = "LAST_NAME", nullable = false)
    public String lastName;

	@Column(name = "GENDER")
    public String gender;

	@Formats.DateTime(pattern = "yyyy-MM-dd")
	@Column(name = "DATE_OF_BIRTH")
	public Date dateOfBirth;

	@Column(name = "CONTACT_NUMBER")
	public String contactNumber;

	@OneToOne(targetEntity=Address.class, mappedBy="user", cascade=CascadeType.ALL)
	@Column(name = "ADDRESS_ID")
	public Address address;

	@Column(name = "PROFILE_DESCRIPTION")
	public String aboutMe;

	@Column(name = "ACTIVE")
	public boolean active;

	@Formats.NonEmpty
	@Column(name = "REPUTATION_POINTS")
	public long reputationPoints;

	@Column(name = "PROFILE_IMAGE_PATH")
	public String profileImagePath;

	@Column(name = "EMAIL_VALIDATED")
	public boolean emailValidated;

	@Column(name = "VALIDATION_EMAIL_SENT")
	public Date validationEmailSent;

	@OneToMany(targetEntity=LinkedAccount.class, mappedBy="user", cascade = CascadeType.ALL)
	public List<LinkedAccount> linkedAccounts;
	
    @OneToMany(targetEntity=Recipe.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<Recipe> recipeList;

    @OneToMany(targetEntity=RecipeBook.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<RecipeBook> recipeBookList;

    @OneToMany(targetEntity=Event.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<Event> eventList;
    
    @OneToOne(targetEntity=EventAttendee.class, mappedBy="user", cascade=CascadeType.ALL)
    public EventAttendee eventAttendee;
    
    @OneToMany(targetEntity=RecipeReview.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<RecipeReview> recipeReviewList;
        
    @OneToMany(targetEntity=RecipeReviewComment.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<RecipeReviewComment> recipeReviewCommentList;
    
    @OneToMany(targetEntity=RecipeReviewCommentFlag.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<RecipeReviewCommentFlag> recipeReviewCommentFlagList;
    
    @OneToMany(targetEntity=Payment.class, mappedBy="payee", cascade=CascadeType.ALL)
    public List<Payment> paymentPayeeList;
    
    @OneToMany(targetEntity=Payment.class, mappedBy="recipient", cascade=CascadeType.ALL)
    public List<Payment> paymentRecipientList;
    
    @OneToMany(targetEntity=Notification.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<Notification> notificationList;
    
    @OneToMany(targetEntity=Follower.class, mappedBy="follower", cascade=CascadeType.ALL)
    public List<Follower> followerList;
    
    @OneToMany(targetEntity=RecipeLikes.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<RecipeLikes> recipeLikesList;
    
    @OneToMany(targetEntity=FeaturedUser.class, mappedBy="user", cascade=CascadeType.ALL)
    public List<FeaturedUser> featuredUserList;

	@OneToMany(targetEntity= CookForMeOrders.class, mappedBy="user", cascade=CascadeType.ALL)
	public List<CookForMeOrders> orderList;


	// -- Queries (long id, user.class)
    public static Model.Finder<Long, User> find = new Model.Finder<Long, User>(Long.class, User.class);
    
    /**
     * Retrieve a user from an email.
     *
     * @param email email to search
     * @return a user
     */
    public static User findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }
    
    /**
     * Retrieves a user from a confirmation token.
     *
     * @param token the confirmation token to use.
     * @return a user if the confirmation token is found, null otherwise.
     */
    public static User findByConfirmationToken(String token) {
        return find.where().eq("confirmationToken", token).findUnique();
    }

    public static List<User> findPopularUsers(int page) {
        return find.where().order().desc("REPUTATION_POINTS").findPagingList(Constants.MAX_USER_FETCH_LIMIT).getPage(page).getList();
    }
    
	public static boolean existsByAuthUserIdentity(final AuthUserIdentity identity) {
		final ExpressionList<User> exp;
		if (identity instanceof UsernamePasswordAuthUser) {
			exp = getUsernamePasswordAuthUserFind((UsernamePasswordAuthUser) identity);
		} else {
			exp = getAuthUserFind(identity);
		}
		return exp.findRowCount() > 0;
	}
	
	private static ExpressionList<User> getUsernamePasswordAuthUserFind(final UsernamePasswordAuthUser identity) {
		return getEmailUserFind(identity.getEmail()).eq("linkedAccounts.providerKey", identity.getProvider());
	}
	
	private static ExpressionList<User> getEmailUserFind(final String email) {
		return find.where().eq("active", true).eq("email", email);
	}
	
	private static ExpressionList<User> getAuthUserFind(final AuthUserIdentity identity) {
		return find.where().eq("active", true)
				.eq("linkedAccounts.providerUserId", identity.getId())
				.eq("linkedAccounts.providerKey", identity.getProvider());
	}
	
	public static User findByUsernamePasswordIdentity(final UsernamePasswordAuthUser identity) {
		return getUsernamePasswordAuthUserFind(identity).findUnique();
	}
	
	public static User create(final AuthUser authUser) {
		final User user = new User();

		// user.permissions = new ArrayList<UserPermission>();
		// user.permissions.add(UserPermission.findByValue("printers.edit"));
		user.active = true;
		user.lastLogin = new Date();
		LinkedAccount account = LinkedAccount.create(authUser);

		if (authUser instanceof EmailIdentity) {
			final EmailIdentity identity = (EmailIdentity) authUser;
			// Remember, even when getting them from FB & Co., emails should be
			// verified within the application as a security breach there might
			// break your security as well!
			user.email = identity.getEmail();
			user.emailValidated = false;
		}

		if (authUser instanceof NameIdentity) {
			final NameIdentity identity = (NameIdentity) authUser;
			final String name = identity.getName();
			if (name != null) {
				user.lastName = name;
			}
		}

		if (authUser instanceof FirstLastNameIdentity) {
			final FirstLastNameIdentity identity = (FirstLastNameIdentity) authUser;
			final String firstName = identity.getFirstName();
			final String lastName = identity.getLastName();
			if (firstName != null) {
				user.firstName = firstName;
			}
			if (lastName != null) {
				user.lastName = lastName;
			}
		}
		
		if (authUser instanceof PicturedIdentity) {
			final PicturedIdentity identity = (PicturedIdentity) authUser;
			final String picture = identity.getPicture() + "?width=9999";
			if (picture != null) {
			    if(picture.contains("http:"))
				    user.profileImagePath = picture.substring(6);
			    else if(picture.contains("https:"))
				    user.profileImagePath = picture.substring(7);
				    
			    account.providerProfileImagePath = picture;
			}
		}
		
		if (authUser instanceof ExtendedIdentity) {
			final ExtendedIdentity identity = (ExtendedIdentity) authUser;
			final String gender = identity.getGender();
			if (gender != null) {
				user.gender = Character.toUpperCase(gender.charAt(0)) + gender.substring(1);
				//user.gender = ""+SubsetCode.getSubsetCodeByDescription(gender.trim()).pk.subsetCodeId;
			}
		}
		
		user.linkedAccounts = Collections.singletonList(account);
		user.save();
		
		//Create a recipe book - Favourites upon signup
		RecipeBook rb = new RecipeBook();
		rb.bookName = "Favourites";
		rb.monetizationCode = SubsetCode.getSubsetCodeById(Constants.MONETIZATION_STATUS_FREE);
		rb.price = 0;
		rb.privacyCode = SubsetCode.getSubsetCodeById(Constants.PRIVACY_STATUS_PUBLIC);
		rb.user = user;
		rb.save();
		
		//user.saveManyToManyAssociations("roles");
		//user.saveManyToManyAssociations("permissions");
		return user;
	}
	
	public void changePassword(final UsernamePasswordAuthUser authUser, final boolean create) {
		LinkedAccount acc = this.getAccountByProvider(authUser.getProvider());
		if (acc == null) {
			if (create) {
				acc = LinkedAccount.create(authUser);
				acc.user = this;
			} else {
				throw new RuntimeException("Account not enabled for password usage");
			}
		}
		acc.providerUserId = authUser.getHashedPassword();
		acc.save();
	}
	
	public LinkedAccount getAccountByProvider(final String providerKey) {
		return LinkedAccount.findByProviderKey(this, providerKey);
	}
	
	public static User findByAuthUserIdentity(final AuthUserIdentity identity) {
		if (identity == null) {
			return null;
		}
		if (identity instanceof UsernamePasswordAuthUser) {
			return findByUsernamePasswordIdentity((UsernamePasswordAuthUser) identity);
		} else {
			return getAuthUserFind(identity).findUnique();
		}
	}
	
	public static void verify(final User unverified) {
		// You might want to wrap this into a transaction
		unverified.emailValidated = true;
		unverified.save();
		Token.deleteByUser(unverified, Type.EMAIL_VERIFICATION);
	}
	
	public static void addLinkedAccount(final AuthUser oldUser, final AuthUser newUser) {
		final User u = User.findByAuthUserIdentity(oldUser);
		u.linkedAccounts.add(LinkedAccount.create(newUser));
		u.save();
	}
	
	public void addLinkedAccount(final AuthUser newUser) {
		this.linkedAccounts.add(LinkedAccount.create(newUser));
		this.save();
	}
	
	public void updateLastLogin() {
		this.lastLogin = new Date();
		this.save();
	}
	
	public Set<String> getProviders() {
		final Set<String> providerKeys = new HashSet<String>(linkedAccounts.size());
		for (final LinkedAccount acc : linkedAccounts) {
			providerKeys.add(acc.providerKey);
		}
		return providerKeys;
	}

	public void createLinkAccount(AuthUser authUser) {
		this.linkedAccounts.add(LinkedAccount.create(authUser));
		this.save();
	}
}
