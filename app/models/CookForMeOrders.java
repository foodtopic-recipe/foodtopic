package models;

import com.avaje.ebean.*;
import controllers.utils.Constants;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="COOK_FOR_ME_ORDERS")
public class CookForMeOrders extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ORDER_ID", unique = true, nullable = false)
	public Long orderId;

    @ManyToOne
    @JoinColumn(name="DINER_ID", referencedColumnName="USER_ID")
    public User user;

    @ManyToOne
    @JoinColumn(name="COOK_FOR_ME_ID", referencedColumnName="COOK_FOR_ME_ID")
    public CookForMe cookForMe;

    @OneToOne
    @JoinColumn(name = "ORDER_STATUS", referencedColumnName = "SUBSET_CODE_ID")
    public SubsetCode orderStatus;

    @OneToOne
    @JoinColumn(name = "PAYMENT_TYPE", referencedColumnName = "SUBSET_CODE_ID")
    public SubsetCode paymentType;

    @OneToOne
    @JoinColumn(name = "COLLECTION_MODE", referencedColumnName = "SUBSET_CODE_ID")
    public SubsetCode collectionMode;

    @Constraints.Required
    @Column(name = "BOOKING_DATE", nullable = false)
    public Timestamp bookingDate;

    @Constraints.Required
    @Column(name = "SERVINGS")
    public int servings;

    @Constraints.Required
    @Column(name = "CURRENCY")
    public String currency;

    @Constraints.Required
    @Column(name = "COOKING_COST")
    public double cookingCost;

    @Column(name = "DELIVERY_FEE")
    public double deliveryFee;

    @Column(name = "ADDRESS_ID")
    public Address address;

    @Column(name = "REMARKS")
    public String remarks;

    @OneToMany(targetEntity=CookForMeReview.class, mappedBy="cookForMeOrders", cascade=CascadeType.ALL)
    public List<CookForMeReview> cookForMeReviewList;

    @OneToMany(targetEntity=CookForMeReviewComment.class, mappedBy="cookForMeOrders", cascade=CascadeType.ALL)
    public List<CookForMeReviewComment> cookForMeReviewCommentList;


	// -- Queries (long id, CreateCookForMeOrder.class)
    public static Finder<Long, CookForMeOrders> find = new Finder<Long, CookForMeOrders>(Long.class, CookForMeOrders.class);

    /**
     * Retrieve a list of orders for you based on a particular cook for me id.
     *
     * @param cookForMeId to search
     * @return list of orders
     */
    public static List<CookForMeOrders> findByCookForMeId(long cookForMeId, int page) {
        return find.where().eq("COOK_FOR_ME_ID", cookForMeId).findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }

    public static int countByUserId(long userId) {
        return find.where().or(Expr.eq("cookForMe.recipe.user.userId", userId), Expr.eq("DINER_ID", userId)).findRowCount();
    }

    public static int completedOrderCountByUserId(long userId) {
        return find.where().eq("ORDER_STATUS", Constants.ORDER_STATUS_COMPLETED).or(Expr.eq("cookForMe.recipe.user.userId", userId), Expr.eq("DINER_ID", userId)).findRowCount();
    }

    public static List<CookForMeOrders> findByHostId(long userId, SubsetCode orderStatus, int page) {
        if(orderStatus == null)
            return find.where().eq("cookForMe.recipe.user.userId", userId).order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
        else
            return find.where().eq("cookForMe.recipe.user.userId", userId).eq("ORDER_STATUS", orderStatus.subsetCodeId).order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }

    public static int countByHostId(long userId, SubsetCode orderStatus, Date start, Date end, Date createStart, Date createEnd) {
        ExpressionList<CookForMeOrders> query = find.where().eq("cookForMe.recipe.user.userId", userId);

        if (orderStatus != null)
            query.eq("ORDER_STATUS", orderStatus.subsetCodeId);

        if (start != null || end != null || createStart != null || createEnd != null) {
            Junction<CookForMeOrders> disjunct = query.disjunction();

            if (start != null && end != null) {
                disjunct.add(Expr.between("BOOKING_DATE", start, end));
            } else if (start != null && end == null){
                disjunct.add(Expr.ge("BOOKING_DATE", start));
            } else if (start == null && end != null) {
                disjunct.add(Expr.le("BOOKING_DATE", end));
            }

            if (createStart != null && createEnd != null) {
                disjunct.add(Expr.between("t0.CREATED_DATE", createStart, createEnd));
            } else if (createStart != null && createEnd == null){
                disjunct.add(Expr.ge("t0.CREATED_DATE", createStart));
            } else if (createStart == null && createEnd != null) {
                disjunct.add(Expr.le("t0.CREATED_DATE", createEnd));
            }
        }

        return query.order().asc("BOOKING_DATE").findRowCount();
    }

    public static List<CookForMeOrders> findByHostId(long userId, SubsetCode orderStatus, int page, Date start, Date end, Date createStart, Date createEnd) {
        ExpressionList<CookForMeOrders> query = find.where().eq("cookForMe.recipe.user.userId", userId);

        if (orderStatus != null)
            query.eq("ORDER_STATUS", orderStatus.subsetCodeId);

        if (start != null || end != null || createStart != null || createEnd != null) {
            Junction<CookForMeOrders> disjunct = query.disjunction();

            if (start != null && end != null) {
                disjunct.add(Expr.between("BOOKING_DATE", start, end));
            } else if (start != null && end == null){
                disjunct.add(Expr.ge("BOOKING_DATE", start));
            } else if (start == null && end != null) {
                disjunct.add(Expr.le("BOOKING_DATE", end));
            }

            if (createStart != null && createEnd != null) {
                disjunct.add(Expr.between("t0.CREATED_DATE", createStart, createEnd));
            } else if (createStart != null && createEnd == null){
                disjunct.add(Expr.ge("t0.CREATED_DATE", createStart));
            } else if (createStart == null && createEnd != null) {
                disjunct.add(Expr.le("t0.CREATED_DATE", createEnd));
            }
        }

        return query.order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }

    public static List<CookForMeOrders> findByDinerId(long userId, SubsetCode orderStatus, int page) {
        if(orderStatus == null)
            return find.where().eq("DINER_ID", userId).order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
        else
            return find.where().eq("DINER_ID", userId).eq("ORDER_STATUS", orderStatus.subsetCodeId).order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }

    public static List<CookForMeOrders> findByDinerId(long userId, SubsetCode orderStatus, int page, Date start, Date end, Date createStart, Date createEnd) {
        ExpressionList<CookForMeOrders> query = find.where().eq("DINER_ID", userId);

        if (orderStatus != null)
            query.eq("ORDER_STATUS", orderStatus.subsetCodeId);

        if (start != null || end != null || createStart != null || createEnd != null) {
            Junction<CookForMeOrders> disjunct = query.disjunction();

            if (start != null && end != null) {
                disjunct.add(Expr.between("BOOKING_DATE", start, end));
            } else if (start != null && end == null) {
                disjunct.add(Expr.ge("BOOKING_DATE", start));
            } else if (start == null && end != null) {
                disjunct.add(Expr.le("BOOKING_DATE", end));
            }

            if (createStart != null && createEnd != null) {
                disjunct.add(Expr.between("t0.CREATED_DATE", createStart, createEnd));
            } else if (createStart != null && createEnd == null) {
                disjunct.add(Expr.ge("t0.CREATED_DATE", createStart));
            } else if (createStart == null && createEnd != null) {
                disjunct.add(Expr.le("t0.CREATED_DATE", createEnd));
            }
        }

        return query.order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }

    public static int countByDinerId(long userId, SubsetCode orderStatus, Date start, Date end, Date createStart, Date createEnd) {
        ExpressionList<CookForMeOrders> query = find.where().eq("DINER_ID", userId);

        if (orderStatus != null)
            query.eq("ORDER_STATUS", orderStatus.subsetCodeId);

        if (start != null || end != null || createStart != null || createEnd != null) {
            Junction<CookForMeOrders> disjunct = query.disjunction();

            if (start != null && end != null) {
                disjunct.add(Expr.between("BOOKING_DATE", start, end));
            } else if (start != null && end == null) {
                disjunct.add(Expr.ge("BOOKING_DATE", start));
            } else if (start == null && end != null) {
                disjunct.add(Expr.le("BOOKING_DATE", end));
            }

            if (createStart != null && createEnd != null) {
                disjunct.add(Expr.between("t0.CREATED_DATE", createStart, createEnd));
            } else if (createStart != null && createEnd == null) {
                disjunct.add(Expr.ge("t0.CREATED_DATE", createStart));
            } else if (createStart == null && createEnd != null) {
                disjunct.add(Expr.le("t0.CREATED_DATE", createEnd));
            }
        }

        return query.order().asc("BOOKING_DATE").findRowCount();
    }

    public static List<CookForMeOrders> findByUserId(long userId, SubsetCode orderStatus, int page, Date start, Date end, Date createStart, Date createEnd) {
        ExpressionList<CookForMeOrders> query = find.where();

        Junction<CookForMeOrders> userDist = query.disjunction();
        userDist.add(Expr.eq("DINER_ID", userId));
        userDist.add(Expr.eq("cookForMe.recipe.user.userId", userId));

        if (orderStatus != null)
            query.eq("ORDER_STATUS", orderStatus.subsetCodeId);

        if (start != null || end != null || createStart != null || createEnd != null) {
            Junction<CookForMeOrders> disjunct = query.disjunction();

            if (start != null && end != null) {
                disjunct.add(Expr.between("BOOKING_DATE", start, end));
            } else if (start != null && end == null) {
                disjunct.add(Expr.ge("BOOKING_DATE", start));
            } else if (start == null && end != null) {
                disjunct.add(Expr.le("BOOKING_DATE", end));
            }

            if (createStart != null && createEnd != null) {
                disjunct.add(Expr.between("t0.CREATED_DATE", createStart, createEnd));
            } else if (createStart != null && createEnd == null) {
                disjunct.add(Expr.ge("t0.CREATED_DATE", createStart));
            } else if (createStart == null && createEnd != null) {
                disjunct.add(Expr.le("t0.CREATED_DATE", createEnd));
            }
        }

        return query.order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }

    public static int countByUserId(long userId, SubsetCode orderStatus, Date start, Date end, Date createStart, Date createEnd) {
        ExpressionList<CookForMeOrders> query = find.where();

        Junction<CookForMeOrders> userDist = query.disjunction();
        userDist.add(Expr.eq("DINER_ID", userId));
        userDist.add(Expr.eq("cookForMe.recipe.user.userId", userId));

        if (orderStatus != null)
            query.eq("ORDER_STATUS", orderStatus.subsetCodeId);

        if (start != null || end != null || createStart != null || createEnd != null) {
            Junction<CookForMeOrders> disjunct = query.disjunction();

            if (start != null && end != null) {
                disjunct.add(Expr.between("BOOKING_DATE", start, end));
            } else if (start != null && end == null) {
                disjunct.add(Expr.ge("BOOKING_DATE", start));
            } else if (start == null && end != null) {
                disjunct.add(Expr.le("BOOKING_DATE", end));
            }

            if (createStart != null && createEnd != null) {
                disjunct.add(Expr.between("t0.CREATED_DATE", createStart, createEnd));
            } else if (createStart != null && createEnd == null) {
                disjunct.add(Expr.ge("t0.CREATED_DATE", createStart));
            } else if (createStart == null && createEnd != null) {
                disjunct.add(Expr.le("t0.CREATED_DATE", createEnd));
            }
        }

        return query.order().asc("BOOKING_DATE").findRowCount();
    }


    /**
     * Retrieve a list of new or accepted orders similar to date time parameter
     *
     * @param userId to search
     * @param recipeId to search
     * @param repeatedStartDateTime to search
     * @param repeatedEndDateTime to search
     * @return list of orders
     */
    public static List<CookForMeOrders> findRepeatedBookingByTime(long userId, long recipeId, Date repeatedStartDateTime, Date repeatedEndDateTime) {
        return find.where().eq("DINER_ID", userId).eq("cookForMe.recipe.recipeId", recipeId).or(Expr.eq("ORDER_STATUS",Constants.ORDER_STATUS_NEW), Expr.eq("ORDER_STATUS", Constants.ORDER_STATUS_ACCEPTED)).between("BOOKING_DATE", repeatedStartDateTime, repeatedEndDateTime).order().asc("ORDER_STATUS").order().asc("BOOKING_DATE").findList();
    }

    public static List<CookForMeOrders> findByDinerAndRecipeId(long userId, long recipeId, int page) {
        return find.where().eq("DINER_ID", userId).eq("cookForMe.recipe.recipeId", recipeId).order().asc("ORDER_STATUS").order().asc("BOOKING_DATE").findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }

    public static List<CookForMeOrders> findNewOrAcceptedOrders(long cookForMeId, int page) {
        return find.where().eq("COOK_FOR_ME_ID", cookForMeId).eq("ORDER_STATUS", Constants.ORDER_STATUS_NEW).eq("ORDER_STATUS", Constants.ORDER_STATUS_ACCEPTED).findPagingList(Constants.MAX_COOK_FOR_ME_ORDER_FETCH_LIMIT).getPage(page).getList();
    }
}
