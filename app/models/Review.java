package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import controllers.utils.Constants;
import play.data.format.Formats;
import play.data.validation.Constraints.Max;
import play.data.validation.Constraints.Min;
import play.db.ebean.Model;

//Superclass for RecipeReview, etc
@MappedSuperclass
public abstract class Review extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "REVIEW_ID", unique = true, nullable = false)
	public Long reviewId;

	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
	public User user;

	@Min(1)
	@Max(5)
	@Column(name = "REVIEW_SCORE")
	public int reviewScore;

	@Formats.NonEmpty
	@Column(name = "REVIEW_STATUS") // REVIEWED (1/TRUE) or UNREVIEWED (0/FALSE)
	public boolean reviewStatus;

	// -- Queries (long id, Review.class)
	public static Model.Finder<Long, Review> find = new Model.Finder<Long, Review>(Long.class, Review.class);

	public static List<Review> findActiveReviewByUser(long userId, int page) {
		return find.where().eq("USER_ID", userId).eq("REVIEW_STATUS", Constants.REVIEWED).order().desc("CREATED_DATE").findPagingList(Constants.MAX_REVIEW_FETCH_LIMIT).getPage(page).getList();
	}

	public static List<Review> findAllReviewByUser(long userId, int page) {
		return find.where().eq("USER_ID", userId).order().desc("CREATED_DATE").findPagingList(Constants.MAX_REVIEW_FETCH_LIMIT).getPage(page).getList();
	}
}
