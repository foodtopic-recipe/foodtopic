package models;

import controllers.utils.Constants;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="FOLLOWER")
public class Follower extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
    public Long followerId;
    
	@ManyToOne
	@JoinColumn(name="FOLLOWER_ID", referencedColumnName="USER_ID")
    public User follower;

	@ManyToOne
	@JoinColumn(name="FOLLOWING_ID", referencedColumnName="USER_ID")
    public User following;

    @Formats.NonEmpty
	@Column(name = "FOLLOWER_STATUS") //Follow (1/TRUE) or Unfollow (0/FALSE)
	public boolean followerStatus;
	
    // -- Queries (long id, Follower.class)
    public static Model.Finder<Long, Follower> find = new Model.Finder<Long, Follower>(Long.class, Follower.class);

    public static List<Follower> findByFollower(Long followerId) {
        return find.where().eq("FOLLOWER_ID", followerId).findList();
    }
    public static List<Follower> findByActiveFollower(Long followerId) {
        return find.where().eq("FOLLOWER_ID", followerId).eq("FOLLOWER_STATUS", Constants.FOLLOW).findList();
    }

    public static List<Follower> findByFollowing(Long followingId) {
        return find.where().eq("FOLLOWING_ID", followingId).findList();
    }
    public static List<Follower> findByActiveFollowing(Long followingId) {
        return find.where().eq("FOLLOWING_ID", followingId).eq("FOLLOWER_STATUS", Constants.FOLLOW).findList();
    }
    
    public static Follower checkFollowingRelationExists(Long followerId, Long followingId) {
        return find.where().eq("FOLLOWER_ID", followerId).eq("FOLLOWING_ID", followingId).findUnique();
    }
    public static Follower checkFollowingActiveRelationExists(Long followerId, Long followingId) {
        return find.where().eq("FOLLOWER_ID", followerId).eq("FOLLOWING_ID", followingId).eq("FOLLOWER_STATUS", Constants.FOLLOW).findUnique();
    }
}
