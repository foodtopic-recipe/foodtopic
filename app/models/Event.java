package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name="EVENT")
public class Event extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "EVENT_ID", unique = true, nullable = false)
    public Long eventId;

	@ManyToOne
	@JoinColumn(name="HOST_ID", referencedColumnName="USER_ID")
    public User user;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "EVENT_NAME")
    public String eventName;

    @Constraints.Required
    @Formats.NonEmpty
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "EVENT_DATE")
    public Date eventDate;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "EVENT_DESCRIPTION")
    public String eventDescription;

    @OneToOne
    @JoinColumn(name = "EVENT_TYPE", referencedColumnName = "SUBSET_CODE_ID") 
    public SubsetCode eventType;

    @OneToOne
    @JoinColumn(name = "STATUS_CODE", referencedColumnName = "SUBSET_CODE_ID") 
    public SubsetCode statusCode;

    @Formats.NonEmpty
	@Column(name = "MAX_PARTICIPANTS")
    public int maxParticipants;

    @OneToOne
    @JoinColumn(name = "MONETIZATION_CODE", referencedColumnName = "SUBSET_CODE_ID") //Free or Paid
    public SubsetCode monetizationCode;

	@Column(name = "COST_PER_PAX")
    public double costPerPax;
    
	@Column(name = "LOCATION")
    public String location;

    @OneToMany(targetEntity=EventAttendee.class, mappedBy="event", cascade=CascadeType.ALL)
    public List<EventAttendee> eventAttendeeList;

    @ManyToMany
    @JoinTable(name="EVENT_PAYMENT", joinColumns={@JoinColumn(name="EVENT_ID", referencedColumnName = "EVENT_ID")}, inverseJoinColumns={@JoinColumn(name="PAYMENT_ID", referencedColumnName = "PAYMENT_ID")})
    public List<Payment> paymentList = new ArrayList<Payment>();
    
    
	// -- Queries (long id, Event.class)
    public static Model.Finder<Long, Event> find = new Model.Finder<Long, Event>(Long.class, Event.class);

    /**
     * Retrieve a list of Events based from a particular user.
     *
     * @param userId to search
     * @return list of events
     */
    public static List<Event> findByUser(String userId) {
        return find.where().eq("HOST_ID", userId).findList();
    }
}
