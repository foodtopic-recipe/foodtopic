package models;

import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="ADDRESS")
public class Address extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ADDRESS_ID", unique = true, nullable = false)
	public Long addressId;

    @OneToOne
    @JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
    public User user;

	@Column(name = "ADDRESS_LINE_1")
    public String addressLine1;

    @Column(name = "ADDRESS_LINE_2")
    public String addressLine2;

    @Column(name = "ADDRESS_LINE_3")
    public String addressLine3;

    @Constraints.Required
    @Formats.NonEmpty
    @Column(name = "COUNTRY_CODE")
    public String countryCode;

    @Constraints.Required
    @Formats.NonEmpty
    @Column(name = "POSTAL_CODE")
    public String postalCode;


	// -- Queries (long id, Address.class)
    public static Finder<Long, Address> find = new Finder<Long, Address>(Long.class, Address.class);

}
