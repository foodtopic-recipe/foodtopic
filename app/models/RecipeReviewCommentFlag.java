package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_REVIEW_COMMENT_FLAG")
public class RecipeReviewCommentFlag extends Flag {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="COMMENT_ID", referencedColumnName="COMMENT_ID")
    public RecipeReviewComment recipeReviewComment;

	
	// -- Queries (long id, RecipeReviewCommentFlag.class)
    public static Model.Finder<Long, RecipeReviewCommentFlag> find = new Model.Finder<Long, RecipeReviewCommentFlag>(Long.class, RecipeReviewCommentFlag.class);

    public static List<RecipeReviewCommentFlag> findByComment(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).findList();
    }
    
    public static RecipeReviewCommentFlag findByUserAndComment(long userId, long commentId) {
        return find.where().eq("USER_ID", userId).eq("COMMENT_ID", commentId).findUnique();
    }
    
    public static int countDownVoteByComment(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).eq("FLAG", false).findRowCount();
    }
    
    public static int countUpVoteByComment(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).eq("FLAG", true).findRowCount();
    }
}
