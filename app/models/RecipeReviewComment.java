package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_REVIEW_COMMENT")
public class RecipeReviewComment extends Comment {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
    public Recipe recipe;

    @OneToOne
    @JoinColumn(name="REVIEW_ID", referencedColumnName="REVIEW_ID")
    public RecipeReview recipeReview;

    @OneToMany(targetEntity=RecipeReviewCommentFlag.class, mappedBy="recipeReviewComment", cascade=CascadeType.ALL)
    public List<RecipeReviewCommentFlag> recipeReviewCommentFlagList;

    
	// -- Queries (long id, RecipeReviewComment.class)
    public static Model.Finder<Long, RecipeReviewComment> find = new Model.Finder<Long, RecipeReviewComment>(Long.class, RecipeReviewComment.class);

    public static RecipeReviewComment findByCommentId(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).findUnique();
    }
    
    public static List<RecipeReviewComment> findByRecipe(long recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).findList();
    }

    public static List<RecipeReviewComment> findByReview(long reviewId) {
        return find.where().eq("REVIEW_ID", reviewId).findList();
    }
}
