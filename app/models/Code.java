package models;

import controllers.utils.Constants;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="CODE")
public class Code extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CODE_ID", unique = true, nullable = false)
    public int codeId;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "CODE_DESCRIPTION")
    public String codeTypeDescription;

    @Constraints.Required
    @Formats.NonEmpty
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "EFFECTIVE_DATE")
    public Date effectiveDate;
    
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "EXPIRED_DATE")
    public Date expiredDate;

    @OneToMany(targetEntity=SubsetCode.class, mappedBy="code", cascade=CascadeType.ALL)
    public List<SubsetCode> subsetCodeList;
    

	// -- Queries (long id, Code.class)
    public static Model.Finder<Long, Code> find = new Model.Finder<Long, Code>(Long.class, Code.class);

    public static List<Code> listAllCodes() {
        return find.findList();
    }

    public static List<Code> listAllEffectiveCodes() {
        return find.where().isNull("EXPIRED_DATE").findList();
    }

    public static List<Code> listAllExpiredCodes() {
        return find.where().isNotNull("EXPIRED_DATE").findList();
    }

    public static Code getAccountStatus() { //Active, Inactive, Pending Verification
        return find.where().eq("CODE_ID", Constants.ACCOUNT_STATUS).findUnique();
    }

    public static Code getNumberOfCoverPhotos() {
        return find.where().eq("CODE_ID", Constants.COVER_PHOTO_LIMIT).findUnique();
    }

    public static Code getNumberOfDirections() {
        return find.where().eq("CODE_ID", Constants.RECIPE_DIRECTIONS_LIMIT).findUnique();
    }

    public static Code getMonetizationStatus() { //Free, Paid or Bundled along in a Recipe Book
        return find.where().eq("CODE_ID", Constants.MONETIZATION_STATUS).findUnique();
    }

    public static Code getPublishStatus() { //Published or Draft
        return find.where().eq("CODE_ID", Constants.PUBLISH_STATUS).findUnique();
    }

    public static Code getPrivacyStatus() { //Public or Private
        return find.where().eq("CODE_ID", Constants.PRIVACY_STATUS).findUnique();
    }
    
    public static Code getGender() { //Male or Female
        return find.where().eq("CODE_ID", Constants.GENDER).findUnique();
    }

    public static Code getNotificationType() { //Favourites, Comment, Liked & Followed
        return find.where().eq("CODE_ID", Constants.NOTIFICATION_TYPE).findUnique();
    }

    public static Code getDifficulty() { //Easy, Moderate or Hard
        return find.where().eq("CODE_ID", Constants.DIFFICULTY_TYPE).findUnique();
    }

    public static Code getOrderStatus() { //New, Accepted, Rejected, Cancelled, Completed
        return find.where().eq("CODE_ID", Constants.ORDER_STATUS).findUnique();
    }

    public static Code getCollectionMode() { //Delivery, Self Collection, Both
        return find.where().eq("CODE_ID", Constants.COLLECTION_MODE).findUnique();
    }

    public static Code getPaymentType() { //Cash
        return find.where().eq("CODE_ID", Constants.PAYMENT_TYPE).findUnique();
    }
}
