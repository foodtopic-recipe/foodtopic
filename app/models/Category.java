package models;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.libs.Scala;
import scala.Tuple2;
import scala.collection.Seq;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="CATEGORY")
public class Category extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CATEGORY_ID", unique = true, nullable = false)
    public int categoryId;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "NAME")
    public String categoryName;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "DESCRIPTION")
    public String categoryDescription;

    @Constraints.Required
    @Formats.NonEmpty
    @Column(name = "MENUBAR_STATUS") //DISPLAY ON MENUBAR (1/TRUE) or NO DISPLAY (0/FALSE)
    public boolean menubarStatus;

    @Constraints.Required
    @Formats.NonEmpty
    @Column(name = "MENUBAR_SEQ") //SEQUENCE ON MENUBAR
    public int menubarSequence;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "STATUS")
    public String categoryStatus;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "IMAGE")
    public byte[] categoryImage;

    @OneToMany(targetEntity=RecipeCategory.class, mappedBy="category", cascade=CascadeType.ALL)
    public List<RecipeCategory> recipeCategoryList;
	
	// -- Queries (long id, Category.class)
    public static Model.Finder<Long, Category> find = new Model.Finder<Long, Category>(Long.class, Category.class);

    
    public static Seq<Tuple2<String, String>> allCategoryOptions() {
		List<Tuple2<String, String>> categoryOptions = new ArrayList<Tuple2<String, String>>();
        for(Category c : listAllCategories()) {
			categoryOptions.add(Scala.Tuple("" + c.categoryId, c.categoryName));
		}
		return Scala.toSeq(categoryOptions);
    }
    
    public static List<Category> listAllCategories() {
        return find.findList();
    }

    public static List<Category> listCategoryByMenubarStatus() {
        return find.where().eq("MENUBAR_STATUS","1").orderBy("MENUBAR_SEQ").findList();
    }

    public static Category getCategoryById(int categoryId) {
        return find.where().eq("CATEGORY_ID", categoryId).findUnique();
    }
    
    public static Category searchCategoryByName(String categoryName) {
        return find.where().ieq("NAME", categoryName).findUnique();
    }
    
    public static List<Category> searchCategoryContainingName(String categoryName) {
        return find.where().icontains("NAME", categoryName).findList();
    }
}
