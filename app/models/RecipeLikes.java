package models;

import controllers.utils.Constants;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="RECIPE_LIKES")
public class RecipeLikes extends Likes {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
    public Recipe recipe;
	
	// -- Queries (long id, RecipeLikes.class)
    public static Model.Finder<Long, RecipeLikes> find = new Model.Finder<Long, RecipeLikes>(Long.class, RecipeLikes.class);

    public static List<RecipeLikes> findRecipeLikes(long recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).eq("LIKES_STATUS", Constants.LIKE).findList();
    }
    public static int findRecipeLikesCount(long recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).eq("LIKES_STATUS", Constants.LIKE).findRowCount();
    }
    
    public static RecipeLikes findAllUserAndRecipeLikes(long userId, long recipeId) {
        return find.where().eq("USER_ID", userId).eq("RECIPE_ID", recipeId).findUnique();
    }
    public static RecipeLikes findUserAndRecipeActiveLikes(long userId, long recipeId) {
        return find.where().eq("USER_ID", userId).eq("RECIPE_ID", recipeId).eq("LIKES_STATUS", Constants.LIKE).findUnique();
    }
}
