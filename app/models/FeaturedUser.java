package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import controllers.utils.Constants;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="FEATURED_USER")
public class FeaturedUser extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "FEATURED_USER_ID", unique = true, nullable = false)
	public Long featuredUserId;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
	public User user;

	@Column(name = "PRIORITY")
	public int priority;
    
    @Constraints.Required
    @CreatedTimestamp
	@Column(name = "START_DATE", nullable = false)
	public Timestamp startDate;
    
    @Constraints.Required
    @CreatedTimestamp
	@Column(name = "END_DATE", nullable = false)
	public Timestamp endDate;
    
	// -- Queries (long id, FeaturedUser.class)
    public static Model.Finder<Long, FeaturedUser> find = new Model.Finder<Long, FeaturedUser>(Long.class, FeaturedUser.class);

    public static List<FeaturedUser> findAllFeaturedUser(int page) {
        return find.where().gt("START_DATE", new Date()).le("END_DATE", new Date()).order().desc("PRIORITY").findPagingList(Constants.MAX_USER_FETCH_LIMIT).getPage(page).getList();
    }
}
