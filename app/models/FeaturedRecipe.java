package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.annotation.CreatedTimestamp;

import controllers.utils.Constants;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name="FEATURED_RECIPE")
public class FeaturedRecipe extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "FEATURED_RECIPE_ID", unique = true, nullable = false)
	public Long featuredRecipeId;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
    public Recipe recipe;

	@Column(name = "PRIORITY")
	public int priority;
    
    @Constraints.Required
    @CreatedTimestamp
	@Column(name = "START_DATE", nullable = false)
	public Timestamp startDate;
    
    @Constraints.Required
    @CreatedTimestamp
	@Column(name = "END_DATE", nullable = false)
	public Timestamp endDate;
    
	// -- Queries (long id, Recipe.class)
    public static Model.Finder<Long, FeaturedRecipe> find = new Model.Finder<Long, FeaturedRecipe>(Long.class, FeaturedRecipe.class);

    public static List<FeaturedRecipe> findAllFeaturedRecipe(int page) {
        return find.where().gt("START_DATE", new Date()).le("END_DATE", new Date()).order().desc("PRIORITY").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
}
