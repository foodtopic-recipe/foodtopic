package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import controllers.utils.Constants;
import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_CATEGORY")
public class RecipeCategory extends Base {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
	public Recipe recipe;

	@ManyToOne
	@JoinColumn(name="CATEGORY_ID", referencedColumnName="Category_ID")
	public Category category;
	
	public RecipeCategory(Recipe recipe, Category category) {
		this.recipe = recipe;
		this.category = category;
	}

	// -- Queries (long id, RecipeCategory.class)
    public static Model.Finder<Long, RecipeCategory> find = new Model.Finder<Long, RecipeCategory>(Long.class, RecipeCategory.class);

	/**
	 * Retrieve a list of RecipeCategory based on category Id
	 *
	 * @param categoryId
	 * @return list of RecipeCategory
	 */
	public static List<RecipeCategory> findByCategoryId(long categoryId) {
		return find.where().eq("CATEGORY_ID", categoryId).findList();
	}

	/**
	 * Delete RecipeCategory based on recipe Id & category Id
	 *
	 * @param recipeId, categoryId
	 */
	public static int deleteRecipeCategory(long recipeId, long categoryId) {
		SqlUpdate deleteRecipeCategory = Ebean.createSqlUpdate("DELETE FROM RECIPE_CATEGORY WHERE RECIPE_ID = :recipeId AND CATEGORY_ID = :categoryId");
		deleteRecipeCategory.setParameter("recipeId", recipeId);
		deleteRecipeCategory.setParameter("categoryId", categoryId);
		return deleteRecipeCategory.execute();
	}

	/**
	 * Retrieve a list of RecipeCategory based on recipe Id
	 *
	 * @param recipeId to search
	 * @return list of RecipeCategory
	 */
	public static List<RecipeCategory> findByRecipeId(long recipeId) {
		return find.where().eq("RECIPE_ID", recipeId).findList();
	}

    public static List<Recipe> searchPublishedPublicRecipeByCategoryName(String categoryName, int page) {
    	List<Recipe> recipeList = new ArrayList<Recipe>();
		List<RecipeCategory> recipeCategoryList = find.where().eq("recipe.statusCode.subsetCodeId", Constants.PUBLISH_STATUS_PUBLISHED).eq("recipe.privacyCode.subsetCodeId", Constants.PRIVACY_STATUS_PUBLIC).eq("category.categoryName", categoryName).isNotNull("recipe.publishedDate").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
	    for(RecipeCategory recipeCategory : recipeCategoryList) {
			recipeList.add(recipeCategory.recipe);
	    }
	    return recipeList;
	}
    
    public static List<RecipeCategory> findAllPublishedPublicRecipeCategoryByCategory(long categoryId, int page) {
        return find.where().eq("recipe.statusCode.subsetCodeId", Constants.PUBLISH_STATUS_PUBLISHED).eq("recipe.privacyCode.subsetCodeId", Constants.PRIVACY_STATUS_PUBLIC).eq("CATEGORY_ID", categoryId).isNotNull("recipe.publishedDate").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
}
