package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name="RECIPE_INGREDIENT")
public class RecipeIngredient extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "RECIPE_INGREDIENT_ID", unique = true, nullable = false)
    public Long recipeDirectionId;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
    public Recipe recipe;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "INGREDIENT_NAME")
    public String ingredientName;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "INGREDIENT_QUANTITY")
    public String ingredientQuantity;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "INGREDIENT_UNIT")
    public String ingredientUnit;
    
	
	// -- Queries (long id, RecipeIngredient.class)
    public static Model.Finder<Long, RecipeIngredient> find = new Model.Finder<Long, RecipeIngredient>(Long.class, RecipeIngredient.class);

    /**
     * Retrieve a list of Recipe Ingredients based on a particular recipe.
     *
     * @param recipeId to search
     * @return list of recipe ingredients
     */
    public static List<RecipeIngredient> findByRecipe(String recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).findList();
    }
    
    public static List<RecipeIngredient> searchIngredientContainingName(String input, int limit) {
        return find.where().startsWith("INGREDIENT_NAME", input).order().asc("INGREDIENT_NAME").findList();
    }
    
    public static List<RecipeIngredient> searchIngredientContainingQuantity(String input, int limit) {
        return find.where().startsWith("INGREDIENT_QUANTITY", input).order().asc("INGREDIENT_QUANTITY").findList();
    }
    
    public static List<RecipeIngredient> searchIngredientContainingUnit(String input, int limit) {
        return find.where().startsWith("INGREDIENT_UNIT", input).order().asc("INGREDIENT_UNIT").findList();
    }
    
}
