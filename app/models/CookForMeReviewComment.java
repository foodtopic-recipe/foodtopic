package models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="COOK_FOR_ME_REVIEW_COMMENT")
public class CookForMeReviewComment extends Comment {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="ORDER_ID", referencedColumnName="ORDER_ID")
    public CookForMeOrders cookForMeOrders;

    @OneToOne
    @JoinColumn(name="REVIEW_ID", referencedColumnName="REVIEW_ID")
    public CookForMeReview cookForMeReview;

    @OneToMany(targetEntity=CookForMeReviewCommentFlag.class, mappedBy="cookForMeReviewComment", cascade=CascadeType.ALL)
    public List<CookForMeReviewCommentFlag> cookForMeReviewCommentFlagList;


    public static Finder<Long, CookForMeReviewComment> find = new Finder<Long, CookForMeReviewComment>(Long.class, CookForMeReviewComment.class);

    public static CookForMeReviewComment findByCommentId(long commentId) {
        return find.where().eq("COMMENT_ID", commentId).findUnique();
    }
    
    public static List<CookForMeReviewComment> findByCookForMeOrder(long orderId) {
        return find.where().eq("ORDER_ID", orderId).findList();
    }

    public static List<CookForMeReviewComment> findByReview(long reviewId) {
        return find.where().eq("REVIEW_ID", reviewId).findList();
    }
}
