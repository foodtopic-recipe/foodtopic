package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.annotation.CreatedTimestamp;
import controllers.utils.Constants;
import controllers.utils.Utilities;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="RECIPE")
public class Recipe extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "RECIPE_ID", unique = true, nullable = false)
	public Long recipeId;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
	public User user;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "RECIPE_NAME", nullable = false)
    public String recipeName;
    
	@Column(name = "RECIPE_DESCRIPTION")
	public String recipeDescription;
    
    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "PREPARATION_TIME")
    public int preparationTime; //Preparation Time in mins (int)

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "SERVINGS")
    public int servings;

    //@Column(name = "PRICE")
    //public double price;

    @OneToOne
    @JoinColumn(name = "MONETIZATION_CODE", referencedColumnName = "SUBSET_CODE_ID") //Free, Paid or Bundled w Recipe Book
    public SubsetCode monetizationCode;

    @OneToOne
    @JoinColumn(name = "STATUS_CODE", referencedColumnName = "SUBSET_CODE_ID") //Published, Draft, Deleted, Hidden
    public SubsetCode statusCode;

    @OneToOne
    @JoinColumn(name = "PRIVACY_CODE", referencedColumnName = "SUBSET_CODE_ID") //Public, Private or Corporate
    public SubsetCode privacyCode;

    @OneToOne
    @JoinColumn(name = "DIFFICULTY_CODE", referencedColumnName = "SUBSET_CODE_ID") //Easy, Moderate, Hard
    public SubsetCode difficultyCode;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "COOK_FOR_ME", referencedColumnName = "COOK_FOR_ME_ID", nullable=true)
    public CookForMe cookForMe;

	@Column(name = "VIEW_COUNT")
    public int views;
	
    @Constraints.Required
    @CreatedTimestamp
	@Column(name = "PUBLISHED_DATE", nullable = false)
	public Timestamp publishedDate;

	@Column(name = "PREVIOUS_RECIPE_ID")
	public Long previousRecipeId;
	
    @OneToMany(targetEntity=RecipeCategory.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeCategory> recipeCategoryList;

    @OneToMany(targetEntity=RecipeTag.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeTag> recipeTagList;
    
    @OneToMany(targetEntity=RecipeDirection.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeDirection> recipeDirectionList;

    @OneToMany(targetEntity=RecipeIngredient.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeIngredient> recipeIngredientList;
    
    @OneToMany(targetEntity=RecipeImage.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeImage> recipeImageList;

    @OneToMany(targetEntity=RecipeReview.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeReview> recipeReviewList;
    
    @OneToMany(targetEntity=RecipeReviewComment.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeReviewComment> recipeReviewCommentList;

    @OneToMany(targetEntity=BookRecipe.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<BookRecipe> bookRecipeList;

    @OneToMany(targetEntity=RecipeLikes.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<RecipeLikes> recipeLikesList;
    
    @OneToMany(targetEntity=FeaturedRecipe.class, mappedBy="recipe", cascade=CascadeType.ALL)
    public List<FeaturedRecipe> featuredRecipeList;

    /*
    @ManyToMany
    @JoinTable(name="RECIPE_PAYMENT", joinColumns={@JoinColumn(name="RECIPE_ID", referencedColumnName = "RECIPE_ID")}, inverseJoinColumns={@JoinColumn(name="PAYMENT_ID", referencedColumnName = "PAYMENT_ID")})
    public List<Payment> paymentList = new ArrayList<Payment>();
    */

    public String getCoverPhoto() {
    	if (recipeImageList.size() != 0) {
    		return recipeImageList.get(0).imagePath;
    	}
    	return null;
    }
    
	// -- Queries (long id, Recipe.class)
    public static Model.Finder<Long, Recipe> find = new Model.Finder<Long, Recipe>(Long.class, Recipe.class);

    public static List<Recipe> findAllRecipeByUser(User user, int page) {
        return find.where().eq("USER_ID", user.userId).findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static int findAllNonDeletedNonHiddenRecipeCountByUser(User user) {
        return find.where().ne("STATUS_CODE", Constants.PUBLISH_STATUS_DELETED).ne("STATUS_CODE", Constants.PUBLISH_STATUS_HIDDEN).eq("USER_ID", user.userId).findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getTotalRowCount();
    }

    public static List<Recipe> findAllNonDeletedNonHiddenRecipeByUser(User user, int page) {
        return find.where().ne("STATUS_CODE", Constants.PUBLISH_STATUS_DELETED).ne("STATUS_CODE", Constants.PUBLISH_STATUS_HIDDEN).eq("USER_ID", user.userId).findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }

    public static List<Recipe> findAllDraftRecipeByUser(User user, int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_DRAFT).eq("USER_ID", user.userId).findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }

    public static List<Recipe> findAllPublishedPublicRecipe(int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<Recipe> findAllPublishedRecipeByUser(User user, int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("USER_ID", user.userId).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }

    public static List<Recipe> findAllPublishedPublicRecipeByUser(User user, int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).eq("USER_ID", user.userId).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }

    //-- Searching Methods - START -
    public static List<Recipe> searchPublishedPublicRecipeByName(String searchInput, int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).contains("RECIPE_NAME", searchInput).contains("RECIPE_DESCRIPTION", searchInput).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<Recipe> searchPublishedPublicRecipeByIngredient(String searchInput, int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).contains("recipeIngredientList.ingredientName", searchInput).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<Recipe> searchPublishedPublicRecipeByDirection(String searchInput, int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).contains("recipeDirectionList.directionDescription", searchInput).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<Recipe> searchPublishedPublicRecipeByUserName(String searchInput, int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).contains("user.firstName", searchInput).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<Recipe> findActiveFollowingLatestPublishedPublicRecipe(User user, int limit, int page) {
    	List<Long> followingList = new ArrayList<Long>();
    	for(Follower f : user.followerList) {
    		if(f.followerStatus)
    			followingList.add(f.following.userId);
    	}
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED)
        		.eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC)
        		.in("USER_ID", followingList)
        		.order().desc("PUBLISHED_DATE")
        		.findPagingList(limit)
        		.getPage(page)
        		.getList();
    }
    //-- Searching Methods - END -

    public static List<Recipe> findPopularPublishedPublicRecipe(int page) {    
        List<Recipe> recipeList = new ArrayList<Recipe>();
    	
		String query = "SELECT r.RECIPE_ID as POPULAR_RECIPE_ID, "
					 + " (SELECT count(1) FROM BOOK_RECIPE br, RECIPE_BOOK rb "
					 + "WHERE br.RECIPE_ID = r.RECIPE_ID "
					 + "AND br.RECIPE_BOOK_ID = rb.RECIPE_BOOK_ID "
					 + ") as FAV_COUNT, "
					 
					 + "(SELECT count(1) FROM RECIPE_LIKES rl "
					 + "WHERE rl.RECIPE_ID = r.RECIPE_ID "
					 + ") as LIKES_COUNT, "
					 
					 + "r.VIEW_COUNT, "
					 
					 + "(SELECT REPUTATION_POINTS FROM USER "
					 + "WHERE USER_ID = r.USER_ID "
					 + ") as REPUTATION_POINTS, "
					 
					 + "r.PUBLISHED_DATE "
					 
					 + "FROM RECIPE r "
					 + "WHERE r.STATUS_CODE = :status "
					 + "AND r.PRIVACY_CODE = :privacy "
					 + "ORDER BY FAV_COUNT+LIKES_COUNT desc, r.VIEW_COUNT desc, REPUTATION_POINTS desc, r.PUBLISHED_DATE "
					 + "LIMIT :limit "
					 + "OFFSET :offset ";

    	//Order by adding Like & Favs count together, recipe views, reputation of the user and then lastly published date
        List<SqlRow> sqlRows = Ebean.createSqlQuery(query)
        					   .setParameter("status", Constants.PUBLISH_STATUS_PUBLISHED)
        					   .setParameter("privacy", Constants.PRIVACY_STATUS_PUBLIC)
        					   .setParameter("limit", Constants.MAX_RECIPE_FETCH_LIMIT)
        					   .setParameter("offset", page * Constants.MAX_RECIPE_FETCH_LIMIT)
        					   .findList();
        
        Logger.debug("FindPopularPublishedPublicRecipe sqlRows.size() : " + sqlRows.size());
        
        for(SqlRow row : sqlRows) {
            if(!row.getString("POPULAR_RECIPE_ID").isEmpty()) {
                Logger.debug("FindPopularPublishedPublicRecipe Recipe : " + row.getString("POPULAR_RECIPE_ID") 
                			+ "'s Fav Count : " + row.getString("FAV_COUNT")
                			+ ", Likes Count : " + row.getString("LIKES_COUNT")
                			+ ", View Count : " + row.getString("VIEW_COUNT")
                			+ ", Owner's Rep Points : " + row.getString("REPUTATION_POINTS")
                			+ " & Published Date : " + row.getString("PUBLISHED_DATE"));
                
                recipeList.add(Recipe.find.byId(Long.valueOf(row.getString("POPULAR_RECIPE_ID"))));
            }
        }
        return recipeList;
    }

    //What define latest/new? Select published, public and order by published date in desc order
    public static List<Recipe> findLatestPublishedPublicRecipe(int page) {
        return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED)
        		.eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC)
        		.order().desc("PUBLISHED_DATE")
        		.findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT)
        		.getPage(page)
        		.getList();
    }

    public static List<Recipe> findSimilarPublishedPublicRecipe(long recipeId, int page) {
        Recipe currentRecipe = find.byId(recipeId);
        //Smth like search for now but exclude ancestry & descendant recipes. 

        List<Recipe> similarRecipeList = find.where().not(Expr.in("RECIPE_ID",findAncestryRecipeIds(recipeId)))
        		.not(Expr.in("RECIPE_ID",findDescendantRecipeIds(recipeId)))
                .ne("PREVIOUS_RECIPE_ID", currentRecipe.previousRecipeId)
        		.like("RECIPE_NAME", currentRecipe.recipeName)
        		.ne("RECIPE_ID", recipeId)
        		.eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED)
        		.eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC)
        		.findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT)
        		.getPage(page)
        		.getList();
        similarRecipeList.add(0, currentRecipe); // Append the first recipe to be remove at front end before displaying
        return similarRecipeList;
    }
    
    public static List<Recipe> findPublishedPublicRecipeByCategory(int categoryId, int page) {    
        List<Recipe> recipeList = new ArrayList<Recipe>();
    	
		String query = "SELECT r.RECIPE_ID as RECIPE_ID, "
					 + " (SELECT count(1) FROM BOOK_RECIPE br, RECIPE_BOOK rb "
					 + "WHERE br.RECIPE_ID = r.RECIPE_ID "
					 + "AND br.RECIPE_BOOK_ID = rb.RECIPE_BOOK_ID "
					 + ") as FAV_COUNT, "
					 
					 + "(SELECT count(1) FROM RECIPE_LIKES rl "
					 + "WHERE rl.RECIPE_ID = r.RECIPE_ID "
					 + ") as LIKES_COUNT, "
					 
					 + "r.VIEW_COUNT, "
					 
					 + "(SELECT REPUTATION_POINTS FROM USER "
					 + "WHERE USER_ID = r.USER_ID "
					 + ") as REPUTATION_POINTS, "
					 
					 + "r.PUBLISHED_DATE "
					 
					 + "FROM RECIPE r, RECIPE_CATEGORY rc "
					 + "WHERE r.STATUS_CODE = :status "
					 + "AND r.PRIVACY_CODE = :privacy "
					 + "AND rc.RECIPE_ID = r.RECIPE_ID "
					 + "AND rc.CATEGORY_ID = :categoryId "
					 + "ORDER BY FAV_COUNT+LIKES_COUNT desc, r.VIEW_COUNT desc, REPUTATION_POINTS desc, r.PUBLISHED_DATE "
					 + "LIMIT :limit "
					 + "OFFSET :offset ";

    	List<SqlRow> sqlRows = Ebean.createSqlQuery(query)
				   			   .setParameter("categoryId", categoryId)
        					   .setParameter("status", Constants.PUBLISH_STATUS_PUBLISHED)
        					   .setParameter("privacy", Constants.PRIVACY_STATUS_PUBLIC)
        					   .setParameter("limit", Constants.MAX_RECIPE_FETCH_LIMIT)
        					   .setParameter("offset", page * Constants.MAX_RECIPE_FETCH_LIMIT)
        					   .findList();
        
        Logger.debug("FindPublishedPublicRecipeCategory sqlRows.size() : " + sqlRows.size());
        
        for(SqlRow row : sqlRows) {
            if(!row.getString("RECIPE_ID").isEmpty()) {
                recipeList.add(Recipe.find.byId(Long.valueOf(row.getString("RECIPE_ID"))));
            }
        }
        //return find.where().eq("STATUS_CODE", Constants.PUBLISH_STATUS_PUBLISHED).eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).eq("recipeCategoryList.categoryId", categoryId).isNotNull("PUBLISHED_DATE").findPagingList(Constants.MAX_RECIPE_FETCH_LIMIT).getPage(page).getList();
        return recipeList;
    }
    
    public static List<String> findAncestryRecipeIds(long recipeId) {
        List<String> recipeIdList = new ArrayList<String>();
        
        String query = "SELECT RECIPE_ID, GetAncestryTree(RECIPE_ID) AS AncestryIDs FROM RECIPE WHERE RECIPE_ID = :recipeId";
        
        List<SqlRow> sqlRows = Ebean.createSqlQuery(query).setParameter("recipeId", recipeId).findList();
        Logger.debug("FindAncestryRecipeIds sqlRows.size() : " + sqlRows.size());
        for(SqlRow row : sqlRows) {
            //Logger.info("row : " + row);
            if(!row.getString("AncestryIDs").isEmpty()) {
                Logger.debug("FindAncestryRecipeIds row.getString(AncestryIDs) : " + row.getString("AncestryIDs"));
                String[] recipeSpiltList = Utilities.spiltStringByComma(row.getString("AncestryIDs"));    	
                for(int i=0; i<recipeSpiltList.length; i++) {
                    if(!StringUtils.isEmpty(recipeSpiltList[i])) {
                        String id = recipeSpiltList[i].replaceAll("\\s+","");
                        Logger.debug("FindAncestryRecipeIds AncestryID " + i + " : " + id);
                        recipeIdList.add(id);
                    }
                }
            }
        }
        return recipeIdList;
    }

    public static List<Recipe> findAncestryRecipe(long recipeId) {
        List<Recipe> recipeList = new ArrayList<Recipe>();
        
        for(String id : findAncestryRecipeIds(recipeId)) {
            recipeList.add(Recipe.find.byId(Long.valueOf(id)));
        }
        return recipeList;
    }
    
    public static List<String> findDescendantRecipeIds(long recipeId) {
        List<String> recipeIdList = new ArrayList<String>();
        
        String query = "SELECT RECIPE_ID, GetDescendantTree(RECIPE_ID) AS DescendantIDs FROM RECIPE WHERE RECIPE_ID = :recipeId";

        List<SqlRow> sqlRows = Ebean.createSqlQuery(query).setParameter("recipeId",recipeId).findList();
        Logger.debug("FindDescendantRecipeIds sqlRows.size() : " + sqlRows.size());
        for(SqlRow row : sqlRows) {
            if(!row.getString("DescendantIDs").isEmpty()) {
                Logger.debug("FindDescendantRecipeIds row.getString(DescendantIDs) : " + row.getString("DescendantIDs"));
                String[] recipeSpiltList = Utilities.spiltStringByComma(row.getString("DescendantIDs"));    	
                for(int i=0; i<recipeSpiltList.length; i++) {
                    if(!StringUtils.isEmpty(recipeSpiltList[i])) {
                        String id = recipeSpiltList[i].replaceAll("\\s+","");
                        Logger.debug("FindDescendantRecipeIds DescendantID " + i + " : " + id);
                        recipeIdList.add(id);
                    }
                }
            }
        }
        return recipeIdList;
    }

    public static List<Recipe> findDescendantRecipe(long recipeId) {
        List<Recipe> recipeList = new ArrayList<Recipe>();
        
        for(String id : findDescendantRecipeIds(recipeId)) {
            recipeList.add(Recipe.find.byId(Long.valueOf(id)));
        }
        return recipeList;
    }
}
