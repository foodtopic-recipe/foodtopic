package models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import models.User;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

//Superclass for CommentFlag, etc
@MappedSuperclass
public abstract class Flag extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "FLAG_ID", unique = true, nullable = false)
    public Long flagId;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
    public User user;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "FLAG")
    public boolean flag;
    
	
	// -- Queries (long id, CommentFlag.class)
    public static Model.Finder<Long, Flag> find = new Model.Finder<Long, Flag>(Long.class, Flag.class);

    public static List<Flag> findByUser(long userId) {
        return find.where().eq("USER_ID", userId).findList();
    }

}
