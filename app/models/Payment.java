package models;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="PAYMENT")
public class Payment extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PAYMENT_ID", unique = true, nullable = false)
	public Long paymentId;

	@ManyToOne
	@JoinColumn(name="PAYEE_ID", referencedColumnName="USER_ID")
	public User payee;

	@ManyToOne
	@JoinColumn(name="RECIPIENT_ID", referencedColumnName="USER_ID")
	public User recipient;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "AMOUNT")
    public double amount;
    
    @OneToOne
    @JoinColumn(name = "PAYMENT_TYPE", referencedColumnName = "SUBSET_CODE_ID") 
    public SubsetCode paymentType;

    @OneToOne
    @JoinColumn(name = "PAYMENT_STATUS", referencedColumnName = "SUBSET_CODE_ID") 
    public SubsetCode paymentStatus;

    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "BANK_REF_ID")
    public String bankRefId;
    
    @Constraints.Required
    @Formats.NonEmpty
    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "PAYMENT_DATE")
    public Date paymentDate;

    @ManyToMany(mappedBy = "paymentList")
    @JoinTable(name = "EVENT_PAYMENT", joinColumns = {@JoinColumn(name="PAYMENT_ID", referencedColumnName = "PAYMENT_ID")}, inverseJoinColumns = {@JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID")})
    public List<Event> eventList = new ArrayList<Event>();

    /*
    @ManyToMany(mappedBy = "paymentList")
    @JoinTable(name = "RECIPE_PAYMENT", joinColumns = {@JoinColumn(name = "PAYMENT_ID", referencedColumnName = "PAYMENT_ID")}, inverseJoinColumns = {@JoinColumn(name = "RECIPE_ID", referencedColumnName = "RECIPE_ID")})
    public List<Recipe> recipeList = new ArrayList<Recipe>();
    */
    
	// -- Queries (long id, Payment.class)
    public static Model.Finder<Long, Payment> find = new Model.Finder<Long, Payment>(Long.class, Payment.class);


    /**
     * Retrieve a list of payments based from a particular user.
     *
     * @param userId to search
     * @return list of payments
     */
    public static List<Payment> findByPayee(String userId) {
        return find.where().eq("PAYEE_ID", userId).findList();
    }

    /**
     * Retrieve a list of payments based from a particular user.
     *
     * @param userId to search
     * @return list of payments
     */
    public static List<Payment> findByRecipient(String userId) {
        return find.where().eq("RECIPIENT_ID", userId).findList();
    }
}
