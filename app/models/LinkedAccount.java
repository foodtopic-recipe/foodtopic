package models;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.feth.play.module.pa.user.AuthUser;

import play.data.format.Formats;
import play.data.validation.Constraints;

@Entity
@Table(name="LINKED_ACCOUNT")
public class LinkedAccount extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "LINKED_ACCOUNT_ID", unique = true, nullable = false)
	public Long linkedAccountId;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
	public User user;

	@Constraints.Required
    @Formats.NonEmpty
	@Column(name = "PROVIDER_USER_ID")
	public String providerUserId; //Password field for password user login method.
	
	@Constraints.Required
    @Formats.NonEmpty
	@Column(name = "PROVIDER_KEY")
	public String providerKey;
	
	@Column(name = "PROVIDER_IMAGE_PATH")
    public String providerProfileImagePath;

	public static final Finder<Long, LinkedAccount> find = new Finder<Long, LinkedAccount>(Long.class, LinkedAccount.class);

	public static LinkedAccount findByProviderKey(final User user, String key) {
		return find.where().eq("user", user).eq("providerKey", key).findUnique();
	}

	public static LinkedAccount create(final AuthUser authUser) {
		final LinkedAccount ret = new LinkedAccount();
		ret.update(authUser);
		return ret;
	}
	
	public void update(final AuthUser authUser) {
		this.providerKey = authUser.getProvider();
		this.providerUserId = authUser.getId();
	}

	public static LinkedAccount create(final LinkedAccount acc) {
		final LinkedAccount ret = new LinkedAccount();
		ret.providerKey = acc.providerKey;
		ret.providerUserId = acc.providerUserId;
		
		return ret;
	}
}