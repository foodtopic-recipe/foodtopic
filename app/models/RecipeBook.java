package models;

import controllers.utils.Constants;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.libs.Scala;
import scala.Tuple2;
import scala.collection.Seq;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="RECIPE_BOOK")
public class RecipeBook extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "RECIPE_BOOK_ID", unique = true, nullable = false)
    public Long recipeBookId;

	@ManyToOne
	@JoinColumn(name="USER_ID", referencedColumnName="USER_ID")
    public User user;
	
    @Constraints.Required
    @Formats.NonEmpty
	@Column(name = "BOOK_NAME")
    public String bookName;

    @OneToOne
    @JoinColumn(name = "MONETIZATION_CODE", referencedColumnName = "SUBSET_CODE_ID") //Free, Paid
    public SubsetCode monetizationCode;

	@Column(name = "PRICE")
    public double price;

    @OneToOne
    @JoinColumn(name = "PRIVACY_CODE", referencedColumnName = "SUBSET_CODE_ID") //Public or Private
    public SubsetCode privacyCode;

    @OneToMany(targetEntity=BookRecipe.class, mappedBy="recipeBook", cascade=CascadeType.ALL)
    public List<BookRecipe> bookRecipeList;
    
    @OneToMany(targetEntity=RecipeBookTag.class, mappedBy="recipeBook", cascade=CascadeType.ALL)
    public List<RecipeBookTag> recipeBookTagList;
    
	// -- Queries (long id, RecipeBook.class)
    public static Model.Finder<Long, RecipeBook> find = new Model.Finder<Long, RecipeBook>(Long.class, RecipeBook.class);


    public static List<RecipeBook> findAllRecipeBookByUser(long userId, int page) {
        return find.where().eq("USER_ID", userId).findPagingList(Constants.MAX_RECIPE_BOOK_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<RecipeBook> findPopularRecipeBook(int page) {
        return find.where().order().desc("CREATED_DATE").findPagingList(Constants.MAX_RECIPE_BOOK_FETCH_LIMIT).getPage(page).getList();
    }
    
    public static List<RecipeBook> findPopularPublicRecipeBook(int page) {
        return find.where().eq("PRIVACY_CODE", Constants.PRIVACY_STATUS_PUBLIC).order().desc("CREATED_DATE").findList();
    }
    
    public static Seq<Tuple2<String, String>> allRecipeBookOptions(long userId) {
		List<Tuple2<String, String>> recipeBookOptions = new ArrayList<Tuple2<String, String>>();
        for(RecipeBook rb : findAllRecipeBookByUser(userId, Constants.FETCH_RECIPE_BOOK_PAGE_ONE)) {
        	recipeBookOptions.add(Scala.Tuple(rb.recipeBookId.toString(), rb.bookName));
		}
		return Scala.toSeq(recipeBookOptions);
    }
    
    public static Seq<Tuple2<String, String>> freeRecipeBookOptions(long userId) {
		List<Tuple2<String, String>> recipeBookOptions = new ArrayList<Tuple2<String, String>>();
        for(RecipeBook rb : findAllRecipeBookByUser(userId, Constants.FETCH_RECIPE_BOOK_PAGE_ONE)) {
        	if(rb.monetizationCode.subsetCodeId == Constants.MONETIZATION_STATUS_FREE)
        			recipeBookOptions.add(Scala.Tuple(rb.recipeBookId.toString(), rb.bookName));
		}
		return Scala.toSeq(recipeBookOptions);
    }

    public static Seq<Tuple2<String, String>> paidRecipeBookOptions(long userId) {
		List<Tuple2<String, String>> recipeBookOptions = new ArrayList<Tuple2<String, String>>();
        for(RecipeBook rb : findAllRecipeBookByUser(userId, Constants.FETCH_RECIPE_BOOK_PAGE_ONE)) {
        	if(rb.monetizationCode.subsetCodeId == Constants.MONETIZATION_STATUS_PAID)
        		recipeBookOptions.add(Scala.Tuple(rb.recipeBookId.toString(), rb.bookName));
		}
		return Scala.toSeq(recipeBookOptions);
    }
}
