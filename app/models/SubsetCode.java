package models;

import controllers.utils.Constants;
import controllers.utils.Utilities;
import org.apache.commons.lang3.StringUtils;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.libs.Scala;
import scala.Tuple2;
import scala.collection.Seq;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "SUBSET_CODE", uniqueConstraints = @UniqueConstraint(columnNames={"SUBSET_CODE_ID", "CODE_ID"})
)
public class SubsetCode extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "SUBSET_CODE_ID", nullable = false)
	public int subsetCodeId;
	
	@ManyToOne
	@JoinColumn(name = "CODE_ID", referencedColumnName = "CODE_ID")
	public Code code;

	@Constraints.Required
	@Formats.NonEmpty
	@Column(name = "SUBSET_CODE_VALUE")
	public String subsetCodeValue;

	@Column(name = "SUBSET_CODE_DESCRIPTION")
	public String subsetCodeDescription;

	@Constraints.Required
	@Formats.NonEmpty
	@Column(name = "SUBSET_CODE_STATUS")
	public boolean subsetCodeStatus;

	@Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "EFFECTIVE_DATE")
	public Date effectiveDate;

	@Formats.DateTime(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "EXPIRED_DATE")
	public Date expiredDate;
    
	// -- Queries (long id, Code.class)
	public static Model.Finder<Long, SubsetCode> find = new Model.Finder<Long, SubsetCode>(Long.class, SubsetCode.class);

	/**
	 * Retrieve a list of Subset Codes.
	 *
	 * @return list of subset codes
	 */
	public static List<SubsetCode> listAllSubsetCodes() {
		return find.findList();
	}

	/**
	 * Retrieve a list of Subset Codes based on a particular Code.
	 *
	 * @param code to search
	 * @return list of subset codes
	 */
	public static List<SubsetCode> listSubsetCodesByCode(Code code) {
		return find.where().eq("CODE_ID", code.codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findList();
	}

	public static SubsetCode getSubsetCodeById(int subsetCodeId) {
		return find.where().eq("SUBSET_CODE_ID", subsetCodeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findUnique();
	}

	public static SubsetCode validateSubsetCode(int codeId, int subsetCodeId) {
		return find.where().eq("CODE_ID", codeId).eq("SUBSET_CODE_ID", subsetCodeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findUnique();
	}
	
	public static List<SubsetCode> getMonetizationStatus() {
		return find.where().eq("CODE_ID", Code.getMonetizationStatus().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findList();
	}

	public static Seq<Tuple2<String, String>> allMonetizationOptions() {
		List<Tuple2<String, String>> monetizationOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getMonetizationStatus()) {
			if (sc.expiredDate == null)
				monetizationOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(monetizationOptions);
	}

	public static Seq<Tuple2<String, String>> freeMonetizationOptions() {
		List<Tuple2<String, String>> monetizationOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getMonetizationStatus()) {
			if (sc.expiredDate == null && !StringUtils.containsIgnoreCase(sc.subsetCodeValue, "paid"))
				monetizationOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(monetizationOptions);
	}

	public static Seq<Tuple2<String, String>> paidMonetizationOptions() {
		List<Tuple2<String, String>> monetizationOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getMonetizationStatus()) {
			if (sc.expiredDate == null && !StringUtils.containsIgnoreCase(sc.subsetCodeValue, "free"))
				monetizationOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(monetizationOptions);
	}

	public static Seq<Tuple2<String, String>> recipeBookMonetizationOptions() {
		List<Tuple2<String, String>> monetizationOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getMonetizationStatus()) {
			if (sc.expiredDate == null && !StringUtils.containsIgnoreCase(sc.subsetCodeValue, "bundle"))
				monetizationOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(monetizationOptions);
	}

	public static List<SubsetCode> getPublishStatus() {
		return find.where().eq("CODE_ID", Code.getPublishStatus().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).isNull("EXPIRED_DATE").findList();
	}

	public static Seq<Tuple2<String, String>> deletePublishOptions() {
		List<Tuple2<String, String>> publishOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getPublishStatus()) {
			if (sc.expiredDate == null && (sc.subsetCodeId == Constants.PUBLISH_STATUS_DELETED
					|| sc.subsetCodeId == Constants.PUBLISH_STATUS_PUBLISHED))
				publishOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(publishOptions);
	}

	public static Seq<Tuple2<String, String>> allPublishOptions() {
		List<Tuple2<String, String>> publishOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getPublishStatus()) {
			if (sc.expiredDate == null)
				publishOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(publishOptions);
	}

	public static List<SubsetCode> getPrivacyStatus() {
		//return find.where().eq("CODE_ID", Code.getPrivacyStatus().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).or(Expr.isNull("EXPIRED_DATE"), Expr.gt("EXPIRED_DATE", new Date())).findList();
		return find.where().eq("CODE_ID", Code.getPrivacyStatus().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).isNull("EXPIRED_DATE").findList();
	}

	public static Seq<Tuple2<String, String>> allPrivacyOptions() {
		List<Tuple2<String, String>> privacyOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getPrivacyStatus()) {
			if (sc.expiredDate == null)
				privacyOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(privacyOptions);
	}

	public static List<SubsetCode> getGender() {
		return find.where().eq("CODE_ID", Code.getGender().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findList();
	}

	public static Seq<Tuple2<String, String>> allGenderOptions() {
		List<Tuple2<String, String>> genderOptions = new ArrayList<Tuple2<String, String>>();
		genderOptions.add(Scala.Tuple("", ""));
		for (SubsetCode sc : getGender()) {
			if (sc.expiredDate == null)
				genderOptions.add(Scala.Tuple(sc.subsetCodeValue, sc.subsetCodeValue));
		}
		return Scala.toSeq(genderOptions);
	}

	public static int getNumberOfDirectionsLimit() {
		SubsetCode result = find.where().eq("CODE_ID", Code.getNumberOfDirections().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findUnique();
		String directionLimit = result != null? result.subsetCodeValue : ""+Constants.MAX_RECIPE_DIRECTIONS_LIMIT;
		return StringUtils.isEmpty(directionLimit) ? Constants.MAX_RECIPE_DIRECTIONS_LIMIT : Integer.parseInt(directionLimit);
	}

	public static int getNumberOfCoverPhotosLimit() {
		SubsetCode result = find.where().eq("CODE_ID", Code.getNumberOfCoverPhotos().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findUnique();
		String coverPhotoLimit = result != null? result.subsetCodeValue : ""+SubsetCode.getNumberOfCoverPhotosLimit();
		return StringUtils.isEmpty(coverPhotoLimit) ? SubsetCode.getNumberOfCoverPhotosLimit() : Integer.parseInt(coverPhotoLimit);
	}

	public static List<SubsetCode> getNotificationType() {
		return find.where().eq("CODE_ID", Code.getNotificationType().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findList();
	}

	public static List<SubsetCode> getRecipeDifficulty() {
		return find.where().eq("CODE_ID", Code.getDifficulty().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findList();
	}

	public static Seq<Tuple2<String, String>> allRecipeDifficultyOptions() {
		List<Tuple2<String, String>> recipeDifficultyOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getRecipeDifficulty()) {
			if (sc.expiredDate == null)
				recipeDifficultyOptions.add(Scala.Tuple(""+sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(recipeDifficultyOptions);
	}

	public static List<SubsetCode> getOrderStatus() {
		return find.where().eq("CODE_ID", Code.getOrderStatus().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).orderBy().asc("SUBSET_CODE_ID").findList();
	}

	public static Seq<Tuple2<String, String>> allOrderStatusOptions() {
		List<Tuple2<String, String>> orderStatusOptions = new ArrayList<Tuple2<String, String>>();
		orderStatusOptions.add(Scala.Tuple("", ""));
		for (SubsetCode sc : getOrderStatus()) {
			if (sc.expiredDate == null)
				orderStatusOptions.add(Scala.Tuple(""+sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(orderStatusOptions);
	}

	public static List<SubsetCode> getCollectionMode() {
		return find.where().eq("CODE_ID", Code.getCollectionMode().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).orderBy().desc("SUBSET_CODE_ID").findList();
	}

	public static Seq<Tuple2<String, String>> allCollectionModeOptions() {
		List<Tuple2<String, String>> collectionModeOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getCollectionMode()) {
			if (sc.expiredDate == null)
				collectionModeOptions.add(Scala.Tuple(""+sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(collectionModeOptions);
	}

	public static Seq<Tuple2<String, String>> cookForMeCollectionModeOptions(Recipe recipe) {
		List<Tuple2<String, String>> cookForMeCollectionModeOptions = new ArrayList<Tuple2<String, String>>();
		if(recipe.cookForMe != null) {
			for (SubsetCode sc : Utilities.spiltStringByCommaToSubsetCode(recipe.cookForMe.collectionMode)) {
				if (sc.expiredDate == null)
					cookForMeCollectionModeOptions.add(Scala.Tuple("" + sc.subsetCodeId, sc.subsetCodeValue));
			}
		}
		return Scala.toSeq(cookForMeCollectionModeOptions);
	}

	public static List<SubsetCode> getPaymentType() {
		return find.where().eq("CODE_ID", Code.getPaymentType().codeId).eq("SUBSET_CODE_STATUS", Constants.ACTIVE).findList();
	}

	public static Seq<Tuple2<String, String>> allPaymentTypeOptions() {
		List<Tuple2<String, String>> paymentTypeOptions = new ArrayList<Tuple2<String, String>>();
		for (SubsetCode sc : getPaymentType()) {
			if (sc.expiredDate == null)
				paymentTypeOptions.add(Scala.Tuple(""+sc.subsetCodeId, sc.subsetCodeValue));
		}
		return Scala.toSeq(paymentTypeOptions);
	}
}
