package models;

import com.avaje.ebean.*;
import com.avaje.ebean.Query;
import controllers.utils.Constants;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="BOOK_RECIPE")
public class BookRecipe extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "BOOK_RECIPE_ID", unique = true, nullable = false)
    public Long bookRecipeId;

	@ManyToOne
	@JoinColumn(name="RECIPE_BOOK_ID", referencedColumnName="RECIPE_BOOK_ID")
	public RecipeBook recipeBook;

	@ManyToOne
	@JoinColumn(name="RECIPE_ID", referencedColumnName="RECIPE_ID")
	public Recipe recipe;

    @Formats.NonEmpty
	@Column(name = "BOOK_RECIPE_STATUS") //FAV (1/TRUE) or UNFAV (0/FALSE)
	public boolean bookRecipeStatus;
		
	// -- Queries (long id, BookRecipe.class)
    public static Model.Finder<Long, BookRecipe> find = new Model.Finder<Long, BookRecipe>(Long.class, BookRecipe.class);

	/**
     * Retrieve a list of Recipes based on a user's recipe book.
     *
     * @param recipeBookId
     * @return list of recipes in a recipeBook
     */
    public static List<BookRecipe> findBookRecipesByRecipeBook(long recipeBookId) {
        return find.where().eq("RECIPE_BOOK_ID", recipeBookId).findList();
    }
    
	/**
     * Retrieve a list of Recipes based on a recipe.
     *
     * @param recipeId to search
     * @return list of recipes in a recipeBook
     */
    public static List<BookRecipe> findBookRecipesByRecipe(long recipeId) {
        return find.where().eq("RECIPE_ID", recipeId).findList();
    }
    
    public static int findActiveRecipeFavCount(long recipeId) {
        String sql = "SELECT 1 AS COUNT FROM BOOK_RECIPE br, RECIPE_BOOK rb WHERE br.RECIPE_ID = :recipeId AND br.RECIPE_BOOK_ID = rb.RECIPE_BOOK_ID AND BOOK_RECIPE_STATUS = 1 GROUP BY rb.USER_ID";
        SqlQuery sqlQuery = Ebean.createSqlQuery(sql).setParameter("recipeId",recipeId);
        return sqlQuery.findList().size();
    }
    public static int findAllRecipeFavCount(long recipeId) {
        String sql = "SELECT 1 AS COUNT FROM BOOK_RECIPE br, RECIPE_BOOK rb WHERE br.RECIPE_ID = :recipeId AND br.RECIPE_BOOK_ID = rb.RECIPE_BOOK_ID GROUP BY rb.USER_ID";
        SqlQuery sqlQuery = Ebean.createSqlQuery(sql).setParameter("recipeId",recipeId);
        return sqlQuery.findList().size();
    }
    
    public static List<User> findUniqueUserPerActiveRecipe(long recipeId) {
        RawSql rawSql = RawSqlBuilder.parse("SELECT rb.USER_ID FROM BOOK_RECIPE br, RECIPE_BOOK rb WHERE br.RECIPE_ID = :recipeId AND br.RECIPE_BOOK_ID = rb.RECIPE_BOOK_ID AND BOOK_RECIPE_STATUS = 1 GROUP BY rb.USER_ID").create();
        Query<User> query = Ebean.find(User.class);
        query.setRawSql(rawSql);
        return query.findList();
    }
    public static List<User> findUniqueUserPerRecipe(long recipeId) {
        RawSql rawSql = RawSqlBuilder.parse("SELECT rb.USER_ID FROM BOOK_RECIPE br, RECIPE_BOOK rb WHERE br.RECIPE_ID = :recipeId AND br.RECIPE_BOOK_ID = rb.RECIPE_BOOK_ID GROUP BY rb.USER_ID").create();
        Query<User> query = Ebean.find(User.class);
        query.setRawSql(rawSql);
        query.setParameter("recipeId",recipeId);
        return query.findList();
    }
    
	/**
     * Retrieve a list of RecipeBooks based on a recipe & user.
     *
     * @param recipeId
     * @param userId 
     * @return list of recipes in a recipeBook
     */
    public static List<BookRecipe> findActiveRecipesByUserAndRecipe(long userId, long recipeId) {
        return find.where().eq("recipeBook.user.userId", userId).eq("RECIPE_ID", recipeId).eq("BOOK_RECIPE_STATUS", Constants.FAVOURITE).findList();
    }
    
    public static List<BookRecipe> findAllRecipesByUserAndRecipe(long userId, long recipeId) {
        return find.where().eq("recipeBook.user.userId", userId).eq("RECIPE_ID", recipeId).findList();
    }
    
	/**
     * Retrieve a list of Recipes based on a recipe & recipeBook.
     *
     * @param recipeId
     * @param recipeBookId 
     * @return list of recipes in a recipeBook
     */
    public static List<BookRecipe> findActiveRecipesByRecipeAndRecipeBook(long recipeId, long recipeBookId) {
        return find.where().eq("RECIPE_BOOK_ID", recipeBookId).eq("RECIPE_ID", recipeId).eq("BOOK_RECIPE_STATUS", Constants.FAVOURITE).findList();
    }
    public static List<BookRecipe> findAllRecipesByRecipeAndRecipeBook(long recipeId, long recipeBookId) {
        return find.where().eq("RECIPE_BOOK_ID", recipeBookId).eq("RECIPE_ID", recipeId).findList();
    }
    

	/**
     * Retrieve a list of Recipes based on a recipeBook.
     *
     * @param recipeBookId 
     * @return list of recipes in a recipeBook
     */
    public static List<BookRecipe> findActiveRecipesByRecipeBook(long recipeBookId) {
        return find.where().eq("RECIPE_BOOK_ID", recipeBookId).eq("BOOK_RECIPE_STATUS", Constants.FAVOURITE).findList();
    }
    
    public static List<BookRecipe> findAllRecipesByUserAndRecipe(long recipeBookId) {
        return find.where().eq("RECIPE_BOOK_ID", recipeBookId).findList();
    }
}
