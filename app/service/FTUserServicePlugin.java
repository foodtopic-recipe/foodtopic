package service;

import models.User;
import play.Application;

import com.feth.play.module.pa.service.UserServicePlugin;
import com.feth.play.module.pa.user.AuthUser;
import com.feth.play.module.pa.user.AuthUserIdentity;
import com.feth.play.module.pa.user.EmailIdentity;

public class FTUserServicePlugin extends UserServicePlugin {

    public FTUserServicePlugin(final Application app) {
        super(app);
    }

    @Override
    public Object save(AuthUser authUser) {
		final boolean isLinked = User.existsByAuthUserIdentity(authUser);
		if (!isLinked) {
			//check if email exist
			if (authUser instanceof EmailIdentity) {
				final EmailIdentity identity = (EmailIdentity) authUser;
				User user = User.findByEmail(identity.getEmail());
				if (user!=null) {
					user.addLinkedAccount(authUser);
					return user.userId;
				}
			}
			
			return User.create(authUser).userId;
			
		} else {
			// we have this user already, so return null
			return null;
		}
    }

    @Override
    public Object getLocalIdentity(final AuthUserIdentity identity) {
        // For production: Caching might be a good idea here...
        // ...and dont forget to sync the cache when users get deactivated/deleted
    	User u = User.findByAuthUserIdentity(identity);
        
        if(u != null) {
            return u.userId;
        } else {
            return null;
        }
    }

    @Override
    public AuthUser merge(final AuthUser newUser, final AuthUser oldUser) {
        if (!oldUser.equals(newUser)) {
            //User.merge(oldUser, newUser);
        }
        return oldUser;
    }

    @Override
    public AuthUser link(final AuthUser oldUser, final AuthUser newUser) {
        User.addLinkedAccount(oldUser, newUser);
        return null;
    }
    
    // Do any updating to the user after login here
    @Override
    public AuthUser update(AuthUser knownUser) {
        User user = User.findByAuthUserIdentity(knownUser);
        if (user!=null) {
        	user.updateLastLogin();
        }
        return knownUser;
    }

}