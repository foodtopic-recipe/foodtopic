package providers;

import providers.FTPasswordAuthProvider.FTSignup;

import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;
import com.feth.play.module.pa.user.FirstLastNameIdentity;

public class FTPasswordAuthUser extends UsernamePasswordAuthUser implements FirstLastNameIdentity {

	private static final long serialVersionUID = 1L;
	private final String firstName, lastName;

	public FTPasswordAuthUser(final FTSignup signup) {
		super(signup.password, signup.email);
		this.firstName = signup.firstName;
		this.lastName = signup.lastName;
	}

	/**
	 * Used for password reset only - do not use this to signup a user!
	 * @param password
	 */
	public FTPasswordAuthUser(final String password) {
		super(password, null);
		firstName = null;
		lastName = null;
	}
	
	@Override
	public String getName() {
		return lastName + " " + firstName;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}
	
	@Override
	public String getLastName() {
		return lastName;
	}
}
