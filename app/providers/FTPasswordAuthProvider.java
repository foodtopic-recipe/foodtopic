package providers;

import static play.data.Form.form;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.feth.play.module.mail.Mailer.Mail.Body;
import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider;
import com.feth.play.module.pa.providers.password.UsernamePasswordAuthUser;

import models.LinkedAccount;
import models.Token;
import models.Token.Type;
import models.User;
import play.Application;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.data.validation.ValidationError;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Call;
import play.mvc.Http.Context;

public class FTPasswordAuthProvider extends
		UsernamePasswordAuthProvider<String, FTLoginAuthUser, FTPasswordAuthUser, FTPasswordAuthProvider.FTLogin, FTPasswordAuthProvider.FTSignup> {

	private static final String SETTING_KEY_VERIFICATION_LINK_SECURE = SETTING_KEY_MAIL+ "." + "verificationLink.secure";
	private static final String SETTING_KEY_PASSWORD_RESET_LINK_SECURE = SETTING_KEY_MAIL+ "." + "passwordResetLink.secure";
	private static final String SETTING_KEY_LINK_LOGIN_AFTER_PASSWORD_RESET = "loginAfterPasswordReset";

	private static final String EMAIL_TEMPLATE_FALLBACK_LANGUAGE = "en";

	@Override
	protected List<String> neededSettingKeys() {
		final List<String> needed = new ArrayList<String>(super.neededSettingKeys());
		needed.add(SETTING_KEY_VERIFICATION_LINK_SECURE);
		needed.add(SETTING_KEY_PASSWORD_RESET_LINK_SECURE);
		needed.add(SETTING_KEY_LINK_LOGIN_AFTER_PASSWORD_RESET);
		return needed;
	}

	public static FTPasswordAuthProvider getProvider() {
		return (FTPasswordAuthProvider) PlayAuthenticate.getProvider(UsernamePasswordAuthProvider.PROVIDER_KEY);
	}


	public static class FTLogin implements com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider.UsernamePassword {

		@Required
		@Email
		public String email;
		
		@Required
		@MinLength(5)
		public String password;

		@Override
		public String getEmail() {
			return email;
		}

		@Override
		public String getPassword() {
			return password;
		}
	}

	public static class FTSignup extends FTLogin {

		@Required
		@MinLength(5)
		public String confirmPassword;

		@Required
		public String firstName;
		
		@Required
		public String lastName;

		public String validate() {
			if (password == null || !password.equals(confirmPassword)) {
				return Messages.get("signup.password.notmatch");
			}
			return null;
		}
	}
	
	public static class FTpasswordChange {
		
		@Required
		@MinLength(5)
		public String currentPassword;
		
		@Required
		@MinLength(5)
		public String newPassword;

		@Required
		@MinLength(5)
		public String confirmPassword;

		List<ValidationError> errors = new ArrayList<ValidationError>();
		
		public List<ValidationError> validate() {
			if(currentPassword != null && currentPassword.equals(newPassword)) {
			    errors.add(new ValidationError("newPassword", Messages.get("signup.password.mustnotmatch")));
			}
			if (newPassword == null || !newPassword.equals(confirmPassword)) {
			    errors.add(new ValidationError("confirmPassword", Messages.get("signup.password.notmatch")));
			}
            return errors.isEmpty() ? null : errors;
		}
	}
	
	public static class FTpasswordReset {
		
		@Required
		@MinLength(5)
		public String newPassword;
        
		@Required
		@MinLength(5)
		public String confirmPassword;
        
		List<ValidationError> errors = new ArrayList<ValidationError>();
		
		public List<ValidationError> validate() {
			if (newPassword == null || !newPassword.equals(confirmPassword)) {
			    errors.add(new ValidationError("confirmPassword", Messages.get("signup.password.notmatch")));
			}
            return errors.isEmpty() ? null : errors;
		}
	}
	
	public static class FTpasswordForgot {
		@Required
		@Email
		public String email;
	}

	public static final Form<FTSignup> SIGNUP_FORM = form(FTSignup.class);
	public static final Form<FTLogin> LOGIN_FORM = form(FTLogin.class);
	public static final Form<FTpasswordChange> PASSWORD_CHANGE_FORM = form(FTpasswordChange.class);
	public static final Form<FTpasswordForgot> PASSWORD_FORGOT_FORM = form(FTpasswordForgot.class);

	public FTPasswordAuthProvider(Application app) {
		super(app);
	}

	protected Form<FTSignup> getSignupForm() {
		return form(FTSignup.class);
	}

	protected Form<FTLogin> getLoginForm() {
		return LOGIN_FORM;
	}
	
	private enum Case {
		SIGNUP, LOGIN
	}

	@Override
	protected com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider.SignupResult signupUser(final FTPasswordAuthUser ftUser) {
		User user = User.findByUsernamePasswordIdentity(ftUser);
		// User with this login method is found
		if (user != null) {
			if (user.emailValidated) {
				// This user exists, has its email validated and is active
				return SignupResult.USER_EXISTS;
			} else {
				// this user exists, is active but has not yet validated its email
				// Usually the email should be verified before allowing login, however if you return SignupResult.USER_CREATED,
		        // the user will be able to go into dashboard but denied access to change password & upload of recipes
	        	return SignupResult.USER_CREATED;
		        // while if you return SignupResult.USER_EXISTS_UNVERIFIED, the user is unable to login until email verification
				//return SignupResult.USER_EXISTS_UNVERIFIED;
			}
		// User with this login method is not found
		} else {
			user = User.findByEmail(ftUser.getEmail());
			if (user != null) {
				user.createLinkAccount(ftUser);
				return SignupResult.USER_EXISTS_UNVERIFIED;
			}
		}
		// The user either does not exist or is inactive - create a new one
		@SuppressWarnings("unused")
		final User newUser = User.create(ftUser);
		// Usually the email should be verified before allowing login, however if you return SignupResult.USER_CREATED,
		// the user will be able to go into dashboard but denied access to change password & upload of recipes
		return SignupResult.USER_CREATED;
		// while if you return SignupResult.USER_CREATED_UNVERIFIED, the user is unable to login until email verification
		//return SignupResult.USER_CREATED_UNVERIFIED;
	}

	@Override
	protected com.feth.play.module.pa.providers.password.UsernamePasswordAuthProvider.LoginResult loginUser(final FTLoginAuthUser authUser) {
		final User u = User.findByUsernamePasswordIdentity(authUser);
		if (u == null) {
			return LoginResult.NOT_FOUND;
		} else {
			if (!u.emailValidated) {
				// Usually the email should be verified before allowing login, however if you return LoginResult.USER_LOGGED_IN,
		        // the user will be able to go into dashboard but denied access to change password & upload of recipes
	        	return LoginResult.USER_LOGGED_IN;
		        // while if you return LoginResult.USER_UNVERIFIED, the user is unable to login until email verification
				//return LoginResult.USER_UNVERIFIED;
			} else {
				for (final LinkedAccount acc : u.linkedAccounts) {
					if (getKey().equals(acc.providerKey)) {
						if (authUser.checkPassword(acc.providerUserId, authUser.getPassword())) {
							// Password was correct
							return LoginResult.USER_LOGGED_IN;
						} else {
							// if you don't return here,
							// you would allow the user to have
							// multiple passwords defined
							// usually we don't want this
							return LoginResult.WRONG_PASSWORD;
						}
					}
				}
				return LoginResult.WRONG_PASSWORD;
			}
		}
	}

	@Override
	protected Call userExists(final UsernamePasswordAuthUser authUser) {
		return controllers.account.routes.Register.exists();
	}

	@Override
	protected Call userUnverified(final UsernamePasswordAuthUser authUser) {
		return controllers.account.routes.Register.unverified(false);
	}

	@Override
	protected FTPasswordAuthUser buildSignupAuthUser(final FTSignup signup, final Context ctx) {
		return new FTPasswordAuthUser(signup);
	}

	@Override
	protected FTLoginAuthUser buildLoginAuthUser(final FTLogin login, final Context ctx) {
		return new FTLoginAuthUser(login.getPassword(), login.getEmail());
	}
	

	@Override
	protected FTLoginAuthUser transformAuthUser(final FTPasswordAuthUser authUser, final Context context) {
		return new FTLoginAuthUser(authUser.getEmail());
	}

	@Override
	protected String getVerifyEmailMailingSubject(final FTPasswordAuthUser user, final Context ctx) {
		return Messages.get("signup.password.verify_signup.subject", Messages.get("title"));
	}

	@Override
	protected String onLoginUserNotFound(final Context context) {
		context.flash().put(controllers.Application.FLASH_ERROR_KEY,Messages.get("signin.error"));
		return super.onLoginUserNotFound(context);
	}

	@Override
	protected Body getVerifyEmailMailingBody(final String token, final FTPasswordAuthUser user, final Context ctx) {

		final boolean isSecure = getConfiguration().getBoolean(SETTING_KEY_VERIFICATION_LINK_SECURE);
		final String url = controllers.account.routes.Register.verify(token).absoluteURL(ctx.request(), isSecure);

		final Lang lang = Lang.preferred(ctx.request().acceptLanguages());
		final String langCode = lang.code();

		final String html = getEmailTemplate(
				"views.html.emailTemplate.verify_email", langCode, url,
				token, user.getFirstName() + " " + user.getLastName(), user.getEmail());
		final String text = getEmailTemplate(
				"views.txt.emailTemplate.verify_email", langCode, url,
				token, user.getFirstName() + " " + user.getLastName(), user.getEmail());

		return new Body(text, html);
	}

	private static String generateToken() {
		return UUID.randomUUID().toString();
	}

	@Override
	protected String generateVerificationRecord(final FTPasswordAuthUser user) {
		return generateVerificationRecord(User.findByAuthUserIdentity(user));

	}

	protected String generateVerificationRecord(final User user) {
		final String token = generateToken();
		// Do database actions, etc.
		Token.create(Type.EMAIL_VERIFICATION, token, user);
		return token;
	}

	protected String generatePasswordResetRecord(final User u) {
		final String token = generateToken();
		Token.create(Type.PASSWORD_RESET, token, u);
		return token;
	}

	protected String getPasswordResetMailingSubject(final User user, final Context ctx) {
		return Messages.get("resetpassword.reset_email.subject", Messages.get("title"));
	}

	protected Body getPasswordResetMailingBody(final String token, final User user, final Context ctx) {

		final boolean isSecure = getConfiguration().getBoolean(SETTING_KEY_PASSWORD_RESET_LINK_SECURE);
		final String url = controllers.account.routes.Password.resetPassword(token).absoluteURL(ctx.request(), isSecure);

		final Lang lang = Lang.preferred(ctx.request().acceptLanguages());
		final String langCode = lang.code();

		final String html = getEmailTemplate("views.html.emailTemplate.password_reset", langCode, url, token, user.firstName, user.email);
		final String text = getEmailTemplate("views.txt.emailTemplate.password_reset", langCode, url, token, user.firstName, user.email);

		return new Body(text, html);
	}

	public void sendPasswordResetMailing(final User user, final Context ctx) {
		final String token = generatePasswordResetRecord(user);
		final String subject = getPasswordResetMailingSubject(user, ctx);
		final Body body = getPasswordResetMailingBody(token, user, ctx);
		sendMail(subject, body, getEmailName(user));
	}

	public boolean isLoginAfterPasswordReset() {
		return getConfiguration().getBoolean( SETTING_KEY_LINK_LOGIN_AFTER_PASSWORD_RESET);
	}

	protected String getVerifyEmailMailingSubjectAfterSignup(final User user, final Context ctx) {
		return Messages.get("signup.password.verify_email.subject", Messages.get("title"));
	}

	protected String getEmailTemplate(final String template,
			final String langCode, final String url, final String token,
			final String name, final String email) {
		Class<?> cls = null;
		String ret = null;
		try {
			cls = Class.forName(template + "_" + langCode);
		} catch (ClassNotFoundException e) {
			Logger.warn("Template: '"+ template+ "_"+ langCode+ "' was not found! Trying to use English fallback template instead.");
		}
		if (cls == null) {
			try {
				cls = Class.forName(template + "_" + EMAIL_TEMPLATE_FALLBACK_LANGUAGE);
			} catch (ClassNotFoundException e) {
				Logger.error("Fallback template: '" + template + "_" + EMAIL_TEMPLATE_FALLBACK_LANGUAGE + "' was not found either!");
			}
		}
		if (cls != null) {
			Method htmlRender = null;
			try {
				htmlRender = cls.getMethod("render", String.class,String.class, String.class, String.class);
				ret = htmlRender.invoke(null, url, token, name, email).toString();

			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	protected Body getVerifyEmailMailingBodyAfterSignup(final String token, final User user, final Context ctx) {

		final boolean isSecure = getConfiguration().getBoolean(SETTING_KEY_VERIFICATION_LINK_SECURE);
		final String url = controllers.account.routes.Register.verify(token).absoluteURL(ctx.request(), isSecure);

		final Lang lang = Lang.preferred(ctx.request().acceptLanguages());
		final String langCode = lang.code();

		final String html = getEmailTemplate("views.html.emailTemplate.verify_email", langCode, url, token, user.firstName + " " + user.lastName, user.email);
		final String text = getEmailTemplate("views.txt.emailTemplate.verify_email", langCode, url, token, user.firstName + " " + user.lastName, user.email);

		return new Body(text, html);
	}

	public void sendVerifyEmailMailingAfterSignup(final User user, final Context ctx) {

		final String subject = getVerifyEmailMailingSubjectAfterSignup(user, ctx);
		final String token = generateVerificationRecord(user);
		final Body body = getVerifyEmailMailingBodyAfterSignup(token, user, ctx);
		sendMail(subject, body, getEmailName(user));
		
		user.validationEmailSent = new Date();
		user.save();
	}

	private String getEmailName(final User user) {
		return getEmailName(user.email, user.firstName + " " + user.lastName);
	}
	
	protected String getEmailTemplate(final String template, final String to, final String from, final String comment) {
		Class<?> cls = null;
		String ret = null;
		
		try {
			cls = Class.forName(template + "_en");
		} catch (ClassNotFoundException e) {
			Logger.warn("Template: '" + template + "_en" + "' was not found!");
		}
		if (cls != null) {
			Method htmlRender = null;
			try {
				htmlRender = cls.getMethod("render", String.class, String.class, String.class);
				ret = htmlRender.invoke("", to, from, comment).toString();

			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
    public void sendInternalEmailAlert(String to_Email, String to_Name, String from, String comment, String subject) {
        String html = getEmailTemplate("views.html.emailTemplate.internal_email_alert", to_Name, from, comment);
		String text = getEmailTemplate("views.txt.emailTemplate.internal_email_alert", to_Name, from, comment);
        
		final Body body = new Body(text, html);
		sendMail(subject, body, to_Email);
	}

	public void sendCookForMeEmailAlert(String to_Email, String to_Name, String from, String comment, String subject) {
		String html = getEmailTemplate("views.html.emailTemplate.CFM_email_alert", to_Name, from, comment);
		String text = getEmailTemplate("views.txt.cookForMe.CFM_email_alert", to_Name, from, comment);

		final Body body = new Body(text, html);
		sendMail(subject, body, to_Email);
	}
}
