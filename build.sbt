import sbt.Keys._

name := """FoodTopic"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  jdbc,
  javaEbean,
  cache,
  "org.mindrot" % "jbcrypt" % "0.3m",
  "mysql" % "mysql-connector-java" % "5.1.18",
  "javax.activation" % "activation" % "1.1.1",
  "com.typesafe" %% "play-plugins-mailer" % "2.2.0",
  "com.feth" %% "play-authenticate" % "0.6.5-SNAPSHOT",
  javaWs,
  "commons-io" % "commons-io" % "2.4",
  "org.javassist" % "javassist" % "3.18.2-GA",
  "com.clever-age" % "play2-elasticsearch" % "1.1.0",
  //"org.elasticsearch" % "elasticsearch" % "1.5.2",
  filters
)

resolvers ++= Seq(
    "Apache" at "http://repo1.maven.org/maven2/",
    "jBCrypt Repository" at "http://repo1.maven.org/maven2/org/",
    "Sonatype OSS Snasphots" at "http://oss.sonatype.org/content/repositories/snapshots",
    "play-easymail (release)" at "http://joscha.github.io/play-easymail/repo/releases/",
  	"play-easymail (snapshot)" at "http://joscha.github.io/play-easymail/repo/snapshots/",
  	"play-authenticate (release)" at "http://joscha.github.io/play-authenticate/repo/releases/",
  	"play-authenticate (snapshot)" at "http://joscha.github.io/play-authenticate/repo/snapshots/"
)

javaOptions ++= Seq(
	"-Xmx1024M", "-Xmx2048M"
)
    
lazy val root = (project in file(".")).enablePlugins(play.PlayJava)


//fork in run := true