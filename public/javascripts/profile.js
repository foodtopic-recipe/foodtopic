$(document).ready(function() {
    $('#addressLine1').tooltip({title: "", trigger: "manual", placement:"bottom"});
    $('#postalCode').tooltip({title: "", trigger: "manual", placement:"bottom"});

    var dateOfBirth = $("#dateOfBirth");
    if(dateOfBirth.length > 0) {
        dateOfBirth.datetimepicker({
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
            widgetPositioning: {
                horizontal: 'right',
                vertical: 'bottom'
            }
        });
    }

    $('#followBtn').on('click', function () {
        addFollowerFunction();
    });

    $('#unfollowBtn').on('click', function () {
        removeFollowerFunction();
    });

    $('#profilePhoto').on('change', function () {
        readProfileImageURL(this, $("#profileImage"));
    });

    $('#profile-main-menu-block a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $('.tab-content .tab-pane a').unbind();
});

function readProfileImageURL(input, output) {
    output.each(function(i) {
        if (input.files && input.files[i]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-picture').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[i]);
        }
    });
}

function addFollowerFunction() {
    var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

    jsRoutes.controllers.follower.EditFollower.followUser().ajax({
        data : {"userId": $("#followBtn").data('profile')},
        success : function(data) {
            $('#profileFollowAction').load(location.href + " #unfollowBtn", function() {
                $("#unfollowBtn").on('click', removeFollowerFunction);
            });

            $('#following-block').load(location.href + " .follow-col");

            $('#followingBadge').load(location.href + " #followingCount");
            $('#followerBadge').load(location.href + " #followerCount");
        },
        error : function(err) {
            //alert(err.responseText);
        }
    });
    loader.remove();
    return false;
}

function removeFollowerFunction() {
    var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

    jsRoutes.controllers.follower.EditFollower.unfollowUser().ajax({
        data : {"userId": $("#unfollowBtn").data('profile')},
        success : function(data) {
            $('#profileFollowAction').load(location.href + " #followBtn", function() {
                $("#followBtn").on('click', addFollowerFunction);
            });

            $('#following-block').load(location.href + " .follow-col");

            $('#followingBadge').load(location.href + " #followingCount");
            $('#followerBadge').load(location.href + " #followerCount");
        },
        error : function(err) {
            //alert(err.responseText);
        }
    });
    loader.remove();
    return false;
}

function updateProfileForCFM() {
	$(".updateAddress-alert").hide();

    var addressLine1 = $('#addressLine1');
    var postalCode = $('#postalCode');
    var countryCode = $('#countryCode');
    var contactNumber = $('#contactNumber');
    jsRoutes.controllers.account.UpdateProfile.updateProfileForCFM().ajax({
    	dataType: "json",
        data : {"addressLine1":addressLine1.val(), "addressLine2":$('#addressLine2').val(),
                "addressLine3":$('#addressLine3').val(), "postalCode":postalCode.val(),
                "countryCode":countryCode.val(), "contactNumber":contactNumber.val()},
        success : function(data) {
            if ($('#modal').is(':visible')) {
                $('#modal').modal('hide');
            };
        },
        error : function(err) {
        	response = jQuery.parseJSON(err.responseText);
        	if (response.flash != null) {
        		$(".updateProfile-alert").html(response.flash);
        		$(".updateProfile-alert").show();
        	}
        	if (response.addressLine1 != null) {
        		 addressLine1.attr('data-original-title', response.addressLine1)
                 .tooltip('fixTitle')
                 .tooltip('show');
        		 addressLine1.addClass("errorinput");
        	} else {
        		addressLine1.tooltip('hide');
        		addressLine1.removeClass("errorinput");
        	}

        	if (response.postalCode != null) {
        		postalCode.attr('data-original-title', response.postalCode)
                .tooltip('fixTitle')
                .tooltip('show');
        		postalCode.addClass("errorinput");
        	} else {
        		postalCode.tooltip('hide');
        		postalCode.removeClass("errorinput");
            }

        	if (response.countryCode != null) {
        		countryCode.attr('data-original-title', response.countryCode)
                .tooltip('fixTitle')
                .tooltip('show');
        		countryCode.addClass("errorinput");
        	} else {
        		countryCode.tooltip('hide');
        		countryCode.removeClass("errorinput");
            }

        	if (response.contactNumber != null) {
        		contactNumber.attr('data-original-title', response.contactNumber)
                .tooltip('fixTitle')
                .tooltip('show');
        		contactNumber.addClass("errorinput");
        	} else {
        		contactNumber.tooltip('hide');
        		contactNumber.removeClass("errorinput");
            }
        }
    });
    return false;
};