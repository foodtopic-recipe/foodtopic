$(document).ready(function() {

    $('#cookForMeContainerTrigger').on('click', function () {
        var cookForMeContainer = $("#cookForMeContainer");
        if(cookForMeContainer.is(":hidden")) {
            cookForMeContainer.fadeIn();
        } else {
            cookForMeContainer.hide();
        }
    });

    $(document).on('click','.print', function() {
        window.print();
    });

    $(document).on('click', '.likeRecipe', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.Like.recipeLike().ajax({
            data : {"recipeId" : $("h1").data('recipe')},
            success : function(data) {
                $('#recipeLikes').load(location.href + " #recipeLikes > *");
                $('.recipe-tools').load(location.href + " .tool");
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '#deleteRecipeBtn', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.recipe.DeleteRecipe.deleteRecipe().ajax({
            data : {"recipeId" : $("h1").data('recipe')},
            success : function(data) {
                $('#deleteRecipeModal').modal('hide');
                location.reload();
                alert(data);
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '#followBtn', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.follower.EditFollower.followUser().ajax({
            data : {"userId": $(this).data('profile')},
            success : function(data) {
                $('#profileFollowAction').load(location.href + " #unfollowBtn");
                $('#followingBadge').load(location.href + " #followingCount");
                $('#followerBadge').load(location.href + " #followerCount");
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '#unfollowBtn', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.follower.EditFollower.unfollowUser().ajax({
            data : {"userId": $(this).data('profile')},
            success : function(data) {
                $('#profileFollowAction').load(location.href + " #followBtn");
                $('#followingBadge').load(location.href + " #followingCount");
                $('#followerBadge').load(location.href + " #followerCount");
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });
});