$(document).ready(function() {
    var clickCount = 1;
    $(".rating").rating();

    $("#reviewScore").on("rating.change", function(event, value, caption) {
        $("#reviewScore").val(value)
    });

    var commentTextArea = $("#comment");
    if(commentTextArea.length > 0) {
        var maxLength = parseInt(commentTextArea.attr('maxlength'));
        commentTextArea.closest('.form-input-group').find('.descriptionCounter').text('(Characters left: ' + (maxLength - commentTextArea.val().length) + ')');

        commentTextArea.keyup(function () {
            commentTextArea.closest('.form-input-group').find('.descriptionCounter').text('(Characters left: ' + (maxLength - commentTextArea.val().length) + ')');
        });
    }

    $(document).on('click', '#postReviewBtn', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
        var rating = $(".rating-container");

        jsRoutes.controllers.recipeReview.CreateRecipeReview.postReview().ajax({
            data : $("#createReviewForm").serialize(),
            success : function() {
                $('#reviewList').load('/listRecipeReview/'+$("h1").data('recipe'), function() {
                    $(".rating").rating();
                    $('#review').html("");
                });
            },
            error : function(err) {
                if (err.responseText != "") {
                    rating.attr('data-original-title', err.responseText)
                        .tooltip('fixTitle')
                        .tooltip('show');
                    rating.addClass("errorinput");
                    rating.focus();
                } else {
                    rating.tooltip('hide');
                    rating.removeClass("errorinput");
                }
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '#updateReviewBtn', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.recipeReview.EditRecipeReview.updateReview().ajax({
            data : {"reviewId" : $(this).data('review'), "rating" : $('#editRating').val(), "comment" : $('#comment').val()},
            success : function() {
                $('#reviewList').load('/listRecipeReview/'+$("h1").data('recipe'), function() {
                    $(".rating").rating();
                    $('#modal').modal('hide');
                });
            },
            error : function(err) {
                if (err.responseText != "") {
                    $(".edit-review-alert").html(err.responseText);
                    $(".edit-review-alert").show();
                }
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '#deleteReviewBtn', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.recipeReview.DeleteRecipeReview.deleteReview().ajax({
            data : {"reviewId" : $(this).data('review')},
            success : function() {
                $('#reviewList').load('/listRecipeReview/'+$("h1").data('recipe'), function() {
                    $('#review').load(location.href + " #createReview", function() {
                        $("#createReview #reviewScore").rating();
                    });
                    $('#modal').modal('hide');
                });
            },
            error : function(err) {
                if (err.responseText != "") {
                    $(".edit-review-alert").html(err.responseText);
                    $(".edit-review-alert").show();
                }
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '#loadMoreReviews', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
        $(this).hide(); //hide load more button

        jsRoutes.controllers.recipeReview.ViewRecipeReview.getNextReviewList().ajax({
            data : {"page":clickCount, "limit":6, "recipeId":$("h1").data('recipe')},
            success : function(data) {
                if(data != null && data != "")
                    $("#loadMoreReviews").show(); //show load more button

                $(".list-review").append(data); //append data received from server
                $(".rating").rating();
                clickCount++;
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '.downvote', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.commentFlag.EditCommentFlag.recipeReviewDownVote().ajax({
            data : {"commentId": $(this).data('cid')},
            success : function() {
                $('#reviewList').load('/listRecipeReview/'+$("h1").data('recipe'), function() {
                    $(".rating").rating();
                    $('#review').fadeOut('fast');
                });
            },
            error : function(err) {
                if (err.responseText != "") {
                    $(this).attr('data-original-title', err.responseText)
                        .tooltip('fixTitle')
                        .tooltip('show');
                    $(this).addClass("errorinput");
                    $(this).focus();
                } else {
                    $(this).tooltip('hide');
                    $(this).removeClass("errorinput");
                }
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '.upvote', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.commentFlag.EditCommentFlag.recipeReviewUpVote().ajax({
            data : {"commentId": $(this).data('cid')},
            success : function() {
                $('#reviewList').load('/listRecipeReview/'+$("h1").data('recipe'), function() {
                    $(".rating").rating();
                    $('#review').fadeOut('fast');
                });
            },
            error : function(err) {
                if (err.responseText != "") {
                    $(this).attr('data-original-title', err.responseText)
                        .tooltip('fixTitle')
                        .tooltip('show');
                    $(this).addClass("errorinput");
                    $(this).focus();
                } else {
                    $(this).tooltip('hide');
                    $(this).removeClass("errorinput");
                }
            }
        });
        loader.remove();
        return false;
    });
});