$(document).ready(function() {
    var clickCount = 1;
    var loadMoreBtn = $("#loadMoreNotification");
    loadMoreBtn.on('click', function() {
        var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
        $(this).hide(); //hide load more button

        jsRoutes.controllers.notification.ViewNotification.getNextNotificationList().ajax({
            data : {"page":clickCount},
            success : function(data) {
                if(data != null && data != "")
                    loadMoreBtn.show(); //show load more button

                $("#notificationContainer").append(data); //append data received from server
                clickCount++;
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });
});