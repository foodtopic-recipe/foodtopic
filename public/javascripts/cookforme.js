$(document).ready(function() {
    var hostClickCount = 1;
    var dinerClickCount = 1;
	var pageCount = 0;

	var dateRangePick = $('.daterangepick');
	if(dateRangePick.length > 0 ) {
		dateRangePick.daterangepicker();
	}

	//-- Used at Cook For Me Order Modal --//
	var bookingDate = $('#bookingDate');
    bookingDate.tooltip({title: "", trigger: "manual", placement:"bottom"});
    if(bookingDate.length > 0) {
		var minBookingDate = new Date();
		minBookingDate.setDate(minBookingDate.getDate() + numberOfDaysToAdd);

		bookingDate.datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
            minDate: minBookingDate,
            sideBySide: true,
            widgetPositioning: {
                horizontal: 'left',
                vertical: 'bottom'
            }
        });
    }

	var servings = $("#servings");
	servings.on('change', function () {
		var cookForMe = $("#cookForMeCost");
		if (cookForMe.length > 0) {
			cookForMe.html($("#individualCookForMeCost").val() * servings.val())
			updateDirectionFeeContainer();
		}
	});

	updateDirectionFeeContainer();
	$("#collectionMode").on('change', function () {
		updateDirectionFeeContainer();
	});
	//-- END --//

	$(document).on('click', ".review-toggle", function(){
		var content = $(this).parent().find('.review-container');
		if (content.is(":visible")) {
			content.hide();
			$(this).removeClass('dropup');
		} else {
			content.show();
			$(this).addClass('dropup');
		}
	});

	$('.acceptBtn').on("click", function() {
    	var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.EditCookForMeOrder.acceptOrder().ajax({
			data : {"orderId": $(this).parent().data("order")},
			success : function(data) {
				alert(data);

				var day = $('#day');
				if(day.length > 0) {
					processCookingDate(day.daterangepicker.start, day.daterangepicker.end, $('#day .ddselected').text());
				}
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
    	loader.remove();
		return false;
	});

	$('.rejectBtn').on("click", function() {
    	var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.EditCookForMeOrder.rejectOrder().ajax({
			data : {"orderId": $(this).parent().data("order")},
			success : function(data) {
				alert(data);

				var day = $('#day');
				if(day.length > 0) {
					processCookingDate(day.daterangepicker.start, day.daterangepicker.end, $('#day .ddselected').text());
				}
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
    	loader.remove();
		return false;
	});

	$('.cancelBtn').on("click", function() {
    	var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.EditCookForMeOrder.cancelOrder().ajax({
			data : {"orderId": $(this).parent().data("order")},
			success : function(data) {
				alert(data);

				var day = $('#day');
				if(day.length > 0) {
					processCookingDate(day.daterangepicker.start, day.daterangepicker.end, $('#day .ddselected').text());
				}
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
    	loader.remove();
		return false;
	});

	$('#completeOrder').on("click", function() {
		var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.EditCookForMeOrder.completeOrder().ajax({
			data : {"orderId": $(this).parents().find("#orderId").val()},
			success : function(data) {
				alert(data);

				var day = $('#day');
				if(day.length > 0) {
					processCookingDate(day.daterangepicker.start, day.daterangepicker.end, $('#day .ddselected').text());
				}
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
		loader.remove();
		return false;
	});

	$('.request-address').on("click", function() {
		var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.EditCookForMeOrder.requestForAddress().ajax({
			data : {"orderId": $(this).closest(".order-entry-info").data("order")},
			success : function(data) {
				alert(data);
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
		loader.remove();
		return false;
	});

	//-- Used at View Cook For Me Order page --//
	var day = $('#day');
	if(day.length > 0) {
		day.daterangepicker({
			opens: 'middle',
			ranges: {
				'Today': [moment(), moment()],
				'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'All Days': [null, null],
			}
		}, processCookingDate);
	}

	var created = $('#created');
	if(created.length > 0) {
		created.daterangepicker({
			opens: 'middle',
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'All Days': [null, null],
			}
		}, processCreatedDate);
	}

	$("#statusselect .dropdown-menu li a").click(function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
			data : {"page":0, "search": $('#ordertype .ddselected').attr('value'), "orderStatus":$(this).attr('value'),
				"created":$('#created .ddselected').attr('value'), "date":$('#day .ddselected').attr('value'), "forWhom":$(".rw-row[data-who]").attr("data-who")},
			success : function(data) {
				$("#orderRow").html(data); //append data received from server
				$(".rating").rating();
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
		$("#statusselect").removeClass("open")
		$("#statusselect .ddselected").html($(this).html())
		$("#statusselect .ddselected").attr('value', $(this).attr('value'))
		loader.remove();
		return false;
	})

	$("#ordertype .dropdown-menu li a").click(function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
			data : {"page":0, "search":$(this).attr('value'), "orderStatus":$('#statusselect .ddselected').attr('value'),
				"created":$('#created .ddselected').attr('value'), "date":$('#day .ddselected').attr('value'), "forWhom":$(".rw-row[data-who]").attr("data-who")},
			success : function(data) {
				$("#orderRow").html(data); //append data received from server
				$(".rating").rating();
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
		$("#ordertype").removeClass("open")
		$("#ordertype .ddselected").html($(this).html())
		$("#ordertype .ddselected").attr('value', $(this).attr('value'))
		loader.remove();
		return false;
	})

	var loadMoreOrderBtn = $('#loadMoreOrder');

	$(document).on('click', "#loadMoreOrder", function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
		loadMoreOrderBtn.hide(); //hide load more button

		jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
			data : {"page":++pageCount, "search":$('#ordertype .ddselected').attr('value'), "orderStatus":$('#statusselect .ddselected').attr('value'),
				"created":$('#created .ddselected').attr('value'), "date":$('#day .ddselected').attr('value'), "forWhom":$(".rw-row[data-who]").attr("data-who")},
			success : function(data) {
				if(data != null && data.trim() != "")
					loadMoreOrderBtn.show(); //show load more button

				$("#orderRow").append(data); //append data received from server
				$(".rating").rating();
			},
			error : function(err) {
				//alert(err.responseText);
			}
		});
		$(this).remove();
		loader.remove();
		return false;
	});
	//-- END --//

	//-- Used at Profile Page - View Cook For Me Tab Panel --//
	var hostOrderSelection = $('#hostOrderSelection');
	if(hostOrderSelection.length > 0) {
		hostOrderSelection.on('change', function () {
			loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

			jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
				data: {
					"page": 0,
					"search": "2",
					"orderStatus": hostOrderSelection.val(),
					"date": "All Days",
					"forWhom": $(".rw-row[data-who]").attr("data-who")
				},
				success: function (data) {
					$("#hostOrderRow").html(data); //append data received from server
				},
				error: function (err) {
					//alert(err.responseText);
				}
			});
			loader.remove();
			return false;
		});
	}

	var dinerOrderSelection = $('#dinerOrderSelection');
	if(dinerOrderSelection.length > 0) {
		dinerOrderSelection.on('change', function () {
			loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

			jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
				data: {
					"page": 0,
					"search": "1",
					"orderStatus": dinerOrderSelection.val(),
					"date": "All Days",
					"forWhom": $(".rw-row[data-who]").attr("data-who")
				},
				success: function (data) {
					$("#dinerOrderRow").html(data); //append data received from server
				},
				error: function (err) {
					//alert(err.responseText);
				}
			});
			loader.remove();
			return false;
		});
	}

	var loadMoreHostOrderBtn = $('#loadMoreHostOrder');
	if(loadMoreHostOrderBtn.length > 0) {
		loadMoreHostOrderBtn.on('click', function () {
			loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
			loadMoreHostOrderBtn.hide(); //hide load more button

			jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
				data: {
					"page": hostClickCount,
					"search": "2",
					"orderStatus": hostOrderSelection.val(),
					"date": "All Days",
					"forWhom": $(".rw-row[data-who]").attr("data-who")
				},
				success: function (data) {
					if (data != null && data.trim() != "")
						loadMoreHostOrderBtn.show(); //show load more button

					$("#hostOrderRow").append(data); //append data received from server
					hostClickCount++;
				},
				error: function (err) {
					//alert(err.responseText);
				}
			});
			loader.remove();
			return false;
		});
	}

	var loadMoreDinerOrderBtn = $('#loadMoreDinerOrder');
	if(loadMoreDinerOrderBtn.length > 0) {
		loadMoreDinerOrderBtn.on('click', function () {
			loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
			loadMoreDinerOrderBtn.hide(); //hide load more button

			jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
				data: {
					"page": dinerClickCount,
					"search": "1",
					"orderStatus": dinerOrderSelection.val(),
					"date": "All Days",
					"forWhom": $(".rw-row[data-who]").attr("data-who")
				},
				success: function (data) {
					if (data != null && data.trim() != "")
						loadMoreDinerOrderBtn.show(); //show load more button

					$("#dinerOrderRow").append(data); //append data received from server
					dinerClickCount++;
				},
				error: function (err) {
					//alert(err.responseText);
				}
			});
			loader.remove();
			return false;
		});
	}
	//-- END --//

	$('#placeCFMOrder').on('click', function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
		$(".new-order-alert").hide();

		var bookingDate = $('#bookingDate');
		var servings = $('#servings');
		var collectionMode = $('#collectionMode');
		var paymentType = $('#paymentType');
		var cookForMeCost = $('#cookForMeCost');
		var deliveryFee = $('#deliveryFee');
		jsRoutes.controllers.cookForMe.CreateCookForMeOrder.placeRecipeOrder().ajax({
			dataType: "json",
			data : {"cookForMeId":$('#cookForMeId').val(), "bookingDate":bookingDate.val(),
				"servings":servings.val(), "collectionMode":collectionMode.val(),
				"paymentType":paymentType.val(), "cookForMeCost":cookForMeCost.text(),
				"deliveryFee":deliveryFee.text(), "remarks":$('#remarks').val()},
			success : function(data) {
				var commonModal = $('#modal');
				commonModal.find('.modal-content').html(data.result);
			},
			error : function(err) {
				response = jQuery.parseJSON(err.responseText);
				if (response.flash != null) {
					$(".new-order-alert").html(response.flash);
					$(".new-order-alert").show();
				}
				if (response.bookingDate != null) {
					bookingDate.attr('data-original-title', response.bookingDate)
						.tooltip('fixTitle')
						.tooltip('show');
					bookingDate.addClass("errorinput");
					bookingDate.focus();
				} else {
					bookingDate.tooltip('hide');
					bookingDate.removeClass("errorinput");
				}

				if (response.servings != null) {
					servings.attr('data-original-title', response.servings)
						.tooltip('fixTitle')
						.tooltip('show');
					servings.addClass("errorinput");
					servings.focus();
				} else {
					servings.tooltip('hide');
					servings.removeClass("errorinput");
				}

				if (response.collectionMode != null) {
					collectionMode.attr('data-original-title', response.collectionMode)
						.tooltip('fixTitle')
						.tooltip('show');
					collectionMode.addClass("errorinput");
					collectionMode.focus();
				} else {
					collectionMode.tooltip('hide');
					collectionMode.removeClass("errorinput");
				}

				if (response.paymentType != null) {
					paymentType.attr('data-original-title', response.paymentType)
						.tooltip('fixTitle')
						.tooltip('show');
					paymentType.addClass("errorinput");
					paymentType.focus();
				} else {
					paymentType.tooltip('hide');
					paymentType.removeClass("errorinput");
				}

				if (response.cookForMeCost != null) {
					cookForMeCost.attr('data-original-title', response.cookForMeCost)
						.tooltip('fixTitle')
						.tooltip('show');
					cookForMeCost.addClass("errorinput");
					cookForMeCost.html(response.updatedCookForMeCost)
					cookForMeCost.focus();
				} else {
					cookForMeCost.tooltip('hide');
					cookForMeCost.removeClass("errorinput");
				}

				if (response.deliveryFee != null) {
					deliveryFee.attr('data-original-title', response.deliveryFee)
						.tooltip('fixTitle')
						.tooltip('show');
					deliveryFee.addClass("errorinput");
					deliveryFee.html(response.updatedDeliveryFee)
					deliveryFee.focus();
				} else {
					deliveryFee.tooltip('hide');
					deliveryFee.removeClass("errorinput");
				}
			}
		});
		loader.remove();
		return false;
	});

	$('#updateCFMOrder').on('click', function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
		$(".new-order-alert").hide();

		var commonModal = $('#modal');
		var modalErrorMsg = commonModal.find('.error')
		var bookingDate = $('#bookingDate');
		var servings = $('#servings');
		var collectionMode = $('#collectionMode');
		var paymentType = $('#paymentType');
		var cookForMeCost = $('#cookForMeCost');
		var deliveryFee = $('#deliveryFee');
		jsRoutes.controllers.cookForMe.EditCookForMeOrder.updateRecipeOrder().ajax({
			dataType: "json",
			data : {"cookForMeId":$('#cookForMeId').val(), "bookingDate":bookingDate.val(),
				"servings":servings.val(), "collectionMode":collectionMode.val(),
				"paymentType":paymentType.val(), "cookForMeCost":cookForMeCost.text(),
				"deliveryFee":deliveryFee.text(), "remarks":$('#remarks').val()},
			success : function(data) {
				commonModal.find('.modal-content').html(data.result);
				commonModal.on('hidden.bs.modal', function () {
					location.reload();
				})
			},
			error : function(err) {
				response = jQuery.parseJSON(err.responseText);
				if (response.flash != null) {
					$(".new-order-alert").html(response.flash);
					$(".new-order-alert").show();
				}
				if (response.bookingDate != null) {
					bookingDate.attr('data-original-title', response.bookingDate)
						.tooltip('fixTitle')
						.tooltip('show');
					bookingDate.addClass("errorinput");
					bookingDate.focus();
				} else {
					bookingDate.tooltip('hide');
					bookingDate.removeClass("errorinput");
				}

				if (response.servings != null) {
					servings.attr('data-original-title', response.servings)
						.tooltip('fixTitle')
						.tooltip('show');
					servings.addClass("errorinput");
					servings.focus();
				} else {
					servings.tooltip('hide');
					servings.removeClass("errorinput");
				}

				if (response.collectionMode != null) {
					collectionMode.attr('data-original-title', response.collectionMode)
						.tooltip('fixTitle')
						.tooltip('show');
					collectionMode.addClass("errorinput");
					collectionMode.focus();
				} else {
					collectionMode.tooltip('hide');
					collectionMode.removeClass("errorinput");
				}

				if (response.paymentType != null) {
					paymentType.attr('data-original-title', response.paymentType)
						.tooltip('fixTitle')
						.tooltip('show');
					paymentType.addClass("errorinput");
					paymentType.focus();
				} else {
					paymentType.tooltip('hide');
					paymentType.removeClass("errorinput");
				}

				if (response.cookForMeCost != null) {
					cookForMeCost.attr('data-original-title', response.cookForMeCost)
						.tooltip('fixTitle')
						.tooltip('show');
					cookForMeCost.addClass("errorinput");
					cookForMeCost.html(response.updatedCookForMeCost)
					cookForMeCost.focus();
				} else {
					cookForMeCost.tooltip('hide');
					cookForMeCost.removeClass("errorinput");
				}

				if (response.deliveryFee != null) {
					deliveryFee.attr('data-original-title', response.deliveryFee)
						.tooltip('fixTitle')
						.tooltip('show');
					deliveryFee.addClass("errorinput");
					deliveryFee.html(response.updatedDeliveryFee)
					deliveryFee.focus();
				} else {
					deliveryFee.tooltip('hide');
					deliveryFee.removeClass("errorinput");
				}

				if (response.result != null) {
					modalErrorMsg.html(response.result);
					modalErrorMsg.focus();
				} else {
					modalErrorMsg.html("");
				}
				updateDirectionFeeContainer();
			}
		});
		loader.remove();
		return false;
	});
});

function updateDirectionFeeContainer() {
	var collectionMode = $("#collectionMode");
	if(collectionMode.length > 0) {
		var selectedCollectionMode = $("#collectionMode").find(":selected").val();
		var deliveryFeeContainer = $("#deliveryFee").closest(".form-input-group");
		if (deliveryFeeContainer.length > 0) {
			if (selectedCollectionMode == $("#deliveryCollectionMode").val()) {
				deliveryFeeContainer.show();
				$("#totalCookForMeCost").html(($("#individualCookForMeCost").val() * $("#servings").val()) + parseInt($("#deliveryFee").text(), 10))
			} else {
				deliveryFeeContainer.hide();
				$("#totalCookForMeCost").html($("#individualCookForMeCost").val() * $("#servings").val())
			}
		}
	}
}

function processCookingDate(start, end, label) {
	var startDate = 0, endDate = 0;
	var dateformat = "DDMMYYYY";
	var labelDateformat = "DD MMM YYYY";
	var loadMoreOrderBtn = $('#loadMoreOrder');

	try {
		startDate = start.format(dateformat);
		if (startDate == "Invalid date")
			startDate = 0;
	} catch (ex) {
		startDate = 0;
	}

	try {
		endDate = end.format(dateformat);
		if (endDate == "Invalid date")
			endDate = 0;
	} catch (ex) {
		endDate = 0;
	}

	if (label=="Custom Range") {
		$('#day .ddselected').html(start.format(labelDateformat) + " - " + end.format(labelDateformat));
	} else {
		$('#day .ddselected').html(label);
	}
	$('#day .ddselected').attr('value', startDate + "-" + endDate);
	loadMoreOrderBtn.hide(); //hide load more button

	if($('#ordertype').length > 0 && $('#statusselect').length > 0 && $('#created').length > 0) {
		loader = new ajaxLoader($('html'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
			data: {
				"page": 0,
				"search": $('#ordertype .ddselected').attr('value'),
				"orderStatus": $('#statusselect .ddselected').attr('value'),
				"created": $('#created .ddselected').attr('value'),
				"date": startDate + "-" + endDate,
				"forWhom": $(".rw-row[data-who]").attr("data-who")
			},
			success: function (data) {
				if (data != null && data.trim() != "")
					loadMoreOrderBtn.show(); //show load more button

				$("#orderRow").html(data); //append data received from server
				$(".rating").rating();
			},
			error: function (err) {
				//alert(err.responseText);
			}
		});
		loader.remove();
	}
	return false;
}

function initCreatedDate(start, end, label) {
	var startDate = 0, endDate = 0;
	var dateformat = "DDMMYYYY";
	var labelDateformat = "DD MMM YYYY";

	try {
		startDate = start.format(dateformat);
		if (startDate == "Invalid date")
			startDate = 0;
	} catch (ex) {
		startDate = 0;
	}

	try {
		endDate = end.format(dateformat);
		if (endDate == "Invalid date")
			endDate = 0;
	} catch (ex) {
		endDate = 0;
	}

	if (label=="Custom Range") {
		$('#created .ddselected').html(start.format(labelDateformat) + " - " + end.format(labelDateformat));
	} else {
		$('#created .ddselected').html(label);
	}
	$('#created .ddselected').attr('value', startDate + "-" + endDate);
}

function processCreatedDate(start, end, label) {
	var startDate = 0, endDate = 0;
	var dateformat = "DDMMYYYY";
	var labelDateformat = "DD MMM YYYY";
	var loadMoreOrderBtn = $('#loadMoreOrder');

	try {
		startDate = start.format(dateformat);
		if (startDate == "Invalid date")
			startDate = 0;
	} catch (ex) {
		startDate = 0;
	}

	try {
		endDate = end.format(dateformat);
		if (endDate == "Invalid date")
			endDate = 0;
	} catch (ex) {
		endDate = 0;
	}

	if (label=="Custom Range") {
		$('#created .ddselected').html(start.format(labelDateformat) + " - " + end.format(labelDateformat));
	} else {
		$('#created .ddselected').html(label);
	}
	$('#created .ddselected').attr('value', startDate + "-" + endDate);
	loadMoreOrderBtn.hide(); //hide load more button

	if($('#ordertype').length > 0 && $('#statusselect').length > 0 && $('#day').length > 0) {
		loader = new ajaxLoader($('html'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		jsRoutes.controllers.cookForMe.ViewCookForMeOrder.getNextOrderList().ajax({
			data: {
				"page": 0,
				"search": $('#ordertype .ddselected').attr('value'),
				"orderStatus": $('#statusselect .ddselected').attr('value'),
				"date": $('#day .ddselected').attr('value'),
				"created": startDate + "-" + endDate,
				"forWhom": $(".rw-row[data-who]").attr("data-who")
			},
			success: function (data) {
				if (data != null && data.trim() != "")
					loadMoreOrderBtn.show(); //show load more button

				$("#orderRow").html(data); //append data received from server
				$(".rating").rating();
			},
			error: function (err) {
				//alert(err.responseText);
			}
		});
		loader.remove();
	}
	return false;
}

