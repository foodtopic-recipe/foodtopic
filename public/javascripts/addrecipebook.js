$(document).ready(function() {
    var recipeBookList = $("#recipeBookList");
	recipeBookList.chosen({
		disable_search_threshold: 10
   	}).on('change', function(evt, params) {
		recipeBookList.val($(this).val())
 	});

    var saveFavBtn = $("#saveFavBtn");
    saveFavBtn.on('click', function() {
        jsRoutes.controllers.bookRecipe.EditBookRecipe.addFavourites().ajax({

        data : {"recipeId" : saveFavBtn.data('recipe'), "recipeBookIds" : recipeBookList.serialize()},
            success : function(data) {
                $('#modal').modal('hide')
                $('#recipeAddFavAction').load(location.href + " #addFav > *");
                $('.recipe-tools').load(location.href + " .recipe-tools > *");
                var recipeData = saveFavBtn.data('recipe');
                $('#recipeFavs').load(location.href + " #recipeFavs > *");
                $('#decc-'+recipeData).load(location.href + " #decc-"+recipeData + " > *");
                $('#thumbnail-controls-'+recipeData).load(location.href + " #thumbnail-controls-"+recipeData + " > *");
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        return false;
    });
});