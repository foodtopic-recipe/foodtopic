$(document).ready(function() {
    //Delete Recipe in the Recipe Book
    $(document).on('click', '.removeBookRecipe', function() {
        loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.bookRecipe.EditBookRecipe.deleteBookRecipe().ajax({
            data: {"bookRecipeId": $(this).attr('id')},
            success : function(data) {
                loader.remove();
                location.reload();
            },
            error : function(err) {
                loader.remove();
                //alert(err.responseText);
            }
        });
        return false;
    });

    //Delete Recipe Book
    $(document).on('click', '#deleteRecipeBookBtn', function() {
        loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.recipeBook.DeleteRecipeBook.deleteRecipeBook().ajax({
            data : {"recipeBookId" : $(this).data("book")},
            success : function(data) {
                loader.remove();
                $('#deleteRecipeBookModal').modal('hide');
                var response = jQuery.parseJSON(data);
                if (response.redirect) {
                    window.location.href = response.redirect;
                }
            },
            error : function(err) {
                loader.remove();
                //alert(err.responseText);
            }
        });
        return false;
    });
});