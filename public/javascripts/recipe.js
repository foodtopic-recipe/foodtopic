$(document).ready(function() {
    renumberIngredients();
    renumberDirections();
    validateCookForMeSection();

    $("#recipeBookList").chosen({
        disable_search_threshold: 10
    }).on('change', function(evt, params) {
        $("#recipeBookList").val($(this).val())
    });

    $("#categoryList").chosen({
        disable_search_threshold: 10
    }).on('change', function(evt, params) {
        $("#categoryList").val($(this).val())
    });

    $("#cookForMe").on('change', function() {
        validateCookForMeSection();
    });

    $(".collectionMode").on('change', function() {
        checkCollectionMode();
    });

    $('#coverPhoto').on('click', function () {
        $(".coverImage").each(function(i) {
            $('#coverImage'+i).attr('src', '@routes.Assets.at("images/recipe-noimage.jpg")');
        });
    }).on('change', function () {
        $(".coverImage").each(function(i) {
            $(this).attr('id', 'coverImage' + i)
        });
        readCoverImageURL(this, $(".coverImage"));
    });

    $('#addIngredient').on('click', function () {
        $('.ingredient-template').each(function() {
            $('input', this).each(function() {
                $(this).attr('value', "");
            })
        })

        var template = $('.ingredient-template')
        template.before('<div class="ingredient">' + template.html() + '</div>')
        renumberIngredients()
    });

    $('#addDirection').on('click', function() {
        $('.direction-template').each(function() {
            $('textarea', this).each(function() {
                $(this).html("");
            })
            $('input', this).each(function() {
                $(this).attr('value', "");
            })
        })

        var template = $('.direction-template')
        template.before('<div class="direction">' + template.html() + '</div>')
        renumberDirections()
    })

    $(document).on('click', ".removeIngredient", function() {
        $(this).parents('.ingredient').remove()
        renumberIngredients()
    });

    $(document).on('click', '.removeDirection', function() {
        $(this).parents('.direction').remove()
        renumberDirections()
    });

    $("#tags").tagit({
        allowDuplicates: false,
        allowSpaces: true,
        removeConfirmation: true,
        autocomplete: {
            delay: 0,
            minLength: 2,
            source : function(request, response) {
                $.getJSON("/tagsAutoComplete?input="+extractLast(request.term), {
                    term: extractLast(request.term)
                }, response);
            },
            search : function() {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            }
        }
    })
});

function readCoverImageURL(input, output) {
    output.each(function(i) {
        if (input.files && input.files[i]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#coverImage'+i).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[i]);
        }
    });
}

function renumberIngredients() {
    $('.ingredient').each(function(i) {
        $('input', this).each(function() {
            var ingredientName = $(this).attr('name');
            if(ingredientName.search(/recipeIngredientList\[/g) >= 0)
                $(this).attr('name', ingredientName.replace(/recipeIngredientList\[.+?\]/g, 'recipeIngredientList[' + i + ']'))
            else
                $(this).attr('name', ingredientName.replace(/recipeIngredientList/g, 'recipeIngredientList[' + i + ']'))
            
            if(ingredientName == 'recipeIngredientList[' + i + '].ingredientName') {
                $(this).bind("keydown", function(event) {
                    if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                }).autocomplete({
                    source : function(request, response) {
                        $.getJSON("/ingredientNameAutoComplete?input="+extractLast(request.term), {
                            term: extractLast(request.term)
                        }, response);
                    },
                    search : function() {
                        // custom minLength
                        var term = extractLast(this.value);
                        if (term.length < 1) {
                            return false;
                        }
                    },
                    focus : function() {
                        // prevent value inserted on focus
                        return false;
                    },
                    select : function(event, ui) {
                        var terms = split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        this.value = terms;
                        return false;
                    }
                });
            }
            
            if(ingredientName == 'recipeIngredientList[' + i + '].ingredientQuantity') {
                $(this).bind("keydown", function(event) {
                    if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                }).autocomplete({
                    source : function(request, response) {
                        $.getJSON("/ingredientQuantityAutoComplete?input="+extractLast(request.term), {
                            term: extractLast(request.term)
                        }, response);
                    },
                    search : function() {
                        // custom minLength
                        var term = extractLast(this.value);
                        if (term.length < 1) {
                            return false;
                        }
                    },
                    focus : function() {
                        // prevent value inserted on focus
                        return false;
                    },
                    select : function(event, ui) {
                        var terms = split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        this.value = terms;
                        return false;
                    }
                });
            }
            
            if(ingredientName == 'recipeIngredientList[' + i + '].ingredientUnit') {
                $(this).bind("keydown", function(event) {
                    if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                }).autocomplete({
                    source : function(request, response) {
                        $.getJSON("/ingredientUnitAutoComplete?input="+extractLast(request.term), {
                            term: extractLast(request.term)
                        }, response);
                    },
                    search : function() {
                        // custom minLength
                        var term = extractLast(this.value);
                        if (term.length < 1) {
                            return false;
                        }
                    },
                    focus : function() {
                        // prevent value inserted on focus
                        return false;
                    },
                    select : function(event, ui) {
                        var terms = split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        this.value = terms;
                        return false;
                    }
                });
            }
        })
        
        $('.ingredientNumber', this).each(function() {
            $(this).attr('id', 'ingredientTitle' + i)
            $('#ingredientTitle'+i).text((i + 1))
        })
    })
}

function renumberDirections() {
    $('.direction').each(function(i) {
        $('textarea', this).each(function() {
            var maxLength = parseInt($(this).attr('maxlength'));
            $(this).closest('.form-input-group').find('.descriptionCounter').text('(Characters left: ' + (maxLength - $(this).val().length) + ')');

            var directionName = $(this).attr('name');
            if(directionName.search(/recipeDirectionList\[/g) >= 0)
                $(this).attr('name', directionName.replace(/recipeDirectionList\[.+?\]/g, 'recipeDirectionList[' + i + ']'))
            else 
                $(this).attr('name', directionName.replace(/recipeDirectionList/g, 'recipeDirectionList[' + i + ']'))

            $(this).keyup(function() {
                $(this).closest('.form-input-group').find('.descriptionCounter').text('(Characters left: ' + (maxLength - $(this).val().length) + ')');
            });
        })
        $('input', this).each(function() {
            var directionName = $(this).attr('name');
            if(directionName.search(/recipeDirectionList\[/g) >= 0)
                $(this).attr('name', directionName.replace(/recipeDirectionList\[.+?\]/g, 'recipeDirectionList[' + i + ']'))
            else 
                $(this).attr('name', directionName.replace(/recipeDirectionList/g, 'recipeDirectionList[' + i + ']'))
        })
        
        $('.directionNumber', this).each(function() {
            $(this).attr('id', 'directionTitle' + i)
            $('#directionTitle'+i).text((i + 1))
        })
    })
    
    $(".directionImage").each(function(i) {
        $(this).attr('id', 'directionImage' + i)
    });
    
    $(".imagePathClass").each(function(i) {
        $(this).change(function() {
            readDirectionsURL(this, i);
        })
    });
}

function readDirectionsURL(input, i) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#directionImage'+i).attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

function validateCookForMeSection() {
    if($("#cookForMe").is(':checked')) {
        $("#cookForMeContainer").show();
    } else {
        $("#cookForMeContainer").hide();
    }
}

function checkCollectionMode() {
    $.each($('.collectionMode'), function (i, checkbox) {
        if ($(checkbox).is(':checked')) {
            if ($(checkbox).data("profile-required")) {
                if (!$('#modal').is(':visible')) {
                    jsRoutes.controllers.account.UpdateProfile.loadProfileForCFMModal().ajax({
                        success : function(data) {
                            if(data != null && data != "") {
                                $(".modal-content").html(data); //append data received from server
                                $("#updateProfileModal").trigger("click");
                            }
                        }
                    });
                    return false;
                }
            }
        }
    });
}