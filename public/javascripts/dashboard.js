$(document).ready(function() {
    $('#homeNav').addClass('active');

    $(document).on('click', '.likeRecipe', function() {
        loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

        jsRoutes.controllers.Like.recipeLike().ajax({
            data : {"recipeId" : $(this).data('recipe')},
            success : function(data) {
                var response = jQuery.parseJSON(data);
                $('#decc-'+response.recipeId).load(location.href + " #decc-"+response.recipeId+" > *");
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });

    var dashboardClickCount = 1;
    var loadMoreDashboardBtn = $('#loadMoreDashboard');
    loadMoreDashboardBtn.on('click', function() {
        loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
        loadMoreDashboardBtn.hide(); //hide load more button

        jsRoutes.controllers.Dashboard.getNextDashboardList().ajax({
            data : {"page" : dashboardClickCount},
            success : function(data) {
                if(data != null && data.trim() != "")
                    loadMoreDashboardBtn.show(); //show load more button

                $("#dashboardRow").append(data); //append data received from server
                dashboardClickCount++;
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });
});