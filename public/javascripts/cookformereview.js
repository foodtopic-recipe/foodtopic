$(document).ready(function() {
	$(".rating").rating();

	var commentTextArea = $("#comment");
	if(commentTextArea.length > 0) {
		var maxLength = parseInt(commentTextArea.attr('maxlength'));
		commentTextArea.closest('.form-input-group').find('.descriptionCounter').text('(Characters left: ' + (maxLength - commentTextArea.val().length) + ')');

		commentTextArea.keyup(function () {
			commentTextArea.closest('.form-input-group').find('.descriptionCounter').text('(Characters left: ' + (maxLength - commentTextArea.val().length) + ')');
		});
	}

	$('#reviewOrder').on('click', function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		var commonModal = $('#modal');
		var modalErrorMsg = commonModal.find('.edit-review-alert')
		var ratingContainer = $('.rating-container');
		var reviewScore = $('#reviewScore');
		var comment = $('#comment');
		jsRoutes.controllers.cookForMeReview.CreateCookForMeReview.postCookForMeReview().ajax({
			dataType: "json",
			data : {"orderId":$('#orderId').val(), "reviewScore":reviewScore.val(), "comment":comment.val()},
			success : function(data) {
				commonModal.find('.modal-content').html(data.result);
				commonModal.on('hidden.bs.modal', function () {
					var day = $('#day');
					if(day.length > 0) {
						processCookingDate(day.daterangepicker.start, day.daterangepicker.end, $('#day .ddselected').text());
					}
				})
			},
			error : function(err) {
				response = jQuery.parseJSON(err.responseText);
				if (response.reviewScore != null) {
					ratingContainer.attr('data-original-title', response.reviewScore)
						.tooltip('fixTitle')
						.tooltip('show');
					ratingContainer.addClass("errorinput");
					ratingContainer.focus();
				} else {
					ratingContainer.tooltip('hide');
					ratingContainer.removeClass("errorinput");
				}

				if (response.comment != null) {
					comment.attr('data-original-title', response.comment)
						.tooltip('fixTitle')
						.tooltip('show');
					comment.addClass("errorinput");
					comment.focus();
				} else {
					comment.tooltip('hide');
					comment.removeClass("errorinput");
				}

				if (response.result != null) {
					modalErrorMsg.html(response.result);
					modalErrorMsg.focus();
				} else {
					modalErrorMsg.html("");
				}
			}
		});
		loader.remove();
		return false;
	});

	var updateCookForMeReviewBtn = $('#updateCookForMeReviewBtn');
	updateCookForMeReviewBtn.on('click', function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		var commonModal = $('#modal');
		jsRoutes.controllers.cookForMeReview.EditCookForMeReview.updateCookForMeReview().ajax({
			dataType: "json",
			data : {"reviewId" : updateCookForMeReviewBtn.data('review'), "rating" : $('#editRating').val(), "comment" : $('#comment').val()},
			success : function(data) {
				updateCookForMeReviewBtn.hide();
				commonModal.find('.modal-content').html(data.result);
				commonModal.on('hidden.bs.modal', function () {
					var day = $('#day');
					if(day.length > 0) {
						processCookingDate(day.daterangepicker.start, day.daterangepicker.end, $('#day .ddselected').text());
					}
				})
			},
			error : function(err) {
				deleteCookForMeReviewBtn.hide();
				commonModal.find('.control-label').html(err);
			}
		});
		loader.remove();
		return false;
	});

	var deleteCookForMeReviewBtn = $('#deleteCookForMeReviewBtn');
	deleteCookForMeReviewBtn.on('click', function() {
		loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

		var commonModal = $('#modal');
		jsRoutes.controllers.cookForMeReview.DeleteCookForMeReview.deleteCookForMeReview().ajax({
			dataType: "json",
			data : {"reviewId" : deleteCookForMeReviewBtn.data('review')},
			success : function(data) {
				deleteCookForMeReviewBtn.hide();
				commonModal.find('.modal-content').html(data.result);
				commonModal.on('hidden.bs.modal', function () {
					var day = $('#day');
					if(day.length > 0) {
						processCookingDate(day.daterangepicker.start, day.daterangepicker.end, $('#day .ddselected').text());
					}
				})
			},
			error : function(err) {
				deleteCookForMeReviewBtn.hide();
				commonModal.find('.control-label').html(err);
			}
		});
		loader.remove();
		return false;
	});
});