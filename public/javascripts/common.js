$(document).ready(function() {
    setTimeout(function() {
        $( ".alert" ).fadeOut( "slow");
    },3000)

    setInterval(function() {
        $('#followingBadge').load(location.href + " #followingCount");
        $('#followerBadge').load(location.href + " #followerCount");
        $('#notificationBadge').load(location.href + " #notificationCount");
        $('#notification-dropdown').load(location.href + " #notification-dropdown > *");
    }, 30000);

    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });

    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });

    $("#searchInput")
        // don't navigate away from the field on tab when selecting an item
        .bind("keydown", function(event) {
            if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        }).autocomplete({
        source : function(request, response) {
            $.getJSON("/searchAutoComplete?input="+extractLast(request.term), {
                term: extractLast(request.term)
            }, response);
        },
        search : function() {
            // custom minLength
            var term = extractLast(this.value);
            if (term.length < 1) {
                return false;
            }
        },
        focus : function() {
            // prevent value inserted on focus
            return false;
        },
        select : function(event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
});

function scrollNav() {
	$('.nav a').on("click", function() {
		//Toggle Class
        $('.nav .active').removeClass('active');
        $(this).closest('li').addClass("active");
		var theClass = $(this).attr("class");
		$('.'+theClass).parent('li').addClass('active');

        //Animate
        if($(this).attr('href') != "/") {
            $('html, body').stop().animate({
                scrollTop: $($(this).attr('href').substring(1)).offset().top
            }, 400);
        }

        return false;
	});

	$('.scrollTop a').scrollTop();
}

function split(val) {
	return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

function ajaxLoader (el, options) {
    // Becomes this.options
    var defaults = {
        bgColor 		: '#fff',
        duration		: 1000,
        opacity			: 0.7,
        classOveride 	: false
    }
    this.options 	= jQuery.extend(defaults, options);
    this.container 	= $(el);
    
    this.init = function() {
        var container = this.container;
        // Delete any other loaders
        this.remove(); 
        // Create the overlay 
        var overlay = $('<div></div>').css({
            'background-color': this.options.bgColor,
            'opacity':this.options.opacity,
            'width':container.width(),
            'height':container.height(),
            'position':'absolute',
            'top':'0px',
            'left':'0px',
            'z-index':99999
        }).addClass('ajax_overlay');
        // add an overiding class name to set new loader style 
        if (this.options.classOveride) {
            overlay.addClass(this.options.classOveride);
        }
        // insert overlay and loader into DOM 
        container.append(
            overlay.append($('<div></div>').addClass('ajax_loader')).fadeIn(this.options.duration)
        );
    };
    
    this.remove = function() {
        var overlay = this.container.children(".ajax_overlay");
        if (overlay.length) {
            overlay.fadeOut(this.options.classOveride, function() {
                overlay.remove();
            });
        }	
    }
    this.init();
}

function readNotification() {
    jsRoutes.controllers.notification.ViewNotification.readAllNotification().ajax({
    	dataType: "json",
        success : function(data) {
            $('#notificationBadge').load(location.href + " #notificationCount");
            $(".notification").removeAttr("active");
        },
        error : function(err) {
            //alert(err.responseText);
        }
    });
    return false;
};

function submitLogin() {
    //loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
	$(".signin-alert").hide();
	
    jsRoutes.controllers.account.Login.authenticate().ajax({
    	dataType: "json",
        data : {"email":$('#loginEmail').val(), "password":$('#loginPassword').val(), "ajax":"1"},
        success : function(data) {
            window.location.href = "/dashboard";
        },
        error : function(err) {
        	response = jQuery.parseJSON(err.responseText);
        	if (response.flash != null) {
        		$(".signin-alert").html(response.flash);
        		$(".signin-alert").show();
        	}
        	if (response.email != null) {
        		 $('#loginEmail').attr('data-original-title', response.email)
                 .tooltip('fixTitle')
                 .tooltip('show');
        		 $('#loginEmail').addClass("errorinput");
        	} else {
        		$('#loginEmail').tooltip('hide');
        		$('#loginEmail').removeClass("errorinput");
        	}
        	
        	if (response.password != null) {
        		$('#loginPassword').attr('data-original-title', response.password)
                .tooltip('fixTitle')
                .tooltip('show');
        		$('#loginPassword').addClass("errorinput");
        	} else {
        		$('#loginPassword').tooltip('hide');
        		$('#loginPassword').removeClass("errorinput");
            }
        }
    });
    return false;
};

function changePassword() {
    $(".changePassword-alert").hide();

    jsRoutes.controllers.account.Password.changePassword().ajax({
        dataType: "json",
        data : {"currentPassword" : $("#currentPassword").val(), "newPassword" : $("#newPassword").val(), "confirmPassword" : $("#confirmPassword").val()},
        success : function(data) {
            window.location.href = "/profile";
        },
        error : function(err) {
            response = jQuery.parseJSON(err.responseText);
            if (response.flash != null) {
                $(".changePassword-alert").html(response.flash);
                $(".changePassword-alert").show();
            }

            if (response.currentPassword !=null) {
                $('#currentPassword').attr('data-original-title', response.currentPassword)
                .tooltip('fixTitle')
                .tooltip('show');
                $('#currentPassword').addClass("errorinput");
                
            } else {
                $('#currentPassword').tooltip('hide');
                $('#currentPassword').removeClass("errorinput");
            }
            
            if (response.newPassword !=null) {
                $('#newPassword').attr('data-original-title', response.newPassword)
                .tooltip('fixTitle')
                .tooltip('show');
                $('#newPassword').addClass("errorinput");
            } else {
                $('#newPassword').tooltip('hide');
                $('#newPassword').removeClass("errorinput");
            }
            
            if (response.confirmPassword !=null) {
                $('#confirmPassword').attr('data-original-title', response.confirmPassword)
                .tooltip('fixTitle')
                .tooltip('show');
                $('#confirmPassword').addClass("errorinput");
            } else {
                $('#confirmPassword').tooltip('hide');
                $('#confirmPassword').removeClass("errorinput");
            }
        }
    });
    return false;
}

function getMultiScripts(arr, path) {
    var _arr = $.map(arr, function(scr) {
        return $.getScript( (path||"") + scr );
    });

    _arr.push($.Deferred(function( deferred ){
        $( deferred.resolve );
    }));

    return $.when.apply($, _arr);
}