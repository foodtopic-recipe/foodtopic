$(document).ready(function() {
    var loader;
    var clickCount = 1;
    $('.lownavbar .nav li a').each( function() {
         if ($(this).html() == "@title") {
            $(this).parent().addClass("active");
        }
    })

    var loadMoreBtn = $('#loadMoreRecipe');
    loadMoreBtn.on('click', function() {
        loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});
        loadMoreBtn.hide(); //hide load more button

        jsRoutes.controllers.recipe.ViewRecipe.getNextRecipeList().ajax({
            data : {"page":clickCount, "recipeId":loadMoreBtn.data("similar"), "search":$("#listRecipeTitle").text(), "categoryName":"", "forWhom":loadMoreBtn.data("who")},
            success : function(data) {
                if(data != null && data != "")
                    loadMoreBtn.show(); //show load more button

                $("#recipeRow").append(data); //append data received from server
                clickCount++;
            },
            error : function(err) {
                //alert(err.responseText);
            }
        });
        loader.remove();
        return false;
    });

    $(document).on('click', '.thumbnail-like', function() {
        var id = $(this).attr('id');
        if(id != undefined && id != null) {
            loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

            jsRoutes.controllers.Like.recipeLike().ajax({
                data: {"recipeId": $(this).attr('id')},
                success: function (data) {
                    var response = jQuery.parseJSON(data);
                    $('#thumbnail-controls-' + response.recipeId).load(location.href + " #thumbnail-controls-" + response.recipeId + " > *");
                },
                error: function (err) {
                    //alert(err.responseText);
                }
            });
            loader.remove();
        }
        return false;
    });
});