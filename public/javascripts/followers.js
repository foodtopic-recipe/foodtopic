$(document).ready(function() {
    $('.followBtn').on('click', function () {
        addFollowerFunction($(this).data('profile'));
        return false;
    });

    $('.unfollowBtn').on('click', function () {
        removeFollowerFunction($(this).data('profile'));
        return false;
    });
});

function addFollowerFunction(input) {
    var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

    jsRoutes.controllers.follower.EditFollower.followUser().ajax({
        data : {"userId": input},
        success : function(data) {
            $('#profileFollowAction-'+input).load(location.href + " #profileFollowAction-"+input+" > *");

            $('.follow-block').load(location.href + " .follow-block > *");

            $('#following-block').load(location.href + " .follow-col");
            $('#profileFollowAction').load(location.href + " .unfollowBtn");

            $('#followingBadge').load(location.href + " #followingCount");
            $('#followerBadge').load(location.href + " #followerCount");
        },
        error : function(err) {
            //alert(err.responseText);
        }
    });
    loader.remove();
}

function removeFollowerFunction(input) {
    var loader = new ajaxLoader($('body'), {classOveride: 'blue-loader', bgColor: '#000', opacity: '0.3'});

    jsRoutes.controllers.follower.EditFollower.unfollowUser().ajax({
        data : {"userId": input},
        success : function(data) {
            $('#profileFollowAction-'+input).load(location.href + " #profileFollowAction-"+input+" > *");

            $('.follow-block').load(location.href + " .follow-block > *");

            $('#following-block').load(location.href + " .follow-col");
            $('#profileFollowAction').load(location.href + " .followBtn");

            $('#followingBadge').load(location.href + " #followingCount");
            $('#followerBadge').load(location.href + " #followerCount");
        },
        error : function(err) {
            //alert(err.responseText);
        }
    });
    loader.remove();
}