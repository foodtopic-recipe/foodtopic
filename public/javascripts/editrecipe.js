$(document).ready(function() {
    $(".ingredient").each(function(i) {
        $("input", this).each(function() {
            $(this).change(function() {
                $("#editRecipeForm").data("changed",true);
            });
        });
    });

    $(".direction").each(function(i) {
        $("textarea", this).each(function() {
            $(this).change(function() {
                $("#editRecipeForm").data("changed",true);
            });
        });
    });

    $("#updateBtn").on("click", function() {
        var editForm = $("#editRecipeForm");
        if (editForm.data("changed")) {
            if (editForm.data("acknowledged")) {
                return true;
            } else {
                jsRoutes.controllers.recipe.EditRecipe.initAcknowledgementModal().ajax({
                    success: function (data) {
                        var commonModal = $("#modal");
                        commonModal.find(".modal-content").html(data);
                        commonModal.modal('show');
                        commonModal.find("#acknowledgeBtn").on("click", function () {
                            commonModal.modal("hide");
                            editForm.data("acknowledged",true);
                            $("#updateBtn").click();
                        });
                    }
                });
            }
        } else {
            return true;
        }
        return false;
    })
});

