title = FoodTopic
seoDescription = FoodTopic is the one stop social platform that engage you and your friends or anyone across the web to come up with new creative variations of recipes.
seoKeywords = recipe, reinvent recipes, recipes search, creative recipes, recipes sharing, friends recipes, recipe DIY, cook for me, order recipes
seoProfileKeywords = recipe, recipe sites, food profile, profile search, recipe profile, friends recipes, friends profile, friends cooking
seoRobots = INDEX,FOLLOW
seoRobotsNoIndexFollow = NOINDEX,FOLLOW
seoRobotsIndexNoFollow = INDEX,NOFOLLOW
seoRobotsNoIndexNoFollow = NOINDEX,NOFOLLOW

#===============================
# COMMON MESSAGES
#===============================
add = Add
edit = Edit
create = Create
update = Update
delete = Delete
submit = Submit
accept = Accept
reject = Reject
cancel = Cancel
complete = Complete
back = Back
close = Close
reset = Reset
upload = Upload
publish = Publish
draft = Save Draft
post = Post
save = Save
like = Like
view = View
author = Author
acknowledgement = acknowledgement
loadMore = Load More
dont.rate = Dont Rate
rate = Rate
request.date = Date Order Placed

monetization.code = Monetization
monetization.free = Free
monetization.info.free = Setting the monetization option to ''Free'' is permanent. You cannot change it back to ''Paid'' again after publishing.
monetization.paid = Paid
monetization.bundle = Bundled

privacy.code = Privacy
privacy.public = Public
privacy.private = Private

tags.code = Tags
tag.description = Enter tags separated by comma

search.result = Search Result

#===============================
# COMMON VALIDATION MESSAGES
#===============================
validate.required = {0} is required
invalid.code = {0} is Invalid
validate.not.numeric = Require numeric value
validate.not.date = Date requires dd/MM/yyyy format
invalid.rights = Invalid Rights
invalid.reinvent = Invalid Recipe Reinvention
photo.maxsize = Photo Filesize must be lesser than 500KB
tags.not.alphabets.with.comma = Please enter only alphabets and separate tags using commas
error.technical = Encountered technical error, please try again.
error.404 = Page Not Available
error.404.desc = the page you were looking for does not exist.
category.not.found = Category Not Found
category.not.found.desc = we couldn''t find the category ''{0}'' you were looking for.

#===============================
# LOGIN & ACCOUNT MESSAGES
#===============================
email = Email
profile.email.required = Please enter your email in order for us to contact you
password = Password
validate = Validate
confirm = Confirmation
confirm.password = Confirm Password
current.password = Current Password
new.password = New Password

#Menu
settings = Settings
account.settings.password = Click OK to receive instructions by mail.
account.settings.email = Enter your new email address. Click OK to receive instructions on this new mail.

logout = Sign Out
logout.success = You''ve been successfully logged out.

#Profile
profile = Profile
profiles = Profiles
profile.edit = Edit Profile
profile.update = Update Profile
profile.verified = Verified
profile.unverified = Unverified - Click To Verify
profile.picture = Profile Picture
profile.aboutMe = About Me
profile.firstName = First Name
profile.firstName.required = Please enter your first name for us to address you
profile.lastName = Last Name
profile.lastName.required = Please enter your last name for us to address you
profile.joinedDate = Member Since
profile.gender = Gender
profile.DOB = Date of Birth
profile.contactNumber = Contact Number
profile.contactNumber.short = Contact No.
profile.reputation = Reputation
profile.follow = Follow
profile.followers = Followers
profile.following = Following
profile.country = Country
profile.awards = Awards
profile.address = Address
profile.addressLine1 = Address Line 1
profile.addressLine2 = Address Line 2
profile.addressLine3 = Address Line 3
profile.countryCode = Country
profile.postalCode = Postal Code

profile.providers_many = There are {0} providers linked with your account:
profile.providers_one = There is one provider linked with your account:
profile.logged = You are currently logged in with:
profile.session = Your user ID is {0} and your session will expire on {1}
profile.session_endless = Your user ID is {0} and your session will not expire, as it is endless
profile.password_change = Change/set a password for your account
profile.update.required = Please update your address & contact details in order to use Cook For Me

profile.update.success = Profile updated successfully.
profile.edit.invalid.rights = You are not authorize to modify or delete this profile

account.not.validated.check.mail = Account not validated. Please check your email.
account.successfully.validated = Account validated successfully
account.notfound = Account Not Found

error.sending.email = Error sending email
error.sending.confirm.email = Error while sending confirmation mail
error.email.already.exist = Email already exist
error.account.already.validated = Account already validated
error.unknown.email = Unknown email
error.confirm = Confirmation failed
error.expiredresetlink = This password reset has expired.
error.expiredmaillink = This mail change has expired.

signin.required = Please signup/login first
signin.required.desc = To better enjoy FoodTopic, please signup or login first.
signin.signin = Sign In
signin.your.email = Your email address
signin.your.password = Your password
signin.oauth = New to {0}? {1} or log in using one of the following providers:
signin.error = The email or password you entered is incorrect.

signup.signup = Sign Up
signup.successful = Sign Up - Successful!
signup.new = New User?
signup.reset.password = Reset your password
signup.generate.password = Generate a password
signup.verify.account.title=E-mail Verification Required
signup.verify.account.requirement=Before you can {0} {1}, you first need to verify your e-mail address.
signup.verify.account=An e-mail has been sent to the registered address. Please follow the embedded link to activate your account.
signup.verify_email.success=E-mail address ({0}) successfully verified.
signup.verify_email.error.already_validated=Your e-mail has already been validated.
signup.verify_email.message.instructions_sent=Instructions on how to verify your e-mail address have been sent to {0}.
signup.verify_email.error.set_email_first=You need to set an e-mail address first.
signup.msg.created = You will receive a confirmation email soon. Check your email to activate your account.
signup.verify_email.sent = An verification e-mail has already been sent. Please check your email or wait approx 15 mins before trying again for a new verification e-mail to be sent.
signup.valid.email = Invalid Email
signup.user.exists=This user already exists.
signup.password.notmatch = New Password do not match with Confirm Password.
signup.password.mustnotmatch = Current Password may not be reuse.
signup.password.verify_signup.subject={0}: Complete your signup
signup.password.verify_email.subject={0}: Confirm your e-mail address

password.forgot = Forgotten your password?
password.change = Change Password
password.change.success = Password has been changed successfully.
password.current.wrong = Wrong current password entered.
password.current.new.same = Old Password Must Be Different From New Password
password.new.confirm.wrong = New & Confirm Password does not match
password.not.changed = Your password has not been changed.
password.reset.no_password_account=  This user has not yet been set up for password usage.
password.reset.success.auto_login = Your password has been reset.
password.reset.success.manual_login = Your password has been reset. Please now log in using your new password.
password.reset.new_password = Please enter your new password.

resetpassword = Reset Password
resetpassword.instructions = To reset your password, enter the email address you use to sign in to FoodTopic. You will receive an email on how to reset your password.
resetpassword.mailsent = Password reset email has been sent, please check your email inbox.
resetpassword.success = Your password has been reset.
resetpassword.reset_email.subject={0}: Password Reset

user.not.found = User Not Found
user.not.found.desc = we couldn''t find the user you were looking for.

changemail.mailsent = A mail has been sent, please check your mail.

mail.confirm.subject = Welcome to FoodTopic - Ask for confirmation
mail.confirm.message = Welcome to FoodTopic. <br> Click on this link : {0} to confirm your email.

mail.welcome.subject = Welcome to FoodTopic
mail.welcome.message = Welcome to FoodTopic. <br> Account validated successfully.

mail.reset.ask.subject = FoodTopic - New password
mail.reset.ask.message = You have requested for your password to be reset.  <br> Click on this link : {0} to change your password.

mail.change.ask.subject = FoodTopic - Validate email
mail.change.ask.message = You have requested for your email to be changed.  <br> Click on this link : {0} to validate your email.

mail.reset.fail.subject = Account Access Attempted
mail.reset.fail.message = You (or someone else) entered this email address when trying to change the password.  However, this email address is not on our list of registered users, and so the attempted password reset has failed.

mail.reset.confirm.subject = FoodTopic - New password
mail.reset.confirm.message = Hello, <br><br> Your password has been reset.


#===============================
# DASHBOARD MESSAGES
#===============================
dashboard.feeds = Feeds
dashboard.updates = Updates
dashboard.like.this = {0} like
dashboard.book.this = {0} added to their books



#===============================
# RECIPE MESSAGES
#===============================
recipe = Recipe
recipe.my = My Recipes
recipe.new = New Recipe
recipe.create.new = Create New Recipe
recipe.privacyCode.invalid = Invalid Privacy Code
recipe.privacy.description = Choose whether to allow or disallow others to see your recipe.
recipe.cover.photo = Cover Photo
recipe.name = Name
recipe.price = Price
recipe.description = Description
recipe.description.description = Tell others what makes you come up with this recipe? Is there any story behind this recipe?
recipe.servings = Servings
recipe.servings.help = serving
recipe.servings.description = How many people does this recipe serve.
recipe.preparation = Preparation
recipe.preparationTime = Preparation Time
recipe.preparationTime.help = minute
recipe.preparationTime.description = How long it takes to do and finish everything (in minutes).
recipe.difficulty = Difficulty Level
recipe.difficulty.description = In your opinion the level of difficulty for people to prepare this recipe.
recipe.difficulty.invalid = Invalid Difficulty Code
recipe.ingredient = Ingredient
recipe.ingredient.description = Include amounts/measurements for each ingredient.
recipe.ingredients = Ingredients
recipe.ingredientName = Name
recipe.ingredientQuantity = Quantity
recipe.ingredientUnit = Unit
recipe.direction = Direction
recipe.direction.description = List steps in the way they should be done in order to successfully make the dish.
recipe.directions = Directions
recipe.directionDescription = Description
recipe.directionDescription.description = Be specific, but keep it short.
recipe.directionImage = Image
recipe.category = Category
recipe.categories = Categories
recipe.category.description = Choose wisely in order to optimize search
recipe.image = Image
recipe.statusCode = Status
recipe.max.100chara = (Maximum characters: 100)
recipe.max.250chara = (Maximum characters: 250)
recipe.edit = Modify
recipe.view.popular = View Popular Recipes
recipe.popular = Popular
recipe.latest = Latest
recipe.featured = Featured
recipe.similar = You may also like..
recipe.view.own.draft = View Draft Recipes
recipe.view.own.published = View Published Recipes
recipe.delete = Delete Recipe
recipe.reinvent = Reinvent
recipe.bookit = Book It
recipe.add.to.book = Add to Book Collection
recipe.book.collection = Book Collection
recipe.favourite = Favourite
recipe.delete.draft.confirm = This recipe will be permanently deleted and cannot be recovered. Are you sure?
recipe.delete.published.confirm = This recipe will be deleted but users who have favourite this recipe will still be able to view. You can however still recover this recipe via edit recipe function.
recipe.creator = Recipe Creator
recipe.original.creator.and.recipe = Original Creator & Recipe
recipe.current.reinventing = You are currently reinventing
recipe.not.found = Recipe Not Found
recipe.not.found.desc = we couldn''t find the recipe you were looking for.
recipe.deleted.status = Recipe Has Been Deleted
recipe.cookForMeOrders.pending = You still have cook for me orders pending. Please clear up the orders before attempting to delete the recipe.

recipe.coverPhoto.exceed = Uploading number of cover photos exceed limit of
recipe.directions.exceed = Please limit the directions to *LIMIT* steps

recipe.create.success = Recipe created successfully
recipe.create.success.draft = Draft saved successfully

recipe.update.success = Recipe updated successfully
recipe.update.success.draft = Draft updated successfully
recipe.update.success.new.version = Recipe updated successfully to new version
recipe.edit.invalid.rights = You are not authorize to modify this recipe.
recipe.edit.save.new.version = We notice that you have updated your ingredients/directions. We will be saving these changes into another version of the recipe. This new recipe version will not carry forward likes, favourites and views your recipe had gained so far.

recipe.delete.success = Recipe deleted successfully
recipe.delete.success.draft = Draft deleted successfully
recipe.delete.invalid.rights = You are not authorize to delete this recipe.

recipe.reinvent.duplicate.user = Unable to reinvent own recipe. Please edit or create a new recipe.
recipe.reinvention.lack.changes = Recipe does not have enough changes to be considered a reinvention
recipe.reinvent.success = Recipe reinvented successfully

error.upload.recipe.images = Recipe created/updated successfully. Error encountered while uploading images


#===============================
# COOK FOR ME MESSAGES
#===============================
cookForMe = Cook For Me
cookForMe.not.found = Sorry, the recipe owner does not offer Cook For Me Service for this recipe.
cookForMe.same.user.found = Sorry, you cannot place an order on your recipe that you have uploaded.
cookForMe.available = Cook For Me - Available
cookForMe.unavailable = Cook For Me - Unavailable
cookForMe.placeOrder = Place Order
cookForMe.placeOrder.fail = Failed to Place New Order
cookForMe.placeOrder.msg = Cook For Me is still a FoodTopic''s Beta Feature. Do note that we are still working on improving this and please read the terms carefully before engaging the Host. Note: Cooking cost are based on the recipe''s servings.
cookForMe.updateOrder = Update Order
cookForMe.updateOrder.fail = Failed to Update Order
cookForMe.description = FoodTopic Feature: Allow registered users of FoodTopic to engage you in cooking this recipe for them.
cookForMe.tooltip = As this is still a Beta Feature. We are still working on improving this. Do read the terms carefully before using this.
cookForMe.collectionMode = Collection Mode
cookForMe.collectionMode.description = Select the type of collection mode you (Host) provides for your Diner.
cookForMe.bookingDate = Booking Date
cookForMe.bookingDate.description = Date & Time you wish to engage the Host for their culinary service. (min {0} days in advance)
cookForMe.bookingDate.tooltip = We have a {0} days cut-off period for order amendment/cancellation if the order has been accepted by the host. Should you wish to make amendment or cancel the order, please try do it at least {0} days before the booking date.
cookForMe.bookingDate.future = Booking Date has to be in the future.
cookForMe.currency = Currency
cookForMe.currency.description = What currency do you wish to receive as payment?
cookForMe.paymentType = Payment Type
cookForMe.paymentType.description = Currently, FoodTopic does NOT act as an payment collection agent and therefore, all payment will be standardize to Cash.
cookForMe.deliveryFee = Delivery Fee
cookForMe.deliveryFee.description = The rate that you intend to charge for delivery service
cookForMe.cookForMeCost = Cooking Cost
cookForMe.cookForMeCost.description = The rate that you intend to charge based on the recipe''s number of servings
cookForMe.tabulatedCost = Total Cost
cookForMe.remarks = Remarks
cookForMe.remarks.description = Any additional remarks to let your Diner know in advance?
cookForMe.updateProfile.msg = Interested in using FoodTopic''s Cook For Me function? We would require you to fill up your contact & address details in order for both diner/host to contact you directly. FoodTopic will attempt to display only your Country & Address Line 1 for other users to better engage you, whereas your full contact & address details only once the order has been accepted by both parties.
cookForMe.same.hour.booking = Booking already made within {0} hour.
cookForMe.same.day.booking = Booking already made on the same day.
cookForMe.see.details = See Cook For Me Details

host = Host
diner = Diner
cookForMeOrder = Cook For Me Order
cookForMeOrders.orders = Orders
cookForMeOrders.my = My Orders
cookForMeOrders.incoming = Hosting Orders
cookForMeOrders.outgoing = Dining Orders
cookForMeOrders.no.diner.records = No {0} dining orders placed.
cookForMeOrders.no.records.start = Start by looking thru <a href="{0}">here</a>
cookForMeOrders.no.host.records = No {0} hosting service orders.
cookForMeOrders.no.records = No order records found.
cookForMeOrder.servings.description = The amount of food servings you (Diner) is requesting from the Host
cookForMeOrder.cookForMeCost.description = The rate that you (Diner) will be charged based on the recipe''s number of servings
cookForMeOrder.cookForMeCost.updated = There is a change in the cost.
cookForMeOrder.deliveryFee.description = The rate that you (Diner) will be charged for delivery service if any
cookForMeOrder.deliveryFee.updated = There is a change in the delivery fee.
cookForMeOrder.remarks.description = Any additional remarks to let your Host know in advance?
cookForMeOrder.order.success.header = Order Placed Successfully
cookForMeOrder.order.success.desc = Placed order successfully. Please wait for Host confirmation.
cookForMeOrder.order.update.success.header = Order Updated Successfully.
cookForMeOrder.order.update.success.desc = Updated order successfully. Please wait for Host re-confirmation.
cookForMeOrder.new.order.email.header = New Cook For Me Order Received!
cookForMeOrder.new.order.email.content = <br><a href="http://www.thefoodtopic.com/{0}">{1}</a> just requested for a Cook For Me <b>{2}</b> order scheduled on the coming <b><a href="http://www.thefoodtopic.com/{3}">{4}</a></b>.<br>Please login to <a href="http://www.thefoodtopic.com">FoodTopic</a> and to accept or reject the order.
cookForMeOrder.update.order.email.header = Updates to Cook For Me Order!
cookForMeOrder.update.order.email.content = <a href="http://www.thefoodtopic.com/{0}">{1}</a> just updated the Cook For Me <b>{2}</a> order scheduled on the coming <b><a href="http://www.thefoodtopic.com/{3}">{4}</a></b>.<br>Please login to FoodTopic and accept or reject the updated order.
cookForMeOrder.accepted.order.email.header = Cook For Me Order Accepted!
cookForMeOrder.accepted.order.email.content = <br>Please note that <a href="http://www.thefoodtopic.com/{0}">{1}</a> had <b>accepted</b> your request for the upcoming <b><a href="http://www.thefoodtopic.com/{2}">{3}</a></b> {4} order for {5} servings.
cookForMeOrder.cancelled.order.email.header = Cancelled Cook For Me Order!
cookForMeOrder.cancelled.order.email.content = <br>Please note that <a href="http://www.thefoodtopic.com/{0}">{1}</a> had <b>cancelled</b> the upcoming <b><a href="http://www.thefoodtopic.com/{2}">{3}</a></b> {4} order for {5} servings.
cookForMeOrder.completed.order.email.header = Cook For Me Order Completed!
cookForMeOrder.completed.order.email.content = <br>Please note that <a href="http://www.thefoodtopic.com/{0}">{1}</a> had <b>updated</b> the <b><a href="http://www.thefoodtopic.com/{2}">{3}</a></b> {4} order for {5} servings to completed{6}
cookForMeOrder.completed.order.review.email.content = and left a review for you.
cookForMeOrder.not.found = Order Not Found.
cookForMeOrder.not.found.desc = we couldn''t find the order you were looking for.
cookForMeOrder.update.invalid.rights = You are not authorize to update this order.
cookForMeOrder.edit.invalid.rights = You are not authorize to modify this order.
cookForMeOrder.edit.duration.past = Unable to further amend your order due to booking date lockdown. Contact the host directly to make the necessary update.
cookForMeOrder.cancel.duration.past = Unable to cancel your order due to booking date lockdown. Contact the host directly to cancel to avoid receiving negative reviews.
cookForMeOrder.accepted = Order accepted.
cookForMeOrder.not.accepted = Order not accepted.
cookForMeOrder.rejected = Order rejected.
cookForMeOrder.cancelled = Order cancelled.
cookForMeOrder.completed = Order completed.
cookForMeOrder.placed.order = You placed an order with
cookForMeOrder.already.accepted = You accepted this order already.
cookForMeOrder.already.rejected = You rejected this order already.
cookForMeOrder.already.cancelled = You cancelled this order already.
cookForMeOrder.already.completed = You completed this order already.
cookForMeOrder.already.expired = This order's booking date had passed. Order will be cancelled automatically.
cookForMeOrder.request.address = Request for address
cookForMeOrder.request.address.wait = Address requested. Please wait for diner to get back.
cookForMeOrder.request.address.before = You have already requested for diner''s address. Please wait for diner to update their address.
cookForMeOrder.address.hidden = Once the order had been cancelled or completed, the address will no longer show to protect our diners.

cookForMeReview.create = Rate this Dining Experience
cookForMeReview.create.desc = Leave a review to rate your {0} on this dining experience.
cookForMeReview.completed.success.desc = Completed order.
cookForMeReview.submitted = Review submitted.
cookForMeReview.review.success.desc = Review submitted successfully.
cookForMeReview.edit = Update your Dining Experience Review
cookForMeReview.edit.desc = Update your review on this dining experience.
cookForMeReview.update.success = Review updated successfully.
cookForMeReview.delete = Delete your Dining Experience Review?
cookForMeReview.delete.success = Review deleted successfully.
cookForMeReview.host.review = Host Review on Diner
cookForMeReview.diner.review = Diner Review on Host

#===============================
# REVIEW MESSAGES
#===============================
review = Review
comment = Comment
reviews = Reviews
review.rating = Rating
review.rating.required = Please select 1 rating
review.comment.placeholder = Write your comments here...
review.create.profile = Posted By:
review.edit = Edit Review
review.delete = Delete Review
review.edit.date = Last Edited On:
review.create.date = First Posted On:

review.not.found = Review Not Found
review.not.found.desc = we couldn''t find the review you were looking for.

review.create.success = Review created successfully
review.recipe.user.duplicate = Each recipe does not accept more than 1 review per user

review.update.success = Review updated successfully
review.edit.invalid.rights = You are not authorize to modify this review.

review.delete.success = Review deleted successfully
review.delete.invalid.rights = You are not authorize to delete this review.
review.delete.confirm = This review will be permanently deleted and cannot be recovered. Are you sure?

#===============================
# RECIPE BOOK MESSAGES
#===============================
recipeBook = Recipe Book
recipeBook.my = My Books
recipeBook.popular = Popular Books
recipeBook.new = New Book
recipeBook.create.new = Create New Book
recipeBook.edit = Edit Book
recipeBook.name = Book Name
recipeBook.privacy.description = Choose whether to allow or disallow others to see your recipe book.
recipeBook.max.25chara = (Maximum characters: 25)

recipeBook.not.found = Book Not Found
recipeBook.not.found.desc = we couldn''t find the book you were looking for.

recipeBook.create.success = Book created successfully
recipeToRecipeBook.add.success = Add recipe to book successfully
start.adding.recipes.to.book = Start adding recipes

recipeBook.update.success = Book updated successfully
recipeBook.edit.invalid.rights = You are not authorize to modify this book.

recipeBook.delete.success = Book deleted successfully
recipeBook.delete.invalid.rights = You are not authorize to delete this book.
recipeBook.delete.confirm = This book will be permanently deleted and cannot be recovered. Are you sure?

recipeBook.view.popular = View Popular Books
recipeBook.view.own.draft = View Draft Books
recipeBook.view.own.published = View Published Books

recipeBook.favourite.info = ''Free'' recipes are only allowed to be favourited in ''Free'' books.
recipeFromRecipeBook.remove.success = Remove recipe from book successfully

no.recipeBook.association = No Recipes favourited.
no.recipeBook.association.desc = couldn''t find any recipes favourited in this book.

#===============================
# EVENT MESSAGES
#===============================
event = Event
event.new = Create Event
event.name = Event Name

event.edit = Update Event
event.create.success = Event created successfully
event.create.success.draft = Draft saved successfully

#===============================
# NOTIFICATION MESSAGES
#===============================
notification.newReview = gave you a review of <b>{0}</b> stars for your recipe: <a href="{1}"><b>{2}</b></a>
notification.newCookForMeReview = gave you a review of <b>{0}</b> stars for your Cook For Me {1} service scheduled on <b>{2}</b>
notification.editReview = updated their review to your recipe: <a href="{0}"><b>{1}</b></a>
notification.editCookForMeReview = updated their review to your Cook For Me {0} service scheduled on <a href="{1}"><b>{2}</b></a>
notification.followed = is now following you.
notification.newComment = commented on
notification.editComment = has just updated their comment on
notification.liked = like your recipe: <a href="{0}"><b>{1}</b></a>
notification.favourites = favourite your recipe: <a href="{0}"><b>{1}</b></a>
notification.newRecipe = just posted a new recipe: <a href="{0}"><b>{1}</b></a>
notification.acceptOrder = just <b>accepted</b> your request for a Cook For Me {0} order scheduled on {1}
notification.newOrder = just <b>requested</b> for a Cook For Me {0} order scheduled on {1}
notification.updateOrder = just <b>updated</b> the Cook For Me {0} order scheduled on {1}
notification.cancelOrder = just <b>cancelled</b> the Cook For Me {0} order scheduled on {1}
notification.expired.cancelOrder = has failed to accept the Cook For Me {0} order before {1} and will now be <b>cancelled</b>
notification.completeOrder = just updated the Cook For Me {0} order scheduled on {1} to <b>Completed</b>
notification.requestAddress = is requesting for your <a href="{0}" data-toggle="modal" data-target="#modal" id="updateProfileModal">address</a> for the Cook For Me {1} order scheduled on {2}
notification.editRecipe = just updated their recipe that you have favourited.
notification.viewAll = View all notifications


#===============================
# FOLLOWER/FOLLOWING MESSAGES
#===============================
following.you.since = Following You Since
follow.back = Follow Back
cannot.follow.yourself = Sorry, you are unable to follow yourself
not.following.user = You are not following this user
already.following = You are already following this user
user.follow.success = Started following..
user.unfollow.success = Unfollowed


#===============================
# LIKES
#===============================
likes.delete.invalid.rights = You are not authorize to update
likes.not.found = Like successfully
likes.remove.success = Unlike successfully


#===============================
# COMMENT VOTING
#===============================
commentFlag.upvote.success = Upvote successfully
upvote.only.once = You have already upvoted
commentFlag.downvote.success = Downvote successfully
downvote.only.once = You have already downvoted


#===============================
# FOOTER MESSAGES
#===============================
email.link = mailto:
support.email = support@thefoodtopic.com

aboutUs = About Us
aboutUs.message1 = FoodTopic is the one stop social platform for food lovers to share their ideas and culinary tips. We aim to drive social sharing of recipes, boost food creativity and provide a safe platform for users to provide "Cook For Me" service. We hope everyone around the globe can come up with more new creative variations of recipes.
aboutUs.message2 = Create recipes and choose whether to share it publicly with the world or just keep it private as your personal recipe hosting diary. Find like minded individuals and improve one another recipes. Follow users to find out new recipes they have cook up. Earn reputation points and get featured. Provide "Cook For Me" service to other users who would like to try your recipes.
aboutUs.caption.header = Here''s what you can do with FoodTopic
aboutUs.caption.title1 = Create recipes and share with the world
aboutUs.caption.quote1 = In FoodTopic, you can create your own recipes and store it or share it with other FoodTopic''s users. You can also find other FoodTopic user''s shared recipes and improve it!
aboutUs.caption.title2 = Follow friends, try and improve on their recipes
aboutUs.caption.quote2 = Follow your friends or featured users on FoodTopic and get latest updates on their recipes. Try out their recipes by preparing by yourself or engaging them to cook for you, leave them a review, favourite it or even choose to reinvent it!
aboutUs.caption.title3 = Earn reputation points and get featured
aboutUs.caption.quote3 = The more likes and reviews your recipes receive, the higher your reputation points. Compete reputation points with other users to get featured. Start sharing recipes now!

aboutUs.team = The FoodTopic Team
aboutUs.bio.title = Co-Founder of FoodTopic

aboutUs.bio1.name = WeiFeng Zhuo 
aboutUs.bio1.photo = http://graph.facebook.com/10154613413950246/picture?width=9999
aboutUs.bio1.desc = Wei Feng is a butt kicking ninja.
aboutUs.bio1.email = weifeng@thefoodtopic.com

aboutUs.bio2.name = ChinLeong Keh
aboutUs.bio2.photo = http://graph.facebook.com/10155217869460246/picture?width=9999
aboutUs.bio2.desc = A.K.A Agent K.
aboutUs.bio2.email = chinleong@thefoodtopic.com

faq = Frequently Asked Questions
faq.account = Account
faq.account.qn1 = Why do i need a account?
faq.account.ans1 = You need an account in order to upload recipes, give reviews etc. As these actions impact others one way or another, we require a account for others to verify your credibility. 
faq.account.qn2 = Can i change the email address i signup using?
faq.account.ans2 = We currently are unable to change the email address you use on FoodTopic.
faq.profile = Profile
faq.profile.qn1 = Can i edit the profile?
faq.profile.ans1 = Yes, you can click on your own profile and you will be able to see the edit button.
faq.recipe = Recipe
faq.recipe.qn1 = How do review works?
faq.recipe.ans1 = Review are written to evaluate how well the recipe is made. Anyone with a verifed FoodTopic account can leave a review with/without comments. 
faq.recipe.qn2 = How do reputation works?
faq.recipe.ans2 = Reputation are calculated by the number of recipes you uploaded, reviews, likes and favourites given by other verified members of the FoodTopic community.
faq.recipe.qn3 = What is the use of reputation?
faq.recipe.ans3 = We have many plans for FoodTopic and reputation is use in many ways for our plans. Currently, the use of reputation is use show members of the FoodTopic community your level of contribution.

advertising = Advertising
advertising.message = Interested in advertising on FoodTopic? Drop us a message and we will get back to you ASAP.

contactUs = Contact Us
contactUs.message = Feel free to ask as anything. Just hit the submit button and we promise to get back to you ASAP.

advertising.submission.success = Thanks for the opportunity. We will get back to you ASAP!
contactUs.submission.success = Thanks for your input. We will get back to you ASAP!
comment.required = Let us know what you want to say

privacyPolicy = Privacy Policy
termsOfService = Terms Of Service





